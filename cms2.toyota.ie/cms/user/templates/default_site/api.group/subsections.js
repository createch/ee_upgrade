{if segment_3}
[{exp:channel:categories channel="staff" style="linear"}
{if category_name=="{segment_3}" }
<?php $json='';?>
{exp:low_reorder:entries set="staff" category="{category_id}"}
<?php $json.='{';?>
<?php $json.='"title":"{title}",';?>
<?php $json.='"url_title":"{url_title}",';?>
<?php $json.='"entry_id":"{entry_id}",';?>
<?php $json.='"channel_id":"{channel_id}",';?>
<?php $json.='"author_id":"{author_id}",';?>
<?php $json.='"status":"{status}",';?>
<?php $json.='"entry_date":"{entry_date}",';?>
<?php $json.='"edit_date":"{edit_date}",';?>
<?php $json.='"expiration_date":"{expiration_date}",';?>
<?php $json.='"team_intro":"{exp:utility:removedoublequotes}{team_intro}{/exp:utility:removedoublequotes}",';?>
<?php $json.='"profiles":[';?>
{profiles}
<?php $json.='{"row_id":"{profiles:row_id}",';?>
<?php $json.='"full_name":"{profiles:full_name}",';?>
<?php $json.='"image_profile":"{profiles:image_profile}",';?>
<?php $json.='"contact_number":"{profiles:contact_number}",';?>
<?php $json.='"mobile_number":"{profiles:mobile_number}",';?>
<?php $json.='"email_address":"{profiles:email_address}",';?>
<?php $json.='"job_title":"{profiles:job_title}",';?>
<?php $json.='"short_bio":"{exp:utility:htmlspecialchars}{exp:utility:removequotes}{profiles:short_bio}{/exp:utility:removequotes}{/exp:utility:htmlspecialchars}"}';?>
{/profiles}
<?php $json.='],';?>
<?php $json.='"contact_hours":[';?>
{contact_hours}
<?php $json.='{"row_id":"{contact_hours:row_id}",';?>
<?php $json.='"day_of_the_week":"{contact_hours:day_of_the_week}",';?>
<?php $json.='"from_time":"{contact_hours:from_time}",';?>
<?php $json.='"to_time":"{contact_hours:to_time}"}';?>
{/contact_hours}
<?php $json.=']';?>
<?php $json.='},';?>
{/exp:low_reorder:entries}

<?php 
$json=substr($json, 0, -1);
$json=str_replace("}{","},{",$json);
$json=str_replace("\n","\\n",$json);
echo $json;
?>
{/if}
{/exp:channel:categories}]
{if:else}
[{exp:low_reorder:entries set="staff"}
{
"title":"{title}",
"url_title":"{url_title}",
"entry_id":"{entry_id}",
"channel_id":"{channel_id}",
"author_id":"{author_id}",
"status":"{status}",
"entry_date":"{entry_date}",
"edit_date":"{edit_date}",
"expiration_date":"{expiration_date}",
"profiles":[
{profiles}
{
"row_id":"{profiles:row_id}",
"full_name":"{profiles:full_name}",
"image_profile":"{profiles:image_profile}",
"contact_number":"{profiles:contact_number}",
"mobile_number":"{profiles:mobile_number}",
"email_address":"{profiles:email_address}",
"job_title":"{profiles:job_title}",
"short_bio":"{profiles:short_bio}"
}
{/profiles}	
],
"contact_hours":[
{contact_hours}
{
"row_id":"{contact_hours:row_id}",
"day_of_the_week":"{contact_hours:day_of_the_week}",
"from_time":"{contact_hours:from_time}",
"to_time":"{contact_hours:to_time}"
},
{/contact_hours}	
],
"team_intro":"{team_intro}"
}
{if count != total_results},{/if}
{/exp:low_reorder:entries}]
{/if}

