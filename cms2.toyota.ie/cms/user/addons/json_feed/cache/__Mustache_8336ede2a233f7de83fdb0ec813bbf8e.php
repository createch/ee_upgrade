<?php

class __Mustache_8336ede2a233f7de83fdb0ec813bbf8e extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '{header}
';
        // 'metaData' section
        $value = $context->find('metaData');
        $buffer .= $this->sectionAd51dadf2f055c9528d77359575b173e($context, $indent, $value);
        $buffer .= $indent . '{footer}
';
        $buffer .= $indent . '
';

        return $buffer;
    }

    private function section80df6655301613a2b6ceb5fa70805d0b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
					{{>bigSpotlight}}
				';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                if ($partial = $this->mustache->loadPartial('bigSpotlight')) {
                    $buffer .= $partial->renderInternal($context, $indent . '					');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7c955b8248876c3350235bd8844fcce6(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
					{{>smallSpotlight}}
				';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                if ($partial = $this->mustache->loadPartial('smallSpotlight')) {
                    $buffer .= $partial->renderInternal($context, $indent . '					');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionD7e53286f7adf7426759fd5145788a89(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
				
				{{#bigSpotlights}}
					{{>bigSpotlight}}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{>smallSpotlight}}
				{{/smallSpotlights}}

			';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '				
';
                // 'bigSpotlights' section
                $value = $context->find('bigSpotlights');
                $buffer .= $this->section80df6655301613a2b6ceb5fa70805d0b($context, $indent, $value);
                $buffer .= $indent . '
';
                // 'smallSpotlights' section
                $value = $context->find('smallSpotlights');
                $buffer .= $this->section7c955b8248876c3350235bd8844fcce6($context, $indent, $value);
                $buffer .= $indent . '
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section77fe0fed740e5d937e795413c7bf05c9(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			
			{{#searchData}}
				
				{{#bigSpotlights}}
					{{>bigSpotlight}}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{>smallSpotlight}}
				{{/smallSpotlights}}

			{{/searchData}}
			<br/>
			<br/>
			<br/>
		
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '			
';
                // 'searchData' section
                $value = $context->find('searchData');
                $buffer .= $this->sectionD7e53286f7adf7426759fd5145788a89($context, $indent, $value);
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '		
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionAd51dadf2f055c9528d77359575b173e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		
		{{#sections}}
			
			{{#searchData}}
				
				{{#bigSpotlights}}
					{{>bigSpotlight}}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{>smallSpotlight}}
				{{/smallSpotlights}}

			{{/searchData}}
			<br/>
			<br/>
			<br/>
		
		{{/sections}}
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '		
';
                // 'sections' section
                $value = $context->find('sections');
                $buffer .= $this->section77fe0fed740e5d937e795413c7bf05c9($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
