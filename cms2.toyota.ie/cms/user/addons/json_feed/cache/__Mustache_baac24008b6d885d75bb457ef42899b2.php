<?php

class __Mustache_baac24008b6d885d75bb457ef42899b2 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'promotions' section
        $value = $context->find('promotions');
        $buffer .= $this->sectionE4cc9a0571b2ebf5018354665e7a1ddf($context, $indent, $value);

        return $buffer;
    }

    private function sectionE4cc9a0571b2ebf5018354665e7a1ddf(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
<?php
$dateStart= substr(\'{{promotion_date_start}}\',0,-3);
$dateEnd=  substr(\'{{promotion_date_end}}\',0,-3);
$dateNow= strtotime(\'now\');

//echo "NOW: ".$dateNow."<br/>";
//echo "START: ".$dateStart."<br/>";
//echo "END: ".$dateEnd."<br/>";

if ($dateNow >= $dateStart && $dateNow < $dateEnd)
{
	echo "<div class=\'col-xs-12 col-sm-6 element clear-md clear-sm\' style=\'display: block;\'><a href=\'article{{url}}\'><span class=\'image-container\'><img src=\'{{promotion_image}}\' alt=\'spotlight large image\'></span><h3>{{title}}</h3><p>{{subtitle}}</p></a></div>";
}
?>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '<?php
';
                $buffer .= $indent . '$dateStart= substr(\'';
                $value = $this->resolveValue($context->find('promotion_date_start'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\',0,-3);
';
                $buffer .= $indent . '$dateEnd=  substr(\'';
                $value = $this->resolveValue($context->find('promotion_date_end'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\',0,-3);
';
                $buffer .= $indent . '$dateNow= strtotime(\'now\');
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '//echo "NOW: ".$dateNow."<br/>";
';
                $buffer .= $indent . '//echo "START: ".$dateStart."<br/>";
';
                $buffer .= $indent . '//echo "END: ".$dateEnd."<br/>";
';
                $buffer .= $indent . '
';
                $buffer .= $indent . 'if ($dateNow >= $dateStart && $dateNow < $dateEnd)
';
                $buffer .= $indent . '{
';
                $buffer .= $indent . '	echo "<div class=\'col-xs-12 col-sm-6 element clear-md clear-sm\' style=\'display: block;\'><a href=\'article';
                $value = $this->resolveValue($context->find('url'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\'><span class=\'image-container\'><img src=\'';
                $value = $this->resolveValue($context->find('promotion_image'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\' alt=\'spotlight large image\'></span><h3>';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '</h3><p>';
                $value = $this->resolveValue($context->find('subtitle'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '</p></a></div>";
';
                $buffer .= $indent . '}
';
                $buffer .= $indent . '?>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
