<?php

class __Mustache_e82515ea7e26c0a822025ba8f4d48753 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'sections' section
        $value = $context->find('sections');
        $buffer .= $this->section2cda0a0f18fc601792ae13a32cfc0e55($context, $indent, $value);

        return $buffer;
    }

    private function section2cda0a0f18fc601792ae13a32cfc0e55(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
	{exp:json_feed:render_html template="components/{{type}}"}
	{/exp:json_feed:render_html}
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '	{exp:json_feed:render_html template="components/';
                $value = $this->resolveValue($context->find('type'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '"}
';
                $buffer .= $indent . '	{/exp:json_feed:render_html}
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
