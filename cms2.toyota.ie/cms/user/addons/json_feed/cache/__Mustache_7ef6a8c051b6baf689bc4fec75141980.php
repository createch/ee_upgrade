<?php

class __Mustache_7ef6a8c051b6baf689bc4fec75141980 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'pages' section
        $value = $context->find('pages');
        $buffer .= $this->sectionCe6bbcd097e3475c51b9a0d303527743($context, $indent, $value);
        $buffer .= $indent . '
';
        $buffer .= $indent . '
';

        return $buffer;
    }

    private function sectionCe6bbcd097e3475c51b9a0d303527743(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
<div class="col-xs-12 col-sm-6 element clear-md clear-sm" style="display: block;">
	<a href="news-article/{{url}}">
		<span class="image-container">
			<img src="{{image}}" alt="spotlight large image">
		</span>
		<h3>
			<span class="category-tag news">
			    {{type}}
			</span>
			{{title}}
		</h3>
		<p>
			{{description}}
		</p>
	</a>
</div>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '<div class="col-xs-12 col-sm-6 element clear-md clear-sm" style="display: block;">
';
                $buffer .= $indent . '	<a href="news-article/';
                $value = $this->resolveValue($context->find('url'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '">
';
                $buffer .= $indent . '		<span class="image-container">
';
                $buffer .= $indent . '			<img src="';
                $value = $this->resolveValue($context->find('image'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '" alt="spotlight large image">
';
                $buffer .= $indent . '		</span>
';
                $buffer .= $indent . '		<h3>
';
                $buffer .= $indent . '			<span class="category-tag news">
';
                $buffer .= $indent . '			    ';
                $value = $this->resolveValue($context->find('type'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '
';
                $buffer .= $indent . '			</span>
';
                $buffer .= $indent . '			';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '
';
                $buffer .= $indent . '		</h3>
';
                $buffer .= $indent . '		<p>
';
                $buffer .= $indent . '			';
                $value = $this->resolveValue($context->find('description'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '
';
                $buffer .= $indent . '		</p>
';
                $buffer .= $indent . '	</a>
';
                $buffer .= $indent . '</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
