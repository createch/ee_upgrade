<?php

class __Mustache_803c96d5438cbf89ae877fa6c19682c3 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . 'test
';
        // 'sections' section
        $value = $context->find('sections');
        $buffer .= $this->section6bfa5ee13feb609a1a984a6b8737d5f2($context, $indent, $value);
        $buffer .= $indent . '
';
        $buffer .= $indent . '
';

        return $buffer;
    }

    private function section89d91133e015b315183c8b530c61a0db(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			{{>bigSpotlight}}
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                if ($partial = $this->mustache->loadPartial('bigSpotlight')) {
                    $buffer .= $partial->renderInternal($context, $indent . '			');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section18da51efdc3ce6b268e944b15f959629(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			{{>smallSpotlight}}
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                if ($partial = $this->mustache->loadPartial('smallSpotlight')) {
                    $buffer .= $partial->renderInternal($context, $indent . '			');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section369d502a1810fac3ab110cd2bf9ca2ca(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
				
		{{#bigSpotlights}}
			{{>bigSpotlight}}
		{{/bigSpotlights}}

		{{#smallSpotlights}}
			{{>smallSpotlight}}
		{{/smallSpotlights}}

	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '				
';
                // 'bigSpotlights' section
                $value = $context->find('bigSpotlights');
                $buffer .= $this->section89d91133e015b315183c8b530c61a0db($context, $indent, $value);
                $buffer .= $indent . '
';
                // 'smallSpotlights' section
                $value = $context->find('smallSpotlights');
                $buffer .= $this->section18da51efdc3ce6b268e944b15f959629($context, $indent, $value);
                $buffer .= $indent . '
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section6bfa5ee13feb609a1a984a6b8737d5f2(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
	{{#searchData}}
				
		{{#bigSpotlights}}
			{{>bigSpotlight}}
		{{/bigSpotlights}}

		{{#smallSpotlights}}
			{{>smallSpotlight}}
		{{/smallSpotlights}}

	{{/searchData}}
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                // 'searchData' section
                $value = $context->find('searchData');
                $buffer .= $this->section369d502a1810fac3ab110cd2bf9ca2ca($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
