<?php

class __Mustache_7aebae4ff0f3fe05435369988cdb0a53 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'sections' section
        $value = $context->find('sections');
        $buffer .= $this->sectionAc0e9706a64fc3a64b54bd5695e5a691($context, $indent, $value);

        return $buffer;
    }

    private function sectionAc0e9706a64fc3a64b54bd5695e5a691(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
	{exp:json_feed:render_html template=\'templates/{{type}}\'}
		{{>section}}
	{/exp:json_feed:render_html}
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '	{exp:json_feed:render_html template=\'templates/';
                $value = $this->resolveValue($context->find('type'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\'}
';
                if ($partial = $this->mustache->loadPartial('section')) {
                    $buffer .= $partial->renderInternal($context, $indent . '		');
                }
                $buffer .= $indent . '	{/exp:json_feed:render_html}
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
