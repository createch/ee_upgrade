<?php

class __Mustache_ff2277b9ab9a6649777e1b87d09ea259 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'metaData' section
        $value = $context->find('metaData');
        $buffer .= $this->section9c9f9a655bab222a96c501b02155ec4f($context, $indent, $value);

        return $buffer;
    }

    private function sectionB0cc496bfbf4162dcab3bc53e0a600c1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		seo_title:{{title}}<br/>
		seo_title:{{description}}<br/>
		seo_title:{{keywords}}<br/>
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '		seo_title:';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $buffer .= $indent . '		seo_title:';
                $value = $this->resolveValue($context->find('description'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $buffer .= $indent . '		seo_title:';
                $value = $this->resolveValue($context->find('keywords'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionC8ce6e54b80cccf1c655de9578f01815(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			<img src="{{desktopImageUrl}}" title="{{title}}" /><br/>
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '			<img src="';
                $value = $this->resolveValue($context->find('desktopImageUrl'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '" title="';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '" /><br/>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionC9ad07f052bcacf72a1c69049d3dc834(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		{{#multimedia}}
			<img src="{{desktopImageUrl}}" title="{{title}}" /><br/>
		{{/multimedia}}
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                // 'multimedia' section
                $value = $context->find('multimedia');
                $buffer .= $this->sectionC8ce6e54b80cccf1c655de9578f01815($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section8e3491a2402f6547c561da4d272c15fe(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		section_type:{{type}}<br/>
		section_type:{{title}}<br/>	
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '		section_type:';
                $value = $this->resolveValue($context->find('type'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $buffer .= $indent . '		section_type:';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>	
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9c9f9a655bab222a96c501b02155ec4f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
	{{#seo}}
		seo_title:{{title}}<br/>
		seo_title:{{description}}<br/>
		seo_title:{{keywords}}<br/>
	{{/seo}}
	{{#header}}
		{{#multimedia}}
			<img src="{{desktopImageUrl}}" title="{{title}}" /><br/>
		{{/multimedia}}
	{{/header}}
	{{#sections}}
		section_type:{{type}}<br/>
		section_type:{{title}}<br/>	
	{{/sections}}
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                // 'seo' section
                $value = $context->find('seo');
                $buffer .= $this->sectionB0cc496bfbf4162dcab3bc53e0a600c1($context, $indent, $value);
                // 'header' section
                $value = $context->find('header');
                $buffer .= $this->sectionC9ad07f052bcacf72a1c69049d3dc834($context, $indent, $value);
                // 'sections' section
                $value = $context->find('sections');
                $buffer .= $this->section8e3491a2402f6547c561da4d272c15fe($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
