<?php

class __Mustache_32f73a58317a27699b9e284964aadc8b extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'seo' section
        $value = $context->find('seo');
        $buffer .= $this->section8b2a409f16d628af23023e4d8e710005($context, $indent, $value);

        return $buffer;
    }

    private function section8b2a409f16d628af23023e4d8e710005(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
<title>{site_name}{{title}}</title>
<meta name=\'keywords\' content=\'{{keywords}}\'/>
<meta name=\'description\' content=\'{{intro}}\'/>
<link rel=\'canonical\' href=\'{insecure_site_url}/{segment_2}/{segment_3}/{segment_4}/{segment_5}\' />
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '<title>{site_name}';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '</title>
';
                $buffer .= $indent . '<meta name=\'keywords\' content=\'';
                $value = $this->resolveValue($context->find('keywords'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\'/>
';
                $buffer .= $indent . '<meta name=\'description\' content=\'';
                $value = $this->resolveValue($context->find('intro'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\'/>
';
                $buffer .= $indent . '<link rel=\'canonical\' href=\'{insecure_site_url}/{segment_2}/{segment_3}/{segment_4}/{segment_5}\' />
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
