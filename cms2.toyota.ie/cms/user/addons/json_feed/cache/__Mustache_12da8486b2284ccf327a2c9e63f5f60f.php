<?php

class __Mustache_12da8486b2284ccf327a2c9e63f5f60f extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'seo' section
        $value = $context->find('seo');
        $buffer .= $this->section080588860c400f304b33c784393acbfe($context, $indent, $value);

        return $buffer;
    }

    private function section080588860c400f304b33c784393acbfe(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
<title>{{title}} - {site_name}</title>
<meta name=\'keywords\' content=\'{{keywords}}\'/>
<meta name=\'description\' content=\'{{intro}}\'/>
<link rel=\'canonical\' href=\'{current_url}\'/>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '<title>';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= ' - {site_name}</title>
';
                $buffer .= $indent . '<meta name=\'keywords\' content=\'';
                $value = $this->resolveValue($context->find('keywords'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\'/>
';
                $buffer .= $indent . '<meta name=\'description\' content=\'';
                $value = $this->resolveValue($context->find('intro'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\'/>
';
                $buffer .= $indent . '<link rel=\'canonical\' href=\'{current_url}\'/>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
