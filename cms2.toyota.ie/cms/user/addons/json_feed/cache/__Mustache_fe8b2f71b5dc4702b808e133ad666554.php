<?php

class __Mustache_fe8b2f71b5dc4702b808e133ad666554 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'content' section
        $value = $context->find('content');
        $buffer .= $this->section766edc1ae1c79884020b585f89eacd24($context, $indent, $value);

        return $buffer;
    }

    private function section3f9ec37361f3cebea2916be9688c357b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '{{image}}|';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $value = $this->resolveValue($context->find('image'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '|';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section06b9be96721b39885d6bb815ee56cd70(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
				<div class="hero-inner container_12 container_12_absolute block " data-carousel-link="{{link}}">
					<h1 class="heading1">{{title}}</h1>
					<span class="heading2">{{description}}</span>
				</div>
			';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '				<div class="hero-inner container_12 container_12_absolute block " data-carousel-link="';
                $value = $this->resolveValue($context->find('link'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '">
';
                $buffer .= $indent . '					<h1 class="heading1">';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '</h1>
';
                $buffer .= $indent . '					<span class="heading2">';
                $value = $this->resolveValue($context->find('description'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '</span>
';
                $buffer .= $indent . '				</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section766edc1ae1c79884020b585f89eacd24(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
<header class="header-container carousel">
		<div class="bg-image-hero assetWidthContain">
			<div class="coverBG carousel-bg" style="background-image: url(/inc/images/feature_1.jpg);" data-carousel-assets="{{#homepage_carousel}}{{image}}|{{/homepage_carousel}}"></div>
			{{#homepage_carousel}}
				<div class="hero-inner container_12 container_12_absolute block " data-carousel-link="{{link}}">
					<h1 class="heading1">{{title}}</h1>
					<span class="heading2">{{description}}</span>
				</div>
			{{/homepage_carousel}}
			<a href="#" class="carousel-link"></a>
		</div>
</header>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '<header class="header-container carousel">
';
                $buffer .= $indent . '		<div class="bg-image-hero assetWidthContain">
';
                $buffer .= $indent . '			<div class="coverBG carousel-bg" style="background-image: url(/inc/images/feature_1.jpg);" data-carousel-assets="';
                // 'homepage_carousel' section
                $value = $context->find('homepage_carousel');
                $buffer .= $this->section3f9ec37361f3cebea2916be9688c357b($context, $indent, $value);
                $buffer .= '"></div>
';
                // 'homepage_carousel' section
                $value = $context->find('homepage_carousel');
                $buffer .= $this->section06b9be96721b39885d6bb815ee56cd70($context, $indent, $value);
                $buffer .= $indent . '			<a href="#" class="carousel-link"></a>
';
                $buffer .= $indent . '		</div>
';
                $buffer .= $indent . '</header>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
