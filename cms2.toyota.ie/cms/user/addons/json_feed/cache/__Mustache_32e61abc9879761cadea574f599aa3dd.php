<?php

class __Mustache_32e61abc9879761cadea574f599aa3dd extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '{header}
';
        // 'metaData' section
        $value = $context->find('metaData');
        $buffer .= $this->section5be98b77f51f1124f01cb2d9e408910a($context, $indent, $value);
        $buffer .= $indent . '{footer}
';
        $buffer .= $indent . '
';

        return $buffer;
    }

    private function section4495336ad665dc829825dfe0932810db(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
					{{> bigSpotlight }}
				';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                if ($partial = $this->mustache->loadPartial('bigSpotlight')) {
                    $buffer .= $partial->renderInternal($context, $indent . '					');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9fea04783a04ec18bfd1d732d8888838(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
					{{> smallSpotlight }}
				';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                if ($partial = $this->mustache->loadPartial('smallSpotlight')) {
                    $buffer .= $partial->renderInternal($context, $indent . '					');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9306c6168c8b8a4b47a839845bbdbf84(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
				
				{{#bigSpotlights}}
					{{> bigSpotlight }}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{> smallSpotlight }}
				{{/smallSpotlights}}

			';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '				
';
                // 'bigSpotlights' section
                $value = $context->find('bigSpotlights');
                $buffer .= $this->section4495336ad665dc829825dfe0932810db($context, $indent, $value);
                $buffer .= $indent . '
';
                // 'smallSpotlights' section
                $value = $context->find('smallSpotlights');
                $buffer .= $this->section9fea04783a04ec18bfd1d732d8888838($context, $indent, $value);
                $buffer .= $indent . '
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1bb8f38b79ed1f61794966296e1fb4b3(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			
			{{#searchData}}
				
				{{#bigSpotlights}}
					{{> bigSpotlight }}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{> smallSpotlight }}
				{{/smallSpotlights}}

			{{/searchData}}
			<br/>
			<br/>
			<br/>
		
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '			
';
                // 'searchData' section
                $value = $context->find('searchData');
                $buffer .= $this->section9306c6168c8b8a4b47a839845bbdbf84($context, $indent, $value);
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '		
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section5be98b77f51f1124f01cb2d9e408910a(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		
		{{#sections}}
			
			{{#searchData}}
				
				{{#bigSpotlights}}
					{{> bigSpotlight }}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{> smallSpotlight }}
				{{/smallSpotlights}}

			{{/searchData}}
			<br/>
			<br/>
			<br/>
		
		{{/sections}}
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '		
';
                // 'sections' section
                $value = $context->find('sections');
                $buffer .= $this->section1bb8f38b79ed1f61794966296e1fb4b3($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
