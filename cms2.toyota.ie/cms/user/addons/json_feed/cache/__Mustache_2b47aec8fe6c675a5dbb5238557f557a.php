<?php

class __Mustache_2b47aec8fe6c675a5dbb5238557f557a extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'pid' section
        $value = $context->find('pid');
        $buffer .= $this->section19edbf5d0cdc9668fce395372dd61e46($context, $indent, $value);

        return $buffer;
    }

    private function section56d3b5da37dc5261d877ae2ee9ea9e47(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			{{.}},
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= '
';
                $buffer .= $indent . '			';
                $value = $this->resolveValue($context->last(), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= ',
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section971402f3caf1a112a84aa13652225d2e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			IMAGE_URL:{{url}}<br/>
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '			IMAGE_URL:';
                $value = $this->resolveValue($context->find('url'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE07533cbbd9a7b1a5ef8fe27b2246355(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		TYPE:{{type}}<br/>
		TITLE:{{title}}<br/>
		KEYWORDS:{{#keywords}}
			{{.}},
		{{/keywords}}
		<br/>
		DESCRIPTION:{{description}}<br/>
		{{#binary}}
			IMAGE_URL:{{url}}<br/>
		{{/binary}}
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '		TYPE:';
                $value = $this->resolveValue($context->find('type'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $buffer .= $indent . '		TITLE:';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $buffer .= $indent . '		KEYWORDS:';
                // 'keywords' section
                $value = $context->find('keywords');
                $buffer .= $this->section56d3b5da37dc5261d877ae2ee9ea9e47($context, $indent, $value);
                $buffer .= $indent . '		<br/>
';
                $buffer .= $indent . '		DESCRIPTION:';
                $value = $this->resolveValue($context->find('description'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                // 'binary' section
                $value = $context->find('binary');
                $buffer .= $this->section971402f3caf1a112a84aa13652225d2e($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section19edbf5d0cdc9668fce395372dd61e46(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
	{{#taxonomy}}
		TYPE:{{type}}<br/>
		TITLE:{{title}}<br/>
		KEYWORDS:{{#keywords}}
			{{.}},
		{{/keywords}}
		<br/>
		DESCRIPTION:{{description}}<br/>
		{{#binary}}
			IMAGE_URL:{{url}}<br/>
		{{/binary}}
	{{/taxonomy}}
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                // 'taxonomy' section
                $value = $context->find('taxonomy');
                $buffer .= $this->sectionE07533cbbd9a7b1a5ef8fe27b2246355($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
