<div id="expresso_global_settings">

	<div id="expresso_export_settings">
		<h3><?=lang('export_settings')?></h3>
		<textarea readonly="readonly"><?=$export_settings?></textarea>
		<a href="#" class="expresso_close_settings" />Close</a>
	</div>

	<div id="expresso_import_settings">
		<h3><?=lang('import_settings')?></h3>
		<textarea name="import_settings"></textarea>
		<input type="submit" name="import" class="submit" value="<?=lang('import')?>" />
		<?=lang('or')?> <a href="#" class="expresso_close_settings" /><?=lang('close')?></a>
	</div>


<?php
$docs = array(
    'custom_toolbar' => 'http://docs.ckeditor.com/#!/guide/dev_toolbar',
    'styles' => 'http://docs.ckeditor.com/#!/guide/dev_howtos_styles'
);

$this->table->set_template($cp_pad_table_template);
$this->table->set_heading(
    array('data' => lang('global_settings'), 'width' => '15%'),
    '',
    array('width' => '15%')
);

foreach ($settings as $key => $val) {
    switch ($key) {
        case 'license_number':
            $license = ($license == 'invalid_license') ? '<em class="notice">'.lang($license).'</em>' : '<em>'.lang($license).'</em>';
            $this->table->add_row('<label>'.lang($key).'&nbsp;<span class="notice">*</span></label>', $val, $license);
            break;

        case 'uiColor':
        case 'height':
        case 'autoGrow_maxHeight':
            break;

        case 'contentsCss':
        case 'custom_toolbar':
        case 'styles':
            $this->table->add_row('<label>'.lang($key).'</label>'.(isset($docs[$key]) ? ' (<i><a href="'.$docs[$key].'" target="_blank">docs</a></i>)' : ''), $val, '<i><a href="#" class="add_sample_code" id="'.$key.'">'.lang('add_sample_code').'</a></i>');
            break;

        case 'channel_uris':
            $html = '<em><i>Add a URI for each channel to make its entries linkable in the editor. Leave blank to exclude a channel.<br/>Example: template_group/template/{url_title}<br/>Allowed Tags: {url_title}, {entry_id}</i></em><br/><br/>';
            $html .= '<table>';
            foreach ($val as $channel) {
                $html .= '<tr><td class="thin">'.$channel->channel_title.'</td><td>'.form_input('channel_uris['.$channel->channel_id.']', $channel->url).'</td></tr>';
            }
            $html .= '</table>';
            $this->table->add_row('<label>'.lang($key).'</label>', $html, '');
            break;

        default:
            $this->table->add_row('<label>'.lang($key).'</label>', $val, '');
            break;
    }
}

echo $this->table->generate();
?>

</div>
