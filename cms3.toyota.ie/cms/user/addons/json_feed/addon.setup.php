<?php

return array(
    'author'         => 'Salvo Vaccarino',
    'author_url'     => '',
    'name'           => 'Json Feed',
    'description'    => 'Adds Json Feed from external sources.',
    'version'        => '1.3.2',
    'namespace'      => '\\',
    'settings_exist' => TRUE,
);