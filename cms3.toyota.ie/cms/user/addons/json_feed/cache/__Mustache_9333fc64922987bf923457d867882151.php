<?php

class __Mustache_9333fc64922987bf923457d867882151 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '{header}
';
        // 'metaData' section
        $value = $context->find('metaData');
        $buffer .= $this->sectionF1baa830e13bca44b59d8a1fd5856a3e($context, $indent, $value);
        $buffer .= $indent . '{footer}
';
        $buffer .= $indent . '
';

        return $buffer;
    }

    private function section184f1071d194656333d1173cee78c2e2(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
							{{.}},
						';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '							';
                $value = $this->resolveValue($context->last(), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= ',
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionAa6a53be64dadf9766e2bad4fb5cf7f8(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
							{{url}}<br/>
						';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '							';
                $value = $this->resolveValue($context->find('url'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionBeb8b0492feb8463917c8bc097c3aafa(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
						{{type}}<br/>
						{{title}}<br/>
						{{#keywords}}
							{{.}},
						{{/keywords}}
						<br/>
						{{description}}<br/>
						{{#binary}}
							{{url}}<br/>
						{{/binary}}
					';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '						';
                $value = $this->resolveValue($context->find('type'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $buffer .= $indent . '						';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                // 'keywords' section
                $value = $context->find('keywords');
                $buffer .= $this->section184f1071d194656333d1173cee78c2e2($context, $indent, $value);
                $buffer .= $indent . '						<br/>
';
                $buffer .= $indent . '						';
                $value = $this->resolveValue($context->find('description'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                // 'binary' section
                $value = $context->find('binary');
                $buffer .= $this->sectionAa6a53be64dadf9766e2bad4fb5cf7f8($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionB2db6adc725acff3d0f954dc74bc19dc(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
					{{#taxonomy}}
						{{type}}<br/>
						{{title}}<br/>
						{{#keywords}}
							{{.}},
						{{/keywords}}
						<br/>
						{{description}}<br/>
						{{#binary}}
							{{url}}<br/>
						{{/binary}}
					{{/taxonomy}}
				';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                // 'taxonomy' section
                $value = $context->find('taxonomy');
                $buffer .= $this->sectionBeb8b0492feb8463917c8bc097c3aafa($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7c955b8248876c3350235bd8844fcce6(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
					{{>smallSpotlight}}
				';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                if ($partial = $this->mustache->loadPartial('smallSpotlight')) {
                    $buffer .= $partial->renderInternal($context, $indent . '					');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9834dba5a276c85e38f0223ddef943c2(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
				
				{{#bigSpotlights}}
					{{#taxonomy}}
						{{type}}<br/>
						{{title}}<br/>
						{{#keywords}}
							{{.}},
						{{/keywords}}
						<br/>
						{{description}}<br/>
						{{#binary}}
							{{url}}<br/>
						{{/binary}}
					{{/taxonomy}}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{>smallSpotlight}}
				{{/smallSpotlights}}

			';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '				
';
                // 'bigSpotlights' section
                $value = $context->find('bigSpotlights');
                $buffer .= $this->sectionB2db6adc725acff3d0f954dc74bc19dc($context, $indent, $value);
                $buffer .= $indent . '
';
                // 'smallSpotlights' section
                $value = $context->find('smallSpotlights');
                $buffer .= $this->section7c955b8248876c3350235bd8844fcce6($context, $indent, $value);
                $buffer .= $indent . '
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionD5676da4c3f090bf3a37479cf52aae6f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			
			{{#searchData}}
				
				{{#bigSpotlights}}
					{{#taxonomy}}
						{{type}}<br/>
						{{title}}<br/>
						{{#keywords}}
							{{.}},
						{{/keywords}}
						<br/>
						{{description}}<br/>
						{{#binary}}
							{{url}}<br/>
						{{/binary}}
					{{/taxonomy}}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{>smallSpotlight}}
				{{/smallSpotlights}}

			{{/searchData}}
			<br/>
			<br/>
			<br/>
		
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '			
';
                // 'searchData' section
                $value = $context->find('searchData');
                $buffer .= $this->section9834dba5a276c85e38f0223ddef943c2($context, $indent, $value);
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '		
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionF1baa830e13bca44b59d8a1fd5856a3e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		
		{{#sections}}
			
			{{#searchData}}
				
				{{#bigSpotlights}}
					{{#taxonomy}}
						{{type}}<br/>
						{{title}}<br/>
						{{#keywords}}
							{{.}},
						{{/keywords}}
						<br/>
						{{description}}<br/>
						{{#binary}}
							{{url}}<br/>
						{{/binary}}
					{{/taxonomy}}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{>smallSpotlight}}
				{{/smallSpotlights}}

			{{/searchData}}
			<br/>
			<br/>
			<br/>
		
		{{/sections}}
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '		
';
                // 'sections' section
                $value = $context->find('sections');
                $buffer .= $this->sectionD5676da4c3f090bf3a37479cf52aae6f($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
