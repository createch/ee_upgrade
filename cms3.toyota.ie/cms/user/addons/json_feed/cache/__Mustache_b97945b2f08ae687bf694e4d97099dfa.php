<?php

class __Mustache_b97945b2f08ae687bf694e4d97099dfa extends Mustache_Template
{
    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $buffer = '';

        $buffer .= $indent . '{#seo}
';
        $buffer .= $indent . '<title>{site_name}';
        $value = $this->resolveValue($context->find('title'), $context, $indent);
        $buffer .= htmlspecialchars($value, 2, 'UTF-8');
        $buffer .= '</title>
';
        $buffer .= $indent . '<meta name=\'keywords\' content=\'';
        $value = $this->resolveValue($context->find('keywords'), $context, $indent);
        $buffer .= htmlspecialchars($value, 2, 'UTF-8');
        $buffer .= '\'/>
';
        $buffer .= $indent . '<meta name=\'description\' content=\'';
        $value = $this->resolveValue($context->find('intro'), $context, $indent);
        $buffer .= htmlspecialchars($value, 2, 'UTF-8');
        $buffer .= '\'/>
';
        $buffer .= $indent . '<link rel=\'canonical\' href=\'{insecure_site_url}/{segment_2}/{segment_3}/{segment_4}/{segment_5}\' />
';
        $buffer .= $indent . '{#/seo}';

        return $buffer;
    }
}
