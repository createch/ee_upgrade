<?php

class __Mustache_15ceaa24fa13cef9e4ea81e7c416a87e extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'items' section
        $value = $context->find('items');
        $buffer .= $this->section170932bf29d006c9ef92af612c8d3ddd($context, $indent, $value);

        return $buffer;
    }

    private function section75de6e649a078a3cca410a85eda2408c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '{{V10_Menu}}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $value = $this->resolveValue($context->find('V10_Menu'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1712215394fdf8d0dc24de0b160af339(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '{{listWithDiscoutText}}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $value = $this->resolveValue($context->find('listWithDiscoutText'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section170932bf29d006c9ef92af612c8d3ddd(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
<li class="fours threes_l">
    <a href="{{url}}" class="model-item">
        <img src="{external_url_images}{{#images}}{{V10_Menu}}{{/images}}" width="150" height="95" alt="{{title}}" />
        <span class="model-name">
            <strong>{{title}}</strong> {{#minPrice}}{{listWithDiscoutText}}{{/minPrice}}
        </span>
    </a>
</li>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '<li class="fours threes_l">
';
                $buffer .= $indent . '    <a href="';
                $value = $this->resolveValue($context->find('url'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '" class="model-item">
';
                $buffer .= $indent . '        <img src="{external_url_images}';
                // 'images' section
                $value = $context->find('images');
                $buffer .= $this->section75de6e649a078a3cca410a85eda2408c($context, $indent, $value);
                $buffer .= '" width="150" height="95" alt="';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '" />
';
                $buffer .= $indent . '        <span class="model-name">
';
                $buffer .= $indent . '            <strong>';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '</strong> ';
                // 'minPrice' section
                $value = $context->find('minPrice');
                $buffer .= $this->section1712215394fdf8d0dc24de0b160af339($context, $indent, $value);
                $buffer .= '
';
                $buffer .= $indent . '        </span>
';
                $buffer .= $indent . '    </a>
';
                $buffer .= $indent . '</li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
