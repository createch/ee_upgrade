<?php

class __Mustache_fc478ca8079cc09b6b590858e9bd619d extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'metaData' section
        $value = $context->find('metaData');
        $buffer .= $this->sectionEb3be19989a7c824646dcd0b8cef00fb($context, $indent, $value);

        return $buffer;
    }

    private function sectionB0cc496bfbf4162dcab3bc53e0a600c1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		seo_title:{{title}}<br/>
		seo_title:{{description}}<br/>
		seo_title:{{keywords}}<br/>
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '		seo_title:';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $buffer .= $indent . '		seo_title:';
                $value = $this->resolveValue($context->find('description'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $buffer .= $indent . '		seo_title:';
                $value = $this->resolveValue($context->find('keywords'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section3920723b7bd13a370210a246a469102d(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			<img src="{{mobileImageUrl}}" title="{{title}}" /><br/>
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '			<img src="';
                $value = $this->resolveValue($context->find('mobileImageUrl'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '" title="';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '" /><br/>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionC25adfb16ae05536800bcd821cf866b3(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		{{#multimedia}}
			<img src="{{mobileImageUrl}}" title="{{title}}" /><br/>
		{{/multimedia}}
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                // 'multimedia' section
                $value = $context->find('multimedia');
                $buffer .= $this->section3920723b7bd13a370210a246a469102d($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section8e3491a2402f6547c561da4d272c15fe(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		section_type:{{type}}<br/>
		section_type:{{title}}<br/>	
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '		section_type:';
                $value = $this->resolveValue($context->find('type'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $buffer .= $indent . '		section_type:';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>	
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionEb3be19989a7c824646dcd0b8cef00fb(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
	{{#seo}}
		seo_title:{{title}}<br/>
		seo_title:{{description}}<br/>
		seo_title:{{keywords}}<br/>
	{{/seo}}
	{{#header}}
		{{#multimedia}}
			<img src="{{mobileImageUrl}}" title="{{title}}" /><br/>
		{{/multimedia}}
	{{/header}}
	{{#sections}}
		section_type:{{type}}<br/>
		section_type:{{title}}<br/>	
	{{/sections}}
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                // 'seo' section
                $value = $context->find('seo');
                $buffer .= $this->sectionB0cc496bfbf4162dcab3bc53e0a600c1($context, $indent, $value);
                // 'header' section
                $value = $context->find('header');
                $buffer .= $this->sectionC25adfb16ae05536800bcd821cf866b3($context, $indent, $value);
                // 'sections' section
                $value = $context->find('sections');
                $buffer .= $this->section8e3491a2402f6547c561da4d272c15fe($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
