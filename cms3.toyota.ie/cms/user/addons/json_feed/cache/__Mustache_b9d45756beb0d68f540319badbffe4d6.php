<?php

class __Mustache_b9d45756beb0d68f540319badbffe4d6 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'promotions' section
        $value = $context->find('promotions');
        $buffer .= $this->section9fdec6fc4ec388964d00eb3b55a5542e($context, $indent, $value);

        return $buffer;
    }

    private function section7e26c5caaadcaa6a87cef4e0f1ee73ed(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			<img src="{{smallImage}}" alt="spotlight large image">
			';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '			<img src="';
                $value = $this->resolveValue($context->find('smallImage'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '" alt="spotlight large image">
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9fdec6fc4ec388964d00eb3b55a5542e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
<div class="col-xs-12 col-sm-6 element clear-md clear-sm" style="display: block;">
	<a href="article{{url}}">
		<span class="image-container">
			{{#smallImage}}
			<img src="{{smallImage}}" alt="spotlight large image">
			{{/smallImage}}
		</span>
		<h3>
			{{title}}
		</h3>
		<p>
			{{subTitle}}
		</p>
	</a>
</div>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '<div class="col-xs-12 col-sm-6 element clear-md clear-sm" style="display: block;">
';
                $buffer .= $indent . '	<a href="article';
                $value = $this->resolveValue($context->find('url'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '">
';
                $buffer .= $indent . '		<span class="image-container">
';
                // 'smallImage' section
                $value = $context->find('smallImage');
                $buffer .= $this->section7e26c5caaadcaa6a87cef4e0f1ee73ed($context, $indent, $value);
                $buffer .= $indent . '		</span>
';
                $buffer .= $indent . '		<h3>
';
                $buffer .= $indent . '			';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '
';
                $buffer .= $indent . '		</h3>
';
                $buffer .= $indent . '		<p>
';
                $buffer .= $indent . '			';
                $value = $this->resolveValue($context->find('subTitle'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '
';
                $buffer .= $indent . '		</p>
';
                $buffer .= $indent . '	</a>
';
                $buffer .= $indent . '</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
