<?php

class __Mustache_3c8187140f0ae896a1efbbea2d391168 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'seo' section
        $value = $context->find('seo');
        $buffer .= $this->sectionD9737bd05c8fea55fb9de5bc99a5bd92($context, $indent, $value);

        return $buffer;
    }

    private function sectionD9737bd05c8fea55fb9de5bc99a5bd92(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
<title>{site_name} - {{title}}</title>
<meta name=\'keywords\' content=\'{{keywords}}\'/>
<meta name=\'description\' content=\'{{intro}}\'/>
<link rel=\'canonical\' href=\'{current_url}\'/>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '<title>{site_name} - ';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '</title>
';
                $buffer .= $indent . '<meta name=\'keywords\' content=\'';
                $value = $this->resolveValue($context->find('keywords'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\'/>
';
                $buffer .= $indent . '<meta name=\'description\' content=\'';
                $value = $this->resolveValue($context->find('intro'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\'/>
';
                $buffer .= $indent . '<link rel=\'canonical\' href=\'{current_url}\'/>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
