<?php

class __Mustache_30fa5f866331a64e3f0f61c5e2ae6e83 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '{#items}
';
        $buffer .= $indent . '<li class="fours threes_l">
';
        $buffer .= $indent . '    <a href="';
        $value = $this->resolveValue($context->find('url'), $context, $indent);
        $buffer .= htmlspecialchars($value, 2, 'UTF-8');
        $buffer .= '" class="model-item">
';
        $buffer .= $indent . '        <img src="{external_url_images}';
        // 'images' section
        $value = $context->find('images');
        $buffer .= $this->section75de6e649a078a3cca410a85eda2408c($context, $indent, $value);
        $buffer .= '" width="150" height="95" alt="';
        $value = $this->resolveValue($context->find('title'), $context, $indent);
        $buffer .= htmlspecialchars($value, 2, 'UTF-8');
        $buffer .= '" />
';
        $buffer .= $indent . '        <span class="model-name">
';
        $buffer .= $indent . '            <strong>';
        $value = $this->resolveValue($context->find('title'), $context, $indent);
        $buffer .= htmlspecialchars($value, 2, 'UTF-8');
        $buffer .= '</strong> ';
        // 'minPrice' section
        $value = $context->find('minPrice');
        $buffer .= $this->section1712215394fdf8d0dc24de0b160af339($context, $indent, $value);
        $buffer .= '
';
        $buffer .= $indent . '        </span>
';
        $buffer .= $indent . '    </a>
';
        $buffer .= $indent . '</li>
';
        $buffer .= $indent . '{/items}';

        return $buffer;
    }

    private function section75de6e649a078a3cca410a85eda2408c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '{{V10_Menu}}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $value = $this->resolveValue($context->find('V10_Menu'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1712215394fdf8d0dc24de0b160af339(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '{{listWithDiscoutText}}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $value = $this->resolveValue($context->find('listWithDiscoutText'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
