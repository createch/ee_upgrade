<?php

class __Mustache_65e58591d728a04c3518beb9ae24c9a1 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'sections' section
        $value = $context->find('sections');
        $buffer .= $this->sectionC1027fc857d3d4e7eb84f04cd9f0b97b($context, $indent, $value);

        return $buffer;
    }

    private function sectionC1027fc857d3d4e7eb84f04cd9f0b97b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
	{exp:json_feed:render_html template=\'templates/{{type}}\'}
		{
			title:{{title}}
		}
	{exp:json_feed:render_html}	
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '	{exp:json_feed:render_html template=\'templates/';
                $value = $this->resolveValue($context->find('type'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\'}
';
                $buffer .= $indent . '		{
';
                $buffer .= $indent . '			title:';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '
';
                $buffer .= $indent . '		}
';
                $buffer .= $indent . '	{exp:json_feed:render_html}	
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
