<?php

class __Mustache_f2e1ca9298d10153f2fe36a3c370d9d7 extends Mustache_Template
{
    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $buffer = '';

        $buffer .= $indent . '{
';
        $buffer .= $indent . '"title":"';
        $value = $this->resolveValue($context->find('title'), $context, $indent);
        $buffer .= htmlspecialchars($value, 2, 'UTF-8');
        $buffer .= '"
';
        $buffer .= $indent . '}';

        return $buffer;
    }
}
