<?php

class __Mustache_f513a64da90344d24ec07f70052fe60f extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '{header}
';
        // 'metaData' section
        $value = $context->find('metaData');
        $buffer .= $this->section5ac2ceddd60eda6e29a8aea4316075d3($context, $indent, $value);
        $buffer .= $indent . '{footer}
';
        $buffer .= $indent . '
';

        return $buffer;
    }

    private function sectionF99ebef01c3e07ba7edf8320893cb624(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
					{{> bigSpotlight}}
				';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                if ($partial = $this->mustache->loadPartial('bigSpotlight')) {
                    $buffer .= $partial->renderInternal($context, $indent . '					');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionB799f965f5f8f45604f32cd08fab90d3(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
					{{> smallSpotlight}}
				';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                if ($partial = $this->mustache->loadPartial('smallSpotlight')) {
                    $buffer .= $partial->renderInternal($context, $indent . '					');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section04472b26428b8a9d364444c59dfcf5b7(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
				
				{{#bigSpotlights}}
					{{> bigSpotlight}}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{> smallSpotlight}}
				{{/smallSpotlights}}

			';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '				
';
                // 'bigSpotlights' section
                $value = $context->find('bigSpotlights');
                $buffer .= $this->sectionF99ebef01c3e07ba7edf8320893cb624($context, $indent, $value);
                $buffer .= $indent . '
';
                // 'smallSpotlights' section
                $value = $context->find('smallSpotlights');
                $buffer .= $this->sectionB799f965f5f8f45604f32cd08fab90d3($context, $indent, $value);
                $buffer .= $indent . '
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7b073555dd8f32e144154eac7b7d48d8(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			
			{{#searchData}}
				
				{{#bigSpotlights}}
					{{> bigSpotlight}}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{> smallSpotlight}}
				{{/smallSpotlights}}

			{{/searchData}}
			<br/>
			<br/>
			<br/>
		
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '			
';
                // 'searchData' section
                $value = $context->find('searchData');
                $buffer .= $this->section04472b26428b8a9d364444c59dfcf5b7($context, $indent, $value);
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '			<br/>
';
                $buffer .= $indent . '		
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section5ac2ceddd60eda6e29a8aea4316075d3(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		
		{{#sections}}
			
			{{#searchData}}
				
				{{#bigSpotlights}}
					{{> bigSpotlight}}
				{{/bigSpotlights}}

				{{#smallSpotlights}}
					{{> smallSpotlight}}
				{{/smallSpotlights}}

			{{/searchData}}
			<br/>
			<br/>
			<br/>
		
		{{/sections}}
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '		
';
                // 'sections' section
                $value = $context->find('sections');
                $buffer .= $this->section7b073555dd8f32e144154eac7b7d48d8($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
