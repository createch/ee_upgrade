<?php

class __Mustache_e17f0267cecfb4401211cef222d60252 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'sections' section
        $value = $context->find('sections');
        $buffer .= $this->sectionA726b4ea5c5cfb1f30287478dbddf932($context, $indent, $value);

        return $buffer;
    }

    private function sectionA726b4ea5c5cfb1f30287478dbddf932(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
	1
	{exp:json_feed:render_html template=\'components/{{type}}\'}
		{{>section}}
	{/exp:json_feed:render_html}
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '	1
';
                $buffer .= $indent . '	{exp:json_feed:render_html template=\'components/';
                $value = $this->resolveValue($context->find('type'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\'}
';
                if ($partial = $this->mustache->loadPartial('section')) {
                    $buffer .= $partial->renderInternal($context, $indent . '		');
                }
                $buffer .= $indent . '	{/exp:json_feed:render_html}
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
