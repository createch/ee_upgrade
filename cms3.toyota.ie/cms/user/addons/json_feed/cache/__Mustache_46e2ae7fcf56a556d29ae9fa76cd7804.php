<?php

class __Mustache_46e2ae7fcf56a556d29ae9fa76cd7804 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'sections' section
        $value = $context->find('sections');
        $buffer .= $this->section873f4fb13b73e9718a8b3cfd000c1870($context, $indent, $value);

        return $buffer;
    }

    private function section873f4fb13b73e9718a8b3cfd000c1870(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
	{exp:json_feed:render_html template=\'components/{{type}}\'}
		{{>section}}
	{/exp:json_feed:render_html}
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '	{exp:json_feed:render_html template=\'components/';
                $value = $this->resolveValue($context->find('type'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\'}
';
                if ($partial = $this->mustache->loadPartial('section')) {
                    $buffer .= $partial->renderInternal($context, $indent . '		');
                }
                $buffer .= $indent . '	{/exp:json_feed:render_html}
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
