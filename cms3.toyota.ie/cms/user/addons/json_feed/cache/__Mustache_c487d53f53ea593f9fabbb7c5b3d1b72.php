<?php

class __Mustache_c487d53f53ea593f9fabbb7c5b3d1b72 extends Mustache_Template
{
    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $buffer = '';

        $buffer .= $indent . '{
';
        $buffer .= $indent . '"title":';
        $value = $this->resolveValue($context->find('title'), $context, $indent);
        $buffer .= htmlspecialchars($value, 2, 'UTF-8');
        $buffer .= '
';
        $buffer .= $indent . '}';

        return $buffer;
    }
}
