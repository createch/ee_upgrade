<?php

class __Mustache_822cf2edb7b054fac170ed0250a19a28 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'promotions' section
        $value = $context->find('promotions');
        $buffer .= $this->section82d7351107bcee774cd1cd7c8aa56bc6($context, $indent, $value);

        return $buffer;
    }

    private function section82d7351107bcee774cd1cd7c8aa56bc6(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
<?php
$dateStart= new DateTime(\'{{promotion_date_start}}\');
$dateEnd= new DateTime(\'{{promotion_date_end}}\');

$dateNow= new DateTime(\'NOW\');

if ($dateNow >= $dateStart && $dateNow < $dateEnd)
{
echo \' <div class="col-xs-12 col-sm-6 element clear-md clear-sm" style="display: block;">
	<a href="article{{url}}">
		<span class="image-container">
			<img src="{{promotion_image}}" alt="spotlight large image">
		</span>
		<h3>
			{{title}}
		</h3>
		<p>
			{{subTitle}}
		</p>
	</a>
	</div> \';
}?>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '<?php
';
                $buffer .= $indent . '$dateStart= new DateTime(\'';
                $value = $this->resolveValue($context->find('promotion_date_start'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\');
';
                $buffer .= $indent . '$dateEnd= new DateTime(\'';
                $value = $this->resolveValue($context->find('promotion_date_end'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '\');
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '$dateNow= new DateTime(\'NOW\');
';
                $buffer .= $indent . '
';
                $buffer .= $indent . 'if ($dateNow >= $dateStart && $dateNow < $dateEnd)
';
                $buffer .= $indent . '{
';
                $buffer .= $indent . 'echo \' <div class="col-xs-12 col-sm-6 element clear-md clear-sm" style="display: block;">
';
                $buffer .= $indent . '	<a href="article';
                $value = $this->resolveValue($context->find('url'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '">
';
                $buffer .= $indent . '		<span class="image-container">
';
                $buffer .= $indent . '			<img src="';
                $value = $this->resolveValue($context->find('promotion_image'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '" alt="spotlight large image">
';
                $buffer .= $indent . '		</span>
';
                $buffer .= $indent . '		<h3>
';
                $buffer .= $indent . '			';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '
';
                $buffer .= $indent . '		</h3>
';
                $buffer .= $indent . '		<p>
';
                $buffer .= $indent . '			';
                $value = $this->resolveValue($context->find('subTitle'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '
';
                $buffer .= $indent . '		</p>
';
                $buffer .= $indent . '	</a>
';
                $buffer .= $indent . '	</div> \';
';
                $buffer .= $indent . '}?>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
