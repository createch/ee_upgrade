<?php

class __Mustache_6d63bd276b7cae32e1f5f25d1f9a0820 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'pid' section
        $value = $context->find('pid');
        $buffer .= $this->section79d088e2671e19417a737ebf6c1724a5($context, $indent, $value);

        return $buffer;
    }

    private function section56d3b5da37dc5261d877ae2ee9ea9e47(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			{{.}},
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '			';
                $value = $this->resolveValue($context->last(), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= ',
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE45288fd9055a60da54e736e3ecf0e01(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
			{{url}}<br/>
		';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '			';
                $value = $this->resolveValue($context->find('url'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionEe7d68f142127d7e5a0ff25efafc3f0e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
		{{type}}<br/>
		{{title}}<br/>
		{{#keywords}}
			{{.}},
		{{/keywords}}
		<br/>
		{{description}}<br/>
		{{#binary}}
			{{url}}<br/>
		{{/binary}}
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                $buffer .= $indent . '		';
                $value = $this->resolveValue($context->find('type'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                $buffer .= $indent . '		';
                $value = $this->resolveValue($context->find('title'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                // 'keywords' section
                $value = $context->find('keywords');
                $buffer .= $this->section56d3b5da37dc5261d877ae2ee9ea9e47($context, $indent, $value);
                $buffer .= $indent . '		<br/>
';
                $buffer .= $indent . '		';
                $value = $this->resolveValue($context->find('description'), $context, $indent);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '<br/>
';
                // 'binary' section
                $value = $context->find('binary');
                $buffer .= $this->sectionE45288fd9055a60da54e736e3ecf0e01($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section79d088e2671e19417a737ebf6c1724a5(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
	{{#taxonomy}}
		{{type}}<br/>
		{{title}}<br/>
		{{#keywords}}
			{{.}},
		{{/keywords}}
		<br/>
		{{description}}<br/>
		{{#binary}}
			{{url}}<br/>
		{{/binary}}
	{{/taxonomy}}
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                // 'taxonomy' section
                $value = $context->find('taxonomy');
                $buffer .= $this->sectionEe7d68f142127d7e5a0ff25efafc3f0e($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }
}
