<?php

return array(
    'author'         => 'Rob Sanchez',
    'author_url'     => '',
    'name'           => 'Json',
    'description'    => 'Output ExpressionEngine data in JSON format.',
    'version'        => '1.1.7',
    'namespace'      => '\\',
    'settings_exist' => TRUE,
);