{"pages":[
{exp:low_reorder:entries set="featured_articles"}
	<?php ob_start();?>{title}<?php $title = json_encode(ob_get_clean( )); ?>
	<?php ob_start();?>{promotion_subtitle}{news_subtitle}<?php $promotion_subtitle = json_encode(ob_get_clean( )); ?>
	<?php ob_start();?>{promotion_summary}{news_summary}<?php $promotion_summary = json_encode(ob_get_clean( )); ?>
	<?php ob_start();?>{disclaimer}<?php $disclaimer = json_encode(ob_get_clean( )); ?>
	{
		"type":"{promotion_type}{news_type}",
		"title":<?php echo $title;?>,
		"channel":"{channel_name}",
		"image":"{promotion_image}{news_image}",
		"subtitle": <?php echo $promotion_subtitle;?>,
		"summary": <?php echo $promotion_summary;?>,
		"url":"/pages/{channel_name}/local-article/{url_title}",
		"promotion_date_start":"{promotion_date_start format="%F %d %Y"}",
		"promotion_date_end":"{promotion_date_end format="%F %d %Y"}",
		"disclaimer":<?php echo $disclaimer;?>
	}
{if count != total_results},{/if}
{/exp:low_reorder:entries}
]}