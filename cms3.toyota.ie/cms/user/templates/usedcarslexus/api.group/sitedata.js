<?php
$query='{segment_3}';
$dealer_id='{segment_4}';


function get_data($url,$suburl='',$field=''){
	$value='';

	
	if($suburl && $field){


		$parent_json=trim(@file_get_contents($url));
		$parent_json=json_decode($parent_json);

		if($field=='models'){

			$items=$parent_json->modelrange->items;

			$aux=array();

			foreach ($items as $item) {
				
				$url=str_replace('/new-cars/','/new-car-details/',$item->url);
				$url=str_replace('/index.json','',$url);

				$url='{insecure_site_url}/api'.$url;

				$model=json_decode(trim(@file_get_contents($url)));

				array_push($aux, $model);

			}

			$value=json_encode(array_reverse($aux));

		}elseif($field=='used-cars'){

			$items=$parent_json[0]->Cars;

			$aux=array();

			foreach ($items as $item) {
				

				$url='http://toyotasurvey.force.com/auc/AUC_Car_Details?carid='.$item->carID;

				$model=json_decode(trim(@file_get_contents($url)));

				$model[0]->LastModifiedDate=date('Y-m-d h:m:s',strtotime($model[0]->LastModifiedDate));
				$model[0]->LastModifiedDate=str_replace(' ', 'T', $model[0]->LastModifiedDate).'Z';

				$aux[]=$model[0];

			}

			$value=json_encode($aux);
		
		}else{

			return 'Error: no field specified';
		}


	}else{
		$value=trim(@file_get_contents($this->feed_url));
	}

	return $value;
	

}

switch($query){

	case 'pages':echo '{exp:json:entries channel="about_us|contact_us|sales|aftersales|custom_slot_1|custom_slot_2|custom_slot_3|service_and_accessories|disclaimer|privacy"}';
	break;

	case 'subpages': echo '{exp:json:entries channel="subpage"}';
	break;

	case 'news': echo '{exp:json_feed:print_feed src="{insecure_site_url}api/news"}';
	break;

	case 'offers': echo '{exp:json_feed:print_feed src="{insecure_site_url}api/offers"}';
	break;

	case 'models': echo get_data('{insecure_site_url}api/new-cars', '{insecure_site_url}api/new-car-details/', 'models');
	break;

	case 'used-cars': echo get_data('{insecure_site_url}api/assured-used-cars/', '{insecure_site_url}api/assured-used-car-details/', 'used-cars');
	break; 

}

?>