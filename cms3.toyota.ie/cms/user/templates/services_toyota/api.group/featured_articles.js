<?php



	$featured_articles='[{exp:json_feed:print_feed src="{insecure_site_url}api/featured_local_articles" section="pages"}{/exp:json_feed:print_feed}]';

	$toyota_articles='[{exp:json_feed:print_feed src="{insecure_site_url}api/toyota-news" section="pages"}{/exp:json_feed:print_feed}]';
	
	$toyota_offers='[{exp:json_feed:print_feed src="{insecure_site_url}api/toyota-promotions" section="promotions"}{/exp:json_feed:print_feed}]';


	$featured_articles=json_decode($featured_articles);
	$toyota_articles=json_decode($toyota_articles);
	$toyota_offers=json_decode($toyota_offers);
	$toyota_offers = $toyota_offers[0]->promotions;
	
	
	$featured_events_offers = array();
	$fa = $featured_articles[0]->pages;
	for($i = 0; $i < count($fa);$i++){
		if($fa[$i]->type === "event"){
			$featured_events_offers[] = $fa[$i];
		}else if($fa[$i]->type === "special offer" || $fa[$i]->type === "offer"){
			$dateStart=$fa[$i]->promotion_date_start;
			$dateEnd=$fa[$i]->promotion_date_end;
			$dateStart=strtotime($dateStart);
			$dateEnd=strtotime($dateEnd);
			$dateNow= strtotime('now');	
			if ($dateNow >= $dateStart && $dateNow < $dateEnd )
				$featured_events_offers[] = $fa[$i];
		}	
	}
	
	$toyota_events = array();
	$ta = $toyota_articles[0]->pages;
	for($i = 0; $i < count($ta);$i++){
		if($ta[$i]->type === "event"){
			$toyota_events[] = $ta[$i];
		}	
	}
	
	
	
		
	// fills posiiton 0 and 1 of the final array  with local offers and events if they exists;
	$featured_final = array();
	if(array_key_exists(0,$featured_events_offers)){
		//echo "featured evets 0 \n";
		array_push($featured_final , $featured_events_offers[0]);
		if(array_key_exists(1,$featured_events_offers)){
			//echo "featured evets 1 \n";
			array_push($featured_final, $featured_events_offers[1]);
		}
	}
	
	// fill the rest of the final array with toyota offers and events intercalated.
	for($i = 0; $i < 8;$i++){
		if($i%2 === 0){
			if(array_key_exists($i,$toyota_offers)){
				$x = $i/2;
				array_push($featured_final, $toyota_offers[$x]);
				//echo "toyota offer $x \n";
			}		
		}else{
			if(array_key_exists($i,$toyota_events)){
				$x = ((int)$i/2) + 1;
				array_push($featured_final, $toyota_events[$x]);
				//echo "toyota event $x \n";
			}	
		}
		if(count($featured_final) >= 4)
			break;
	}
	

	//var_dump( $toyota_offers);
	
	echo json_encode($featured_final);

	
?>