{
	"content":[{
		"meta":"",
		"main":{exp:json:entries channel="home_page"},
		"generic_settings":{exp:json:entries channel="generic_settings"},
		"services":{exp:json_feed:print_feed src="{insecure_site_url}api/service-and-accessories" section="metaData"},
		"dealers":{exp:json_feed:print_feed src="{insecure_site_url}/api/dealer-list"}
	}]
}