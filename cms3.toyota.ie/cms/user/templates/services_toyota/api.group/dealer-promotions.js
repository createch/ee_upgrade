{"promotions":[
{exp:low_reorder:entries set="promotions"}
	<?php ob_start();?>{title}<?php $title = json_encode(ob_get_clean( )); ?>
	<?php ob_start();?>{promotion_subtitle}<?php $promotion_subtitle = json_encode(ob_get_clean( )); ?>
	<?php ob_start();?>{promotion_summary}<?php $promotion_summary = json_encode(ob_get_clean( )); ?>
	<?php ob_start();?>{disclaimer}<?php $disclaimer = json_encode(ob_get_clean( )); ?>
	{
		"code":"{entry_id}",
		"type":"{promotion_type}",
		"title":<?php echo $title;?>,
		"channel":"{channel_name}",
		"promotion_image":"{promotion_image:preview}",
		"promotion_subtitle":<?php echo $promotion_subtitle;?>,
		"promotion_summary":<?php echo $promotion_summary;?>,
		"url_title":"{url_title}",
		"promotion_date_start":"{promotion_date_start format="%F %d %Y"}",
		"promotion_date_end":"{promotion_date_end format="%F %d %Y"}",
		"disclaimer":<?php echo $disclaimer;?>
	}
	{if count != total_results},{/if}	
{/exp:low_reorder:entries}
]}