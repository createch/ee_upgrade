{"pages":[
{exp:low_reorder:entries set="news_set"}
	<?php ob_start();?>{title}<?php $title = json_encode(ob_get_clean( )); ?>
	<?php ob_start();?>{news_summary}<?php $summary = json_encode(ob_get_clean( )); ?>
	<?php ob_start();?>{news_subtitle}<?php $news_subtitle = json_encode(ob_get_clean( )); ?>
	{
		"id":"{entry_id}",
		"type":"{news_type}",
		"title":<?php echo $title;?>,
		"channel":"{channel_name}",
		"news_image":"{news_image:preview}",
		"news_subtitle":<?php echo $news_subtitle;?>,
		"news_summary":<?php echo $summary; ?>,
		"url_title":"{url_title}"
	}
	{if count != total_results},{/if}
{/exp:low_reorder:entries}
]}

