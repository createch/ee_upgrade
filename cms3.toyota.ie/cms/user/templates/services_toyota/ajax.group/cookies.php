<div class="cookie_policy">
	<h1>Cookie policy</h1>
	<p>By accessing and using this website (“the TOYOTA SERVICE  site”), you accept this policy without limitation or qualification. Your access to the TOYOTA SERVICE site is therefore subject to this policy as well as to applicable laws.</p>
	<p>Toyotaservice.ie takes the privacy of its users seriously. We are committed to safeguarding the privacy of our users while providing valuable service. This Privacy Policy Statement explains the data processing practices of Toyotaservice.ie. If you have any requests concerning your personal information or any queries with regard to these practices please contact us at <a href="mailto:aftersales@toyota.ie">aftersales@toyota.ie</a>.</p>
	<h4>Information Collected</h4>
	<p>We collect personal information from you through the use of enquiry, application and registration forms and every time you e-mail us your details. We also collect information automatically about your visit to our site. The information obtained in this way, which includes demographic data and browsing patterns, is only used in aggregate form</p>
	<h4>Use of personal information</h4>
	<p>
		We process personal information collected via ToyotaService.ie for the purposes of:
	</p>
	<p>providing you with information about products and services we offer.</p>
	<p>conducting market research surveys.</p>
	<p>informing you of competitions and promotions.</p>
	<p></p>
	<h4></h4>
	
	
	
	<h4>Disclosures</h4>
	<p>We will not disclose any information we keep about you to any third parties, except for other companies we engage to provide services which involve processing data on our behalf.
	</p>
	<p></p>
	<p></p>
	<p></p>
	<p></p>
	<p></p>
	<p></p>
	<p></p>

</div>
