var Images=function(){
    o={
        $el:$('#image-list'),
        $info:$('#image-info h2'),
        defaultList:[],
        list:[],
        perPage:($(window).width()>=480) ? (($(window).width()>=768) ? 20 : 12) : 9,
        current:0,
        animating:false,

        init:function(){

	            var me=this;
	            me.load();

	            $('.more-images').click(function(e){
	                e.preventDefault();
	                me.nextPage();
	            });

	            $('#image-popup,.close').click(function(e){
	            	e.preventDefault();
	            	me.closePopup();
	            });

	            $(window).resize(function(){
	            	me.resizeAll();
	            });

	            $(document).on('click','.image-block',function(){
	            	if(!me.animating){
		            	me.showPopup($(this));
		            }
	            });

	            $('#search-page-amount').text(' '+me.perPage);

	            return me;
	            
	        },

        load:function(){

	            var me=this;

	            $.ajax({
	            	url     :'http://rdappshost.com/toyota/allthings/api/images?allow=1',
	                cache   :false,
	                async   :true,
	                dataType:'jsonp',
	                jsonp: "callback",

	            	success: function(r){
	            			if(r.status=='success'){
	            				me.defaultList=r.data;
			                    me.list=r.data;
			                    me.clear();
			                    me.show();
	            			} else {
	            				
	            			}
	            		}
	            	});

	        },

	    resizeAll:function(){
	    		var last=0;
		    	this.$el.find('.image-block').each(function(){
		    		var h=(last==0) ? parseInt($(this).width()) : last;
		    		last=h;

		    		$(this).height(h);
		    	});
		    },

	    addImage:function(i){

	    		this.$el.append('<div class="image-block" id="image_'+i.session_code+'"></div>');
	    		this.addImageLoader(i);

	    		if(this.current>=this.list.length-1){
		        	$('.show-more-images').hide();
				} else {
					$('.show-more-images').show();
				}

	    	},

	    addImageLoader:function(i){

	    		var self=this;
		    	i.img = new Image;

				i.img.onload = function(){
					self.makeImage(i);
				};

				i.img.onerror = function(){
					self.removeImage(i);
					self.oneMore();
				};

				self.makeLoader(i);
				i.img.src=i.image_url;

		    },

        show:function(){

        		if(this.list.length>0){
		            var till=this.list.length>this.perPage?this.perPage:this.list.length;
		            for(var q=0;q<till;q++){
		                this.addImage(this.list[q]);
		                this.current=q+1;
		            }
		            this.resizeAll();
		            this.updateInfo();
		        } else {
		        	this.$el.html('<p class="center-text">Your search returned 0 results.</p>');
		        }

				$('.image-block').each(function(){
					if($(this).html()==''){
						$(this).remove();
					}
				});

	        },

	    oneMore:function(){

	            var till=this.list.length>(1+this.current)?(1+this.current):this.list.length;
	            for(var q=this.current;q<till;q++){
	                this.addImage(this.list[q]);
	                this.current=q+1;
	            }
	            this.resizeAll();
	            this.updateInfo();

	        },

        nextPage:function(){

	            var till=this.list.length>(this.perPage+this.current)?(this.perPage+this.current):this.list.length;
	            for(var q=this.current;q<till;q++){
	                this.addImage(this.list[q]);
	                this.current=q+1;
	            }
	            this.resizeAll();
	            this.updateInfo();

	        },

	    search:function(str){

	    		function x(str){
					return str.replace(/[^\w\s]/gi, '').replace(/ /g,'').toUpperCase();
				}

	    		var searchList=[];

	    		for(var q=0;q<this.defaultList.length;q++){
	    			if((x(this.defaultList[q].nominee_name).indexOf(x(str))!==-1) || x(this.defaultList[q].user_name).indexOf(x(str))!==-1){
	    				searchList.push(this.defaultList[q]);
	    			}
	    		}

	    		this.list=(str!='') ? searchList : this.defaultList;
	    		this.clear();
	    		this.show();

	       	},

	    updateInfo:function(){
	    		$('#search-info').html('('+this.list.length+' images)');
	    	},

	    showPopup:function($image){
	    		var me=this;
	    		me.animating=true;

	    		var i=$image.attr('id').replace('image_','');
	    		var rosetteUrl='assets/images/rosettes_w/rosette_text_';

	    		$('#image-popup').css({
	    			opacity:0,
	    			width:1
	    		});

	    		var img=false;
	    		for(var q=0;q<this.defaultList.length;q++){
	    			if(this.defaultList[q].session_code==i){
	    				img=this.defaultList[q];
	    			}
	    		}

	    		if(img){
	    			

	    			$('#image-popup .nominee').text(img.nominee_name);
	    			$('#image-popup .nominater .name').text(img.user_name);
	    			$('#image-popup .rosette .text img').attr('src',rosetteUrl+img.filter+'.png');
	    			
	    			var popupLeft=parseInt($image.position().left)+(parseInt($image.width())/2)-150;

	    			if(popupLeft<10){
	    				popupLeft=10;
	    			} else if((popupLeft+300)>parseInt($(window).width())){
	    				popupLeft=parseInt($(window).width())-310;
	    			}

	    			$('#image-popup').css('top',parseInt($image.position().top)+(($image.width())/2)-150);
	    			$('#image-popup').css('left',popupLeft);
	    			$('#image-popup .content').hide();

	    			$('#image-popup').animate({
	    				opacity:1,
	    				width:300
	    			},500,function(){
	    				$('#image-popup .content').slideDown('slow',function(){

	    					setTimeout(function(){
		    					me.animating=false;
		    				},1000);

	    				});
	    			});

	    			var theTop=parseInt($image.offset().top)-(window.innerHeight/2)+parseInt($image.height())/2;

	    			$('html, body').animate({
				        scrollTop: theTop
				    },200);
	    		}
	    	},

	    closePopup:function(){
	    		var me=this;
	    		me.animating=true;

		    	$('#image-popup .content').slideUp('slow',function(){
		    		$('#image-popup').animate({
	    				opacity:0,
	    				width:1
	    			},500,function(){
	    				setTimeout(function(){
	    					me.animating=false;
	    				},1000);

	    			});
		    	});
	    	},

        clear:function(){

	            this.current=0;
	            this.$el.html('');

	        },

	    makeLoader:function(i){

		    	var html='<div class="loader"><img src="assets/images/loader_image.gif" alt="Loading..." /></div>';
	            this.$el.find('#image_'+i.session_code).append(html);

	            this.$el.find('#image_'+i.session_code).find('.loader').css({
	            	position:'relative',
	            	top:(parseInt(this.$el.find('#image_'+i.session_code).width())-parseInt(this.$el.find('#image_'+i.session_code).find('.loader').height()))/2
	            });

		    },

        makeImage:function(i){

	            this.$el.find('#image_'+i.session_code).html(i.img);

	        },

	    removeImage:function(i){

	        	this.$el.find('#image_'+i.session_code).remove();

	       },

        isMore:function(){

	            if(this.current+1>=this.list.length){
	                return false;
	            } else {
	                return true;
	            }

	        }

    };
    return o.init();
};