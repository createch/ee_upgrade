$(document).ready(function(){
	var parallaxeffect=(function(){
		o={
			currentPos:0,
			effects:[],
			init:function(){
					this.loop();
					return this;
				},
			loop:function(){
				var me=this;
				var theInt=setTimeout(function(){
					me.currentPos=$(document).scrollTop();
					me.doEffects();
					me.loop();
				},50);
			},
			percentageOfWindow:function($el){
				var p=1-($el.offset().top-this.currentPos)/parseInt($(window).height());
				return this.limit(p);
			},
			limit:function(p){
				return p>1?p=1:(p<0?0:p);
			},
			addEffect:function(obj){
				this.effects.push(obj);
			},
			doEffects:function(){
				for(var q=0;q<this.effects.length;q++){
					var effectPerc=this.limit((this.percentageOfWindow(this.effects[q].object)-this.effects[q].time.start)/this.effects[q].time.end);
					for(var w=0;w<this.effects[q].change.length;w++){
						var change=this.effects[q].change[w];
						this.effects[q].object.css(change.type,change.from+((change.to-change.from)*effectPerc));
					}
				}
			}
		};

		return o.init();
	})();


	$('.nomination-quote').each(function(){
		parallaxeffect.addEffect({
			object 	:$(this),
			time 	:{ start:0.1, end:0.3 },
			change 	:[
						{ type:'opacity', from:0, to:1 },
						{ type:'top', from:-30, to:0 }
					]
			});
	});

});


	

	