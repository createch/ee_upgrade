$(document).ready(function(){

	parallaxContentResize();
	$(window).resize(function(){
		parallaxContentResize();
	});

	window.location.hash=hash;
});


function parallaxContentResize(){
	$('.parallax-layer').each(function(i,e){

		if(parseInt($(this).height())<parseInt($(this).find('.container_12').height())){
			var newH=$(this).find('.container_12').height();
			newH+=(marginBottom=30);
			$(this).height(newH);
			$('.parallax:nth-child('+(i+1)+')').height(newH);
			$('.parallax:nth-child('+(i+1)+') .bg-par').height(newH);
		}
	});

	$('.parallax').each(function(i,e){

		if(parseInt($(this).height())<parseInt($(this).find('.move-into-parallax').height())){
			var newH=$(this).find('.move-into-parallax').height();
			newH+=(marginBottom=30);
			$(this).height(newH);
			$(this).find('.background-layer').height(newH);
		}
	});
}
