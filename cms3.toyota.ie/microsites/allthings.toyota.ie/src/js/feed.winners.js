var Winners=function(){
    o={
        $el:$('#winners'),
        defaultList:[],
        list:[],
        perPage:($(window).width()>=480) ? (($(window).width()>=768) ? 3 : 2) : 1,
        current:0,
        animating:false,

        init:function(){

	            var me=this;
	            me.load();

	            $('.more-winners').click(function(e){
	                e.preventDefault();
	                me.nextPage();
	            });

	            $('#winners-page-amount').text(' '+me.perPage);

	            return me;
	            
	        },

        load:function(){

	            var me=this;

	            $.ajax({
	            	url     :'http://rdappshost.com/toyota/allthings/api/winners?allow=1',
	                cache   :false,
	                async   :true,
	                dataType:'jsonp',
	                jsonp: "callback",

	            	success: function(r){
	            			if(r.status=='success'){
	            				me.defaultList=r.data;
			                    me.list=r.data;
			                    me.clear();
			                    me.show();
	            			} else {
	            				
	            			}
	            		}
	            	});

	        },

        show:function(){

        		if(this.list.length>0){
		            var till=this.list.length>this.perPage?this.perPage:this.list.length;
		            for(var q=0;q<till;q++){
		                this.addWinner(this.list[q],((q%2==0)?true:false));
		                this.current=q+1;
		            }
		        } else {
		        	this.$el.html('<p class="center-text">Something went wrong...</p>');
		        }

	        },

	    addWinner:function(w,right){

		    	var html='<div class="story '+((right)?'right':'')+'">'+
					'<div class="story-about center-text">'+
						'<div class="logo rosette">'+
							'<div class="main"><img src="assets/images/rosettes_w/rosette_filled_red.png" alt="Rosette"></div>'+
							'<div class="text"><img src="assets/images/rosettes_w/rosette_text_'+w.filter.replace(' ','').toLowerCase()+'.png" alt="A '+w.filter+'"></div>'+
						'</div>'+
						'<h3 class="quote">"'+w.quote+'"</h3>'+
						'<h4 class="who">- '+w.user_name+' nominates<br> '+w.nominee_who+' '+w.nominee_name+'</h4>'+
					'</div>'+
					'<div class="story-image">'+
						'<img class="image" src="'+w.image_url+'" alt="Story image">'+
					'</div>'+
					'<div class="clear"></div>'+
				'</div>';

				this.$el.append(html);

				if(this.current>=this.list.length-1){
		        	$('.show-more-winners').hide();
				} else {
					$('.show-more-winners').show();
				}
		    },

        nextPage:function(){

	            var till=this.list.length>(this.perPage+this.current)?(this.perPage+this.current):this.list.length;
	            for(var q=this.current;q<till;q++){
	                this.addWinner(this.list[q],((q%2==0)?true:false));
	                this.current=q+1;
	            };

	            if(this.current+this.perPage>this.list.length){
		        	$('.show-more-winners').hide();
				} else {
					$('.show-more-winners').show();
				}

	        },

        clear:function(){

	            this.current=0;
	            this.$el.html('');

	        },

        isMore:function(){

	            if(this.current+1>=this.list.length){
	                return false;
	            } else {
	                return true;
	            }

	        }

    };
    return o.init();
};