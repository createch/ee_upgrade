var Search=function(){
	o={
		$el:$('#search'),
		time:50,
		lastValue:'',
		sinceEdit:0,
		focus:false,
		searched:false,

		init:function(){

			this.loop();
			this.events();
			return this;

		},

		events:function(){
			var search=this;

			search.$el.clearField();

			search.$el.focus(function(){
				search.focus=true;
			}).blur(function(){
				search.focus=false;
			});

		},

		check:function(){

			this.sinceEdit+=this.time;

			if(this.$el.val()!=this.lastValue){
				this.changeOccured();
			}

			if(this.sinceEdit>1000 && !this.searched){
				this.searched=true;
				imgFeed.search((this.$el.val()==this.$el.attr('rel')) ? '' : this.$el.val());
			}
			
		},

		loop:function(){
			var search=this;

			setTimeout(function(){
				
				search.check();
				search.loop();

			},search.time);
		},

		changeOccured:function(){
			this.searched=false;
			this.sinceEdit=0;
			this.lastValue=this.$el.val();
		
		}
	};

	return o.init();
};