<?php
define('SALT', 's78adtgyhj');

$data=encrypt(json_encode($_POST));

echo json_encode(array(
		'status'=>'success',
		'data'=>$data
	));


//------------------------------------

function encrypt($text) 
{ 
    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)))); 
}