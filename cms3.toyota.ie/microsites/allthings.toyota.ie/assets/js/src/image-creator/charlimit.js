
var CharLimit=function(options){

	var defaults={
		selector:$('textarea'),
		limit:500,
		current:0,
		update:false,
		focus:false
	};

    o={

        init:function(){
        		this.initDefaults(defaults,options);
	        	this.events();
	        },

	   	initDefaults:function(obj1,obj2){

			    this.options = {};
			    for (var a in obj1) { this.options[a] = obj1[a]; }
			    for (var a in obj2) { this.options[a] = obj2[a]; }
			    return this.options;

		   	},

        events:function(){

        		var me=this;

	        	me.options.selector.focus(function(){
	        		me.options.focus=true;
	        	}).blur(function(){
	        		me.options.focus=false;
	        	});

	        	me.tick();

	        },

        check:function(element){

	        	if(element.val().length>this.options.limit){
	        		element.val(element.val().substr(0,this.options.limit));
	        	}

	        	if(this.options.update && this.options.current != element.val().length){
	        		this.options.current=element.val().length;

	        		this.options.update(this.options.selector,((this.options.limit-this.options.current)<0)?0:this.options.limit-this.options.current);
	        	}

	        },

	    tick:function(){
	    		var me=this;
	    		setTimeout(function(){

	    			if(me.options.focus){
	    				me.check(me.options.selector);
	    			}

	    			me.tick();

	    		},100);
	    	}
    };

    return o.init();
};