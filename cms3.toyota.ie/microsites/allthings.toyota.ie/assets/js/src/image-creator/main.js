window.cache={};
window.sliderMin=1;
window.friends=[];
window.filters=[];
window.filterIndex=-1;
window.posted=false;
window.apiUrl='http://rdappshost.com/toyota/allthings/';

if(getUrlVars()['session_code']==undefined || getUrlVars()['session_code']==''){
	document.getElementById('stepLoading').style.display = 'none';
	document.getElementById('step1').style.display = 'block';
}

window.fbAsyncInit = function() {
	FB.getLoginStatus(function(response) {
		checkForSessionCode();
	});
};

function resizeImageBox () {
	setTimeout(function(){
		var w=parseInt($('#home').width());
		if(w<640){
			var percentage=w/640;

			$('#image_box_container').css({
				width:w,
				overflow:'hidden'
			});
			
			$('#image_box').css({
			  '-webkit-transform' 	: 'scale(' + percentage + ')',
			  '-moz-transform'    	: 'scale(' + percentage + ')',
			  '-ms-transform'     	: 'scale(' + percentage + ')',
			  '-o-transform'     	: 'scale(' + percentage + ')',
			  'transform'         	: 'scale(' + percentage + ')',
			  'left'				: -((1-percentage)*(640/2)),
			  'top'					: -((1-percentage)*(640/2)),
			  'margin-bottom'		: -(640*(1-percentage))+20
			});

			$('#toggleOverlay').addClass('bigger');
		} else {
			$('#image_box').attr('style','display:'+$('#image_box').css('display')+';');
			$('#toggleOverlay').removeClass('bigger');

		}

		resizeImageBox();
	},1000);
}

function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		vars[key] = value;
	});
	return vars;
}

function runFloodlight () {
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    $('body').prepend('<iframe src="http://2623226.fls.doubleclick.net/activityi;src=2623226;type=thank0;cat=allth0;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
}

function checkForSessionCode(){

	if(getUrlVars()['session_code']!=undefined){

		scrollToComp();

		$('#image_short_url').val(decodeURIComponent(getUrlVars()['short_url']));

		setupImage();

		if(getUrlVars()['action']=='user_friends'){

			getFacebookFriends(function(friendData){
				window.friends=[];
				for(var q=0;q<friendData.data.length;q++){
					friends.push({
						label:friendData.data[q].name,
						value:friendData.data[q].id
					});
				}

				$('#enableTagging').remove();
				$('#message .next-info').html('Tag your Facebook friends using the @symbol and their Facebook name');
				$('#message .next-info').css('marginLeft',0);

				initTagging();
			});

		} else if(getUrlVars()['action']=='publish_actions'){
			$('#share_message').val(decodeURIComponent(getUrlVars()['message'].replace(/\+/g,'%20')));
			postFacebookStory();
		}
	}
}

function setupImage(){
	var session_code=getUrlVars()['session_code'];
	$('#session_code').val(session_code);

	$('#step1,#stepLoading').hide();
	$('#image_box').html('<img src="http://rdappshost.com/toyota/allthings/uploadify/uploads/'+session_code+'/created.png" alt="image" />').show();
	$('#step6').show();
}

$(document).ready(function(e){

	homeControls();

	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/en_US/all.js',function(){
                   
		FB.init({
			appId  		: '717041361711105',
			version    : 'v2.1',
			xfbml  		: true
		});

		FB.Canvas.setAutoGrow();
	});

	resizeImageBox();

	CharLimit({
		selector:$('textarea'),
		update:function(element,limit){
			if(limit<50 && limit>10){
				$('#char-count').removeClass('empty').addClass('warning');
			} else if(limit<10){
				$('#char-count').removeClass('warning').addClass('empty');
			} else {
				$('#char-count').removeClass('warning').removeClass('empty');
			}
			$('#char-count .amount').text(limit); 

		}
	});
	

	$('.backStepLink').click(function(e){
		e.preventDefault();
		var step=$(this).parent().parent();
		var me=$(this);
		step.fadeOut('fast',function(){
			$(me.attr('rel')).fadeIn('fast');
			scrollToComp();
		});
	});


	$('.nextStepLink').click(function(e){
		e.preventDefault();
		var step=$(this).parent().parent();

		if(validate(step)){
			var me=$(this);
			$('#step0').fadeOut('fast');
			step.fadeOut('fast',function(){
				$(me.attr('rel')).fadeIn('fast');
				scrollToComp();
			});
		}

	});

	$('body').on('click','.tagmate-hiliter',function(){
		$(this).parent().find('textarea').focus();
	})

	$('.input.checkbox i').click(function(){
		if(!$(this).parent().find('input[type="checkbox"]').is(':checked')){
			$(this).removeClass('icon-un_check').addClass('icon-active_check');
		} else {
			$(this).removeClass('icon-active_check').addClass('icon-un_check');
		}
	});


	$('#addControls').click(function(){
		addControls();
	});

	$('#completeNomination').click(function(){
		if(!$(this).hasClass('loading')){
			
			if(validate($('#step2'))){
				completeNomination();
				$(this).addClass('loading');
			}
		}
	});

	$('#createImage').click(function(){
		if(!$(this).hasClass('loading')){
			$(this).addClass('loading');
			createImage();
		}
	});

	$('#enableTagging').click(function(e){
		e.preventDefault();

		getFriends(function(friendData){
			window.friends=[];
			for(var q=0;q<friendData.data.length;q++){
				friends.push({
					label:friendData.data[q].name,
					value:friendData.data[q].id
				});
			}

			$('#enableTagging').remove();
			$('#message .next-info').html('Tag your Facebook friends using the @symbol and their Facebook name');
			$('#message .next-info').css('marginLeft',0);

			initTagging();
		});
	});

	$('#facebook-share').click(function(e){
		e.preventDefault();

		if(!$(this).hasClass('loading')){
			$(this).addClass('loading');

			XBrowserLogin(function(response){
				if (response && !response.error) {
					postFacebookStory();
		    	} else {
		    		$(this).removeClass('loading');
		    	}
		    },{ scope: 'publish_actions' });
		}
	});

	$('#twitter-share').click(function(e){
		e.preventDefault();
		var tweet=encodeURIComponent("I've nominated someone for the @toyotaireland #AllThingsToAllPeople awards! "+$('#image_short_url').val());
		window.open('https://twitter.com/intent/tweet?text='+tweet);

		twitterShare();
	});

});

function XBrowserLogin(callback,options){
	if( navigator.userAgent.indexOf('CriOS')!==-1 || (typeof chrome !=undefined && (navigator.userAgent.indexOf('iPhone')!==-1)) ){

		var appID=717041361711105;
		var tagData=doTagging();
		var message=tagData.message;
		var shortUrl=$('#image_short_url').val();
		var uri='http://allthings.toyota.ie?session_code='+$('#session_code').val()+'&action='+options.scope+'&short_url='+shortUrl;

		if(options.scope=='publish_actions'){
			uri+='&message='+encodeURIComponent(message);
		}

		top.location.href='https://www.facebook.com/dialog/oauth?display=popup&client_id='+appID+'&redirect_uri='+encodeURIComponent(uri)+'&scope='+options.scope;

	} else {

	    FB.login(function (response){
	    	callback(response);
	    },options);

	}
}

function XDomainData(url,data,callback) {
	var api_url='http://rdappshost.com/toyota/allthings/';

	$.post('assets/encrypt.php',data,function(response){
		r=$.parseJSON(response);

		if(r.status=='success'){

			$.ajax({
				url 	:api_url+url,
				cache 	:false,
				async 	:true,
				dataType:'jsonp',
				jsonpCallback:'content',
				crossDomain:true,
				data:{
						data:r.data
					},
				success: function(response){
						if(response.status=='success'){
							callback(response);
						} else {
							console.error(response);
						}
					}
				});

		} else {
			console.log('Error with data encryption');
		}

	});

}

function scrollToComp(){
	$('html, body').animate({
        scrollTop: parseInt($('#home').offset().top)-parseInt($('.primary-nav').height())-20
    }, 1000);
}

function postFacebookStory(){
	var tagData=doTagging();

	FB.api('/me/allthingsallpeople:nominate','POST',
	{
		'person':'http://rdappshost.com/toyota/allthings/award.php?session_code='+$('#session_code').val(),
		'message': tagData.message,//mentions
		//'tags': tagData.tags.join(),//tag
		'image:url':'http://rdappshost.com/toyota/allthings/uploadify/uploads/'+$('#session_code').val()+'/created.png',
		'image:user_generated':true
	},
	function(r){
		if (r && !r.error) {
			$('#before,#facebook-share').hide();
			$('#fb-share-success').show();

			facebookShare();
		} else {
			$(this).removeClass('loading');
		}
	});
}

function getFacebookFriends(callback) {
	FB.api('me/taggable_friends',function(r){
		if (r && !r.error) {
			if(typeof callback != 'undefined'){
				callback(r);
			}
		} else {
			//console.error('APP ERROR');
		}
	});
}

function facebookShare () {
	$.ajax({
		url 	:apiUrl+'api/share/facebook',
		cache 	:false,
		async 	:true,
		dataType:'jsonp',
		jsonpCallback:'content',
		crossDomain:true,
		data:{
				session_code:$('#session_code').val()
			},
		success: function(response){
				if(response.status=='success'){
					
				} else {
					
				}
			}
		});
}


function twitterShare () {
	$.ajax({
		url 	:apiUrl+'api/share/twitter',
		cache 	:false,
		async 	:true,
		dataType:'jsonp',
		jsonpCallback:'content',
		crossDomain:true,
		data:{
				session_code:$('#session_code').val()
			},
		success: function(response){
				if(response.status=='success'){
					
				} else {
					
				}
			}
		});
}


function homeControls(){
	getFilters();
	filterControls();
	image_get_controls();
}

function initTagging(){
	$("#step6 textarea").tagmate({
		exprs: {
			"@": Tagmate.NAME_TAG_EXPR
		},
		sources: {
			"@": function(request, response) {
				var filtered = Tagmate.filterOptions(window.friends, request.term);
				response(filtered);
			}
		},
		capture_tag: function(tag) { },
		replace_tag: function(tag, value) { },
		highlight_tags: true,
		highlight_class: "tag",
		menu_class: "menu",
		menu_option_class: "option",
		menu_option_active_class: "active"
	});
}

function getFriends(callback){
	XBrowserLogin(function (response){
		if (response && !response.error) {

			getFacebookFriends(callback);

		}
	}, { scope: 'user_friends' });
}

function doTagging(){
	var message=$('#message textarea').val();
	var tags=[];
	var names=[];
	for(var q=0;q<window.friends.length;q++){
		if(message.indexOf(window.friends[q].label)!=-1){
			message=message.replace(window.friends[q].label,'['+window.friends[q].value+']');
			tags.push(window.friends[q].value);
			names.push(window.friends[q].label);
		}
	}
	return {
			message:message,
			tags:tags,
			names:names
		};
}

function postToStream(){
	
}

function image_get_controls(){
	initUploadifive();
	initUploadifive2();
}

// UPLOADER
function initUploadifive(){
	$('#uploadphoto').uploadifive({
		'buttonText' : 'Upload Photo',
        'uploadScript' : apiUrl+'uploadify/uploadify.php?session_code='+$('#session_code').val(),
        'fileSizeLimit' : '10MB',
        'fileType'     : 'image',
        'auto'         : true,
        'onFallback'   : function() {
        	initUploadify();
        },
        'onUploadComplete' : function(file, data) {
        	var url='https://rdappshost.com/toyota/allthings/uploadify/uploads/'+$('#session_code').val()+'/'+data;
			$('#image_url').val(url);
        	getFilterUI();
        	$('#uploadifive-uploadphoto-queue').html('');
        	$('#step4').addClass('valid');

        	$('#photo-preview-box').html('<img src="'+url+'" alt="uploaded image" />');
        	$('#photo-preview-box').fadeIn('fast');
        	$('#uploaded-image').val(url);

	        startImageCheck();
        },
        'onInit':function(instance){
        	$('#uploadifive-uploadphoto').attr('style','cursor:pointer;');
        	$('#uploadifive-uploadphoto').attr('class','uiBtn greyBtn hasIcon smBtn');
        	$('#uploadifive-uploadphoto').prepend('<i class="icon icon-upload-alt"></i>');
        	$('#uploadifive-uploadphoto').find().attr('style',"opacity: 0; position: absolute; left: 0px; height: 100%; top: 0px; cursor: pointer;");
        	var queue=$('#uploadifive-uploadphoto-queue').get(0);
        	$('#uploadifive-uploadphoto-queue').remove();
        	$('#uploadphoto').parent().after(queue);
        }
    }); 
}

function initUploadify(){
	$('#uploadphoto').uploadify({
		'buttonText' : 'Upload photo',
		'hideButton' : true,                                
		'wmode'    : 'transparent',
		'swf'      : 'assets/uploadify/uploadify.swf',
		'uploader' : apiUrl+'uploadify/uploadify.php?session_code='+$('#session_code').val(),
		'fileSizeLimit' : '10MB',
		'fileTypeExts' : '*.jpg; *.png; *.bmp;',
		'preventCaching' : false,
		'folder'    : 'uploadify/uploads',
      	'auto'      : true,
		'height'   : 34,
		'width'	: '100%',
		'onInit'   : function(instance) {
			var queue=$('#uploadphoto-queue').get(0);
        	$('#uploadphoto-queue').remove();
        	$('#uploadphoto').parent().after(queue);
		},
		'onUploadSuccess' : function(file, data, response) {

			var url='https://rdappshost.com/toyota/allthings/uploadify/uploads/'+$('#session_code').val()+'/'+data;
			$('#image_url').val(url);
        	getFilterUI();
        	$('#uploadphoto-queue').html();
        	$('#step4').addClass('valid');

        	$('#photo-preview-box').html('<img src="'+url+'" alt="uploaded image" />');
        	$('#photo-preview-box').fadeIn('fast');
        	$('#uploaded-image').val(url);

        	startImageCheck();
        }
	});
}

function initUploadifive2(){
	$('#uploadphoto2').uploadifive({
		'buttonText' : 'Upload Photo',
        'uploadScript' : apiUrl+'uploadify/uploadify.php?session_code='+$('#session_code').val(),
        'fileSizeLimit' : '10MB',
        'fileType'     : 'image',
        'auto'         : true,
        'onFallback'   : function() {
        	initUploadify2();
        },
        'onUploadComplete' : function(file, data) {
        	var url='https://rdappshost.com/toyota/allthings/uploadify/uploads/'+$('#session_code').val()+'/'+data;
			$('#image_url').val(url);
        	getFilterUI();
        	$('#uploadifive-uploadphoto2-queue').html('');
        	$('#stepUpload').addClass('valid');

        	$('#photo-preview-box2').html('<img src="'+url+'" alt="uploaded image" />');
        	$('#photo-preview-box2').fadeIn('fast');
        	$('#uploaded-image2').val(url);

	        startImageCheck();
        },
        'onInit':function(instance){
        	$('#uploadifive-uploadphoto2').attr('style','cursor:pointer;');
        	$('#uploadifive-uploadphoto2').attr('class','uiBtn greyBtn hasIcon smBtn');
        	$('#uploadifive-uploadphoto2').prepend('<i class="icon icon-upload-alt"></i>');
        	$('#uploadifive-uploadphoto2').find().attr('style',"opacity: 0; position: absolute; left: 0px; height: 100%; top: 0px; cursor: pointer;");
        	var queue=$('#uploadifive-uploadphoto2-queue').get(0);
        	$('#uploadifive-uploadphoto2-queue').remove();
        	$('#uploadphoto2').parent().after(queue);
        }
    }); 
}

function initUploadify2(){
	$('#uploadphoto2').uploadify({
		'buttonText' : 'Upload photo',
		'hideButton' : true,                                
		'wmode'    : 'transparent',
		'swf'      : 'assets/uploadify/uploadify.swf',
		'uploader' : apiUrl+'uploadify/uploadify.php?session_code='+$('#session_code').val(),
		'fileSizeLimit' : '10MB',
		'fileTypeExts' : '*.jpg; *.png; *.bmp;',
		'preventCaching' : false,
		'folder'    : 'uploadify/uploads',
      	'auto'      : true,
		'height'   : 34,
		'width'	: '100%',
		'onInit'   : function(instance) {
			var queue=$('#uploadphoto2-queue').get(0);
        	$('#uploadphoto2-queue').remove();
        	$('#uploadphoto2').parent().after(queue);
		},
		'onUploadSuccess' : function(file, data, response) {

			var url='https://rdappshost.com/toyota/allthings/uploadify/uploads/'+$('#session_code').val()+'/'+data;
			$('#image_url').val(url);
        	getFilterUI();
        	$('#uploadphoto2-queue').html();
        	$('#step4').addClass('valid');

        	$('#photo-preview-box2').html('<img src="'+url+'" alt="uploaded image" />');
        	$('#photo-preview-box2').fadeIn('fast');
        	$('#uploaded-image2').val(url);

        	startImageCheck();
        }
	});
}

// (end)UPLOADER

function startImageCheck (argument) {
	setTimeout(function(){
    	adjustContainment();
		updateData();

		startImageCheck();
    },500);
}

// FILTER

function getFilterUI(){
	$('#containment').find('img').attr('src',$('#image_url').val());
	setTimeout(function(){
		adjustContainment();
		setTimeout(function(){
			
			var new_top=(parseInt($('#containment').height())/2)-(parseInt($('#containment').find('img').height())/2);
			var new_left=(parseInt($('#containment').width())/2)-(parseInt($('#containment').find('img').width())/2);
			$('#containment').find('img').css({
				top:new_top,
				left:new_left
			});

		},1000);
	},1000);
}


/*======================================
=            Filter load/control       =
======================================*/

function filterControls(){
	$('#creator .next').click(function(){
		getNextFilter();
		$('#step2').animate({opacity:1},500);
	});

	$('#creator .prev').click(function(){
		getPrevFilter();
		$('#step2').animate({opacity:1},500);
	});

	$('.toggle').click(function(){
		toggleFilterOutline();
		$('#step2').animate({opacity:1},500);
	});
}

function getFilters(){

	$.ajax({
		url 	:apiUrl+'api/filters',
		cache 	:false,
		async 	:true,
		dataType:'jsonp',
		jsonpCallback:'content',
		crossDomain:true,
		data:{
				
			},
		success: function(r){
				if(r.status=='success'){
					$('#step3').addClass('valid');
					window.filters=r.data;
					window.filterIndex=0;
					loadFilter();
				}
			}
		});
}


function getNextFilter(){
	$('#filter').fadeIn('fast',function(){
		$('#filter_outline').hide();
	});
	if(window.filterIndex!=-1){//if filters have been loaded
		window.filterIndex++;
		if(window.filterIndex>window.filters.length-1){
			window.filterIndex=0;
		}
		loadFilter();
	}
}

function getPrevFilter(){
	$('#filter').fadeIn('fast',function(){
		$('#filter_outline').hide();
	});
	if(window.filterIndex!=-1){//if filters have been loaded
		window.filterIndex--;
		if(window.filterIndex<0){
			window.filterIndex=window.filters.length-1;
		}
		loadFilter();
	}
}

function loadFilter(){
	if(window.filters.length>0){
		var filter=window.filters[window.filterIndex];

		if (!$.support.leadingWhitespace) {
			$('#filter').css('filter',"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+apiUrl+filter.image+"', sizingMethod='scale')");
			$('#filter').css('background','none !important');
			// $('#filter_outline').css('filter',"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+filter.outline_ie+"', sizingMethod='scale')");
			// $('#filter_outline').css('background','none !important');
		} else {
			$('#filter').css('background-image','url('+apiUrl+filter.image+')');
			// $('#filter_outline').css('background-image','url('+filter.outline+')');
			PointerEventsPolyfill.initialize({});
		}
		$('#image_filter').val(filter.id);
	} else {
		alert('There are no filters!');
	}
}

function toggleFilterOutline(){
	$('#filter').fadeToggle('fast',function(){
		$('#filter_outline').toggle();
	});
}

/*-----  End of Filter load/control  ------*/



function adjustContainment(){
	var e=$('#containment');
	if(e.find('img').width()>=e.find('img').height()){
		sliderMin=(parseInt($('#image_box').width())/parseInt(e.find('img').width()))*50;
	} else {
		sliderMin=(parseInt($('#image_box').height())/parseInt(e.find('img').height()))*50;
	}
	if(!sliderMin||sliderMin==null||sliderMin=='undefined'){
		sliderMin=1;
	}
	var cTop=parseInt(e.find('img').height())/2;
	var cHeight=parseInt(e.find('img').height())+parseInt($('#image_box').height());
	var cLeft=parseInt(e.find('img').width())/2;
	var cWidth=parseInt(e.find('img').width())+parseInt($('#image_box').width());

	//corner locks as percentage of containment which is 2x Height and 2x the Width
	var posPercX=parseInt(e.find('img').css('left'))/cWidth;
	var posPercY=parseInt(e.find('img').css('top'))/cHeight;

	if(parseInt(e.find('img').height())<parseInt($('#image_box').height())){
		cTop=parseInt(e.find('img').height())/2;
		cHeight=parseInt($('#image_box').height())+parseInt(e.find('img').height());
	}
	if(parseInt(e.find('img').width())<parseInt($('#image_box').width())){
		cLeft=parseInt(e.find('img').width())/2;
		cWidth=parseInt($('#image_box').width())+parseInt(e.find('img').width());
	}
	e.css({ top:-cTop,left:-cLeft,height:cHeight,width:cWidth});
	e.find('img').css('top',cHeight*posPercY);
    e.find('img').css('left',cWidth*posPercX);
	updateData();
}

function addControls(){
	var e=$('#containment');

	e.attr('rel',e.find('img').height());


	$('.move-up,.move-down,.move-left,.move-right').click(function(){

		var gap=50;

		if($(this).hasClass('move-up')){
			e.find('img').css('top',parseInt(e.find('img').css('top'))-gap);
		} else if($(this).hasClass('move-down')){
			e.find('img').css('top',parseInt(e.find('img').css('top'))+gap);
		} else if($(this).hasClass('move-left')){
			e.find('img').css('left',parseInt(e.find('img').css('left'))-gap);
		} else if($(this).hasClass('move-right')){
			e.find('img').css('left',parseInt(e.find('img').css('left'))+gap);
		}

		updateData();
	});

	$('.zoom-in').click(function(){
		var scale=parseFloat($(this).parent().attr('rel'));
		if(scale<2){
			scale+=0.1;
			$(this).parent().attr('rel',scale);
		}
		e.find('img').css('height',(scale)*e.attr('rel'));

		adjustContainment();
	    updateData();
	});

	$('.zoom-out').click(function(){
		var scale=parseFloat($(this).parent().attr('rel'));
		if(scale>0.1){
			scale-=0.1;
			$(this).parent().attr('rel',scale);
		}
		e.find('img').css('height',(scale)*e.attr('rel'));

		adjustContainment();
	    updateData();
	});

}

function updateData(){
	var e=$('#containment');
	$('#image_top').val(parseInt(e.find('img').css('top'))+parseInt(e.css('top')));
	$('#image_left').val(parseInt(e.find('img').css('left'))+parseInt(e.css('left')));
	$('#image_height').val(parseInt(e.find('img').height()));
	$('#image_width').val(parseInt(e.find('img').width()));
}
//(end) FILTER

// IMAGE CREATION
function createImage(){

	$.ajax({
		url 	:apiUrl+'api/image/create',
		cache 	:false,
		async 	:true,
		dataType:'jsonp',
		jsonpCallback:'content',
		crossDomain:true,
		data:{
				session_code: $('#session_code').val(),
				//
				image_url: $('#image_url').val(),
				image_filter: $('#image_filter').val(),
				image_message: $('#message').val(),
				image_top: $('#image_top').val(),
				image_left: $('#image_left').val(),
				image_height: $('#image_height').val(),
				image_width: $('#image_width').val()
			},
		success: function(r){
				if(r.status=="success"){
					$('#image_short_url').val(r.data.image_short_url);

					$('#step5').fadeOut('fast',function(){
						$('#step6').fadeIn('fast');
						scrollToComp();
						$('#toggleOverlay').hide();
					});

				} else {
					alert('Error saving your data! Please refresh and try again.');
				}			
			}
		});
			
}

function completeNomination(){

	XDomainData('api/nominee',{
		session_code: $('#session_code').val(),
		//
		nominee_name:$('#nominee_name').val(),
		nominee_why:$('#nominee_why').val(),
		//
		user_title:$('#user_title').val(),
		user_name:$('#user_name').val(),
		user_email:$('#user_email').val(),
		user_phone:$('#user_phone').val(),
		user_tandcs:$('#user_tandcs:checked').val(),
		//
		image_url: $('#image_url').val()
	},function(response){
		if(response.status=='success'){
			runFloodlight();
			$('#step2').fadeOut('fast',function(){
				$('#step3').fadeIn('fast');
				scrollToComp();
			});
		} else {
			alert('Error saving your data! Please refresh and try again.');
		}
	});
}

// (end) IMAGE CREATION

/*
* To be used when the terms and conditions button is loaded.
*/
function initFacebox(){
	$('a[rel*=facebox]').facebox();	
}

/*
* Form validation
* - add/remove fields for validation below.
* Structure for form:
* 	<div class="input_text"><input type="text" id="name" value="" /></div>
* and for email add email class to surrounding div:
*	<div class="input_text email"><input type="text" id="name" value="" /></div>
*/
function validate(e){
	var valid=true;
	e.find('.input').each(function(){
		if(!$(this).hasClass('not-required')){
			if($(this).find('input,textarea,select').length>0){
				var isEmail=false;var isCheck=false;
				if($(this).hasClass('email')){ isEmail=true;
				} else if($(this).hasClass('checkbox')){ isCheck=true; }
				var s=$(this).find('input,textarea,select');
				if(s.val()==''||s.val()==s.attr('rel')||(isEmail&&!self.isValidEmailAddress(s.val()))||(!$(s).is(':checked')&&isCheck)){
					s.parent().removeClass('valid');
					s.parent().addClass('invalid');
					valid=false;
				} else {
					s.parent().removeClass('invalid');
					s.parent().addClass('valid');
				}
			}
		}
	});
	return valid;
}

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}


/*
* Resizes the facebook tab after new content has been loaded.
*/
function topOfPage(){
	if (typeof(FB)!='undefined'&&FB!=null){
		FB.Canvas.scrollTo(0,0);
		FB.Canvas.setAutoGrow(false);
		FB.Canvas.setSize({height:100});
		setTimeout(function(){
			FB.Canvas.setAutoGrow(true);
		}, 100);
	}	
}

(function ($, undefined) {
    $.fn.getCursorPosition = function() {
        var el = $(this).get(0);
        var pos = 0;
        if('selectionStart' in el) {
            pos = el.selectionStart;
        } else if('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    }
})(jQuery);