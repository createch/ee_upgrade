var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var players=new Array();

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
//setTimeout(function(){event.target.playVideo();}, 3000, event);
//event.target.playVideo();
}

function onPlayerError(event) {
//setTimeout(function(){event.target.playVideo();}, 3000, event);
//console.log(event)
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.

var pauseFlag = false;
function onPlayerStateChange(event) {
//console.log(event.target.d.id)
videoID=event.target.d.id;
if (event.data == YT.PlayerState.PLAYING) {
       ga('send', 'event', 'Videos', 'Play', 'VideoId: '+videoID, 0);
       pauseFlag = true;
   }
   // track when user clicks to Pause
   if (event.data == YT.PlayerState.PAUSED && pauseFlag) {
       ga('send', 'event', 'Videos', 'Pause', 'VideoId: '+videoID, 0);
       pauseFlag = false;
   }
   // track when video ends
   if (event.data == YT.PlayerState.ENDED) {
       ga('send', 'event', 'Videos', 'Ended', 'VideoId: '+videoID, 0);
   }
}


function createVideo(vid,width,height){
   var player = new YT.Player(vid, {
     height: height,
     width: width,
     videoId: vid,
     playerVars: { 'autoplay': 1, 'controls': 2 , 'enablejsapi':1 , 'theme': 'light'},
     events: {
       'onReady': onPlayerReady,
       'onStateChange': onPlayerStateChange,
       'onError':onPlayerError
     }
   });

   players.push(player)

}
