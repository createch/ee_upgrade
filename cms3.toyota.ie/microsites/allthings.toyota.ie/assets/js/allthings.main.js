$(document).ready(function(){

	parallaxContentResize();
	$(window).resize(function(){
		parallaxContentResize();
	});

	window.location.hash=hash;
});


function parallaxContentResize(){
	$('.parallax-layer').each(function(i,e){

		if(parseInt($(this).height())<parseInt($(this).find('.container_12').height())){
			var newH=$(this).find('.container_12').height();
			newH+=(marginBottom=30);
			$(this).height(newH);
			$('.parallax:nth-child('+(i+1)+')').height(newH);
			$('.parallax:nth-child('+(i+1)+') .bg-par').height(newH);
		}
	});

	$('.parallax').each(function(i,e){

		if(parseInt($(this).height())<parseInt($(this).find('.move-into-parallax').height())){
			var newH=$(this).find('.move-into-parallax').height();
			newH+=(marginBottom=30);
			$(this).height(newH);
			$(this).find('.background-layer').height(newH);
		}
	});
}

$(document).ready(function(){
	var parallaxeffect=(function(){
		o={
			currentPos:0,
			effects:[],
			init:function(){
					this.loop();
					return this;
				},
			loop:function(){
				var me=this;
				var theInt=setTimeout(function(){
					me.currentPos=$(document).scrollTop();
					me.doEffects();
					me.loop();
				},50);
			},
			percentageOfWindow:function($el){
				var p=1-($el.offset().top-this.currentPos)/parseInt($(window).height());
				return this.limit(p);
			},
			limit:function(p){
				return p>1?p=1:(p<0?0:p);
			},
			addEffect:function(obj){
				this.effects.push(obj);
			},
			doEffects:function(){
				for(var q=0;q<this.effects.length;q++){
					var effectPerc=this.limit((this.percentageOfWindow(this.effects[q].object)-this.effects[q].time.start)/this.effects[q].time.end);
					for(var w=0;w<this.effects[q].change.length;w++){
						var change=this.effects[q].change[w];
						this.effects[q].object.css(change.type,change.from+((change.to-change.from)*effectPerc));
					}
				}
			}
		};

		return o.init();
	})();


	$('.nomination-quote').each(function(){
		parallaxeffect.addEffect({
			object 	:$(this),
			time 	:{ start:0.1, end:0.3 },
			change 	:[
						{ type:'opacity', from:0, to:1 },
						{ type:'top', from:-30, to:0 }
					]
			});
	});

});


	

	

var CharLimit=function(options){

	var defaults={
		selector:$('textarea'),
		limit:500,
		current:0,
		update:false,
		focus:false
	};

    o={

        init:function(){
        		this.initDefaults(defaults,options);
	        	this.events();
	        },

	   	initDefaults:function(obj1,obj2){

			    this.options = {};
			    for (var a in obj1) { this.options[a] = obj1[a]; }
			    for (var a in obj2) { this.options[a] = obj2[a]; }
			    return this.options;

		   	},

        events:function(){

        		var me=this;

	        	me.options.selector.focus(function(){
	        		me.options.focus=true;
	        	}).blur(function(){
	        		me.options.focus=false;
	        	});

	        	me.tick();

	        },

        check:function(element){

	        	if(element.val().length>this.options.limit){
	        		element.val(element.val().substr(0,this.options.limit));
	        	}

	        	if(this.options.update && this.options.current != element.val().length){
	        		this.options.current=element.val().length;

	        		this.options.update(this.options.selector,((this.options.limit-this.options.current)<0)?0:this.options.limit-this.options.current);
	        	}

	        },

	    tick:function(){
	    		var me=this;
	    		setTimeout(function(){

	    			if(me.options.focus){
	    				me.check(me.options.selector);
	    			}

	    			me.tick();

	    		},100);
	    	}
    };

    return o.init();
};
/**
 * jQuery-Plugin "clearField"
 * 
 * @version: 1.1, 04.12.2010
 * 
 * @author: Stijn Van Minnebruggen
 *          stijn@donotfold.be
 *          http://www.donotfold.be
 * 
 * @example: $('selector').clearField();
 * @example: $('selector').clearField({ blurClass: 'myBlurredClass', activeClass: 'myActiveClass' });
 * 
 */

(function($) {
	
	$.fn.clearField = function(settings) {
		
		/**
		 * Settings
		 * 
		 */
		
		settings = jQuery.extend({
			blurClass: 'clearFieldBlurred',
			activeClass: 'clearFieldActive',
			attribute: 'rel',
			value: ''
		}, settings);
		
		
		/**
		 * loop each element
		 * 
		 */
		
		return $(this).each(function() {
			
			/**
			 * Set element
			 * 
			 */
			
			var el = $(this);
			
			
			/**
			 * Get starting value
			 * 
			 */
			
			settings.value = el.val();
			
			
			/**
			 * Add or get attribute
			 * 
			 */
			
			if(el.attr(settings.attribute) == undefined) {
				el.attr(settings.attribute, el.val()).addClass(settings.blurClass);
			} else {
				settings.value = el.attr(settings.attribute);
			}
			
			
			/**
			 * Set focus action
			 * 
			 */
			
			el.focus(function() {
				
				if(el.val() == el.attr(settings.attribute)) {
					el.val('').removeClass(settings.blurClass).addClass(settings.activeClass);
				}
				
			});
			
			
			/**
			 * Set blur action
			 * 
			 */
			
			el.blur(function() {
				
				if(el.val() == '') {
					el.val(el.attr(settings.attribute)).removeClass(settings.activeClass).addClass(settings.blurClass);
				}
				
			});
			
			
		});
		
	};
	
})(jQuery);

/*!
 * jCarousel - Riding carousels with jQuery
 *   http://sorgalla.com/jcarousel/
 *
 * Copyright (c) 2006 Jan Sorgalla (http://sorgalla.com)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Built on top of the jQuery library
 *   http://jquery.com
 *
 * Inspired by the "Carousel Component" by Bill Scott
 *   http://billwscott.com/carousel/
 */

(function(g){var q={vertical:!1,rtl:!1,start:1,offset:1,size:null,scroll:3,visible:null,animation:"normal",easing:"swing",auto:0,wrap:null,initCallback:null,setupCallback:null,reloadCallback:null,itemLoadCallback:null,itemFirstInCallback:null,itemFirstOutCallback:null,itemLastInCallback:null,itemLastOutCallback:null,itemVisibleInCallback:null,itemVisibleOutCallback:null,animationStepCallback:null,buttonNextHTML:"<div></div>",buttonPrevHTML:"<div></div>",buttonNextEvent:"click",buttonPrevEvent:"click", buttonNextCallback:null,buttonPrevCallback:null,itemFallbackDimension:null},m=!1;g(window).bind("load.jcarousel",function(){m=!0});g.jcarousel=function(a,c){this.options=g.extend({},q,c||{});this.autoStopped=this.locked=!1;this.buttonPrevState=this.buttonNextState=this.buttonPrev=this.buttonNext=this.list=this.clip=this.container=null;if(!c||c.rtl===void 0)this.options.rtl=(g(a).attr("dir")||g("html").attr("dir")||"").toLowerCase()=="rtl";this.wh=!this.options.vertical?"width":"height";this.lt=!this.options.vertical? this.options.rtl?"right":"left":"top";for(var b="",d=a.className.split(" "),f=0;f<d.length;f++)if(d[f].indexOf("jcarousel-skin")!=-1){g(a).removeClass(d[f]);b=d[f];break}a.nodeName.toUpperCase()=="UL"||a.nodeName.toUpperCase()=="OL"?(this.list=g(a),this.clip=this.list.parents(".jcarousel-clip"),this.container=this.list.parents(".jcarousel-container")):(this.container=g(a),this.list=this.container.find("ul,ol").eq(0),this.clip=this.container.find(".jcarousel-clip"));if(this.clip.size()===0)this.clip= this.list.wrap("<div></div>").parent();if(this.container.size()===0)this.container=this.clip.wrap("<div></div>").parent();b!==""&&this.container.parent()[0].className.indexOf("jcarousel-skin")==-1&&this.container.wrap('<div class=" '+b+'"></div>');this.buttonPrev=g(".jcarousel-prev",this.container);if(this.buttonPrev.size()===0&&this.options.buttonPrevHTML!==null)this.buttonPrev=g(this.options.buttonPrevHTML).appendTo(this.container);this.buttonPrev.addClass(this.className("jcarousel-prev"));this.buttonNext= g(".jcarousel-next",this.container);if(this.buttonNext.size()===0&&this.options.buttonNextHTML!==null)this.buttonNext=g(this.options.buttonNextHTML).appendTo(this.container);this.buttonNext.addClass(this.className("jcarousel-next"));this.clip.addClass(this.className("jcarousel-clip")).css({position:"relative"});this.list.addClass(this.className("jcarousel-list")).css({overflow:"hidden",position:"relative",top:0,margin:0,padding:0}).css(this.options.rtl?"right":"left",0);this.container.addClass(this.className("jcarousel-container")).css({position:"relative"}); !this.options.vertical&&this.options.rtl&&this.container.addClass("jcarousel-direction-rtl").attr("dir","rtl");var j=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible):null,b=this.list.children("li"),e=this;if(b.size()>0){var h=0,i=this.options.offset;b.each(function(){e.format(this,i++);h+=e.dimension(this,j)});this.list.css(this.wh,h+100+"px");if(!c||c.size===void 0)this.options.size=b.size()}this.container.css("display","block");this.buttonNext.css("display","block");this.buttonPrev.css("display", "block");this.funcNext=function(){e.next()};this.funcPrev=function(){e.prev()};this.funcResize=function(){e.resizeTimer&&clearTimeout(e.resizeTimer);e.resizeTimer=setTimeout(function(){e.reload()},100)};this.options.initCallback!==null&&this.options.initCallback(this,"init");!m&&g.browser.safari?(this.buttons(!1,!1),g(window).bind("load.jcarousel",function(){e.setup()})):this.setup()};var f=g.jcarousel;f.fn=f.prototype={jcarousel:"0.2.8"};f.fn.extend=f.extend=g.extend;f.fn.extend({setup:function(){this.prevLast= this.prevFirst=this.last=this.first=null;this.animating=!1;this.tail=this.resizeTimer=this.timer=null;this.inTail=!1;if(!this.locked){this.list.css(this.lt,this.pos(this.options.offset)+"px");var a=this.pos(this.options.start,!0);this.prevFirst=this.prevLast=null;this.animate(a,!1);g(window).unbind("resize.jcarousel",this.funcResize).bind("resize.jcarousel",this.funcResize);this.options.setupCallback!==null&&this.options.setupCallback(this)}},reset:function(){this.list.empty();this.list.css(this.lt, "0px");this.list.css(this.wh,"10px");this.options.initCallback!==null&&this.options.initCallback(this,"reset");this.setup()},reload:function(){this.tail!==null&&this.inTail&&this.list.css(this.lt,f.intval(this.list.css(this.lt))+this.tail);this.tail=null;this.inTail=!1;this.options.reloadCallback!==null&&this.options.reloadCallback(this);if(this.options.visible!==null){var a=this,c=Math.ceil(this.clipping()/this.options.visible),b=0,d=0;this.list.children("li").each(function(f){b+=a.dimension(this, c);f+1<a.first&&(d=b)});this.list.css(this.wh,b+"px");this.list.css(this.lt,-d+"px")}this.scroll(this.first,!1)},lock:function(){this.locked=!0;this.buttons()},unlock:function(){this.locked=!1;this.buttons()},size:function(a){if(a!==void 0)this.options.size=a,this.locked||this.buttons();return this.options.size},has:function(a,c){if(c===void 0||!c)c=a;if(this.options.size!==null&&c>this.options.size)c=this.options.size;for(var b=a;b<=c;b++){var d=this.get(b);if(!d.length||d.hasClass("jcarousel-item-placeholder"))return!1}return!0}, get:function(a){return g(">.jcarousel-item-"+a,this.list)},add:function(a,c){var b=this.get(a),d=0,p=g(c);if(b.length===0)for(var j,e=f.intval(a),b=this.create(a);;){if(j=this.get(--e),e<=0||j.length){e<=0?this.list.prepend(b):j.after(b);break}}else d=this.dimension(b);p.get(0).nodeName.toUpperCase()=="LI"?(b.replaceWith(p),b=p):b.empty().append(c);this.format(b.removeClass(this.className("jcarousel-item-placeholder")),a);p=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible): null;d=this.dimension(b,p)-d;a>0&&a<this.first&&this.list.css(this.lt,f.intval(this.list.css(this.lt))-d+"px");this.list.css(this.wh,f.intval(this.list.css(this.wh))+d+"px");return b},remove:function(a){var c=this.get(a);if(c.length&&!(a>=this.first&&a<=this.last)){var b=this.dimension(c);a<this.first&&this.list.css(this.lt,f.intval(this.list.css(this.lt))+b+"px");c.remove();this.list.css(this.wh,f.intval(this.list.css(this.wh))-b+"px")}},next:function(){this.tail!==null&&!this.inTail?this.scrollTail(!1): this.scroll((this.options.wrap=="both"||this.options.wrap=="last")&&this.options.size!==null&&this.last==this.options.size?1:this.first+this.options.scroll)},prev:function(){this.tail!==null&&this.inTail?this.scrollTail(!0):this.scroll((this.options.wrap=="both"||this.options.wrap=="first")&&this.options.size!==null&&this.first==1?this.options.size:this.first-this.options.scroll)},scrollTail:function(a){if(!this.locked&&!this.animating&&this.tail){this.pauseAuto();var c=f.intval(this.list.css(this.lt)), c=!a?c-this.tail:c+this.tail;this.inTail=!a;this.prevFirst=this.first;this.prevLast=this.last;this.animate(c)}},scroll:function(a,c){!this.locked&&!this.animating&&(this.pauseAuto(),this.animate(this.pos(a),c))},pos:function(a,c){var b=f.intval(this.list.css(this.lt));if(this.locked||this.animating)return b;this.options.wrap!="circular"&&(a=a<1?1:this.options.size&&a>this.options.size?this.options.size:a);for(var d=this.first>a,g=this.options.wrap!="circular"&&this.first<=1?1:this.first,j=d?this.get(g): this.get(this.last),e=d?g:g-1,h=null,i=0,k=!1,l=0;d?--e>=a:++e<a;){h=this.get(e);k=!h.length;if(h.length===0&&(h=this.create(e).addClass(this.className("jcarousel-item-placeholder")),j[d?"before":"after"](h),this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size)))j=this.get(this.index(e)),j.length&&(h=this.add(e,j.clone(!0)));j=h;l=this.dimension(h);k&&(i+=l);if(this.first!==null&&(this.options.wrap=="circular"||e>=1&&(this.options.size===null||e<= this.options.size)))b=d?b+l:b-l}for(var g=this.clipping(),m=[],o=0,n=0,j=this.get(a-1),e=a;++o;){h=this.get(e);k=!h.length;if(h.length===0){h=this.create(e).addClass(this.className("jcarousel-item-placeholder"));if(j.length===0)this.list.prepend(h);else j[d?"before":"after"](h);if(this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size))j=this.get(this.index(e)),j.length&&(h=this.add(e,j.clone(!0)))}j=h;l=this.dimension(h);if(l===0)throw Error("jCarousel: No width/height set for items. This will cause an infinite loop. Aborting..."); this.options.wrap!="circular"&&this.options.size!==null&&e>this.options.size?m.push(h):k&&(i+=l);n+=l;if(n>=g)break;e++}for(h=0;h<m.length;h++)m[h].remove();i>0&&(this.list.css(this.wh,this.dimension(this.list)+i+"px"),d&&(b-=i,this.list.css(this.lt,f.intval(this.list.css(this.lt))-i+"px")));i=a+o-1;if(this.options.wrap!="circular"&&this.options.size&&i>this.options.size)i=this.options.size;if(e>i){o=0;e=i;for(n=0;++o;){h=this.get(e--);if(!h.length)break;n+=this.dimension(h);if(n>=g)break}}e=i-o+ 1;this.options.wrap!="circular"&&e<1&&(e=1);if(this.inTail&&d)b+=this.tail,this.inTail=!1;this.tail=null;if(this.options.wrap!="circular"&&i==this.options.size&&i-o+1>=1&&(d=f.intval(this.get(i).css(!this.options.vertical?"marginRight":"marginBottom")),n-d>g))this.tail=n-g-d;if(c&&a===this.options.size&&this.tail)b-=this.tail,this.inTail=!0;for(;a-- >e;)b+=this.dimension(this.get(a));this.prevFirst=this.first;this.prevLast=this.last;this.first=e;this.last=i;return b},animate:function(a,c){if(!this.locked&& !this.animating){this.animating=!0;var b=this,d=function(){b.animating=!1;a===0&&b.list.css(b.lt,0);!b.autoStopped&&(b.options.wrap=="circular"||b.options.wrap=="both"||b.options.wrap=="last"||b.options.size===null||b.last<b.options.size||b.last==b.options.size&&b.tail!==null&&!b.inTail)&&b.startAuto();b.buttons();b.notify("onAfterAnimation");if(b.options.wrap=="circular"&&b.options.size!==null)for(var c=b.prevFirst;c<=b.prevLast;c++)c!==null&&!(c>=b.first&&c<=b.last)&&(c<1||c>b.options.size)&&b.remove(c)}; this.notify("onBeforeAnimation");if(!this.options.animation||c===!1)this.list.css(this.lt,a+"px"),d();else{var f=!this.options.vertical?this.options.rtl?{right:a}:{left:a}:{top:a},d={duration:this.options.animation,easing:this.options.easing,complete:d};if(g.isFunction(this.options.animationStepCallback))d.step=this.options.animationStepCallback;this.list.animate(f,d)}}},startAuto:function(a){if(a!==void 0)this.options.auto=a;if(this.options.auto===0)return this.stopAuto();if(this.timer===null){this.autoStopped= !1;var c=this;this.timer=window.setTimeout(function(){c.next()},this.options.auto*1E3)}},stopAuto:function(){this.pauseAuto();this.autoStopped=!0},pauseAuto:function(){if(this.timer!==null)window.clearTimeout(this.timer),this.timer=null},buttons:function(a,c){if(a==null&&(a=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="first"||this.options.size===null||this.last<this.options.size),!this.locked&&(!this.options.wrap||this.options.wrap=="first")&&this.options.size!==null&& this.last>=this.options.size))a=this.tail!==null&&!this.inTail;if(c==null&&(c=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="last"||this.first>1),!this.locked&&(!this.options.wrap||this.options.wrap=="last")&&this.options.size!==null&&this.first==1))c=this.tail!==null&&this.inTail;var b=this;this.buttonNext.size()>0?(this.buttonNext.unbind(this.options.buttonNextEvent+".jcarousel",this.funcNext),a&&this.buttonNext.bind(this.options.buttonNextEvent+".jcarousel",this.funcNext), this.buttonNext[a?"removeClass":"addClass"](this.className("jcarousel-next-disabled")).attr("disabled",a?!1:!0),this.options.buttonNextCallback!==null&&this.buttonNext.data("jcarouselstate")!=a&&this.buttonNext.each(function(){b.options.buttonNextCallback(b,this,a)}).data("jcarouselstate",a)):this.options.buttonNextCallback!==null&&this.buttonNextState!=a&&this.options.buttonNextCallback(b,null,a);this.buttonPrev.size()>0?(this.buttonPrev.unbind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev), c&&this.buttonPrev.bind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev),this.buttonPrev[c?"removeClass":"addClass"](this.className("jcarousel-prev-disabled")).attr("disabled",c?!1:!0),this.options.buttonPrevCallback!==null&&this.buttonPrev.data("jcarouselstate")!=c&&this.buttonPrev.each(function(){b.options.buttonPrevCallback(b,this,c)}).data("jcarouselstate",c)):this.options.buttonPrevCallback!==null&&this.buttonPrevState!=c&&this.options.buttonPrevCallback(b,null,c);this.buttonNextState= a;this.buttonPrevState=c},notify:function(a){var c=this.prevFirst===null?"init":this.prevFirst<this.first?"next":"prev";this.callback("itemLoadCallback",a,c);this.prevFirst!==this.first&&(this.callback("itemFirstInCallback",a,c,this.first),this.callback("itemFirstOutCallback",a,c,this.prevFirst));this.prevLast!==this.last&&(this.callback("itemLastInCallback",a,c,this.last),this.callback("itemLastOutCallback",a,c,this.prevLast));this.callback("itemVisibleInCallback",a,c,this.first,this.last,this.prevFirst, this.prevLast);this.callback("itemVisibleOutCallback",a,c,this.prevFirst,this.prevLast,this.first,this.last)},callback:function(a,c,b,d,f,j,e){if(!(this.options[a]==null||typeof this.options[a]!="object"&&c!="onAfterAnimation")){var h=typeof this.options[a]=="object"?this.options[a][c]:this.options[a];if(g.isFunction(h)){var i=this;if(d===void 0)h(i,b,c);else if(f===void 0)this.get(d).each(function(){h(i,this,d,b,c)});else for(var a=function(a){i.get(a).each(function(){h(i,this,a,b,c)})},k=d;k<=f;k++)k!== null&&!(k>=j&&k<=e)&&a(k)}}},create:function(a){return this.format("<li></li>",a)},format:function(a,c){for(var a=g(a),b=a.get(0).className.split(" "),d=0;d<b.length;d++)b[d].indexOf("jcarousel-")!=-1&&a.removeClass(b[d]);a.addClass(this.className("jcarousel-item")).addClass(this.className("jcarousel-item-"+c)).css({"float":this.options.rtl?"right":"left","list-style":"none"}).attr("jcarouselindex",c);return a},className:function(a){return a+" "+a+(!this.options.vertical?"-horizontal":"-vertical")}, dimension:function(a,c){var b=g(a);if(c==null)return!this.options.vertical?b.outerWidth(!0)||f.intval(this.options.itemFallbackDimension):b.outerHeight(!0)||f.intval(this.options.itemFallbackDimension);else{var d=!this.options.vertical?c-f.intval(b.css("marginLeft"))-f.intval(b.css("marginRight")):c-f.intval(b.css("marginTop"))-f.intval(b.css("marginBottom"));g(b).css(this.wh,d+"px");return this.dimension(b)}},clipping:function(){return!this.options.vertical?this.clip[0].offsetWidth-f.intval(this.clip.css("borderLeftWidth"))- f.intval(this.clip.css("borderRightWidth")):this.clip[0].offsetHeight-f.intval(this.clip.css("borderTopWidth"))-f.intval(this.clip.css("borderBottomWidth"))},index:function(a,c){if(c==null)c=this.options.size;return Math.round(((a-1)/c-Math.floor((a-1)/c))*c)+1}});f.extend({defaults:function(a){return g.extend(q,a||{})},intval:function(a){a=parseInt(a,10);return isNaN(a)?0:a},windowLoaded:function(){m=!0}});g.fn.jcarousel=function(a){if(typeof a=="string"){var c=g(this).data("jcarousel"),b=Array.prototype.slice.call(arguments, 1);return c[a].apply(c,b)}else return this.each(function(){var b=g(this).data("jcarousel");b?(a&&g.extend(b.options,a),b.reload()):g(this).data("jcarousel",new f(this,a))})}})(jQuery);

/*
 * jQuery plugin: fieldSelection - v0.1.0 - last change: 2006-12-16
 * (c) 2006 Alex Brem <alex@0xab.cd> - http://blog.0xab.cd
 * 
 * NOTE: This version of the plugin has been heavily modified from its
 * original version here: https://github.com/localhost/jquery-fieldselection
 * for use with the Tagmate plugin here: https://github.com/pinterest/tagmate.
 * Some of the modifications (see setSelection()) were made specifically for
 * the Tagmate plugin, while others were made by an unknown author (authors?).
 *
 * 2011-10-28: Ryan Probasco <ryan@bitdrift.org> - added setSelection()
 */

(function() {
    var fieldSelection = {
        getSelection: function() {
            var e = this.jquery ? this[0] : this;
            
            return (
                /* mozilla / dom 3.0 */
                ('selectionStart' in e && function() {
                    var l = e.selectionEnd - e.selectionStart;
                    return {
                        start: e.selectionStart,
                        end: e.selectionEnd,
                        length: l,
                        text: e.value.substr(e.selectionStart, l)};
                })
                
                /* exploder */
                || (document.selection && function() {
                    e.focus();
                    
                    var r = document.selection.createRange();
                    if (r == null) {
                        return {
                            start: 0,
                            end: e.value.length,
                            length: 0};
                    }
                    
                    var re = e.createTextRange();
                    var rc = re.duplicate();
                    re.moveToBookmark(r.getBookmark());
                    rc.setEndPoint('EndToStart', re);
                    
                    // IE bug - it counts newline as 2 symbols when getting selection coordinates,
                    //  but counts it as one symbol when setting selection
                    var rcLen = rc.text.length,
                        i,
                        rcLenOut = rcLen;
                    for (i = 0; i < rcLen; i++) {
                        if (rc.text.charCodeAt(i) == 13) rcLenOut--;
                    }
                    var rLen = r.text.length,
                        rLenOut = rLen;
                    for (i = 0; i < rLen; i++) {
                        if (r.text.charCodeAt(i) == 13) rLenOut--;
                    }
                    
                    return {
                        start: rcLenOut,
                        end: rcLenOut + rLenOut,
                        length: rLenOut,
                        text: r.text};
                })
                
                /* browser not supported */
                || function() {
                    return {
                        start: 0,
                        end: e.value.length,
                        length: 0};
                }

            )();

        },
        
        // 
        // Adapted from http://stackoverflow.com/questions/401593/javascript-textarea-selection
        // 
        setSelection: function()
        {
            var e = this.jquery ? this[0] : this;
            var start_pos = arguments[0] || 0;
            var end_pos = arguments[1] || 0;

            return (
                //Mozilla and DOM 3.0
                ('selectionStart' in e && function() {
                    e.focus();
                    e.selectionStart = start_pos;
                    e.selectionEnd = end_pos;
                    return this;
                })

                //IE
                || (document.selection && function() {
                    e.focus();
                    var tr = e.createTextRange();
                
                    //Fix IE from counting the newline characters as two seperate characters
                    var stop_it = start_pos;
                    for (i=0; i < stop_it; i++) if( e.value[i].search(/[\r\n]/) != -1 ) start_pos = start_pos - .5;
                    stop_it = end_pos;
                    for (i=0; i < stop_it; i++) if( e.value[i].search(/[\r\n]/) != -1 ) end_pos = end_pos - .5;
                
                    tr.moveEnd('textedit',-1);
                    tr.moveStart('character',start_pos);
                    tr.moveEnd('character',end_pos - start_pos);
                    tr.select();

                    return this;
                })

                //Not supported
                || function() {
                    return this;
                }
            )();
        },
        
        replaceSelection: function() {
            var e = this.jquery ? this[0] : this;
            var text = arguments[0] || '';
            
            return (
                /* mozilla / dom 3.0 */
                ('selectionStart' in e && function() {
                    e.value = e.value.substr(0, e.selectionStart) + text + e.value.substr(e.selectionEnd, e.value.length);
                    return this;
                })
                
                /* exploder */
                || (document.selection && function() {
                    e.focus();
                    document.selection.createRange().text = text;
                    return this;
                })
                
                /* browser not supported */
                || function() {
                    e.value += text;
                    return this;
                }
            )();
        }
    };
    
    jQuery.each(fieldSelection, function(i) { jQuery.fn[i] = this; });

})();

/**
 * jQuery.ScrollTo
 * Copyright (c) 2007-2009 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * Date: 5/25/2009
 *
 * @projectDescription Easy element scrolling using jQuery.
 * http://flesler.blogspot.com/2007/10/jqueryscrollto.html
 * Works with jQuery +1.2.6. Tested on FF 2/3, IE 6/7/8, Opera 9.5/6, Safari 3, Chrome 1 on WinXP.
 *
 * @author Ariel Flesler
 * @version 1.4.2
 *
 * @id jQuery.scrollTo
 * @id jQuery.fn.scrollTo
 * @param {String, Number, DOMElement, jQuery, Object} target Where to scroll the matched elements.
 *	  The different options for target are:
 *		- A number position (will be applied to all axes).
 *		- A string position ('44', '100px', '+=90', etc ) will be applied to all axes
 *		- A jQuery/DOM element ( logically, child of the element to scroll )
 *		- A string selector, that will be relative to the element to scroll ( 'li:eq(2)', etc )
 *		- A hash { top:x, left:y }, x and y can be any kind of number/string like above.
*		- A percentage of the container's dimension/s, for example: 50% to go to the middle.
 *		- The string 'max' for go-to-end. 
 * @param {Number} duration The OVERALL length of the animation, this argument can be the settings object instead.
 * @param {Object,Function} settings Optional set of settings or the onAfter callback.
 *	 @option {String} axis Which axis must be scrolled, use 'x', 'y', 'xy' or 'yx'.
 *	 @option {Number} duration The OVERALL length of the animation.
 *	 @option {String} easing The easing method for the animation.
 *	 @option {Boolean} margin If true, the margin of the target element will be deducted from the final position.
 *	 @option {Object, Number} offset Add/deduct from the end position. One number for both axes or { top:x, left:y }.
 *	 @option {Object, Number} over Add/deduct the height/width multiplied by 'over', can be { top:x, left:y } when using both axes.
 *	 @option {Boolean} queue If true, and both axis are given, the 2nd axis will only be animated after the first one ends.
 *	 @option {Function} onAfter Function to be called after the scrolling ends. 
 *	 @option {Function} onAfterFirst If queuing is activated, this function will be called after the first scrolling ends.
 * @return {jQuery} Returns the same jQuery object, for chaining.
 *
 * @desc Scroll to a fixed position
 * @example $('div').scrollTo( 340 );
 *
 * @desc Scroll relatively to the actual position
 * @example $('div').scrollTo( '+=340px', { axis:'y' } );
 *
 * @dec Scroll using a selector (relative to the scrolled element)
 * @example $('div').scrollTo( 'p.paragraph:eq(2)', 500, { easing:'swing', queue:true, axis:'xy' } );
 *
 * @ Scroll to a DOM element (same for jQuery object)
 * @example var second_child = document.getElementById('container').firstChild.nextSibling;
 *			$('#container').scrollTo( second_child, { duration:500, axis:'x', onAfter:function(){
 *				alert('scrolled!!');																   
 *			}});
 *
 * @desc Scroll on both axes, to different values
 * @example $('div').scrollTo( { top: 300, left:'+=200' }, { axis:'xy', offset:-20 } );
 */
;(function( $ ){
	
	var $scrollTo = $.scrollTo = function( target, duration, settings ){
		$(window).scrollTo( target, duration, settings );
	};

	$scrollTo.defaults = {
		axis:'xy',
		duration: parseFloat($.fn.jquery) >= 1.3 ? 0 : 1
	};

	// Returns the element that needs to be animated to scroll the window.
	// Kept for backwards compatibility (specially for localScroll & serialScroll)
	$scrollTo.window = function( scope ){
		return $(window)._scrollable();
	};

	// Hack, hack, hack :)
	// Returns the real elements to scroll (supports window/iframes, documents and regular nodes)
	$.fn._scrollable = function(){
		return this.map(function(){
			var elem = this,
				isWin = !elem.nodeName || $.inArray( elem.nodeName.toLowerCase(), ['iframe','#document','html','body'] ) != -1;

				if( !isWin )
					return elem;

			var doc = (elem.contentWindow || elem).document || elem.ownerDocument || elem;
			
			return $.browser.safari || doc.compatMode == 'BackCompat' ?
				doc.body : 
				doc.documentElement;
		});
	};

	$.fn.scrollTo = function( target, duration, settings ){
		if( typeof duration == 'object' ){
			settings = duration;
			duration = 0;
		}
		if( typeof settings == 'function' )
			settings = { onAfter:settings };
			
		if( target == 'max' )
			target = 9e9;
			
		settings = $.extend( {}, $scrollTo.defaults, settings );
		// Speed is still recognized for backwards compatibility
		duration = duration || settings.speed || settings.duration;
		// Make sure the settings are given right
		settings.queue = settings.queue && settings.axis.length > 1;
		
		if( settings.queue )
			// Let's keep the overall duration
			duration /= 2;
		settings.offset = both( settings.offset );
		settings.over = both( settings.over );

		return this._scrollable().each(function(){
			var elem = this,
				$elem = $(elem),
				targ = target, toff, attr = {},
				win = $elem.is('html,body');

			switch( typeof targ ){
				// A number will pass the regex
				case 'number':
				case 'string':
					if( /^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(targ) ){
						targ = both( targ );
						// We are done
						break;
					}
					// Relative selector, no break!
					targ = $(targ,this);
				case 'object':
					// DOMElement / jQuery
					if( targ.is || targ.style )
						// Get the real position of the target 
						toff = (targ = $(targ)).offset();
			}
			$.each( settings.axis.split(''), function( i, axis ){
				var Pos	= axis == 'x' ? 'Left' : 'Top',
					pos = Pos.toLowerCase(),
					key = 'scroll' + Pos,
					old = elem[key],
					max = $scrollTo.max(elem, axis);

				if( toff ){// jQuery / DOMElement
					attr[key] = toff[pos] + ( win ? 0 : old - $elem.offset()[pos] );

					// If it's a dom element, reduce the margin
					if( settings.margin ){
						attr[key] -= parseInt(targ.css('margin'+Pos)) || 0;
						attr[key] -= parseInt(targ.css('border'+Pos+'Width')) || 0;
					}
					
					attr[key] += settings.offset[pos] || 0;
					
					if( settings.over[pos] )
						// Scroll to a fraction of its width/height
						attr[key] += targ[axis=='x'?'width':'height']() * settings.over[pos];
				}else{ 
					var val = targ[pos];
					// Handle percentage values
					attr[key] = val.slice && val.slice(-1) == '%' ? 
						parseFloat(val) / 100 * max
						: val;
				}

				// Number or 'number'
				if( /^\d+$/.test(attr[key]) )
					// Check the limits
					attr[key] = attr[key] <= 0 ? 0 : Math.min( attr[key], max );

				// Queueing axes
				if( !i && settings.queue ){
					// Don't waste time animating, if there's no need.
					if( old != attr[key] )
						// Intermediate animation
						animate( settings.onAfterFirst );
					// Don't animate this axis again in the next iteration.
					delete attr[key];
				}
			});

			animate( settings.onAfter );			

			function animate( callback ){
				$elem.animate( attr, duration, settings.easing, callback && function(){
					callback.call(this, target, settings);
				});
			};

		}).end();
	};
	
	// Max scrolling position, works on quirks mode
	// It only fails (not too badly) on IE, quirks mode.
	$scrollTo.max = function( elem, axis ){
		var Dim = axis == 'x' ? 'Width' : 'Height',
			scroll = 'scroll'+Dim;
		
		if( !$(elem).is('html,body') )
			return elem[scroll] - $(elem)[Dim.toLowerCase()]();
		
		var size = 'client' + Dim,
			html = elem.ownerDocument.documentElement,
			body = elem.ownerDocument.body;

		return Math.max( html[scroll], body[scroll] ) 
			 - Math.min( html[size]  , body[size]   );
			
	};

	function both( val ){
		return typeof val == 'object' ? val : { top:val, left:val };
	};

})( jQuery );
/**
 * jquery.tagmate.js
 * =================
 * Copyright (c) 2011 Cold Brew Labs Inc., http://coldbrewlabs.com
 * Licenced under MIT (see included LICENSE)
 *
 * Requirements
 * ------------
 * jquery.js (http://jquery.com)
 * jquery.scrollTo.js (http://demos.flesler.com/jquery/scrollTo/)
 * jquery.fieldselection.js - (included)
 */

//
// Global namespace stuff. These are provided as a convenience to plugin users.
//
var Tagmate = (function() { 
    var HASH_TAG_EXPR = "\\w+";
    var NAME_TAG_EXPR = "\\w+(?: \\w+)*"; // allow spaces
    var PRICE_TAG_EXPR = "(?:(?:\\d{1,3}(?:\\,\\d{3})+)|(?:\\d+))(?:\\.\\d{2})?";

    return {
        HASH_TAG_EXPR: HASH_TAG_EXPR,
        NAME_TAG_EXPR: NAME_TAG_EXPR,
        PRICE_TAG_EXPR: PRICE_TAG_EXPR,

        DEFAULT_EXPRS: {
            '@': NAME_TAG_EXPR,
            '#': HASH_TAG_EXPR,
            '$': PRICE_TAG_EXPR
        },

        // Remove options that don't match the filter.
        filterOptions: function(options, term) {
            var filtered = [];
            for (var i = 0; i < options.length; i++) {
                var label_lc = options[i].label.toLowerCase();
                var term_lc = term.toLowerCase();
                if (term_lc.length <= label_lc.length && label_lc.indexOf(term_lc) == 0)
                    filtered.push(options[i]);
            }
            return filtered;
        }
    };
})();

//
// jQuery plugin
//
(function($) {
    // Similar to indexOf() but uses RegExp.
    function regex_index_of(str, regex, startpos) {
        var indexOf = str.substring(startpos || 0).search(regex);
        return (indexOf >= 0) ? (indexOf + (startpos || 0)) : indexOf;
    }
    
    // Escape special RegExp chars.
    function regex_escape(text) {
        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    }

    // Parse tags from a textarea (internal).
    function parse_tags(textarea, exprs, sources) {        
        var tags = {};
        for (tok in exprs) {
            if (sources && sources[tok]) {
                // Favor sources to raw tags if available
                var matches = {}, indexes = {};
                for (key in sources[tok]) {
                    var value = sources[tok][key].value;
                    var label = sources[tok][key].label;
                    var tag = regex_escape(tok + label);
                    // This regexp is insane. \b won't work because we allow spaces.
                    var e = ["(?:^(",")$|^(",")\\W|\\W(",")\\W|\\W(",")$)"].join(tag);
                    var i = 0, re = new RegExp(e, "gm");
                    while ((i = regex_index_of(textarea.val(), re, i)) > -1) {
                        var p = indexes[i] ? indexes[i] : null;
                        // Favor longer matches
                        if (!p || matches[p].length < label.length)
                            indexes[i] = value;
                        matches[value] = label;
                        i += label.length + 1;
                    }
                }
                // Keep only longest matches
                for (i in indexes)
                    tags[tok + indexes[i]] = tok;
            } else {
                // Check for raw tags
                var m = null, re = new RegExp("([" + tok + "]" + exprs[tok] + ")", "gm");
                while (m = re.exec(textarea.val()))
                    tags[m[1]] = tok;
            }
        }

        // Keep only uniques
        var results = []
        for (tag in tags)
            results.push(tag);
        return results;
    }

    $.fn.extend({
        getTags: function(exprs, sources) {
            var textarea = $(this);
            exprs = exprs || textarea.data("_tagmate_exprs");
            sources = sources || textarea.data("_tagmate_sources");
            return parse_tags(textarea, exprs, sources);
        },
        tagmate: function(options) {
            var defaults = {
                exprs: Tagmate.DEFAULT_EXPRS,
                sources: null, // { '@': [{label:'foo',value:'bar'}] }
                capture_tag: null, // Callback fired when tags are captured. function(tag) {}
                replace_tag: null, // Callback fired when tag is replaced. function(tag, label) {}
                menu_class: "tagmate-menu",
                menu_option_class: "tagmate-menu-option",
                menu_option_active_class: "tagmate-menu-option-active",
                highlight_tags: false, // EXPERIMENTAL: enable at your own risk!
                highlight_class: 'tagmate-highlight'
            };

            // Get the previous position of tok starting at pos (or -1)
            function prev_tok(str, tok, pos) {
                var re = new RegExp("[" + tok + "]");
                for (; pos >= 0 && !re.test(str[pos]); pos--) {};
                return pos;
            }

            // Get tag value at current cursor position
            function parse_tag(textarea) {
                var text = textarea.val();
                var sel = textarea.getSelection();

                // Search left for closest matching tag token
                var m_pos = -1, m_tok = null;
                for (tok in defaults.exprs) {
                    var pos = prev_tok(text, tok, sel.start);
                    if (pos > m_pos) {
                        m_pos = pos;
                        m_tok = tok;
                    }
                }

                // Match from token to cursor
                var sub = text.substring(m_pos + 1, sel.start);

                // Look for raw matches
                var re = new RegExp("^[" + m_tok + "]" + defaults.exprs[m_tok]);
                if (re.exec(m_tok + sub))
                    return m_tok + sub

                return null;
            }

            // Replace the textarea query text with the suggestion
            function replace_tag(textarea, tag, value) {
                var text = textarea.val();

                // Replace occurrence at cursor position
                var sel = textarea.getSelection();
                var pos = prev_tok(text, tag[0], sel.start);
                var l = text.substr(0, pos);
                var r = text.substr(pos + tag.length);
                textarea.val(l + tag[0] + value + r);

                // Try to move cursor position at end of tag
                var sel_pos = pos + value.length + 1;
                textarea.setSelection(sel_pos, sel_pos);

                // Callback for tag replacement
                if (defaults.replace_tag)
                    defaults.replace_tag(tag, value);
            }

            // Show the menu of options
            function update_menu(menu, options) {
                // Sort results alphabetically
                options = options.sort(function(a, b) {
                    var a_lc = a.label.toLowerCase();
                    var b_lc = b.label.toLowerCase();
                    if (a_lc > b_lc)
                        return 1;
                    else if (a_lc < b_lc)
                        return -1;
                    return 0;
                });

                // Append results to menu
                for (var i = 0; i < options.length; i++) {
                    var label = options[i].label;
                    var value = options[i].value;
                    var image = options[i].image;
                    if (i == 0)
                        menu.html("");
                    var content = "<span>" + label + "</span>";
                    if (image)
                        content = "<img src='" + image + "' alt='" + label + "'/>" + content;
                    var classes = defaults.menu_option_class;
                    if (i == 0)
                        classes += " " + defaults.menu_option_active_class;
                    menu.append("<div class='" + classes + "'>" + content + "</div>");
                }
            }

            // Move up or down in the selection menu
            function scroll_menu(menu, direction) {
                var child_selector = direction == "down" ? ":first-child" : ":last-child";
                var sibling_func = direction == "down" ? "next" : "prev";
                var active = menu.children("." + defaults.menu_option_active_class);

                if (active.length == 0) {
                    active = menu.children(child_selector);
                    active.addClass(defaults.menu_option_active_class);
                } else {
                    active.removeClass(defaults.menu_option_active_class);
                    active = active[sibling_func]().length > 0 ? active[sibling_func]() : active;
                    active.addClass(defaults.menu_option_active_class);
                }

                // Scroll inside menu if necessary
                var i, options = menu.children();
                var n = Math.floor($(menu).height() / $(options[0]).height()) - 1;
                if ($(menu).height() % $(options[0]).height() > 0)
                    n -= 1; // don't scroll if bottom row is only partially visible 
                // Iterate to visible option
                for (i = 0; i < options.length && $(options[i]).html() != $(active).html(); i++) {};
                if (i > n && (i - n) >= 0 && (i - n) < options.length)
                    menu.scrollTo(options[i - n]);
            }

            // TODO: Fix this so that it works.
            function init_hiliter(textarea) {
                textarea.css("background", "transparent");

                var container = $(textarea).wrap("<div class='tagmate-container'/>");

                // Set up highlighter div
                var hiliter = $("<pre class='tagmate-hiliter'></pre>");
                hiliter.css("height", textarea.height() + "px");
                hiliter.css("width", textarea.width() + "px");
                //hiliter.css("border", "1px solid #FFF");
                hiliter.css("position","absolute");
                hiliter.css("top","0");
                hiliter.css("left","0");
                //hiliter.css("top", "-" + textarea.outerHeight() + "px");

                hiliter.css("font-family", textarea.css("font-family"));
                hiliter.css("font-size", textarea.css("font-size"));

                // Enable text wrapping in <pre>
                hiliter.css("white-space", "pre-wrap");
                hiliter.css("white-space", "-moz-pre-wrap !important");
                hiliter.css("white-space", "-pre-wrap");
                hiliter.css("white-space", "-o-pre-wrap");
                hiliter.css("word-wrap", "break-word");

                textarea.before(hiliter);
                //textarea.css("margin-top", "-" + textarea.outerHeight() + "px");

                return hiliter;
            }

            // TODO: Fix this so that it works.
            function update_hiliter(textarea, hiliter) {
                var html = textarea.val();
                var sources = textarea.data("_tagmate_sources");
                var tags = parse_tags(textarea, defaults.exprs, sources);

                for (var i = 0; i < tags.length; i++) {
                    var expr = tags[i], tok = tags[i][0], term = tags[i].substr(1);
                    if (sources && sources[tok]) {
                        for (var j = 0; j < sources[tok].length; j++) {
                            var option = sources[tok][j];
                            if (option.value == term) {
                                expr = tok + option.label;
                                break;
                            }
                        }
                    }

                    // Wrap tags in highlighter span
                    var re = new RegExp(regex_escape(expr), "g");
                    var span = "<span class='" + defaults.highlight_class + "'>" + expr + "</span>";
                    html = html.replace(re, span);
                }

                hiliter.html(html);
            }

            return this.each(function() {
                if (options)
                    $.extend(defaults, options);

                var textarea = $(this);

                // Optionally enable the hiliter
                var hiliter = null;
                if (defaults.highlight_tags)
                    hiliter = init_hiliter(textarea);

                textarea.data("_tagmate_exprs", defaults.exprs);

                // Initialize static lists of sources
                var sources_holder = {};
                for (var tok in defaults.sources)
                    sources_holder[tok] = [];
                textarea.data("_tagmate_sources", sources_holder);

                // Set up the menu
                var menu = $("<div class=" + defaults.menu_class + "></div>");
                textarea.after(menu);

                var pos = textarea.offset();
                menu.css("position", "absolute");
                menu.hide();

                // Activate menu and fire callbacks if cursor enters a tag
                function tag_check() {
                    menu.hide();

                    // Check for user tag
                    var tag = parse_tag(textarea);
                    if (tag) {
                        // Make sure cursor is within token
                        var tok = tag[0], term = tag.substr(1);
                        var sel = textarea.getSelection();
                        var pos = prev_tok(textarea.val(), tok, sel.start);
                        if ((sel.start - pos) <= tag.length) {
                            (function(done) {
                                if (typeof defaults.sources[tok] === 'object')
                                    done(Tagmate.filterOptions(defaults.sources[tok], term));
                                else if (typeof defaults.sources[tok] === 'function')
                                    defaults.sources[tok]({term:term}, done);
                                else if (typeof defaults.sources[tok] === 'string')
                                    $.getJSON(defaults.sources[tok], {term:term}, function(res) {
                                        done(res.options);
                                    });
                                else
                                    done();
                            })(function(options) {
                                if (options && options.length > 0) {
                                    // Update and show the menu
                                    update_menu(menu, options);
                                    menu.css("top", (textarea.outerHeight() - 1) + "px");
                                    menu.show();

                                    // Store for parse_tags()
                                    var _sources = textarea.data("_tagmate_sources");
                                    for (var i = 0; i < options.length; i++) {
                                        var found = false;
                                        for (var j = 0; !found && j < _sources[tok].length; j++)
                                            found = _sources[tok][j].value == options[i].value;
                                        if (!found)
                                            _sources[tok].push(options[i]);
                                    }
                                }

                                // Fire callback if available
                                if (tag && defaults.capture_tag)
                                    defaults.capture_tag(tag);
                            });
                        }
                    }
                }

                var ignore_keyup = false;

                // Check for tags on keyup, focus and click
                $(textarea)
                    .unbind('.tagmate')
                    .bind('focus.tagmate', function(e) {
                        tag_check();
                    })
                    .bind('blur.tagmate', function(e) {
                        // blur on textarea fires before mouse menu click
                        setTimeout(function() { menu.hide(); }, 300);
                    })
                    .bind('click.tagmate', function(e) {
                        tag_check();
                    })
                    .bind('keydown.tagmate', function(e) {
                        if (menu.is(":visible")) {
                            if (e.keyCode == 40) { // down
                                scroll_menu(menu, "down");
                                ignore_keyup = true;
                                return false;
                            } else if (e.keyCode == 38) { // up
                                scroll_menu(menu, "up");
                                ignore_keyup = true;
                                return false;
                            } else if (e.keyCode == 13) { // enter
                                var value = menu.children("." + defaults.menu_option_active_class).text();
                                var tag = parse_tag(textarea);
                                if (tag && value) {
                                    replace_tag(textarea, tag, value);
                                    menu.hide();
                                    ignore_keyup = true;
                                    return false;
                                }
                            } else if (e.keyCode == 27) { // escape
                                menu.hide();
                                ignore_keyup = true;
                                return false;
                            }
                        }
                    })
                    .bind('keyup.tagmate', function(e) {
                        if (ignore_keyup) {
                            ignore_keyup = false;
                            return true;
                        }
                        tag_check();

                        if (hiliter)
                            update_hiliter(textarea, hiliter);
                    });

                // Mouse menu activation
                //menu.find("." + defaults.menu_option_class) // Doesn't work
                $("." + defaults.menu_class + " ." + defaults.menu_option_class)
                    .die("click.tagmate")
                    .live("click.tagmate", function() {
                        var value = $(this).text();
                        var tag = parse_tag(textarea);
                        replace_tag(textarea, tag, value);
                        textarea.keyup();
                    });
            });
        }
    });
})(jQuery);
/*
 * Pointer Events Polyfill: Adds support for the style attribute "pointer-events: none" to browsers without this feature (namely, IE).
 * (c) 2013, Kent Mewhort, licensed under BSD. See LICENSE.txt for details.
 */

// constructor
function PointerEventsPolyfill(options){
    // set defaults
    this.options = {
        selector: '*',
        mouseEvents: ['click','dblclick','mousedown','mouseup'],
        usePolyfillIf: function(){
            if(navigator.appName == 'Microsoft Internet Explorer')
            {
                var agent = navigator.userAgent;
                if (agent.match(/MSIE ([0-9]{1,}[\.0-9]{0,})/) != null){
                    var version = parseFloat( RegExp.$1 );
                    if(version < 11)
                      return true;
                }
            }
            return false;
        }
    };
    if(options){
        var obj = this;
        $.each(options, function(k,v){
          obj.options[k] = v;
        });
    }

    if(this.options.usePolyfillIf())
      this.register_mouse_events();
}

// singleton initializer
PointerEventsPolyfill.initialize = function(options){
    if(PointerEventsPolyfill.singleton == null)
      PointerEventsPolyfill.singleton = new PointerEventsPolyfill(options);
    return PointerEventsPolyfill.singleton;
}

// handle mouse events w/ support for pointer-events: none
PointerEventsPolyfill.prototype.register_mouse_events = function(){
    // register on all elements (and all future elements) matching the selector
    $(document).on(this.options.mouseEvents.join(" "), this.options.selector, function(e){
       if($(this).css('pointer-events') == 'none'){
             // peak at the element below
             var origDisplayAttribute = $(this).css('display');
             $(this).css('display','none');

             var underneathElem = document.elementFromPoint(e.clientX, e.clientY);

            if(origDisplayAttribute)
                $(this)
                    .css('display', origDisplayAttribute);
            else
                $(this).css('display','');

             // fire the mouse event on the element below
            e.target = underneathElem;
            $(underneathElem).trigger(e);

            return false;
        }
        return true;
    });
}
/*
Uploadify v3.2
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php>

SWFUpload: http://www.swfupload.org, http://swfupload.googlecode.com
mmSWFUpload 1.0: Flash upload dialog - http://profandesign.se/swfupload/,  http://www.vinterwebb.se/
SWFUpload is (c) 2006-2007 Lars Huring, Olov Nilzén and Mammon Media and is released under the MIT License:
http://www.opensource.org/licenses/mit-license.php
SWFUpload 2 is (c) 2007-2008 Jake Roberts and is released under the MIT License:
http://www.opensource.org/licenses/mit-license.php

SWFObject v2.2 <http://code.google.com/p/swfobject/> 
is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
*/
;var swfobject=function(){var aq="undefined",aD="object",ab="Shockwave Flash",X="ShockwaveFlash.ShockwaveFlash",aE="application/x-shockwave-flash",ac="SWFObjectExprInst",ax="onreadystatechange",af=window,aL=document,aB=navigator,aa=false,Z=[aN],aG=[],ag=[],al=[],aJ,ad,ap,at,ak=false,aU=false,aH,an,aI=true,ah=function(){var a=typeof aL.getElementById!=aq&&typeof aL.getElementsByTagName!=aq&&typeof aL.createElement!=aq,e=aB.userAgent.toLowerCase(),c=aB.platform.toLowerCase(),h=c?/win/.test(c):/win/.test(e),j=c?/mac/.test(c):/mac/.test(e),g=/webkit/.test(e)?parseFloat(e.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,d=!+"\v1",f=[0,0,0],k=null;if(typeof aB.plugins!=aq&&typeof aB.plugins[ab]==aD){k=aB.plugins[ab].description;if(k&&!(typeof aB.mimeTypes!=aq&&aB.mimeTypes[aE]&&!aB.mimeTypes[aE].enabledPlugin)){aa=true;d=false;k=k.replace(/^.*\s+(\S+\s+\S+$)/,"$1");f[0]=parseInt(k.replace(/^(.*)\..*$/,"$1"),10);f[1]=parseInt(k.replace(/^.*\.(.*)\s.*$/,"$1"),10);f[2]=/[a-zA-Z]/.test(k)?parseInt(k.replace(/^.*[a-zA-Z]+(.*)$/,"$1"),10):0;}}else{if(typeof af.ActiveXObject!=aq){try{var i=new ActiveXObject(X);if(i){k=i.GetVariable("$version");if(k){d=true;k=k.split(" ")[1].split(",");f=[parseInt(k[0],10),parseInt(k[1],10),parseInt(k[2],10)];}}}catch(b){}}}return{w3:a,pv:f,wk:g,ie:d,win:h,mac:j};}(),aK=function(){if(!ah.w3){return;}if((typeof aL.readyState!=aq&&aL.readyState=="complete")||(typeof aL.readyState==aq&&(aL.getElementsByTagName("body")[0]||aL.body))){aP();}if(!ak){if(typeof aL.addEventListener!=aq){aL.addEventListener("DOMContentLoaded",aP,false);}if(ah.ie&&ah.win){aL.attachEvent(ax,function(){if(aL.readyState=="complete"){aL.detachEvent(ax,arguments.callee);aP();}});if(af==top){(function(){if(ak){return;}try{aL.documentElement.doScroll("left");}catch(a){setTimeout(arguments.callee,0);return;}aP();})();}}if(ah.wk){(function(){if(ak){return;}if(!/loaded|complete/.test(aL.readyState)){setTimeout(arguments.callee,0);return;}aP();})();}aC(aP);}}();function aP(){if(ak){return;}try{var b=aL.getElementsByTagName("body")[0].appendChild(ar("span"));b.parentNode.removeChild(b);}catch(a){return;}ak=true;var d=Z.length;for(var c=0;c<d;c++){Z[c]();}}function aj(a){if(ak){a();}else{Z[Z.length]=a;}}function aC(a){if(typeof af.addEventListener!=aq){af.addEventListener("load",a,false);}else{if(typeof aL.addEventListener!=aq){aL.addEventListener("load",a,false);}else{if(typeof af.attachEvent!=aq){aM(af,"onload",a);}else{if(typeof af.onload=="function"){var b=af.onload;af.onload=function(){b();a();};}else{af.onload=a;}}}}}function aN(){if(aa){Y();}else{am();}}function Y(){var d=aL.getElementsByTagName("body")[0];var b=ar(aD);b.setAttribute("type",aE);var a=d.appendChild(b);if(a){var c=0;(function(){if(typeof a.GetVariable!=aq){var e=a.GetVariable("$version");if(e){e=e.split(" ")[1].split(",");ah.pv=[parseInt(e[0],10),parseInt(e[1],10),parseInt(e[2],10)];}}else{if(c<10){c++;setTimeout(arguments.callee,10);return;}}d.removeChild(b);a=null;am();})();}else{am();}}function am(){var g=aG.length;if(g>0){for(var h=0;h<g;h++){var c=aG[h].id;var l=aG[h].callbackFn;var a={success:false,id:c};if(ah.pv[0]>0){var i=aS(c);if(i){if(ao(aG[h].swfVersion)&&!(ah.wk&&ah.wk<312)){ay(c,true);if(l){a.success=true;a.ref=av(c);l(a);}}else{if(aG[h].expressInstall&&au()){var e={};e.data=aG[h].expressInstall;e.width=i.getAttribute("width")||"0";e.height=i.getAttribute("height")||"0";if(i.getAttribute("class")){e.styleclass=i.getAttribute("class");}if(i.getAttribute("align")){e.align=i.getAttribute("align");}var f={};var d=i.getElementsByTagName("param");var k=d.length;for(var j=0;j<k;j++){if(d[j].getAttribute("name").toLowerCase()!="movie"){f[d[j].getAttribute("name")]=d[j].getAttribute("value");}}ae(e,f,c,l);}else{aF(i);if(l){l(a);}}}}}else{ay(c,true);if(l){var b=av(c);if(b&&typeof b.SetVariable!=aq){a.success=true;a.ref=b;}l(a);}}}}}function av(b){var d=null;var c=aS(b);if(c&&c.nodeName=="OBJECT"){if(typeof c.SetVariable!=aq){d=c;}else{var a=c.getElementsByTagName(aD)[0];if(a){d=a;}}}return d;}function au(){return !aU&&ao("6.0.65")&&(ah.win||ah.mac)&&!(ah.wk&&ah.wk<312);}function ae(f,d,h,e){aU=true;ap=e||null;at={success:false,id:h};var a=aS(h);if(a){if(a.nodeName=="OBJECT"){aJ=aO(a);ad=null;}else{aJ=a;ad=h;}f.id=ac;if(typeof f.width==aq||(!/%$/.test(f.width)&&parseInt(f.width,10)<310)){f.width="310";}if(typeof f.height==aq||(!/%$/.test(f.height)&&parseInt(f.height,10)<137)){f.height="137";}aL.title=aL.title.slice(0,47)+" - Flash Player Installation";var b=ah.ie&&ah.win?"ActiveX":"PlugIn",c="MMredirectURL="+af.location.toString().replace(/&/g,"%26")+"&MMplayerType="+b+"&MMdoctitle="+aL.title;if(typeof d.flashvars!=aq){d.flashvars+="&"+c;}else{d.flashvars=c;}if(ah.ie&&ah.win&&a.readyState!=4){var g=ar("div");h+="SWFObjectNew";g.setAttribute("id",h);a.parentNode.insertBefore(g,a);a.style.display="none";(function(){if(a.readyState==4){a.parentNode.removeChild(a);}else{setTimeout(arguments.callee,10);}})();}aA(f,d,h);}}function aF(a){if(ah.ie&&ah.win&&a.readyState!=4){var b=ar("div");a.parentNode.insertBefore(b,a);b.parentNode.replaceChild(aO(a),b);a.style.display="none";(function(){if(a.readyState==4){a.parentNode.removeChild(a);}else{setTimeout(arguments.callee,10);}})();}else{a.parentNode.replaceChild(aO(a),a);}}function aO(b){var d=ar("div");if(ah.win&&ah.ie){d.innerHTML=b.innerHTML;}else{var e=b.getElementsByTagName(aD)[0];if(e){var a=e.childNodes;if(a){var f=a.length;for(var c=0;c<f;c++){if(!(a[c].nodeType==1&&a[c].nodeName=="PARAM")&&!(a[c].nodeType==8)){d.appendChild(a[c].cloneNode(true));}}}}}return d;}function aA(e,g,c){var d,a=aS(c);if(ah.wk&&ah.wk<312){return d;}if(a){if(typeof e.id==aq){e.id=c;}if(ah.ie&&ah.win){var f="";for(var i in e){if(e[i]!=Object.prototype[i]){if(i.toLowerCase()=="data"){g.movie=e[i];}else{if(i.toLowerCase()=="styleclass"){f+=' class="'+e[i]+'"';}else{if(i.toLowerCase()!="classid"){f+=" "+i+'="'+e[i]+'"';}}}}}var h="";for(var j in g){if(g[j]!=Object.prototype[j]){h+='<param name="'+j+'" value="'+g[j]+'" />';}}a.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+f+">"+h+"</object>";ag[ag.length]=e.id;d=aS(e.id);}else{var b=ar(aD);b.setAttribute("type",aE);for(var k in e){if(e[k]!=Object.prototype[k]){if(k.toLowerCase()=="styleclass"){b.setAttribute("class",e[k]);}else{if(k.toLowerCase()!="classid"){b.setAttribute(k,e[k]);}}}}for(var l in g){if(g[l]!=Object.prototype[l]&&l.toLowerCase()!="movie"){aQ(b,l,g[l]);}}a.parentNode.replaceChild(b,a);d=b;}}return d;}function aQ(b,d,c){var a=ar("param");a.setAttribute("name",d);a.setAttribute("value",c);b.appendChild(a);}function aw(a){var b=aS(a);if(b&&b.nodeName=="OBJECT"){if(ah.ie&&ah.win){b.style.display="none";(function(){if(b.readyState==4){aT(a);}else{setTimeout(arguments.callee,10);}})();}else{b.parentNode.removeChild(b);}}}function aT(a){var b=aS(a);if(b){for(var c in b){if(typeof b[c]=="function"){b[c]=null;}}b.parentNode.removeChild(b);}}function aS(a){var c=null;try{c=aL.getElementById(a);}catch(b){}return c;}function ar(a){return aL.createElement(a);}function aM(a,c,b){a.attachEvent(c,b);al[al.length]=[a,c,b];}function ao(a){var b=ah.pv,c=a.split(".");c[0]=parseInt(c[0],10);c[1]=parseInt(c[1],10)||0;c[2]=parseInt(c[2],10)||0;return(b[0]>c[0]||(b[0]==c[0]&&b[1]>c[1])||(b[0]==c[0]&&b[1]==c[1]&&b[2]>=c[2]))?true:false;}function az(b,f,a,c){if(ah.ie&&ah.mac){return;}var e=aL.getElementsByTagName("head")[0];if(!e){return;}var g=(a&&typeof a=="string")?a:"screen";if(c){aH=null;an=null;}if(!aH||an!=g){var d=ar("style");d.setAttribute("type","text/css");d.setAttribute("media",g);aH=e.appendChild(d);if(ah.ie&&ah.win&&typeof aL.styleSheets!=aq&&aL.styleSheets.length>0){aH=aL.styleSheets[aL.styleSheets.length-1];}an=g;}if(ah.ie&&ah.win){if(aH&&typeof aH.addRule==aD){aH.addRule(b,f);}}else{if(aH&&typeof aL.createTextNode!=aq){aH.appendChild(aL.createTextNode(b+" {"+f+"}"));}}}function ay(a,c){if(!aI){return;}var b=c?"visible":"hidden";if(ak&&aS(a)){aS(a).style.visibility=b;}else{az("#"+a,"visibility:"+b);}}function ai(b){var a=/[\\\"<>\.;]/;var c=a.exec(b)!=null;return c&&typeof encodeURIComponent!=aq?encodeURIComponent(b):b;}var aR=function(){if(ah.ie&&ah.win){window.attachEvent("onunload",function(){var a=al.length;for(var b=0;b<a;b++){al[b][0].detachEvent(al[b][1],al[b][2]);}var d=ag.length;for(var c=0;c<d;c++){aw(ag[c]);}for(var e in ah){ah[e]=null;}ah=null;for(var f in swfobject){swfobject[f]=null;}swfobject=null;});}}();return{registerObject:function(a,e,c,b){if(ah.w3&&a&&e){var d={};d.id=a;d.swfVersion=e;d.expressInstall=c;d.callbackFn=b;aG[aG.length]=d;ay(a,false);}else{if(b){b({success:false,id:a});}}},getObjectById:function(a){if(ah.w3){return av(a);}},embedSWF:function(k,e,h,f,c,a,b,i,g,j){var d={success:false,id:e};if(ah.w3&&!(ah.wk&&ah.wk<312)&&k&&e&&h&&f&&c){ay(e,false);aj(function(){h+="";f+="";var q={};if(g&&typeof g===aD){for(var o in g){q[o]=g[o];}}q.data=k;q.width=h;q.height=f;var n={};if(i&&typeof i===aD){for(var p in i){n[p]=i[p];}}if(b&&typeof b===aD){for(var l in b){if(typeof n.flashvars!=aq){n.flashvars+="&"+l+"="+b[l];}else{n.flashvars=l+"="+b[l];}}}if(ao(c)){var m=aA(q,n,e);if(q.id==e){ay(e,true);}d.success=true;d.ref=m;}else{if(a&&au()){q.data=a;ae(q,n,e,j);return;}else{ay(e,true);}}if(j){j(d);}});}else{if(j){j(d);}}},switchOffAutoHideShow:function(){aI=false;},ua:ah,getFlashPlayerVersion:function(){return{major:ah.pv[0],minor:ah.pv[1],release:ah.pv[2]};},hasFlashPlayerVersion:ao,createSWF:function(a,b,c){if(ah.w3){return aA(a,b,c);}else{return undefined;}},showExpressInstall:function(b,a,d,c){if(ah.w3&&au()){ae(b,a,d,c);}},removeSWF:function(a){if(ah.w3){aw(a);}},createCSS:function(b,a,c,d){if(ah.w3){az(b,a,c,d);}},addDomLoadEvent:aj,addLoadEvent:aC,getQueryParamValue:function(b){var a=aL.location.search||aL.location.hash;if(a){if(/\?/.test(a)){a=a.split("?")[1];}if(b==null){return ai(a);}var c=a.split("&");for(var d=0;d<c.length;d++){if(c[d].substring(0,c[d].indexOf("="))==b){return ai(c[d].substring((c[d].indexOf("=")+1)));}}}return"";},expressInstallCallback:function(){if(aU){var a=aS(ac);if(a&&aJ){a.parentNode.replaceChild(aJ,a);if(ad){ay(ad,true);if(ah.ie&&ah.win){aJ.style.display="block";}}if(ap){ap(at);}}aU=false;}}};}();var SWFUpload;if(SWFUpload==undefined){SWFUpload=function(b){this.initSWFUpload(b);};}SWFUpload.prototype.initSWFUpload=function(c){try{this.customSettings={};this.settings=c;this.eventQueue=[];this.movieName="SWFUpload_"+SWFUpload.movieCount++;this.movieElement=null;SWFUpload.instances[this.movieName]=this;this.initSettings();this.loadFlash();this.displayDebugInfo();}catch(d){delete SWFUpload.instances[this.movieName];throw d;}};SWFUpload.instances={};SWFUpload.movieCount=0;SWFUpload.version="2.2.0 2009-03-25";SWFUpload.QUEUE_ERROR={QUEUE_LIMIT_EXCEEDED:-100,FILE_EXCEEDS_SIZE_LIMIT:-110,ZERO_BYTE_FILE:-120,INVALID_FILETYPE:-130};SWFUpload.UPLOAD_ERROR={HTTP_ERROR:-200,MISSING_UPLOAD_URL:-210,IO_ERROR:-220,SECURITY_ERROR:-230,UPLOAD_LIMIT_EXCEEDED:-240,UPLOAD_FAILED:-250,SPECIFIED_FILE_ID_NOT_FOUND:-260,FILE_VALIDATION_FAILED:-270,FILE_CANCELLED:-280,UPLOAD_STOPPED:-290};SWFUpload.FILE_STATUS={QUEUED:-1,IN_PROGRESS:-2,ERROR:-3,COMPLETE:-4,CANCELLED:-5};SWFUpload.BUTTON_ACTION={SELECT_FILE:-100,SELECT_FILES:-110,START_UPLOAD:-120};SWFUpload.CURSOR={ARROW:-1,HAND:-2};SWFUpload.WINDOW_MODE={WINDOW:"window",TRANSPARENT:"transparent",OPAQUE:"opaque"};SWFUpload.completeURL=function(e){if(typeof(e)!=="string"||e.match(/^https?:\/\//i)||e.match(/^\//)){return e;}var f=window.location.protocol+"//"+window.location.hostname+(window.location.port?":"+window.location.port:"");var d=window.location.pathname.lastIndexOf("/");if(d<=0){path="/";}else{path=window.location.pathname.substr(0,d)+"/";}return path+e;};SWFUpload.prototype.initSettings=function(){this.ensureDefault=function(c,d){this.settings[c]=(this.settings[c]==undefined)?d:this.settings[c];};this.ensureDefault("upload_url","");this.ensureDefault("preserve_relative_urls",false);this.ensureDefault("file_post_name","Filedata");this.ensureDefault("post_params",{});this.ensureDefault("use_query_string",false);this.ensureDefault("requeue_on_error",false);this.ensureDefault("http_success",[]);this.ensureDefault("assume_success_timeout",0);this.ensureDefault("file_types","*.*");this.ensureDefault("file_types_description","All Files");this.ensureDefault("file_size_limit",0);this.ensureDefault("file_upload_limit",0);this.ensureDefault("file_queue_limit",0);this.ensureDefault("flash_url","swfupload.swf");this.ensureDefault("prevent_swf_caching",true);this.ensureDefault("button_image_url","");this.ensureDefault("button_width",1);this.ensureDefault("button_height",1);this.ensureDefault("button_text","");this.ensureDefault("button_text_style","color: #000000; font-size: 16pt;");this.ensureDefault("button_text_top_padding",0);this.ensureDefault("button_text_left_padding",0);this.ensureDefault("button_action",SWFUpload.BUTTON_ACTION.SELECT_FILES);this.ensureDefault("button_disabled",false);this.ensureDefault("button_placeholder_id","");this.ensureDefault("button_placeholder",null);this.ensureDefault("button_cursor",SWFUpload.CURSOR.ARROW);this.ensureDefault("button_window_mode",SWFUpload.WINDOW_MODE.WINDOW);this.ensureDefault("debug",false);this.settings.debug_enabled=this.settings.debug;this.settings.return_upload_start_handler=this.returnUploadStart;this.ensureDefault("swfupload_loaded_handler",null);this.ensureDefault("file_dialog_start_handler",null);this.ensureDefault("file_queued_handler",null);this.ensureDefault("file_queue_error_handler",null);this.ensureDefault("file_dialog_complete_handler",null);this.ensureDefault("upload_start_handler",null);this.ensureDefault("upload_progress_handler",null);this.ensureDefault("upload_error_handler",null);this.ensureDefault("upload_success_handler",null);this.ensureDefault("upload_complete_handler",null);this.ensureDefault("debug_handler",this.debugMessage);this.ensureDefault("custom_settings",{});this.customSettings=this.settings.custom_settings;if(!!this.settings.prevent_swf_caching){this.settings.flash_url=this.settings.flash_url+(this.settings.flash_url.indexOf("?")<0?"?":"&")+"preventswfcaching="+new Date().getTime();}if(!this.settings.preserve_relative_urls){this.settings.upload_url=SWFUpload.completeURL(this.settings.upload_url);this.settings.button_image_url=SWFUpload.completeURL(this.settings.button_image_url);}delete this.ensureDefault;};SWFUpload.prototype.loadFlash=function(){var d,c;if(document.getElementById(this.movieName)!==null){throw"ID "+this.movieName+" is already in use. The Flash Object could not be added";}d=document.getElementById(this.settings.button_placeholder_id)||this.settings.button_placeholder;if(d==undefined){throw"Could not find the placeholder element: "+this.settings.button_placeholder_id;}c=document.createElement("div");c.innerHTML=this.getFlashHTML();d.parentNode.replaceChild(c.firstChild,d);if(window[this.movieName]==undefined){window[this.movieName]=this.getMovieElement();}};SWFUpload.prototype.getFlashHTML=function(){return['<object id="',this.movieName,'" type="application/x-shockwave-flash" data="',this.settings.flash_url,'" width="',this.settings.button_width,'" height="',this.settings.button_height,'" class="swfupload">','<param name="wmode" value="',this.settings.button_window_mode,'" />','<param name="movie" value="',this.settings.flash_url,'" />','<param name="quality" value="high" />','<param name="menu" value="false" />','<param name="allowScriptAccess" value="always" />','<param name="flashvars" value="'+this.getFlashVars()+'" />',"</object>"].join("");};SWFUpload.prototype.getFlashVars=function(){var c=this.buildParamString();var d=this.settings.http_success.join(",");return["movieName=",encodeURIComponent(this.movieName),"&amp;uploadURL=",encodeURIComponent(this.settings.upload_url),"&amp;useQueryString=",encodeURIComponent(this.settings.use_query_string),"&amp;requeueOnError=",encodeURIComponent(this.settings.requeue_on_error),"&amp;httpSuccess=",encodeURIComponent(d),"&amp;assumeSuccessTimeout=",encodeURIComponent(this.settings.assume_success_timeout),"&amp;params=",encodeURIComponent(c),"&amp;filePostName=",encodeURIComponent(this.settings.file_post_name),"&amp;fileTypes=",encodeURIComponent(this.settings.file_types),"&amp;fileTypesDescription=",encodeURIComponent(this.settings.file_types_description),"&amp;fileSizeLimit=",encodeURIComponent(this.settings.file_size_limit),"&amp;fileUploadLimit=",encodeURIComponent(this.settings.file_upload_limit),"&amp;fileQueueLimit=",encodeURIComponent(this.settings.file_queue_limit),"&amp;debugEnabled=",encodeURIComponent(this.settings.debug_enabled),"&amp;buttonImageURL=",encodeURIComponent(this.settings.button_image_url),"&amp;buttonWidth=",encodeURIComponent(this.settings.button_width),"&amp;buttonHeight=",encodeURIComponent(this.settings.button_height),"&amp;buttonText=",encodeURIComponent(this.settings.button_text),"&amp;buttonTextTopPadding=",encodeURIComponent(this.settings.button_text_top_padding),"&amp;buttonTextLeftPadding=",encodeURIComponent(this.settings.button_text_left_padding),"&amp;buttonTextStyle=",encodeURIComponent(this.settings.button_text_style),"&amp;buttonAction=",encodeURIComponent(this.settings.button_action),"&amp;buttonDisabled=",encodeURIComponent(this.settings.button_disabled),"&amp;buttonCursor=",encodeURIComponent(this.settings.button_cursor)].join("");};SWFUpload.prototype.getMovieElement=function(){if(this.movieElement==undefined){this.movieElement=document.getElementById(this.movieName);}if(this.movieElement===null){throw"Could not find Flash element";}return this.movieElement;};SWFUpload.prototype.buildParamString=function(){var f=this.settings.post_params;var d=[];if(typeof(f)==="object"){for(var e in f){if(f.hasOwnProperty(e)){d.push(encodeURIComponent(e.toString())+"="+encodeURIComponent(f[e].toString()));}}}return d.join("&amp;");};SWFUpload.prototype.destroy=function(){try{this.cancelUpload(null,false);var g=null;g=this.getMovieElement();if(g&&typeof(g.CallFunction)==="unknown"){for(var j in g){try{if(typeof(g[j])==="function"){g[j]=null;}}catch(h){}}try{g.parentNode.removeChild(g);}catch(f){}}window[this.movieName]=null;SWFUpload.instances[this.movieName]=null;delete SWFUpload.instances[this.movieName];this.movieElement=null;this.settings=null;this.customSettings=null;this.eventQueue=null;this.movieName=null;return true;}catch(i){return false;}};SWFUpload.prototype.displayDebugInfo=function(){this.debug(["---SWFUpload Instance Info---\n","Version: ",SWFUpload.version,"\n","Movie Name: ",this.movieName,"\n","Settings:\n","\t","upload_url:               ",this.settings.upload_url,"\n","\t","flash_url:                ",this.settings.flash_url,"\n","\t","use_query_string:         ",this.settings.use_query_string.toString(),"\n","\t","requeue_on_error:         ",this.settings.requeue_on_error.toString(),"\n","\t","http_success:             ",this.settings.http_success.join(", "),"\n","\t","assume_success_timeout:   ",this.settings.assume_success_timeout,"\n","\t","file_post_name:           ",this.settings.file_post_name,"\n","\t","post_params:              ",this.settings.post_params.toString(),"\n","\t","file_types:               ",this.settings.file_types,"\n","\t","file_types_description:   ",this.settings.file_types_description,"\n","\t","file_size_limit:          ",this.settings.file_size_limit,"\n","\t","file_upload_limit:        ",this.settings.file_upload_limit,"\n","\t","file_queue_limit:         ",this.settings.file_queue_limit,"\n","\t","debug:                    ",this.settings.debug.toString(),"\n","\t","prevent_swf_caching:      ",this.settings.prevent_swf_caching.toString(),"\n","\t","button_placeholder_id:    ",this.settings.button_placeholder_id.toString(),"\n","\t","button_placeholder:       ",(this.settings.button_placeholder?"Set":"Not Set"),"\n","\t","button_image_url:         ",this.settings.button_image_url.toString(),"\n","\t","button_width:             ",this.settings.button_width.toString(),"\n","\t","button_height:            ",this.settings.button_height.toString(),"\n","\t","button_text:              ",this.settings.button_text.toString(),"\n","\t","button_text_style:        ",this.settings.button_text_style.toString(),"\n","\t","button_text_top_padding:  ",this.settings.button_text_top_padding.toString(),"\n","\t","button_text_left_padding: ",this.settings.button_text_left_padding.toString(),"\n","\t","button_action:            ",this.settings.button_action.toString(),"\n","\t","button_disabled:          ",this.settings.button_disabled.toString(),"\n","\t","custom_settings:          ",this.settings.custom_settings.toString(),"\n","Event Handlers:\n","\t","swfupload_loaded_handler assigned:  ",(typeof this.settings.swfupload_loaded_handler==="function").toString(),"\n","\t","file_dialog_start_handler assigned: ",(typeof this.settings.file_dialog_start_handler==="function").toString(),"\n","\t","file_queued_handler assigned:       ",(typeof this.settings.file_queued_handler==="function").toString(),"\n","\t","file_queue_error_handler assigned:  ",(typeof this.settings.file_queue_error_handler==="function").toString(),"\n","\t","upload_start_handler assigned:      ",(typeof this.settings.upload_start_handler==="function").toString(),"\n","\t","upload_progress_handler assigned:   ",(typeof this.settings.upload_progress_handler==="function").toString(),"\n","\t","upload_error_handler assigned:      ",(typeof this.settings.upload_error_handler==="function").toString(),"\n","\t","upload_success_handler assigned:    ",(typeof this.settings.upload_success_handler==="function").toString(),"\n","\t","upload_complete_handler assigned:   ",(typeof this.settings.upload_complete_handler==="function").toString(),"\n","\t","debug_handler assigned:             ",(typeof this.settings.debug_handler==="function").toString(),"\n"].join(""));};SWFUpload.prototype.addSetting=function(d,f,e){if(f==undefined){return(this.settings[d]=e);}else{return(this.settings[d]=f);}};SWFUpload.prototype.getSetting=function(b){if(this.settings[b]!=undefined){return this.settings[b];}return"";};SWFUpload.prototype.callFlash=function(functionName,argumentArray){argumentArray=argumentArray||[];var movieElement=this.getMovieElement();var returnValue,returnString;try{returnString=movieElement.CallFunction('<invoke name="'+functionName+'" returntype="javascript">'+__flash__argumentsToXML(argumentArray,0)+"</invoke>");returnValue=eval(returnString);}catch(ex){throw"Call to "+functionName+" failed";}if(returnValue!=undefined&&typeof returnValue.post==="object"){returnValue=this.unescapeFilePostParams(returnValue);}return returnValue;};SWFUpload.prototype.selectFile=function(){this.callFlash("SelectFile");};SWFUpload.prototype.selectFiles=function(){this.callFlash("SelectFiles");};SWFUpload.prototype.startUpload=function(b){this.callFlash("StartUpload",[b]);};SWFUpload.prototype.cancelUpload=function(d,c){if(c!==false){c=true;}this.callFlash("CancelUpload",[d,c]);};SWFUpload.prototype.stopUpload=function(){this.callFlash("StopUpload");};SWFUpload.prototype.getStats=function(){return this.callFlash("GetStats");};SWFUpload.prototype.setStats=function(b){this.callFlash("SetStats",[b]);};SWFUpload.prototype.getFile=function(b){if(typeof(b)==="number"){return this.callFlash("GetFileByIndex",[b]);}else{return this.callFlash("GetFile",[b]);}};SWFUpload.prototype.addFileParam=function(e,d,f){return this.callFlash("AddFileParam",[e,d,f]);};SWFUpload.prototype.removeFileParam=function(d,c){this.callFlash("RemoveFileParam",[d,c]);};SWFUpload.prototype.setUploadURL=function(b){this.settings.upload_url=b.toString();this.callFlash("SetUploadURL",[b]);};SWFUpload.prototype.setPostParams=function(b){this.settings.post_params=b;this.callFlash("SetPostParams",[b]);};SWFUpload.prototype.addPostParam=function(d,c){this.settings.post_params[d]=c;this.callFlash("SetPostParams",[this.settings.post_params]);};SWFUpload.prototype.removePostParam=function(b){delete this.settings.post_params[b];this.callFlash("SetPostParams",[this.settings.post_params]);};SWFUpload.prototype.setFileTypes=function(d,c){this.settings.file_types=d;this.settings.file_types_description=c;this.callFlash("SetFileTypes",[d,c]);};SWFUpload.prototype.setFileSizeLimit=function(b){this.settings.file_size_limit=b;this.callFlash("SetFileSizeLimit",[b]);};SWFUpload.prototype.setFileUploadLimit=function(b){this.settings.file_upload_limit=b;this.callFlash("SetFileUploadLimit",[b]);};SWFUpload.prototype.setFileQueueLimit=function(b){this.settings.file_queue_limit=b;this.callFlash("SetFileQueueLimit",[b]);};SWFUpload.prototype.setFilePostName=function(b){this.settings.file_post_name=b;this.callFlash("SetFilePostName",[b]);};SWFUpload.prototype.setUseQueryString=function(b){this.settings.use_query_string=b;this.callFlash("SetUseQueryString",[b]);};SWFUpload.prototype.setRequeueOnError=function(b){this.settings.requeue_on_error=b;this.callFlash("SetRequeueOnError",[b]);};SWFUpload.prototype.setHTTPSuccess=function(b){if(typeof b==="string"){b=b.replace(" ","").split(",");}this.settings.http_success=b;this.callFlash("SetHTTPSuccess",[b]);};SWFUpload.prototype.setAssumeSuccessTimeout=function(b){this.settings.assume_success_timeout=b;this.callFlash("SetAssumeSuccessTimeout",[b]);};SWFUpload.prototype.setDebugEnabled=function(b){this.settings.debug_enabled=b;this.callFlash("SetDebugEnabled",[b]);};SWFUpload.prototype.setButtonImageURL=function(b){if(b==undefined){b="";}this.settings.button_image_url=b;this.callFlash("SetButtonImageURL",[b]);};SWFUpload.prototype.setButtonDimensions=function(f,e){this.settings.button_width=f;this.settings.button_height=e;var d=this.getMovieElement();if(d!=undefined){d.style.width=f+"px";d.style.height=e+"px";}this.callFlash("SetButtonDimensions",[f,e]);};SWFUpload.prototype.setButtonText=function(b){this.settings.button_text=b;this.callFlash("SetButtonText",[b]);};SWFUpload.prototype.setButtonTextPadding=function(c,d){this.settings.button_text_top_padding=d;this.settings.button_text_left_padding=c;this.callFlash("SetButtonTextPadding",[c,d]);};SWFUpload.prototype.setButtonTextStyle=function(b){this.settings.button_text_style=b;this.callFlash("SetButtonTextStyle",[b]);};SWFUpload.prototype.setButtonDisabled=function(b){this.settings.button_disabled=b;this.callFlash("SetButtonDisabled",[b]);};SWFUpload.prototype.setButtonAction=function(b){this.settings.button_action=b;this.callFlash("SetButtonAction",[b]);};SWFUpload.prototype.setButtonCursor=function(b){this.settings.button_cursor=b;this.callFlash("SetButtonCursor",[b]);};SWFUpload.prototype.queueEvent=function(d,f){if(f==undefined){f=[];}else{if(!(f instanceof Array)){f=[f];}}var e=this;if(typeof this.settings[d]==="function"){this.eventQueue.push(function(){this.settings[d].apply(this,f);});setTimeout(function(){e.executeNextEvent();},0);}else{if(this.settings[d]!==null){throw"Event handler "+d+" is unknown or is not a function";}}};SWFUpload.prototype.executeNextEvent=function(){var b=this.eventQueue?this.eventQueue.shift():null;if(typeof(b)==="function"){b.apply(this);}};SWFUpload.prototype.unescapeFilePostParams=function(l){var j=/[$]([0-9a-f]{4})/i;var i={};var k;if(l!=undefined){for(var h in l.post){if(l.post.hasOwnProperty(h)){k=h;var g;while((g=j.exec(k))!==null){k=k.replace(g[0],String.fromCharCode(parseInt("0x"+g[1],16)));}i[k]=l.post[h];}}l.post=i;}return l;};SWFUpload.prototype.testExternalInterface=function(){try{return this.callFlash("TestExternalInterface");}catch(b){return false;}};SWFUpload.prototype.flashReady=function(){var b=this.getMovieElement();if(!b){this.debug("Flash called back ready but the flash movie can't be found.");return;}this.cleanUp(b);this.queueEvent("swfupload_loaded_handler");};SWFUpload.prototype.cleanUp=function(f){try{if(this.movieElement&&typeof(f.CallFunction)==="unknown"){this.debug("Removing Flash functions hooks (this should only run in IE and should prevent memory leaks)");for(var h in f){try{if(typeof(f[h])==="function"){f[h]=null;}}catch(e){}}}}catch(g){}window.__flash__removeCallback=function(c,b){try{if(c){c[b]=null;}}catch(a){}};};SWFUpload.prototype.fileDialogStart=function(){this.queueEvent("file_dialog_start_handler");};SWFUpload.prototype.fileQueued=function(b){b=this.unescapeFilePostParams(b);this.queueEvent("file_queued_handler",b);};SWFUpload.prototype.fileQueueError=function(e,f,d){e=this.unescapeFilePostParams(e);this.queueEvent("file_queue_error_handler",[e,f,d]);};SWFUpload.prototype.fileDialogComplete=function(d,f,e){this.queueEvent("file_dialog_complete_handler",[d,f,e]);};SWFUpload.prototype.uploadStart=function(b){b=this.unescapeFilePostParams(b);this.queueEvent("return_upload_start_handler",b);};SWFUpload.prototype.returnUploadStart=function(d){var c;if(typeof this.settings.upload_start_handler==="function"){d=this.unescapeFilePostParams(d);c=this.settings.upload_start_handler.call(this,d);}else{if(this.settings.upload_start_handler!=undefined){throw"upload_start_handler must be a function";}}if(c===undefined){c=true;}c=!!c;this.callFlash("ReturnUploadStart",[c]);};SWFUpload.prototype.uploadProgress=function(e,f,d){e=this.unescapeFilePostParams(e);this.queueEvent("upload_progress_handler",[e,f,d]);};SWFUpload.prototype.uploadError=function(e,f,d){e=this.unescapeFilePostParams(e);this.queueEvent("upload_error_handler",[e,f,d]);};SWFUpload.prototype.uploadSuccess=function(d,e,f){d=this.unescapeFilePostParams(d);this.queueEvent("upload_success_handler",[d,e,f]);};SWFUpload.prototype.uploadComplete=function(b){b=this.unescapeFilePostParams(b);this.queueEvent("upload_complete_handler",b);};SWFUpload.prototype.debug=function(b){this.queueEvent("debug_handler",b);};SWFUpload.prototype.debugMessage=function(h){if(this.settings.debug){var f,g=[];if(typeof h==="object"&&typeof h.name==="string"&&typeof h.message==="string"){for(var e in h){if(h.hasOwnProperty(e)){g.push(e+": "+h[e]);}}f=g.join("\n")||"";g=f.split("\n");f="EXCEPTION: "+g.join("\nEXCEPTION: ");SWFUpload.Console.writeLine(f);}else{SWFUpload.Console.writeLine(h);}}};SWFUpload.Console={};SWFUpload.Console.writeLine=function(g){var e,f;try{e=document.getElementById("SWFUpload_Console");if(!e){f=document.createElement("form");document.getElementsByTagName("body")[0].appendChild(f);e=document.createElement("textarea");e.id="SWFUpload_Console";e.style.fontFamily="monospace";e.setAttribute("wrap","off");e.wrap="off";e.style.overflow="auto";e.style.width="700px";e.style.height="350px";e.style.margin="5px";f.appendChild(e);}e.value+=g+"\n";e.scrollTop=e.scrollHeight-e.clientHeight;}catch(h){alert("Exception: "+h.name+" Message: "+h.message);}};(function(c){var b={init:function(d,e){return this.each(function(){var n=c(this);var m=n.clone();var j=c.extend({id:n.attr("id"),swf:"uploadify.swf",uploader:"uploadify.php",auto:true,buttonClass:"",buttonCursor:"hand",buttonImage:null,buttonText:"SELECT FILES",checkExisting:false,debug:false,fileObjName:"Filedata",fileSizeLimit:0,fileTypeDesc:"All Files",fileTypeExts:"*.*",height:30,itemTemplate:false,method:"post",multi:true,formData:{},preventCaching:true,progressData:"percentage",queueID:false,queueSizeLimit:999,removeCompleted:true,removeTimeout:3,requeueErrors:false,successTimeout:30,uploadLimit:0,width:120,overrideEvents:[]},d);var g={assume_success_timeout:j.successTimeout,button_placeholder_id:j.id,button_width:j.width,button_height:j.height,button_text:null,button_text_style:null,button_text_top_padding:0,button_text_left_padding:0,button_action:(j.multi?SWFUpload.BUTTON_ACTION.SELECT_FILES:SWFUpload.BUTTON_ACTION.SELECT_FILE),button_disabled:false,button_cursor:(j.buttonCursor=="arrow"?SWFUpload.CURSOR.ARROW:SWFUpload.CURSOR.HAND),button_window_mode:SWFUpload.WINDOW_MODE.TRANSPARENT,debug:j.debug,requeue_on_error:j.requeueErrors,file_post_name:j.fileObjName,file_size_limit:j.fileSizeLimit,file_types:j.fileTypeExts,file_types_description:j.fileTypeDesc,file_queue_limit:j.queueSizeLimit,file_upload_limit:j.uploadLimit,flash_url:j.swf,prevent_swf_caching:j.preventCaching,post_params:j.formData,upload_url:j.uploader,use_query_string:(j.method=="get"),file_dialog_complete_handler:a.onDialogClose,file_dialog_start_handler:a.onDialogOpen,file_queued_handler:a.onSelect,file_queue_error_handler:a.onSelectError,swfupload_loaded_handler:j.onSWFReady,upload_complete_handler:a.onUploadComplete,upload_error_handler:a.onUploadError,upload_progress_handler:a.onUploadProgress,upload_start_handler:a.onUploadStart,upload_success_handler:a.onUploadSuccess};if(e){g=c.extend(g,e);}g=c.extend(g,j);var o=swfobject.getFlashPlayerVersion();var h=(o.major>=9);if(h){window["uploadify_"+j.id]=new SWFUpload(g);var i=window["uploadify_"+j.id];n.data("uploadify",i);var l=c("<div />",{id:j.id,"class":"uploadify",css:{height:j.height+"px",width:j.width+"px"}});c("#"+i.movieName).wrap(l);l=c("#"+j.id);l.data("uploadify",i);var f=c("<div />",{id:j.id+"-button","class":"uploadify-button "+j.buttonClass});if(j.buttonImage){f.css({"background-image":"url('"+j.buttonImage+"')","text-indent":"-9999px"});}f.html('<span class="uploadify-button-text">'+j.buttonText+"</span>").css({height:j.height+"px","line-height":j.height+"px",width:j.width+"px"});l.append(f);c("#"+i.movieName).css({position:"absolute","z-index":1});if(!j.queueID){var k=c("<div />",{id:j.id+"-queue","class":"uploadify-queue"});l.after(k);i.settings.queueID=j.id+"-queue";i.settings.defaultQueue=true;}i.queueData={files:{},filesSelected:0,filesQueued:0,filesReplaced:0,filesCancelled:0,filesErrored:0,uploadsSuccessful:0,uploadsErrored:0,averageSpeed:0,queueLength:0,queueSize:0,uploadSize:0,queueBytesUploaded:0,uploadQueue:[],errorMsg:"Some files were not added to the queue:"};i.original=m;i.wrapper=l;i.button=f;i.queue=k;if(j.onInit){j.onInit.call(n,i);}}else{if(j.onFallback){j.onFallback.call(n);}}});},cancel:function(d,f){var e=arguments;this.each(function(){var l=c(this),i=l.data("uploadify"),j=i.settings,h=-1;if(e[0]){if(e[0]=="*"){var g=i.queueData.queueLength;c("#"+j.queueID).find(".uploadify-queue-item").each(function(){h++;if(e[1]===true){i.cancelUpload(c(this).attr("id"),false);}else{i.cancelUpload(c(this).attr("id"));}c(this).find(".data").removeClass("data").html(" - Cancelled");c(this).find(".uploadify-progress-bar").remove();c(this).delay(1000+100*h).fadeOut(500,function(){c(this).remove();});});i.queueData.queueSize=0;i.queueData.queueLength=0;if(j.onClearQueue){j.onClearQueue.call(l,g);}}else{for(var m=0;m<e.length;m++){i.cancelUpload(e[m]);c("#"+e[m]).find(".data").removeClass("data").html(" - Cancelled");c("#"+e[m]).find(".uploadify-progress-bar").remove();c("#"+e[m]).delay(1000+100*m).fadeOut(500,function(){c(this).remove();});}}}else{var k=c("#"+j.queueID).find(".uploadify-queue-item").get(0);$item=c(k);i.cancelUpload($item.attr("id"));$item.find(".data").removeClass("data").html(" - Cancelled");$item.find(".uploadify-progress-bar").remove();$item.delay(1000).fadeOut(500,function(){c(this).remove();});}});},destroy:function(){this.each(function(){var f=c(this),d=f.data("uploadify"),e=d.settings;d.destroy();if(e.defaultQueue){c("#"+e.queueID).remove();}c("#"+e.id).replaceWith(d.original);if(e.onDestroy){e.onDestroy.call(this);}delete d;});},disable:function(d){this.each(function(){var g=c(this),e=g.data("uploadify"),f=e.settings;if(d){e.button.addClass("disabled");if(f.onDisable){f.onDisable.call(this);}}else{e.button.removeClass("disabled");if(f.onEnable){f.onEnable.call(this);}}e.setButtonDisabled(d);});},settings:function(e,g,h){var d=arguments;var f=g;this.each(function(){var k=c(this),i=k.data("uploadify"),j=i.settings;if(typeof(d[0])=="object"){for(var l in g){setData(l,g[l]);}}if(d.length===1){f=j[e];}else{switch(e){case"uploader":i.setUploadURL(g);break;case"formData":if(!h){g=c.extend(j.formData,g);}i.setPostParams(j.formData);break;case"method":if(g=="get"){i.setUseQueryString(true);}else{i.setUseQueryString(false);}break;case"fileObjName":i.setFilePostName(g);break;case"fileTypeExts":i.setFileTypes(g,j.fileTypeDesc);break;case"fileTypeDesc":i.setFileTypes(j.fileTypeExts,g);break;case"fileSizeLimit":i.setFileSizeLimit(g);break;case"uploadLimit":i.setFileUploadLimit(g);break;case"queueSizeLimit":i.setFileQueueLimit(g);break;case"buttonImage":i.button.css("background-image",settingValue);break;case"buttonCursor":if(g=="arrow"){i.setButtonCursor(SWFUpload.CURSOR.ARROW);}else{i.setButtonCursor(SWFUpload.CURSOR.HAND);}break;case"buttonText":c("#"+j.id+"-button").find(".uploadify-button-text").html(g);break;case"width":i.setButtonDimensions(g,j.height);break;case"height":i.setButtonDimensions(j.width,g);break;case"multi":if(g){i.setButtonAction(SWFUpload.BUTTON_ACTION.SELECT_FILES);}else{i.setButtonAction(SWFUpload.BUTTON_ACTION.SELECT_FILE);}break;}j[e]=g;}});if(d.length===1){return f;}},stop:function(){this.each(function(){var e=c(this),d=e.data("uploadify");d.queueData.averageSpeed=0;d.queueData.uploadSize=0;d.queueData.bytesUploaded=0;d.queueData.uploadQueue=[];d.stopUpload();});},upload:function(){var d=arguments;this.each(function(){var f=c(this),e=f.data("uploadify");e.queueData.averageSpeed=0;e.queueData.uploadSize=0;e.queueData.bytesUploaded=0;e.queueData.uploadQueue=[];if(d[0]){if(d[0]=="*"){e.queueData.uploadSize=e.queueData.queueSize;e.queueData.uploadQueue.push("*");e.startUpload();}else{for(var g=0;g<d.length;g++){e.queueData.uploadSize+=e.queueData.files[d[g]].size;e.queueData.uploadQueue.push(d[g]);}e.startUpload(e.queueData.uploadQueue.shift());}}else{e.startUpload();}});}};var a={onDialogOpen:function(){var d=this.settings;this.queueData.errorMsg="Some files were not added to the queue:";this.queueData.filesReplaced=0;this.queueData.filesCancelled=0;if(d.onDialogOpen){d.onDialogOpen.call(this);}},onDialogClose:function(d,f,g){var e=this.settings;this.queueData.filesErrored=d-f;this.queueData.filesSelected=d;this.queueData.filesQueued=f-this.queueData.filesCancelled;this.queueData.queueLength=g;if(c.inArray("onDialogClose",e.overrideEvents)<0){if(this.queueData.filesErrored>0){alert(this.queueData.errorMsg);}}if(e.onDialogClose){e.onDialogClose.call(this,this.queueData);}if(e.auto){c("#"+e.id).uploadify("upload","*");}},onSelect:function(h){var i=this.settings;var f={};for(var g in this.queueData.files){f=this.queueData.files[g];if(f.uploaded!=true&&f.name==h.name){var e=confirm('The file named "'+h.name+'" is already in the queue.\nDo you want to replace the existing item in the queue?');if(!e){this.cancelUpload(h.id);this.queueData.filesCancelled++;return false;}else{c("#"+f.id).remove();this.cancelUpload(f.id);this.queueData.filesReplaced++;}}}var j=Math.round(h.size/1024);var o="KB";if(j>1000){j=Math.round(j/1000);o="MB";}var l=j.toString().split(".");j=l[0];if(l.length>1){j+="."+l[1].substr(0,2);}j+=o;var k=h.name;if(k.length>25){k=k.substr(0,25)+"...";}itemData={fileID:h.id,instanceID:i.id,fileName:k,fileSize:j};if(i.itemTemplate==false){i.itemTemplate='<div id="${fileID}" class="uploadify-queue-item">					<div class="cancel">						<a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')">X</a>					</div>					<span class="fileName">${fileName} (${fileSize})</span><span class="data"></span>					<div class="uploadify-progress">						<div class="uploadify-progress-bar"><!--Progress Bar--></div>					</div>				</div>';}if(c.inArray("onSelect",i.overrideEvents)<0){itemHTML=i.itemTemplate;for(var m in itemData){itemHTML=itemHTML.replace(new RegExp("\\$\\{"+m+"\\}","g"),itemData[m]);}c("#"+i.queueID).append(itemHTML);}this.queueData.queueSize+=h.size;this.queueData.files[h.id]=h;if(i.onSelect){i.onSelect.apply(this,arguments);}},onSelectError:function(d,g,f){var e=this.settings;if(c.inArray("onSelectError",e.overrideEvents)<0){switch(g){case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:if(e.queueSizeLimit>f){this.queueData.errorMsg+="\nThe number of files selected exceeds the remaining upload limit ("+f+").";}else{this.queueData.errorMsg+="\nThe number of files selected exceeds the queue size limit ("+e.queueSizeLimit+").";}break;case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:this.queueData.errorMsg+='\nThe file "'+d.name+'" exceeds the size limit ('+e.fileSizeLimit+").";break;case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:this.queueData.errorMsg+='\nThe file "'+d.name+'" is empty.';break;case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:this.queueData.errorMsg+='\nThe file "'+d.name+'" is not an accepted file type ('+e.fileTypeDesc+").";break;}}if(g!=SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED){delete this.queueData.files[d.id];}if(e.onSelectError){e.onSelectError.apply(this,arguments);}},onQueueComplete:function(){if(this.settings.onQueueComplete){this.settings.onQueueComplete.call(this,this.settings.queueData);}},onUploadComplete:function(f){var g=this.settings,d=this;var e=this.getStats();this.queueData.queueLength=e.files_queued;if(this.queueData.uploadQueue[0]=="*"){if(this.queueData.queueLength>0){this.startUpload();}else{this.queueData.uploadQueue=[];if(g.onQueueComplete){g.onQueueComplete.call(this,this.queueData);}}}else{if(this.queueData.uploadQueue.length>0){this.startUpload(this.queueData.uploadQueue.shift());}else{this.queueData.uploadQueue=[];if(g.onQueueComplete){g.onQueueComplete.call(this,this.queueData);}}}if(c.inArray("onUploadComplete",g.overrideEvents)<0){if(g.removeCompleted){switch(f.filestatus){case SWFUpload.FILE_STATUS.COMPLETE:setTimeout(function(){if(c("#"+f.id)){d.queueData.queueSize-=f.size;d.queueData.queueLength-=1;delete d.queueData.files[f.id];c("#"+f.id).fadeOut(500,function(){c(this).remove();});}},g.removeTimeout*1000);break;case SWFUpload.FILE_STATUS.ERROR:if(!g.requeueErrors){setTimeout(function(){if(c("#"+f.id)){d.queueData.queueSize-=f.size;d.queueData.queueLength-=1;delete d.queueData.files[f.id];c("#"+f.id).fadeOut(500,function(){c(this).remove();});}},g.removeTimeout*1000);}break;}}else{f.uploaded=true;}}if(g.onUploadComplete){g.onUploadComplete.call(this,f);}},onUploadError:function(e,i,h){var f=this.settings;var g="Error";switch(i){case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:g="HTTP Error ("+h+")";break;case SWFUpload.UPLOAD_ERROR.MISSING_UPLOAD_URL:g="Missing Upload URL";break;case SWFUpload.UPLOAD_ERROR.IO_ERROR:g="IO Error";break;case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:g="Security Error";break;case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:alert("The upload limit has been reached ("+h+").");g="Exceeds Upload Limit";break;case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:g="Failed";break;case SWFUpload.UPLOAD_ERROR.SPECIFIED_FILE_ID_NOT_FOUND:break;case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:g="Validation Error";break;case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:g="Cancelled";this.queueData.queueSize-=e.size;this.queueData.queueLength-=1;if(e.status==SWFUpload.FILE_STATUS.IN_PROGRESS||c.inArray(e.id,this.queueData.uploadQueue)>=0){this.queueData.uploadSize-=e.size;}if(f.onCancel){f.onCancel.call(this,e);}delete this.queueData.files[e.id];break;case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:g="Stopped";break;}if(c.inArray("onUploadError",f.overrideEvents)<0){if(i!=SWFUpload.UPLOAD_ERROR.FILE_CANCELLED&&i!=SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED){c("#"+e.id).addClass("uploadify-error");}c("#"+e.id).find(".uploadify-progress-bar").css("width","1px");if(i!=SWFUpload.UPLOAD_ERROR.SPECIFIED_FILE_ID_NOT_FOUND&&e.status!=SWFUpload.FILE_STATUS.COMPLETE){c("#"+e.id).find(".data").html(" - "+g);}}var d=this.getStats();this.queueData.uploadsErrored=d.upload_errors;if(f.onUploadError){f.onUploadError.call(this,e,i,h,g);}},onUploadProgress:function(g,m,j){var h=this.settings;var e=new Date();var n=e.getTime();var k=n-this.timer;if(k>500){this.timer=n;}var i=m-this.bytesLoaded;this.bytesLoaded=m;var d=this.queueData.queueBytesUploaded+m;var p=Math.round(m/j*100);var o="KB/s";var l=0;var f=(i/1024)/(k/1000);f=Math.floor(f*10)/10;if(this.queueData.averageSpeed>0){this.queueData.averageSpeed=Math.floor((this.queueData.averageSpeed+f)/2);}else{this.queueData.averageSpeed=Math.floor(f);}if(f>1000){l=(f*0.001);this.queueData.averageSpeed=Math.floor(l);o="MB/s";}if(c.inArray("onUploadProgress",h.overrideEvents)<0){if(h.progressData=="percentage"){c("#"+g.id).find(".data").html(" - "+p+"%");}else{if(h.progressData=="speed"&&k>500){c("#"+g.id).find(".data").html(" - "+this.queueData.averageSpeed+o);}}c("#"+g.id).find(".uploadify-progress-bar").css("width",p+"%");}if(h.onUploadProgress){h.onUploadProgress.call(this,g,m,j,d,this.queueData.uploadSize);}},onUploadStart:function(d){var e=this.settings;var f=new Date();this.timer=f.getTime();this.bytesLoaded=0;if(this.queueData.uploadQueue.length==0){this.queueData.uploadSize=d.size;}if(e.checkExisting){c.ajax({type:"POST",async:false,url:e.checkExisting,data:{filename:d.name},success:function(h){if(h==1){var g=confirm('A file with the name "'+d.name+'" already exists on the server.\nWould you like to replace the existing file?');if(!g){this.cancelUpload(d.id);c("#"+d.id).remove();if(this.queueData.uploadQueue.length>0&&this.queueData.queueLength>0){if(this.queueData.uploadQueue[0]=="*"){this.startUpload();}else{this.startUpload(this.queueData.uploadQueue.shift());}}}}}});}if(e.onUploadStart){e.onUploadStart.call(this,d);}},onUploadSuccess:function(f,h,d){var g=this.settings;var e=this.getStats();this.queueData.uploadsSuccessful=e.successful_uploads;this.queueData.queueBytesUploaded+=f.size;if(c.inArray("onUploadSuccess",g.overrideEvents)<0){c("#"+f.id).find(".data").html(" - Complete");}if(g.onUploadSuccess){g.onUploadSuccess.call(this,f,h,d);}}};c.fn.uploadify=function(d){if(b[d]){return b[d].apply(this,Array.prototype.slice.call(arguments,1));}else{if(typeof d==="object"||!d){return b.init.apply(this,arguments);}else{c.error("The method "+d+" does not exist in $.uploadify");}}};})($);
/*
UploadiFive 1.1.2
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the UploadiFive Standard License <http://www.uploadify.com/uploadifive-standard-license>
*/
(function(b){var a={init:function(c){return this.each(function(){var g=b(this);g.data("uploadifive",{inputs:{},inputCount:0,fileID:0,queue:{count:0,selected:0,replaced:0,errors:0,queued:0,cancelled:0},uploads:{current:0,attempts:0,successful:0,errors:0,count:0}});var d=g.data("uploadifive");var f=d.settings=b.extend({auto:true,buttonClass:false,buttonText:"Select Files",checkScript:false,dnd:true,dropTarget:false,fileObjName:"Filedata",fileSizeLimit:0,fileType:false,formData:{},height:30,itemTemplate:false,method:"post",multi:true,overrideEvents:[],queueID:false,queueSizeLimit:0,removeCompleted:false,simUploadLimit:0,truncateLength:0,uploadLimit:0,uploadScript:"uploadifive.php",width:100},c);if(isNaN(f.fileSizeLimit)){var e=parseInt(f.fileSizeLimit)*1.024;if(f.fileSizeLimit.indexOf("KB")>-1){f.fileSizeLimit=e*1000;}else{if(f.fileSizeLimit.indexOf("MB")>-1){f.fileSizeLimit=e*1000000;}else{if(f.fileSizeLimit.indexOf("GB")>-1){f.fileSizeLimit=e*1000000000;}}}}else{f.fileSizeLimit=f.fileSizeLimit*1024;}d.inputTemplate=b('<input type="file">').css({"font-size":f.height+"px",opacity:0,position:"absolute",right:"-3px",top:"-3px","z-index":999});d.createInput=function(){var j=d.inputTemplate.clone();var k=j.name="input"+d.inputCount++;if(f.multi){j.attr("multiple",true);}j.bind("change",function(){d.queue.selected=0;d.queue.replaced=0;d.queue.errors=0;d.queue.queued=0;var l=this.files.length;d.queue.selected=l;if((d.queue.count+l)>f.queueSizeLimit&&f.queueSizeLimit!==0){if(b.inArray("onError",f.overrideEvents)<0){alert("The maximum number of queue items has been reached ("+f.queueSizeLimit+").  Please select fewer files.");}if(typeof f.onError==="function"){f.onError.call(g,"QUEUE_LIMIT_EXCEEDED");}}else{for(var m=0;m<l;m++){file=this.files[m];d.addQueueItem(file);}d.inputs[k]=this;d.createInput();}if(f.auto){a.upload.call(g);}if(typeof f.onSelect==="function"){f.onSelect.call(g,d.queue);}});if(d.currentInput){d.currentInput.hide();}d.button.append(j);d.currentInput=j;};d.destroyInput=function(j){b(d.inputs[j]).remove();delete d.inputs[j];d.inputCount--;};d.drop=function(m){d.queue.selected=0;d.queue.replaced=0;d.queue.errors=0;d.queue.queued=0;var l=m.dataTransfer;var k=l.name="input"+d.inputCount++;var j=l.files.length;d.queue.selected=j;if((d.queue.count+j)>f.queueSizeLimit&&f.queueSizeLimit!==0){if(b.inArray("onError",f.overrideEvents)<0){alert("The maximum number of queue items has been reached ("+f.queueSizeLimit+").  Please select fewer files.");}if(typeof f.onError==="function"){f.onError.call(g,"QUEUE_LIMIT_EXCEEDED");}}else{for(var o=0;o<j;o++){file=l.files[o];d.addQueueItem(file);}d.inputs[k]=l;}if(f.auto){a.upload.call(g);}if(typeof f.onDrop==="function"){f.onDrop.call(g,l.files,l.files.length);}m.preventDefault();m.stopPropagation();};d.fileExistsInQueue=function(k){for(var j in d.inputs){input=d.inputs[j];limit=input.files.length;for(var l=0;l<limit;l++){existingFile=input.files[l];if(existingFile.name==k.name&&!existingFile.complete){return true;}}}return false;};d.removeExistingFile=function(k){for(var j in d.inputs){input=d.inputs[j];limit=input.files.length;for(var l=0;l<limit;l++){existingFile=input.files[l];if(existingFile.name==k.name&&!existingFile.complete){d.queue.replaced++;a.cancel.call(g,existingFile,true);}}}};if(f.itemTemplate==false){d.queueItem=b('<div class="uploadifive-queue-item">                        <a class="close" href="#">X</a>                        <div><span class="filename"></span><span class="fileinfo"></span></div>                        <div class="progress">                            <div class="progress-bar"></div>                        </div>                    </div>');}else{d.queueItem=b(f.itemTemplate);}d.addQueueItem=function(k){if(b.inArray("onAddQueueItem",f.overrideEvents)<0){d.removeExistingFile(k);k.queueItem=d.queueItem.clone();k.queueItem.attr("id",f.id+"-file-"+d.fileID++);k.queueItem.find(".close").bind("click",function(){a.cancel.call(g,k);return false;});var m=k.name;if(m.length>f.truncateLength&&f.truncateLength!=0){m=m.substring(0,f.truncateLength)+"...";}k.queueItem.find(".filename").html(m);k.queueItem.data("file",k);d.queueEl.append(k.queueItem);}if(typeof f.onAddQueueItem==="function"){f.onAddQueueItem.call(g,k);}if(f.fileType){if(b.isArray(f.fileType)){var j=false;for(var l=0;l<f.fileType.length;l++){if(k.type.indexOf(f.fileType[l])>-1){j=true;}}if(!j){d.error("FORBIDDEN_FILE_TYPE",k);}}else{if(k.type.indexOf(f.fileType)<0){d.error("FORBIDDEN_FILE_TYPE",k);}}}if(k.size>f.fileSizeLimit&&f.fileSizeLimit!=0){d.error("FILE_SIZE_LIMIT_EXCEEDED",k);}else{d.queue.queued++;d.queue.count++;}};d.removeQueueItem=function(m,l,k){if(!k){k=0;}var j=l?0:500;if(m.queueItem){if(m.queueItem.find(".fileinfo").html()!=" - Completed"){m.queueItem.find(".fileinfo").html(" - Cancelled");}m.queueItem.find(".progress-bar").width(0);m.queueItem.delay(k).fadeOut(j,function(){b(this).remove();});delete m.queueItem;d.queue.count--;}};d.filesToUpload=function(){var k=0;for(var j in d.inputs){input=d.inputs[j];limit=input.files.length;for(var l=0;l<limit;l++){file=input.files[l];if(!file.skip&&!file.complete){k++;}}}return k;};d.checkExists=function(k){if(b.inArray("onCheck",f.overrideEvents)<0){b.ajaxSetup({async:false});var j=b.extend(f.formData,{filename:k.name});b.post(f.checkScript,j,function(l){k.exists=parseInt(l);});if(k.exists){if(!confirm("A file named "+k.name+" already exists in the upload folder.\nWould you like to replace it?")){a.cancel.call(g,k);return true;}}}if(typeof f.onCheck==="function"){f.onCheck.call(g,k,k.exists);}return false;};d.uploadFile=function(k,l){if(!k.skip&&!k.complete&&!k.uploading){k.uploading=true;d.uploads.current++;d.uploads.attempted++;xhr=k.xhr=new XMLHttpRequest();if(typeof FormData==="function"||typeof FormData==="object"){var m=new FormData();m.append(f.fileObjName,k);for(i in f.formData){m.append(i,f.formData[i]);}xhr.open(f.method,f.uploadScript,true);xhr.upload.addEventListener("progress",function(n){if(n.lengthComputable){d.progress(n,k);}},false);xhr.addEventListener("load",function(n){if(this.readyState==4){k.uploading=false;if(this.status==200){if(k.xhr.responseText!=="Invalid file type."){d.uploadComplete(n,k,l);}else{d.error(k.xhr.responseText,k,l);}}else{if(this.status==404){d.error("404_FILE_NOT_FOUND",k,l);}else{if(this.status==403){d.error("403_FORBIDDEN",k,l);}else{d.error("Unknown Error",k,l);}}}}});xhr.send(m);}else{var j=new FileReader();j.onload=function(q){var t="-------------------------"+(new Date).getTime(),p="--",o="\r\n",s="";s+=p+t+o;s+='Content-Disposition: form-data; name="'+f.fileObjName+'"';if(k.name){s+='; filename="'+k.name+'"';}s+=o;s+="Content-Type: application/octet-stream"+o+o;s+=q.target.result+o;for(key in f.formData){s+=p+t+o;s+='Content-Disposition: form-data; name="'+key+'"'+o+o;s+=f.formData[key]+o;}s+=p+t+p+o;xhr.upload.addEventListener("progress",function(u){d.progress(u,k);},false);xhr.addEventListener("load",function(v){k.uploading=false;var u=this.status;if(u==404){d.error("404_FILE_NOT_FOUND",k,l);}else{if(k.xhr.responseText!="Invalid file type."){d.uploadComplete(v,k,l);}else{d.error(k.xhr.responseText,k,l);}}},false);var n=f.uploadScript;if(f.method=="get"){var r=b(f.formData).param();n+=r;}xhr.open(f.method,f.uploadScript,true);xhr.setRequestHeader("Content-Type","multipart/form-data; boundary="+t);if(typeof f.onUploadFile==="function"){f.onUploadFile.call(g,k);}xhr.sendAsBinary(s);};j.readAsBinaryString(k);}}};d.progress=function(l,j){if(b.inArray("onProgress",f.overrideEvents)<0){if(l.lengthComputable){var k=Math.round((l.loaded/l.total)*100);}j.queueItem.find(".fileinfo").html(" - "+k+"%");j.queueItem.find(".progress-bar").css("width",k+"%");}if(typeof f.onProgress==="function"){f.onProgress.call(g,j,l);}};d.error=function(l,j,k){if(b.inArray("onError",f.overrideEvents)<0){switch(l){case"404_FILE_NOT_FOUND":errorMsg="404 Error";break;case"403_FORBIDDEN":errorMsg="403 Forbidden";break;case"FORBIDDEN_FILE_TYPE":errorMsg="Forbidden File Type";break;case"FILE_SIZE_LIMIT_EXCEEDED":errorMsg="File Too Large";break;default:errorMsg="Unknown Error";break;}j.queueItem.addClass("error").find(".fileinfo").html(" - "+errorMsg);j.queueItem.find(".progress").remove();}if(typeof f.onError==="function"){f.onError.call(g,l,j);}j.skip=true;if(l=="404_FILE_NOT_FOUND"){d.uploads.errors++;}else{d.queue.errors++;}if(k){a.upload.call(g,null,true);}};d.uploadComplete=function(l,j,k){if(b.inArray("onUploadComplete",f.overrideEvents)<0){j.queueItem.find(".progress-bar").css("width","100%");j.queueItem.find(".fileinfo").html(" - Completed");j.queueItem.find(".progress").slideUp(250);j.queueItem.addClass("complete");}if(typeof f.onUploadComplete==="function"){f.onUploadComplete.call(g,j,j.xhr.responseText);}if(f.removeCompleted){setTimeout(function(){a.cancel.call(g,j);},3000);}j.complete=true;d.uploads.successful++;d.uploads.count++;d.uploads.current--;delete j.xhr;if(k){a.upload.call(g,null,true);}};d.queueComplete=function(){if(typeof f.onQueueComplete==="function"){f.onQueueComplete.call(g,d.uploads);}};if(window.File&&window.FileList&&window.Blob&&(window.FileReader||window.FormData)){f.id="uploadifive-"+g.attr("id");d.button=b('<div id="'+f.id+'" class="uploadifive-button">'+f.buttonText+"</div>");if(f.buttonClass){d.button.addClass(f.buttonClass);}d.button.css({height:f.height,"line-height":f.height+"px",overflow:"hidden",position:"relative","text-align":"center",width:f.width});g.before(d.button).appendTo(d.button).hide();d.createInput.call(g);if(!f.queueID){f.queueID=f.id+"-queue";d.queueEl=b('<div id="'+f.queueID+'" class="uploadifive-queue" />');d.button.after(d.queueEl);}else{d.queueEl=b("#"+f.queueID);}if(f.dnd){var h=f.dropTarget?b(f.dropTarget):d.queueEl.get(0);h.addEventListener("dragleave",function(j){j.preventDefault();j.stopPropagation();},false);h.addEventListener("dragenter",function(j){j.preventDefault();j.stopPropagation();},false);h.addEventListener("dragover",function(j){j.preventDefault();j.stopPropagation();},false);h.addEventListener("drop",d.drop,false);}if(!XMLHttpRequest.prototype.sendAsBinary){XMLHttpRequest.prototype.sendAsBinary=function(k){function l(n){return n.charCodeAt(0)&255;}var m=Array.prototype.map.call(k,l);var j=new Uint8Array(m);this.send(j.buffer);};}if(typeof f.onInit==="function"){f.onInit.call(g);}}else{if(typeof f.onFallback==="function"){f.onFallback.call(g);}return false;}});},debug:function(){return this.each(function(){console.log(b(this).data("uploadifive"));});},clearQueue:function(){this.each(function(){var f=b(this),c=f.data("uploadifive"),e=c.settings;for(var d in c.inputs){input=c.inputs[d];limit=input.files.length;for(i=0;i<limit;i++){file=input.files[i];a.cancel.call(f,file);}}if(typeof e.onClearQueue==="function"){e.onClearQueue.call(f,b("#"+c.settings.queueID));}});},cancel:function(d,c){this.each(function(){var g=b(this),e=g.data("uploadifive"),f=e.settings;if(typeof d==="string"){if(!isNaN(d)){fileID="uploadifive-"+b(this).attr("id")+"-file-"+d;}d=b("#"+fileID).data("file");}d.skip=true;e.filesCancelled++;if(d.uploading){e.uploads.current--;d.uploading=false;d.xhr.abort();delete d.xhr;a.upload.call(g);}if(b.inArray("onCancel",f.overrideEvents)<0){e.removeQueueItem(d,c);}if(typeof f.onCancel==="function"){f.onCancel.call(g,d);}});},upload:function(c,d){this.each(function(){var h=b(this),e=h.data("uploadifive"),f=e.settings;if(c){e.uploadFile.call(h,c);}else{if((e.uploads.count+e.uploads.current)<f.uploadLimit||f.uploadLimit==0){if(!d){e.uploads.attempted=0;e.uploads.successsful=0;e.uploads.errors=0;var g=e.filesToUpload();if(typeof f.onUpload==="function"){f.onUpload.call(h,g);}}b("#"+f.queueID).find(".uploadifive-queue-item").not(".error, .complete").each(function(){_file=b(this).data("file");if((e.uploads.current>=f.simUploadLimit&&f.simUploadLimit!==0)||(e.uploads.current>=f.uploadLimit&&f.uploadLimit!==0)||(e.uploads.count>=f.uploadLimit&&f.uploadLimit!==0)){return false;}if(f.checkScript){_file.checking=true;skipFile=e.checkExists(_file);_file.checking=false;if(!skipFile){e.uploadFile(_file,true);}}else{e.uploadFile(_file,true);}});if(b("#"+f.queueID).find(".uploadifive-queue-item").not(".error, .complete").size()==0){e.queueComplete();}}else{if(e.uploads.current==0){if(b.inArray("onError",f.overrideEvents)<0){if(e.filesToUpload()>0&&f.uploadLimit!=0){alert("The maximum upload limit has been reached.");}}if(typeof f.onError==="function"){f.onError.call(h,"UPLOAD_LIMIT_EXCEEDED",e.filesToUpload());}}}}});},destroy:function(){this.each(function(){var e=b(this),c=e.data("uploadifive"),d=c.settings;a.clearQueue.call(e);if(!d.queueID){b("#"+d.queueID).remove();}e.siblings("input").remove();e.show().insertBefore(c.button);c.button.remove();if(typeof d.onDestroy==="function"){d.onDestroy.call(e);}});}};b.fn.uploadifive=function(c){if(a[c]){return a[c].apply(this,Array.prototype.slice.call(arguments,1));}else{if(typeof c==="object"||!c){return a.init.apply(this,arguments);}else{b.error("The method "+c+" does not exist in $.uploadify");}}};})(jQuery);
var Images=function(){
    o={
        $el:$('#image-list'),
        $info:$('#image-info h2'),
        defaultList:[],
        list:[],
        perPage:($(window).width()>=480) ? (($(window).width()>=768) ? 20 : 12) : 9,
        current:0,
        animating:false,

        init:function(){

	            var me=this;
	            me.load();

	            $('.more-images').click(function(e){
	                e.preventDefault();
	                me.nextPage();
	            });

	            $('#image-popup,.close').click(function(e){
	            	e.preventDefault();
	            	me.closePopup();
	            });

	            $(window).resize(function(){
	            	me.resizeAll();
	            });

	            $(document).on('click','.image-block',function(){
	            	if(!me.animating){
		            	me.showPopup($(this));
		            }
	            });

	            $('#search-page-amount').text(' '+me.perPage);

	            return me;
	            
	        },

        load:function(){

	            var me=this;

	            $.ajax({
	            	url     :'http://rdappshost.com/toyota/allthings/api/images?allow=1',
	                cache   :false,
	                async   :true,
	                dataType:'jsonp',
	                jsonp: "callback",

	            	success: function(r){
	            			if(r.status=='success'){
	            				me.defaultList=r.data;
			                    me.list=r.data;
			                    me.clear();
			                    me.show();
	            			} else {
	            				
	            			}
	            		}
	            	});

	        },

	    resizeAll:function(){
	    		var last=0;
		    	this.$el.find('.image-block').each(function(){
		    		var h=(last==0) ? parseInt($(this).width()) : last;
		    		last=h;

		    		$(this).height(h);
		    	});
		    },

	    addImage:function(i){

	    		this.$el.append('<div class="image-block" id="image_'+i.session_code+'"></div>');
	    		this.addImageLoader(i);

	    		if(this.current>=this.list.length-1){
		        	$('.show-more-images').hide();
				} else {
					$('.show-more-images').show();
				}

	    	},

	    addImageLoader:function(i){

	    		var self=this;
		    	i.img = new Image;

				i.img.onload = function(){
					self.makeImage(i);
				};

				i.img.onerror = function(){
					self.removeImage(i);
					self.oneMore();
				};

				self.makeLoader(i);
				i.img.src=i.image_url;

		    },

        show:function(){

        		if(this.list.length>0){
		            var till=this.list.length>this.perPage?this.perPage:this.list.length;
		            for(var q=0;q<till;q++){
		                this.addImage(this.list[q]);
		                this.current=q+1;
		            }
		            this.resizeAll();
		            this.updateInfo();
		        } else {
		        	this.$el.html('<p class="center-text">Your search returned 0 results.</p>');
		        }

				$('.image-block').each(function(){
					if($(this).html()==''){
						$(this).remove();
					}
				});

	        },

	    oneMore:function(){

	            var till=this.list.length>(1+this.current)?(1+this.current):this.list.length;
	            for(var q=this.current;q<till;q++){
	                this.addImage(this.list[q]);
	                this.current=q+1;
	            }
	            this.resizeAll();
	            this.updateInfo();

	        },

        nextPage:function(){

	            var till=this.list.length>(this.perPage+this.current)?(this.perPage+this.current):this.list.length;
	            for(var q=this.current;q<till;q++){
	                this.addImage(this.list[q]);
	                this.current=q+1;
	            }
	            this.resizeAll();
	            this.updateInfo();

	        },

	    search:function(str){

	    		function x(str){
					return str.replace(/[^\w\s]/gi, '').replace(/ /g,'').toUpperCase();
				}

	    		var searchList=[];

	    		for(var q=0;q<this.defaultList.length;q++){
	    			if((x(this.defaultList[q].nominee_name).indexOf(x(str))!==-1) || x(this.defaultList[q].user_name).indexOf(x(str))!==-1){
	    				searchList.push(this.defaultList[q]);
	    			}
	    		}

	    		this.list=(str!='') ? searchList : this.defaultList;
	    		this.clear();
	    		this.show();

	       	},

	    updateInfo:function(){
	    		$('#search-info').html('('+this.list.length+' images)');
	    	},

	    showPopup:function($image){
	    		var me=this;
	    		me.animating=true;

	    		var i=$image.attr('id').replace('image_','');
	    		var rosetteUrl='assets/images/rosettes_w/rosette_text_';

	    		$('#image-popup').css({
	    			opacity:0,
	    			width:1
	    		});

	    		var img=false;
	    		for(var q=0;q<this.defaultList.length;q++){
	    			if(this.defaultList[q].session_code==i){
	    				img=this.defaultList[q];
	    			}
	    		}

	    		if(img){
	    			

	    			$('#image-popup .nominee').text(img.nominee_name);
	    			$('#image-popup .nominater .name').text(img.user_name);
	    			$('#image-popup .rosette .text img').attr('src',rosetteUrl+img.filter+'.png');
	    			
	    			var popupLeft=parseInt($image.position().left)+(parseInt($image.width())/2)-150;

	    			if(popupLeft<10){
	    				popupLeft=10;
	    			} else if((popupLeft+300)>parseInt($(window).width())){
	    				popupLeft=parseInt($(window).width())-310;
	    			}

	    			$('#image-popup').css('top',parseInt($image.position().top)+(($image.width())/2)-150);
	    			$('#image-popup').css('left',popupLeft);
	    			$('#image-popup .content').hide();

	    			$('#image-popup').animate({
	    				opacity:1,
	    				width:300
	    			},500,function(){
	    				$('#image-popup .content').slideDown('slow',function(){

	    					setTimeout(function(){
		    					me.animating=false;
		    				},1000);

	    				});
	    			});

	    			var theTop=parseInt($image.offset().top)-(window.innerHeight/2)+parseInt($image.height())/2;

	    			$('html, body').animate({
				        scrollTop: theTop
				    },200);
	    		}
	    	},

	    closePopup:function(){
	    		var me=this;
	    		me.animating=true;

		    	$('#image-popup .content').slideUp('slow',function(){
		    		$('#image-popup').animate({
	    				opacity:0,
	    				width:1
	    			},500,function(){
	    				setTimeout(function(){
	    					me.animating=false;
	    				},1000);

	    			});
		    	});
	    	},

        clear:function(){

	            this.current=0;
	            this.$el.html('');

	        },

	    makeLoader:function(i){

		    	var html='<div class="loader"><img src="assets/images/loader_image.gif" alt="Loading..." /></div>';
	            this.$el.find('#image_'+i.session_code).append(html);

	            this.$el.find('#image_'+i.session_code).find('.loader').css({
	            	position:'relative',
	            	top:(parseInt(this.$el.find('#image_'+i.session_code).width())-parseInt(this.$el.find('#image_'+i.session_code).find('.loader').height()))/2
	            });

		    },

        makeImage:function(i){

	            this.$el.find('#image_'+i.session_code).html(i.img);

	        },

	    removeImage:function(i){

	        	this.$el.find('#image_'+i.session_code).remove();

	       },

        isMore:function(){

	            if(this.current+1>=this.list.length){
	                return false;
	            } else {
	                return true;
	            }

	        }

    };
    return o.init();
};
var Winners=function(){
    o={
        $el:$('#winners'),
        defaultList:[],
        list:[],
        perPage:($(window).width()>=480) ? (($(window).width()>=768) ? 3 : 2) : 1,
        current:0,
        animating:false,

        init:function(){

	            var me=this;
	            me.load();

	            $('.more-winners').click(function(e){
	                e.preventDefault();
	                me.nextPage();
	            });

	            $('#winners-page-amount').text(' '+me.perPage);

	            return me;
	            
	        },

        load:function(){

	            var me=this;

	            $.ajax({
	            	url     :'http://rdappshost.com/toyota/allthings/api/winners?allow=1',
	                cache   :false,
	                async   :true,
	                dataType:'jsonp',
	                jsonp: "callback",

	            	success: function(r){
	            			if(r.status=='success'){
	            				me.defaultList=r.data;
			                    me.list=r.data;
			                    me.clear();
			                    me.show();
	            			} else {
	            				
	            			}
	            		}
	            	});

	        },

        show:function(){

        		if(this.list.length>0){
		            var till=this.list.length>this.perPage?this.perPage:this.list.length;
		            for(var q=0;q<till;q++){
		                this.addWinner(this.list[q],((q%2==0)?true:false));
		                this.current=q+1;
		            }
		        } else {
		        	this.$el.html('<p class="center-text">Something went wrong...</p>');
		        }

	        },

	    addWinner:function(w,right){

		    	var html='<div class="story '+((right)?'right':'')+'">'+
					'<div class="story-about center-text">'+
						'<div class="logo rosette">'+
							'<div class="main"><img src="assets/images/rosettes_w/rosette_filled_red.png" alt="Rosette"></div>'+
							'<div class="text"><img src="assets/images/rosettes_w/rosette_text_'+w.filter.replace(' ','').toLowerCase()+'.png" alt="A '+w.filter+'"></div>'+
						'</div>'+
						'<h3 class="quote">"'+w.quote+'"</h3>'+
						'<h4 class="who">- '+w.user_name+' nominates<br> '+w.nominee_who+' '+w.nominee_name+'</h4>'+
					'</div>'+
					'<div class="story-image">'+
						'<img class="image" src="'+w.image_url+'" alt="Story image">'+
					'</div>'+
					'<div class="clear"></div>'+
				'</div>';

				this.$el.append(html);

				if(this.current>=this.list.length-1){
		        	$('.show-more-winners').hide();
				} else {
					$('.show-more-winners').show();
				}
		    },

        nextPage:function(){

	            var till=this.list.length>(this.perPage+this.current)?(this.perPage+this.current):this.list.length;
	            for(var q=this.current;q<till;q++){
	                this.addWinner(this.list[q],((q%2==0)?true:false));
	                this.current=q+1;
	            };

	            if(this.current+this.perPage>this.list.length){
		        	$('.show-more-winners').hide();
				} else {
					$('.show-more-winners').show();
				}

	        },

        clear:function(){

	            this.current=0;
	            this.$el.html('');

	        },

        isMore:function(){

	            if(this.current+1>=this.list.length){
	                return false;
	            } else {
	                return true;
	            }

	        }

    };
    return o.init();
};
window.cache={};
window.sliderMin=1;
window.friends=[];
window.filters=[];
window.filterIndex=-1;
window.posted=false;
window.apiUrl='http://rdappshost.com/toyota/allthings/';

if(getUrlVars()['session_code']==undefined || getUrlVars()['session_code']==''){
	document.getElementById('stepLoading').style.display = 'none';
	document.getElementById('step1').style.display = 'block';
}

window.fbAsyncInit = function() {
	FB.getLoginStatus(function(response) {
		checkForSessionCode();
	});
};

function resizeImageBox () {
	setTimeout(function(){
		var w=parseInt($('#home').width());
		if(w<640){
			var percentage=w/640;

			$('#image_box_container').css({
				width:w,
				overflow:'hidden'
			});
			
			$('#image_box').css({
			  '-webkit-transform' 	: 'scale(' + percentage + ')',
			  '-moz-transform'    	: 'scale(' + percentage + ')',
			  '-ms-transform'     	: 'scale(' + percentage + ')',
			  '-o-transform'     	: 'scale(' + percentage + ')',
			  'transform'         	: 'scale(' + percentage + ')',
			  'left'				: -((1-percentage)*(640/2)),
			  'top'					: -((1-percentage)*(640/2)),
			  'margin-bottom'		: -(640*(1-percentage))+20
			});

			$('#toggleOverlay').addClass('bigger');
		} else {
			$('#image_box').attr('style','display:'+$('#image_box').css('display')+';');
			$('#toggleOverlay').removeClass('bigger');

		}

		resizeImageBox();
	},1000);
}

function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		vars[key] = value;
	});
	return vars;
}

function runFloodlight () {
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    $('body').prepend('<iframe src="http://2623226.fls.doubleclick.net/activityi;src=2623226;type=thank0;cat=allth0;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
}

function checkForSessionCode(){

	if(getUrlVars()['session_code']!=undefined){

		scrollToComp();

		$('#image_short_url').val(decodeURIComponent(getUrlVars()['short_url']));

		setupImage();

		if(getUrlVars()['action']=='user_friends'){

			getFacebookFriends(function(friendData){
				window.friends=[];
				for(var q=0;q<friendData.data.length;q++){
					friends.push({
						label:friendData.data[q].name,
						value:friendData.data[q].id
					});
				}

				$('#enableTagging').remove();
				$('#message .next-info').html('Tag your Facebook friends using the @symbol and their Facebook name');
				$('#message .next-info').css('marginLeft',0);

				initTagging();
			});

		} else if(getUrlVars()['action']=='publish_actions'){
			$('#share_message').val(decodeURIComponent(getUrlVars()['message'].replace(/\+/g,'%20')));
			postFacebookStory();
		}
	}
}

function setupImage(){
	var session_code=getUrlVars()['session_code'];
	$('#session_code').val(session_code);

	$('#step1,#stepLoading').hide();
	$('#image_box').html('<img src="http://rdappshost.com/toyota/allthings/uploadify/uploads/'+session_code+'/created.png" alt="image" />').show();
	$('#step6').show();
}

$(document).ready(function(e){

	imgFeed=new Images();
	winners=new Winners();
	searchView=new Search();
	homeControls();

	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/en_US/all.js',function(){
                   
		FB.init({
			appId  		: '717041361711105',
			version    : 'v2.1',
			xfbml  		: true
		});

		FB.Canvas.setAutoGrow();
	});

	resizeImageBox();

	CharLimit({
		selector:$('textarea'),
		update:function(element,limit){
			if(limit<50 && limit>10){
				$('#char-count').removeClass('empty').addClass('warning');
			} else if(limit<10){
				$('#char-count').removeClass('warning').addClass('empty');
			} else {
				$('#char-count').removeClass('warning').removeClass('empty');
			}
			$('#char-count .amount').text(limit); 

		}
	});

	$('#nominate_now').click(function(){
		scrollToComp();
	});
	

	$('.backStepLink').click(function(e){
		e.preventDefault();
		var step=$(this).parent().parent();
		var me=$(this);
		step.fadeOut('fast',function(){
			$(me.attr('rel')).fadeIn('fast');
			scrollToComp();
		});
	});


	$('.nextStepLink').click(function(e){
		e.preventDefault();
		var step=$(this).parent().parent();

		if(validate(step)){
			var me=$(this);
			$('#step0').fadeOut('fast');
			step.fadeOut('fast',function(){
				$(me.attr('rel')).fadeIn('fast');
				scrollToComp();
			});
		}

	});

	$('body').on('click','.tagmate-hiliter',function(){
		$(this).parent().find('textarea').focus();
	})

	$('.input.checkbox i').click(function(){
		if(!$(this).parent().find('input[type="checkbox"]').is(':checked')){
			$(this).removeClass('icon-un_check').addClass('icon-active_check');
		} else {
			$(this).removeClass('icon-active_check').addClass('icon-un_check');
		}
	});


	$('#addControls').click(function(){
		addControls();
	});

	$('#completeNomination').click(function(){
		if(!$(this).hasClass('loading')){
			
			if(validate($('#step2'))){
				completeNomination();
				$(this).addClass('loading');
			}
		}
	});

	$('#createImage').click(function(){
		if(!$(this).hasClass('loading')){
			$(this).addClass('loading');
			createImage();
		}
	});

	$('#enableTagging').click(function(e){
		e.preventDefault();

		getFriends(function(friendData){
			window.friends=[];
			for(var q=0;q<friendData.data.length;q++){
				friends.push({
					label:friendData.data[q].name,
					value:friendData.data[q].id
				});
			}

			$('#enableTagging').remove();
			$('#message .next-info').html('Tag your Facebook friends using the @symbol and their Facebook name');
			$('#message .next-info').css('marginLeft',0);

			initTagging();
		});
	});

	$('#facebook-share').click(function(e){
		e.preventDefault();

		if(!$(this).hasClass('loading')){
			$(this).addClass('loading');

			XBrowserLogin(function(response){
				if (response && !response.error) {
					postFacebookStory();
		    	} else {
		    		$(this).removeClass('loading');
		    	}
		    },{ scope: 'publish_actions' });
		}
	});

	$('#twitter-share').click(function(e){
		e.preventDefault();
		var tweet=encodeURIComponent("I've nominated someone for the @toyotaireland #AllThingsToAllPeople awards! "+$('#image_short_url').val());
		window.open('https://twitter.com/intent/tweet?text='+tweet);

		twitterShare();
	});

});

function XBrowserLogin(callback,options){
	if( navigator.userAgent.indexOf('CriOS')!==-1 || (typeof chrome !=undefined && (navigator.userAgent.indexOf('iPhone')!==-1)) ){

		var appID=717041361711105;
		var tagData=doTagging();
		var message=tagData.message;
		var shortUrl=$('#image_short_url').val();
		var uri='http://allthings.toyota.ie?session_code='+$('#session_code').val()+'&action='+options.scope+'&short_url='+shortUrl;

		if(options.scope=='publish_actions'){
			uri+='&message='+encodeURIComponent(message);
		}

		top.location.href='https://www.facebook.com/dialog/oauth?display=popup&client_id='+appID+'&redirect_uri='+encodeURIComponent(uri)+'&scope='+options.scope;

	} else {

	    FB.login(function (response){
	    	callback(response);
	    },options);

	}
}

function XDomainData(url,data,callback) {
	var api_url='http://rdappshost.com/toyota/allthings/';

	$.post('assets/encrypt.php',data,function(response){
		r=$.parseJSON(response);

		if(r.status=='success'){

			$.ajax({
				url 	:api_url+url,
				cache 	:false,
				async 	:true,
				dataType:'jsonp',
				jsonpCallback:'content',
				crossDomain:true,
				data:{
						data:r.data
					},
				success: function(response){
						if(response.status=='success'){
							callback(response);
						} else {
							console.error(response);
						}
					}
				});

		} else {
			console.log('Error with data encryption');
		}

	});

}

function scrollToComp(){
	$('html, body').animate({
        scrollTop: parseInt($('#home').offset().top)-parseInt($('.primary-nav').height())-20
    }, 1000);
}

function postFacebookStory(){
	var tagData=doTagging();

	FB.api('/me/allthingsallpeople:nominate','POST',
	{
		'person':'http://rdappshost.com/toyota/allthings/award.php?session_code='+$('#session_code').val(),
		'message': tagData.message,//mentions
		//'tags': tagData.tags.join(),//tag
		'image:url':'http://rdappshost.com/toyota/allthings/uploadify/uploads/'+$('#session_code').val()+'/created.png',
		'image:user_generated':true
	},
	function(r){
		if (r && !r.error) {
			$('#before,#facebook-share').hide();
			$('#fb-share-success').show();

			facebookShare();
		} else {
			$(this).removeClass('loading');
		}
	});
}

function getFacebookFriends(callback) {
	FB.api('me/taggable_friends',function(r){
		if (r && !r.error) {
			if(typeof callback != 'undefined'){
				callback(r);
			}
		} else {
			//console.error('APP ERROR');
		}
	});
}

function facebookShare () {
	$.ajax({
		url 	:apiUrl+'api/share/facebook',
		cache 	:false,
		async 	:true,
		dataType:'jsonp',
		jsonpCallback:'content',
		crossDomain:true,
		data:{
				session_code:$('#session_code').val()
			},
		success: function(response){
				if(response.status=='success'){
					
				} else {
					
				}
			}
		});
}


function twitterShare () {
	$.ajax({
		url 	:apiUrl+'api/share/twitter',
		cache 	:false,
		async 	:true,
		dataType:'jsonp',
		jsonpCallback:'content',
		crossDomain:true,
		data:{
				session_code:$('#session_code').val()
			},
		success: function(response){
				if(response.status=='success'){
					
				} else {
					
				}
			}
		});
}


function homeControls(){
	getFilters();
	filterControls();
	image_get_controls();
}

function initTagging(){
	$("#step6 textarea").tagmate({
		exprs: {
			"@": Tagmate.NAME_TAG_EXPR
		},
		sources: {
			"@": function(request, response) {
				var filtered = Tagmate.filterOptions(window.friends, request.term);
				response(filtered);
			}
		},
		capture_tag: function(tag) { },
		replace_tag: function(tag, value) { },
		highlight_tags: true,
		highlight_class: "tag",
		menu_class: "menu",
		menu_option_class: "option",
		menu_option_active_class: "active"
	});
}

function getFriends(callback){
	XBrowserLogin(function (response){
		if (response && !response.error) {

			getFacebookFriends(callback);

		}
	}, { scope: 'user_friends' });
}

function doTagging(){
	var message=$('#message textarea').val();
	var tags=[];
	var names=[];
	for(var q=0;q<window.friends.length;q++){
		if(message.indexOf(window.friends[q].label)!=-1){
			message=message.replace(window.friends[q].label,'['+window.friends[q].value+']');
			tags.push(window.friends[q].value);
			names.push(window.friends[q].label);
		}
	}
	return {
			message:message,
			tags:tags,
			names:names
		};
}

function postToStream(){
	
}

function image_get_controls(){
	initUploadifive();
	initUploadifive2();
}

// UPLOADER
function initUploadifive(){
	$('#uploadphoto').uploadifive({
		'buttonText' : 'Upload Photo',
        'uploadScript' : apiUrl+'uploadify/uploadify.php?session_code='+$('#session_code').val(),
        'fileSizeLimit' : '10MB',
        'fileType'     : 'image',
        'auto'         : true,
        'onFallback'   : function() {
        	initUploadify();
        },
        'onUploadComplete' : function(file, data) {
        	var url='https://rdappshost.com/toyota/allthings/uploadify/uploads/'+$('#session_code').val()+'/'+data;
			$('#image_url').val(url);
        	getFilterUI();
        	$('#uploadifive-uploadphoto-queue').html('');
        	$('#step4').addClass('valid');

        	$('#photo-preview-box').html('<img src="'+url+'" alt="uploaded image" />');
        	$('#photo-preview-box').fadeIn('fast');
        	$('#uploaded-image').val(url);

	        startImageCheck();
        },
        'onInit':function(instance){
        	$('#uploadifive-uploadphoto').attr('style','cursor:pointer;');
        	$('#uploadifive-uploadphoto').attr('class','uiBtn greyBtn hasIcon smBtn');
        	$('#uploadifive-uploadphoto').prepend('<i class="icon icon-upload-alt"></i>');
        	$('#uploadifive-uploadphoto').find().attr('style',"opacity: 0; position: absolute; left: 0px; height: 100%; top: 0px; cursor: pointer;");
        	var queue=$('#uploadifive-uploadphoto-queue').get(0);
        	$('#uploadifive-uploadphoto-queue').remove();
        	$('#uploadphoto').parent().after(queue);
        }
    }); 
}

function initUploadify(){
	$('#uploadphoto').uploadify({
		'buttonText' : 'Upload photo',
		'hideButton' : true,                                
		'wmode'    : 'transparent',
		'swf'      : 'assets/uploadify/uploadify.swf',
		'uploader' : apiUrl+'uploadify/uploadify.php?session_code='+$('#session_code').val(),
		'fileSizeLimit' : '10MB',
		'fileTypeExts' : '*.jpg; *.png; *.bmp;',
		'preventCaching' : false,
		'folder'    : 'uploadify/uploads',
      	'auto'      : true,
		'height'   : 34,
		'width'	: '100%',
		'onInit'   : function(instance) {
			var queue=$('#uploadphoto-queue').get(0);
        	$('#uploadphoto-queue').remove();
        	$('#uploadphoto').parent().after(queue);
		},
		'onUploadSuccess' : function(file, data, response) {

			var url='https://rdappshost.com/toyota/allthings/uploadify/uploads/'+$('#session_code').val()+'/'+data;
			$('#image_url').val(url);
        	getFilterUI();
        	$('#uploadphoto-queue').html();
        	$('#step4').addClass('valid');

        	$('#photo-preview-box').html('<img src="'+url+'" alt="uploaded image" />');
        	$('#photo-preview-box').fadeIn('fast');
        	$('#uploaded-image').val(url);

        	startImageCheck();
        }
	});
}

function initUploadifive2(){
	$('#uploadphoto2').uploadifive({
		'buttonText' : 'Upload Photo',
        'uploadScript' : apiUrl+'uploadify/uploadify.php?session_code='+$('#session_code').val(),
        'fileSizeLimit' : '10MB',
        'fileType'     : 'image',
        'auto'         : true,
        'onFallback'   : function() {
        	initUploadify2();
        },
        'onUploadComplete' : function(file, data) {
        	var url='https://rdappshost.com/toyota/allthings/uploadify/uploads/'+$('#session_code').val()+'/'+data;
			$('#image_url').val(url);
        	getFilterUI();
        	$('#uploadifive-uploadphoto2-queue').html('');
        	$('#stepUpload').addClass('valid');

        	$('#photo-preview-box2').html('<img src="'+url+'" alt="uploaded image" />');
        	$('#photo-preview-box2').fadeIn('fast');
        	$('#uploaded-image2').val(url);

	        startImageCheck();
        },
        'onInit':function(instance){
        	$('#uploadifive-uploadphoto2').attr('style','cursor:pointer;');
        	$('#uploadifive-uploadphoto2').attr('class','uiBtn greyBtn hasIcon smBtn');
        	$('#uploadifive-uploadphoto2').prepend('<i class="icon icon-upload-alt"></i>');
        	$('#uploadifive-uploadphoto2').find().attr('style',"opacity: 0; position: absolute; left: 0px; height: 100%; top: 0px; cursor: pointer;");
        	var queue=$('#uploadifive-uploadphoto2-queue').get(0);
        	$('#uploadifive-uploadphoto2-queue').remove();
        	$('#uploadphoto2').parent().after(queue);
        }
    }); 
}

function initUploadify2(){
	$('#uploadphoto2').uploadify({
		'buttonText' : 'Upload photo',
		'hideButton' : true,                                
		'wmode'    : 'transparent',
		'swf'      : 'assets/uploadify/uploadify.swf',
		'uploader' : apiUrl+'uploadify/uploadify.php?session_code='+$('#session_code').val(),
		'fileSizeLimit' : '10MB',
		'fileTypeExts' : '*.jpg; *.png; *.bmp;',
		'preventCaching' : false,
		'folder'    : 'uploadify/uploads',
      	'auto'      : true,
		'height'   : 34,
		'width'	: '100%',
		'onInit'   : function(instance) {
			var queue=$('#uploadphoto2-queue').get(0);
        	$('#uploadphoto2-queue').remove();
        	$('#uploadphoto2').parent().after(queue);
		},
		'onUploadSuccess' : function(file, data, response) {

			var url='https://rdappshost.com/toyota/allthings/uploadify/uploads/'+$('#session_code').val()+'/'+data;
			$('#image_url').val(url);
        	getFilterUI();
        	$('#uploadphoto2-queue').html();
        	$('#step4').addClass('valid');

        	$('#photo-preview-box2').html('<img src="'+url+'" alt="uploaded image" />');
        	$('#photo-preview-box2').fadeIn('fast');
        	$('#uploaded-image2').val(url);

        	startImageCheck();
        }
	});
}

// (end)UPLOADER

function startImageCheck (argument) {
	setTimeout(function(){
    	adjustContainment();
		updateData();

		startImageCheck();
    },500);
}

// FILTER

function getFilterUI(){
	$('#containment').find('img').attr('src',$('#image_url').val());
	setTimeout(function(){
		adjustContainment();
		setTimeout(function(){
			
			var new_top=(parseInt($('#containment').height())/2)-(parseInt($('#containment').find('img').height())/2);
			var new_left=(parseInt($('#containment').width())/2)-(parseInt($('#containment').find('img').width())/2);
			$('#containment').find('img').css({
				top:new_top,
				left:new_left
			});

		},1000);
	},1000);
}


/*======================================
=            Filter load/control       =
======================================*/

function filterControls(){
	$('#creator .next').click(function(){
		getNextFilter();
		$('#step2').animate({opacity:1},500);
	});

	$('#creator .prev').click(function(){
		getPrevFilter();
		$('#step2').animate({opacity:1},500);
	});

	$('.toggle').click(function(){
		toggleFilterOutline();
		$('#step2').animate({opacity:1},500);
	});
}

function getFilters(){

	$.ajax({
		url 	:apiUrl+'api/filters',
		cache 	:false,
		async 	:true,
		dataType:'jsonp',
		jsonpCallback:'content',
		crossDomain:true,
		data:{
				
			},
		success: function(r){
				if(r.status=='success'){
					$('#step3').addClass('valid');
					window.filters=r.data;
					window.filterIndex=0;
					loadFilter();
				}
			}
		});
}


function getNextFilter(){
	$('#filter').fadeIn('fast',function(){
		$('#filter_outline').hide();
	});
	if(window.filterIndex!=-1){//if filters have been loaded
		window.filterIndex++;
		if(window.filterIndex>window.filters.length-1){
			window.filterIndex=0;
		}
		loadFilter();
	}
}

function getPrevFilter(){
	$('#filter').fadeIn('fast',function(){
		$('#filter_outline').hide();
	});
	if(window.filterIndex!=-1){//if filters have been loaded
		window.filterIndex--;
		if(window.filterIndex<0){
			window.filterIndex=window.filters.length-1;
		}
		loadFilter();
	}
}

function loadFilter(){
	if(window.filters.length>0){
		var filter=window.filters[window.filterIndex];

		if (!$.support.leadingWhitespace) {
			$('#filter').css('filter',"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+apiUrl+filter.image+"', sizingMethod='scale')");
			$('#filter').css('background','none !important');
			// $('#filter_outline').css('filter',"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+filter.outline_ie+"', sizingMethod='scale')");
			// $('#filter_outline').css('background','none !important');
		} else {
			$('#filter').css('background-image','url('+apiUrl+filter.image+')');
			// $('#filter_outline').css('background-image','url('+filter.outline+')');
			PointerEventsPolyfill.initialize({});
		}
		$('#image_filter').val(filter.id);
	} else {
		alert('There are no filters!');
	}
}

function toggleFilterOutline(){
	$('#filter').fadeToggle('fast',function(){
		$('#filter_outline').toggle();
	});
}

/*-----  End of Filter load/control  ------*/



function adjustContainment(){
	var e=$('#containment');
	if(e.find('img').width()>=e.find('img').height()){
		sliderMin=(parseInt($('#image_box').width())/parseInt(e.find('img').width()))*50;
	} else {
		sliderMin=(parseInt($('#image_box').height())/parseInt(e.find('img').height()))*50;
	}
	if(!sliderMin||sliderMin==null||sliderMin=='undefined'){
		sliderMin=1;
	}
	var cTop=parseInt(e.find('img').height())/2;
	var cHeight=parseInt(e.find('img').height())+parseInt($('#image_box').height());
	var cLeft=parseInt(e.find('img').width())/2;
	var cWidth=parseInt(e.find('img').width())+parseInt($('#image_box').width());

	//corner locks as percentage of containment which is 2x Height and 2x the Width
	var posPercX=parseInt(e.find('img').css('left'))/cWidth;
	var posPercY=parseInt(e.find('img').css('top'))/cHeight;

	if(parseInt(e.find('img').height())<parseInt($('#image_box').height())){
		cTop=parseInt(e.find('img').height())/2;
		cHeight=parseInt($('#image_box').height())+parseInt(e.find('img').height());
	}
	if(parseInt(e.find('img').width())<parseInt($('#image_box').width())){
		cLeft=parseInt(e.find('img').width())/2;
		cWidth=parseInt($('#image_box').width())+parseInt(e.find('img').width());
	}
	e.css({ top:-cTop,left:-cLeft,height:cHeight,width:cWidth});
	e.find('img').css('top',cHeight*posPercY);
    e.find('img').css('left',cWidth*posPercX);
	updateData();
}

function addControls(){
	var e=$('#containment');

	e.attr('rel',e.find('img').height());


	$('.move-up,.move-down,.move-left,.move-right').click(function(){

		var gap=50;

		if($(this).hasClass('move-up')){
			e.find('img').css('top',parseInt(e.find('img').css('top'))-gap);
		} else if($(this).hasClass('move-down')){
			e.find('img').css('top',parseInt(e.find('img').css('top'))+gap);
		} else if($(this).hasClass('move-left')){
			e.find('img').css('left',parseInt(e.find('img').css('left'))-gap);
		} else if($(this).hasClass('move-right')){
			e.find('img').css('left',parseInt(e.find('img').css('left'))+gap);
		}

		updateData();
	});

	$('.zoom-in').click(function(){
		var scale=parseFloat($(this).parent().attr('rel'));
		if(scale<2){
			scale+=0.1;
			$(this).parent().attr('rel',scale);
		}
		e.find('img').css('height',(scale)*e.attr('rel'));

		adjustContainment();
	    updateData();
	});

	$('.zoom-out').click(function(){
		var scale=parseFloat($(this).parent().attr('rel'));
		if(scale>0.1){
			scale-=0.1;
			$(this).parent().attr('rel',scale);
		}
		e.find('img').css('height',(scale)*e.attr('rel'));

		adjustContainment();
	    updateData();
	});

}

function updateData(){
	var e=$('#containment');
	$('#image_top').val(parseInt(e.find('img').css('top'))+parseInt(e.css('top')));
	$('#image_left').val(parseInt(e.find('img').css('left'))+parseInt(e.css('left')));
	$('#image_height').val(parseInt(e.find('img').height()));
	$('#image_width').val(parseInt(e.find('img').width()));
}
//(end) FILTER

// IMAGE CREATION
function createImage(){

	$.ajax({
		url 	:apiUrl+'api/image/create',
		cache 	:false,
		async 	:true,
		dataType:'jsonp',
		jsonpCallback:'content',
		crossDomain:true,
		data:{
				session_code: $('#session_code').val(),
				//
				image_url: $('#image_url').val(),
				image_filter: $('#image_filter').val(),
				image_message: $('#message').val(),
				image_top: $('#image_top').val(),
				image_left: $('#image_left').val(),
				image_height: $('#image_height').val(),
				image_width: $('#image_width').val()
			},
		success: function(r){
				if(r.status=="success"){
					$('#image_short_url').val(r.data.image_short_url);

					$('#step5').fadeOut('fast',function(){
						$('#step6').fadeIn('fast');
						scrollToComp();
						$('#toggleOverlay').hide();
					});

				} else {
					alert('Error saving your data! Please refresh and try again.');
				}			
			}
		});
			
}

function completeNomination(){

	XDomainData('api/nominee',{
		session_code: $('#session_code').val(),
		//
		nominee_name:$('#nominee_name').val(),
		nominee_why:$('#nominee_why').val(),
		//
		user_title:$('#user_title').val(),
		user_name:$('#user_name').val(),
		user_email:$('#user_email').val(),
		user_phone:$('#user_phone').val(),
		user_tandcs:$('#user_tandcs:checked').val(),
		//
		image_url: $('#image_url').val()
	},function(response){
		if(response.status=='success'){
			runFloodlight();
			$('#step2').fadeOut('fast',function(){
				$('#step3').fadeIn('fast');
				scrollToComp();
			});
		} else {
			alert('Error saving your data! Please refresh and try again.');
		}
	});
}

// (end) IMAGE CREATION

/*
* To be used when the terms and conditions button is loaded.
*/
function initFacebox(){
	$('a[rel*=facebox]').facebox();	
}

/*
* Form validation
* - add/remove fields for validation below.
* Structure for form:
* 	<div class="input_text"><input type="text" id="name" value="" /></div>
* and for email add email class to surrounding div:
*	<div class="input_text email"><input type="text" id="name" value="" /></div>
*/
function validate(e){
	var valid=true;
	e.find('.input').each(function(){
		if(!$(this).hasClass('not-required')){
			if($(this).find('input,textarea,select').length>0){
				var isEmail=false;var isCheck=false;
				if($(this).hasClass('email')){ isEmail=true;
				} else if($(this).hasClass('checkbox')){ isCheck=true; }
				var s=$(this).find('input,textarea,select');
				if(s.val()==''||s.val()==s.attr('rel')||(isEmail&&!self.isValidEmailAddress(s.val()))||(!$(s).is(':checked')&&isCheck)){
					s.parent().removeClass('valid');
					s.parent().addClass('invalid');
					valid=false;
				} else {
					s.parent().removeClass('invalid');
					s.parent().addClass('valid');
				}
			}
		}
	});
	return valid;
}

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}


/*
* Resizes the facebook tab after new content has been loaded.
*/
function topOfPage(){
	if (typeof(FB)!='undefined'&&FB!=null){
		FB.Canvas.scrollTo(0,0);
		FB.Canvas.setAutoGrow(false);
		FB.Canvas.setSize({height:100});
		setTimeout(function(){
			FB.Canvas.setAutoGrow(true);
		}, 100);
	}	
}

(function ($, undefined) {
    $.fn.getCursorPosition = function() {
        var el = $(this).get(0);
        var pos = 0;
        if('selectionStart' in el) {
            pos = el.selectionStart;
        } else if('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    }
})(jQuery);
var Search=function(){
	o={
		$el:$('#search'),
		time:50,
		lastValue:'',
		sinceEdit:0,
		focus:false,
		searched:false,

		init:function(){

			this.loop();
			this.events();
			return this;

		},

		events:function(){
			var search=this;

			search.$el.clearField();

			search.$el.focus(function(){
				search.focus=true;
			}).blur(function(){
				search.focus=false;
			});

		},

		check:function(){

			this.sinceEdit+=this.time;

			if(this.$el.val()!=this.lastValue){
				this.changeOccured();
			}

			if(this.sinceEdit>1000 && !this.searched){
				this.searched=true;
				imgFeed.search((this.$el.val()==this.$el.attr('rel')) ? '' : this.$el.val());
			}
			
		},

		loop:function(){
			var search=this;

			setTimeout(function(){
				
				search.check();
				search.loop();

			},search.time);
		},

		changeOccured:function(){
			this.searched=false;
			this.sinceEdit=0;
			this.lastValue=this.$el.val();
		
		}
	};

	return o.init();
};