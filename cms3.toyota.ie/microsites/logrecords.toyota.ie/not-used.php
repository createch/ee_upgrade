<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <link type="text/css" rel="stylesheet" href="css/style.css"></script>
    </head>
    <header>
        <h1>Change control records log</h1>
        <input type='button' value='Show Checked-out only' />
    </header>

    <h2>Radical</h2>
    {exp:channel:entries channel="log-records-radical"}
    <table class="altrowstable" id="table1">
        <tr>
            <th>Date</th>
            <th>Website Name</th>
            <th>Files to be modified</th>
            <th>Description</th>
            <th>Developer Name</th>
            <th>Status</th>
        </tr>
        {log-records-radical orderby="files_modified"}
        <tr>
            <td>{log-records-radical:current_date format="%D, %F %d, %Y - %g:%i:%s"}</td>
            <td>{log-records-radical:website_name}</td>
            <td><div class="database"><p>{log-records-radical:database}</p></div>{log-records-radical:files_modified}</td>
            <td>{log-records-radical:description}</td>
            <td>{log-records-radical:name}</td>
            <td>{log-records-radical:status}</td>
        </tr>
        {/log-records-radical}
    </table>

    <br><br>
            {/exp:channel:entries}

            <h2>Toyota</h2>
            {exp:channel:entries channel="log-records-toyota"}
            <table class="altrowstable" id="table2">
                <tr>
                    <th>Date</th>
                    <th>Website Name</th>
                    <th>Files to be modified</th>
                    <th>Description</th>
                    <th>Developer Name</th>
                    <th>Status</th>
                </tr>
                {log-records-toyota orderby="files_modified"}
                <tr> 
                    <td>{log-records-toyota:current_date format="%D, %F %d, %Y - %g:%i:%s"}</td>
                    <td>{log-records-toyota:website_name}</td>
                    <td><div class="database"><p>{log-records-toyota:database}</p></div>{log-records-toyota:files_modified}</td>
                    <td>{log-records-toyota:description}</td>
                    <td>{log-records-toyota:name}</td>
                    <td>{log-records-toyota:status}</td>
                </tr>
                {/log-records-toyota}
            </table>

            {/exp:channel:entries}





            </html>