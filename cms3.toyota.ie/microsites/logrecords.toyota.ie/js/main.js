
$(document).ready(function() {
    $('input').click(function() {
        $('td:contains("Checked-in")').parent().toggle();
    });

    $('td').each(function(i) {
        // var database = $(".database").html();
        if ($(this).find('p').html() == 'Yes') {
            $(this).find('.database').addClass('show');
            $(this).find('.database').html('<p>Changes requires(d) database update</p>');
        } else {
            $(this).find('.database').html('<p></p>');
        }
    });
});

