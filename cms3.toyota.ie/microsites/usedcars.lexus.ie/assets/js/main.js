$(function() {
	
	$("#more_filters").click(function() {
		$container = $("#car_filter");
		$container.toggleClass("open");
		
		if ($container.hasClass("open")) {
			$(this).find("text").text("Less search options");
			$container.find(".search-simple").toggleClass("open");
		} else {
			setTimeout(function(){
				$container.find(".search-simple").toggleClass("open");
			}, 800);
			
			$(this).find("text").text("More search options");
		}
	});

	//function takes two params - elements to watch, aspect ratio
	VideoResizing.bindResizing($('.video'), 16 / 9);

	FormElements.bindShowMore($('.show-search-detailed'), $('.search-detailed'));
	FormElements.bindSelects($('.select'));
	FormElements.bindCheckboxes($('.checkbox'));
	FormElements.bindViewControls($('.results'), $('.show-grid'), $('.show-list'));	
});

/* 
 ###########################################################################################################
 ########################## BINDINGS START  ################################################################
 ###########################################################################################################
 */
$(function () {

	/* 
	 #############################################################################
	 Prind buttons
	 #############################################################################
	 */

	$(".printpage").click(function () {
		window.print();
	});

	/* 
	 #############################################################################
	 Filterable content remove empty item from menu
	 #############################################################################
	 */
	$(".filterable-content").each(function (index) {
		var $container = $(this);
		$container.find(".fc-group").each(function () {
			var $group = $(this);
			var name = false;

			if ($group.hasClass("exterior-group"))
				name = "exterior-group";
			else if ($group.hasClass("interior-group"))
				name = "interior-group";
			else if ($group.hasClass("engines-group"))
				name = "engines-group";
			else if ($group.hasClass("safety-group"))
				name = "safety-group";

			var $itms = $group.find(".fc-group-content .item");
			if ($itms.length === 0) {
				var $control = $container.find(".fb-item[data-section-filter='." + name + "']");
				$control.parent().remove();
				$group.remove();
			}
		});
	});

	// horizontall gallery
	TranslationManager.addLocalTranslations({
		"backTo": "Back to"
	});

	window.localisedTranslations = TranslationManager.getLocalTranslations();
	/* 
	 #############################################################################
	 Primary navigation bindings
	 #############################################################################
	 */


	// limit breadcrumb for larger strings
	var bread_length = 0;
	var $bread_text = $(".primary-nav .breadcrumb > li > a  span");
	$bread_text.each(function () {
		bread_length += $(this).text().length;
	});
	if (bread_length > 55) {
		$bread_text.each(function () {
			$el = $(this);
			if ($el.text().length > 18)
				$el.text($el.text().substring(0, 15) + '...');
		});
	}



	PrimaryNavigation.bindFixedNavigation($('.primary-nav'), Math.min($(window).height() / 2));
	PrimaryNavigation.bindFixedNavigation($('.secondary-nav'), Math.min($(window).height() / 2));
	PrimaryNavigation.bindMainMenu($('.primary-nav'));
	PrimaryNavigation.bindSecondaryLinks($('.secondary-nav'));

	//close menu on wen open secondary content
	$("body").on("click", ".nav-content .secContentTrigger", function () {
		$("#main_menu_trigger").click();
		return true;
	});

	$("#main_menu_trigger").click(function () {
		if ($('.secondary-nav').length > 0) {
			if ($('.secondary-nav').css('display') == 'none')
				$('.secondary-nav').css('display', 'block');
			else
				$('.secondary-nav').css('display', 'none');
		}
	})


	/* 
	 #############################################################################
	 Scaled header & footer bindings
	 #############################################################################
	 */

	// Settings
	var fixedNavHeight = 70;
	var contentLenghtVisibleOnLoad = 80;
	var minHeroHeight = 620;
	var $masterWrapper = $('.master-wrapper');
	var $innerWrapper = $('.inner-wrapper');

	// Common element refrences
	var $headerElement = $('.header-container', $masterWrapper).first();
	var $footerElement = $('.footer-container', $masterWrapper).first();
	var $footerDynamicCTA = $('.dynamic-cta', $footerElement).first();

	// Bind footer popup CTA if present
	if ($footerDynamicCTA.length) {
		CommonEffects.footerPopup($footerDynamicCTA, $(window).height() / 1.5);
	}

	// If the header element has the static trigger class bind it
	if ($headerElement.hasClass('fx-header')) {
		CommonEffects.fixedScaledHeader($masterWrapper, $headerElement, contentLenghtVisibleOnLoad, minHeroHeight);
	}

	// If the footer element has the static trigger class bind it
	if ($footerElement.hasClass('fx-footer')) {
		CommonEffects.fixedScaledFooter($masterWrapper, $footerElement, fixedNavHeight, minHeroHeight);
	}

	// If we have both static header & footer, inner wrapper must be at least as tall as the viewport to hide the transition...
	if ($headerElement.hasClass('fx-header') && $footerElement.hasClass('fx-footer')) {
		$(window).on('debouncedresize', function () {
			$innerWrapper.css({"min-height": $(window).height()});
		}).trigger('debouncedresize');
	}

	// Secondary Content bindings
	if (window.location.hostname !== "assuredusedcars.toyota.ie") {
		$('.SecContentContainer').each(function () {
			$(this).SecondaryContent({
				"sectionIDs": $(this).data("id").split("/"),
				"mode": $(this).data("contenttype"),
				"cGroupName": $(this).data("name"),
				"translations": localisedTranslations
			});				
		});
	}



	/* 
	 #############################################################################
	 Generic visual enhancements, no core functionality
	 #############################################################################
	 */

	// Carousel BG
	$('.carousel').each(function () {
		CarouselBG.addCarousel($(this));
	});

	// immitates the background size propery in browser which dont support it
	if (!Modernizr.backgroundsize) {
		CommonEffects.scaleBGs($('.coverBG'));
	}

	CommonEffects.wrapBy($('.slide-up-element'), 4, "<div class='spotlight-row grid_row'><div class='wrapper'></div></div>");

	// Read more rows
	CommonEffects.readMoreRows($('.read-more-rows'), localisedTranslations);

	// remove view more button or the container if it is empty
	$(".spotlight-container").each(function () {
		var x = $(this).find(".more_about_item");
		if (x.length <= 0)
			$(this).hide();
		else if (x.length <= 4)
			$(this).find(".read-more-trigger").remove();
	});

	// Toggle a css class when items come into view
	CommonEffects.toggleClassWhenVisible($('.fadeIn'));

	// Background parralax
	CommonEffects.parraliseBG($('.bg-par-asset'), 422, -229, 0);

	if ($.fn.contentToggle) {
		$("[data-content-toggle]").contentToggle({
			toggleClassOnChange: ["blueBtn", "greyBtn"]
		});
	}

	// create background layers.
	if (Modernizr.csstransforms3d && Development.backgroundLayerScroll) {
		Development.backgroundLayerScroll($("[data-layer-parallax]"));
	}

	// offer spotlight binding.
	$(".offer-spotlight .full-terms, .offer-remote .full-terms").each(function () {
		var $text = $(this).prev(".terms");
		if ($text.length) {
			$(this).ReadMore({
				"translations": $.extend(localisedTranslations, {"link-text": $text.text()}),
				"replacementTextTranslationKey": "link-text"
			});
			$text.css("display", "none");
		}
	});


	// dinamically load content
	$(".ajax-content").html('<div id="#cboxLoadingGraphic">&nbsp;</div>');

	/*$(".ajax-content").load($('.ajax-content').attr('rel'), function() {
	 wrapGrid();
	 });*/

	// Hack for fonts loaded event, replace with real hook...
	setTimeout(function () {
		$(window).trigger("fonts-loaded");
	}, 250);
	setTimeout(function () {
		$(window).trigger("fonts-loaded");
	}, 500);
	setTimeout(function () {
		$(window).trigger("fonts-loaded");
	}, 1000);


	// share tools
	CommonEffects.bindShareTools($(document));
	// quick pick disclaimer
	CommonEffects.bindQuickSpecDislaimer($(document));
	//horixontal gallery
	Development.horizontalGallery($("[data-gallery-horizontal]"));







	/* 
	 #############################################################################
	 Parallax footer and header HEro
	 #############################################################################
	 */


	// Settings
	var fixedNavHeight = 70;
	var contentLenghtVisibleOnLoad = 80;
	var minHeroHeight = 620;
	var $masterWrapper = $('.master-wrapper');

	// Common element refrences
	var $headerElement = $('.header-hero', $masterWrapper).first();
	var $footerElement = $('.footer-hero', $masterWrapper).first();

	// If the header element has the static trigger class bind it
	if ($headerElement.hasClass('fx-header')) {
		CommonEffects.fixedScaledHeader($masterWrapper, $headerElement, contentLenghtVisibleOnLoad, minHeroHeight);
	}

	// If the footer element has the static trigger class bind it
	if ($footerElement.hasClass('fx-footer')) {
		CommonEffects.fixedScaledFooter($masterWrapper, $footerElement, fixedNavHeight, minHeroHeight);
	}

	// If we have both static header & footer, inner wrapper must be at least as tall as the viewport to hide the transition...
	if ($headerElement.hasClass('fx-header') && $footerElement.hasClass('fx-footer')) {
		$(window).on('debouncedresize', function () {
			$innerWrapper.css({"min-height": $(window).height()});
		}).trigger('debouncedresize');
	}

	// immitates the background size propery in browser which dont support it
	if (!Modernizr.backgroundsize) {
		CommonEffects.scaleBGs($('.coverBG'));
	}

	/* 
	 #############################################################################
	 forms
	 #############################################################################
	 */


	$(".contactform input , .contactform select , .contactform textarea").focusin(function () {
		var parent = $(this).parents("div[class^='grid_']");
		parent.first().find(".title1").css("color", "#00a0f0");
	});

	$(".contactform input ,  .contactform select , .contactform textarea").focusout(function () {
		var parent = $(this).parents("div[class^='grid_']");
		parent.first().find(".title1").css("color", "");
	});


	$("input.datepicker").datepicker({
		minDate: +3,
		maxDate: "+1M +20D",
		beforeShowDay: $.datepicker.noWeekends
	});

	$.formUtils.addValidator({
		name: 'select',
		validatorFunction: function (value, $el, config, language, $form) {
			var x = value !== "no_selection";
			return x;
		},
		errorMessage: 'Please select one valid option.',
		errorMessageKey: 'badEvenNumber'
	});


	/* 
	 #############################################################################
	 Grid_3 and grid_6fixes
	 #############################################################################
	 */




	// BINDINGS
	// fix offers grid
	// staff section in about us page
	grid_3_6_stack($(".staff , .fc-group-container"));
	grid_6_stack($(".container_12"));
	grid_more_about_stack($(".spotlight-container"));

	/* 
	 #############################################################################
	 NEWS and OFFERS, Make First 4 lemets grid_6 instead grid_3
	 turn it onto grid_3 when click on any filter button
	 #############################################################################
	 */


	$("#filter").NewsFilter();




	/* 
	 #############################################################################
	 back to menu
	 #############################################################################
	 */


	//smooth scloo to top
	$('.return_menu').on('click', function (e) {
		e.preventDefault();
		$.smoothScroll({
			scrollTarget: '#main_nav'
		});
	});





	/* 
	 #############################################################################
	 SECONDARY NAV
	 #############################################################################
	 */


	var $sec = $(".sec-nav-list");
	var secW = $sec.width();
	var $items = $sec.find("li");


	/* 
	 #############################################################################
	 social.
	 #############################################################################
	 */



	var $activeShare = null;
	$("body").click(function () {
		if ($activeShare) {
			$activeShare.find(".social_box_wrapper").hide();
		}
		$activeShare = null;
	});


	$(".share").click(function (e) {
		e.stopPropagation();
		if ($activeShare) {
			$activeShare.find(".social_box_wrapper").hide();
		}
		$activeShare = $(this);
		$(this).find(".social_box_wrapper").show();

		//hack to display the facebook share
		setTimeout(function () {
			var $span = $("#fbLike > span");
			var $iframe = $span.find("> iframe");
			$span.height(30);
			$iframe.height(30);
		}, 10);
	});


	/* 
	 #############################################################################
	 assured used car detail fix
	 #############################################################################
	 */

	var modelid = $("#assuredmodel");
	if (modelid.length === 1) {
		var bc = $(".breadcrumb span");
		bc.eq(2).text(modelid.text());
	}


	/* 
	 #############################################################################
	 google map secondary content redraw fix
	 #############################################################################
	 */

	$("body").on("click", ".tabtrigger[data-tab='pane-directions-tab']", function (e) {
		resizeMap("#getDirectionsMap");
	});

	$("body").on("click", ".getRoute", function (e) {
		e.preventDefault();
		getRoute($(this).parents(".container_12"));
	});



	/* 
	 #############################################################################
	 youtube videos uploads by users.... we don have control over the code
	 #############################################################################
	 */

	function resizeyoutube() {
		$("iframe[src*='youtube.com/']").each(function (i) {
			var el = $(this);
			el.css("width", "100%");
			var w = el.width();
			el.height(w / 1.778);
		});
	}
	resizeyoutube();
	$(window).resize(resizeyoutube);
	/* 
	 #############################################################################
	 fix some icons in ie7
	 #############################################################################
	 */
	$(".lt-ie8 .close-icon").html("x");
	$(".lt-ie8 .icon-angle-right").html("&raquo;");
	$(".lt-ie8 .icon-angle-left").html("&laquo;");
	$(".lt-ie8 .icon-chevron-down").html("︾");
	$(".lt-ie8 .icon-chevron-up").html("︽");



	/* 
	 #############################################################################
	 Assured Ased Cars 
	 #############################################################################
	 */

	//calculate price
	if ($('.finance_price').length > 0) {

		var model = $('.finance_price').data('rel');
		model = 3;

		$.post('http://finance.toyota.ie/calculator.php?format=feed', {model_string: model},
		function (r) {
			//console.log(r)
			if (typeof r.error === "undefined")
				$('.finance_price').text('€' + r['Monthly Instalments']);
			else
				$('.flex_finance_block').css('display', 'none');
			$('.flex-quote-text').css('display', 'none');
		});

	}

	// change null string by not available text
	$(".ussuredCarValue").each(function () {
		var el = $(this);
		if (el.text() === "null") {
			el.parent().html("Not Available");
		}
	});

	// equal height colum on red call to action
	function resizeContact() {
		var assuredText = $(".assuredContact .text");
		var assuredFlex = $(".assuredFlex .text");
		if (assuredText.length && assuredFlex.length) {
			var h1 = assuredText.height();
			var h2 = assuredFlex.height();
			if (h2 > h1)
				assuredText.height(h2);
		}

	}

	resizeContact();
	$(window).resize(resizeContact);


	loadMap();
	InitAssuredUsedCars();

});
/* 
 ###########################################################################################################
 ############################## BINDINGS END ###############################################################
 ###########################################################################################################
 */





function wrapGrid() {
	stack = $(".groupby2 .grid_6");
	for (var i = 0; i < stack.length; i += 2) {
		stack.slice(i, i + 2).wrapAll("<div class='row'></div>");
	}
}


// add classes every 2 or 4 elements to succesive grid_3
// so they stak correctly even with diferrent sizes
function grid_3_6_stack($wrapper, onlyVisivle) {
	var aux = (onlyVisivle) ? ":visible" : "";
	$wrapper.each(function (index) {
		var $gridElems = $(this).find(".grid_3" + aux);
		for (var i = 2; i < $gridElems.length; i++) {
			var $elem = $gridElems.eq(i);
			$elem.removeClass("m2 m4");
			var m4 = (i) % 4;
			var m2 = (i) % 2;
			if (m4 === 0) {
				$elem.addClass("m4");
				continue;
			} else if (m2 === 0) {
				$elem.addClass("m2");
				continue;
			} else {

			}
		}
	});

}

function grid_6_stack($wrapper) {
	$wrapper.each(function (index) {
		var $gridElems = $(this).find("> .grid_6");
		for (var i = 2; i < $gridElems.length; i++) {
			var $elem = $gridElems.eq(i);
			$elem.removeClass("m2 m4");
			var m4 = (i) % 4;
			var m2 = (i) % 2;
			if (m4 === 0) {
				$elem.addClass("m4");
				continue;
			} else if (m2 === 0) {
				$elem.addClass("m2");
				continue;
			} else {

			}
		}
	});

}

function loadMap(elemId) {
	var selector = ".ajax-content";
	if (elemId) {
		selector = elemId;
	}
	$.each($(selector), function (key, value) {
		$(value).load($(value).data('rel'), function () {
			wrapGrid();
		});
	});
}

function grid_more_about_stack($wrapper, onlyVisivle) {
	var aux = (onlyVisivle) ? ":visible" : "";
	$wrapper.each(function (index) {
		var $gridElems = $(this).find(".more_about_item" + aux);
		for (var i = 2; i < $gridElems.length; i++) {
			var $elem = $gridElems.eq(i);
			$elem.removeClass("m2 m4");
			var m4 = (i) % 4;
			var m2 = (i) % 2;
			if (m4 === 0) {
				$elem.addClass("m4");
				continue;
			} else if (m2 === 0) {
				$elem.addClass("m2");
				continue;
			} else {

			}
		}
	});

}

$(function () {
	/* 
	 #############################################################################
	 Dealer Change requests
	 #############################################################################
	 */

	new offerCarousel("#promotions-carousel");
	new offerCarousel("#spotlight-carousel");
	new offerCarousel("#latest-offers-carousel");
	new offerCarousel("#latest-news-carousel");
	initSecondaryNav();
	$(window).resize(initSecondaryNav);


	function initSecondaryNav() {
		var $dropdown = null, $submenu = null;
		var gap = 350;

		var $container = $('.secondary-nav .grid_12');
		var $sec_nav_list = $container.find(".sec-nav-list");
		var $items = $sec_nav_list.find("li:not(.dropdown)").detach();
		var cont_w = $container.width();
		var items_w = 0;
		$sec_nav_list.empty();
		for (var i = 0, max = $items.length; i < max; i++) {
			var $elem = $items.eq(i);
			if (items_w + gap >= cont_w) {
				if (!$dropdown)
					createDropdown();
				$elem.detach().appendTo($submenu);
			} else {
				$sec_nav_list.append($elem);
				items_w += $elem.width();
			}
		}

		if ($dropdown)
			$sec_nav_list.append($dropdown);

		function createDropdown() {
			$dropdown = $('<li class="dropdown" style="display: inline-table;"><a href="#">More &nbsp;<i class="icon icon-chevron-down display-md"></i> </a></li>')
			$submenu = $('<ul class="dropdown-sub-menu"></ul>');
			$dropdown.append($submenu);
		}
	}


//	/// test te embed carsearch details.
//	var carSearchElems = null;
//	var $carSearch = $("#carsearch");
//	var $carDetails = $("#car_details");
//	$('body').on("click","#resultsAsuredUsedCars a",function(e){
//		e.preventDefault();
//		var $link = $(this);
//		var href = $link.attr("href");
//		$carDetails.load( href+" #car-details",function(){
//			$carSearch.css("display","none");
//		});
//	});
//
//	
//	$('body').on("click","#carback",function(e){
//		e.preventDefault();
//		$carDetails.empty();
//		$carSearch.attr("style","");
//		CarouselBG.addCarousel($carSearch.find(".carousel"));
//	});
});

/**
 * Horizontally scrolling gallery
 * @author Luke Channings
 * @date 04/09/13
 * @lastModified 05/09/13
 */
(function($, tr, Modernizr) {

    // ensure window.Development exists as an object.
    window.Development = typeof window.Development === "object" ? window.Development : {};

    window.Development.horizontalGallery = function($rootGalleryNodes, options) {
        return $rootGalleryNodes.each(function() {
            var ins = $.extend(true, {}, HorizontalGallery, {});
            ($.extend(true, ins, { options: options, $node: $(this) })).init();
        });
    };

    var HorizontalGallery = {

        /**
         * initialises the horizontal gallery.
         */
        init: function() {
            var self = this;
            self.createUsageIndicator();
            self.layoutRows();
            self.$node.bind(self.options.destroyUsageIndicatorEvents, $.proxy(self.destroyUsageIndicator, self) );
            if ( ! Modernizr.touch ) self.initialiseNavigation();
            self.$node.find("." + self.options.galleryRowClass).each(function() {
                var $this = $(this);
                self.initialiseDragging($this);

                var tallestItemHeight = 100;
                $this.find(".item-content").each(function() {
                    if ($(this).height() > tallestItemHeight) tallestItemHeight = $(this).height();
                });
                $this.find(".item-content").height(tallestItemHeight);
            });
            self.$node.addClass("active");
        },

        // default options.
        options: {
            "usageIndicatorClass": "gallery-horizontal-indicator",
            "usageIndicatorTransitionDurationMs": 500,
            "galleryRowClass": "gallery-horizontal-row",
            "galleryRowInnerClass": "gallery-horizontal-row-inner",
            "destroyUsageIndicatorEvents": "touchstart mouseup",
            "navigationArrowContainerClass": "navigation-arrows",
            "navigationArrowClass": "gallery-nav-arrow",
            "navigationArrowLeftClass": "gallery-nav-arrow left icon icon-angle-left",
            "navigationArrowRightClass": "gallery-nav-arrow right icon icon-angle-right",
            "navigationScrollDistance": 500, // distance in px.
            "navigationScrollSpeedMs": 600,
            "navigationScrollEasing": "easeOutCirc"
        },

        /**
         * creates a usage indicator.
         */
        createUsageIndicator: function() {
            // create the indicator.
            this.$usageIndicator = $("<div>", {
                "class": this.options.usageIndicatorClass,
                "html": $("<p>", {
                    "text" : Modernizr.touch? tr("Drag to navigate") :  tr("Drag or use arrow controls")
                })
            }).appendTo(this.$node);
        },

        /**
         * transitions the indicator out and removes it from the DOM tree.
         */
        destroyUsageIndicator: function() {
            var self = this;
            self.$usageIndicator = $('.'+ this.options.usageIndicatorClass, self.$node);
            if (!self.usageRemoved) {
                self.usageRemoved = true;
                self.$usageIndicator.css("opacity", 0);
                setTimeout(function() {
                    self.$usageIndicator.remove();
                    self.$usageIndicator = undefined;
                    self.$node.unbind(self.options.destroyUsageIndicatorEvents, self.destroyUsageIndicator);
                }, self.options.usageIndicatorTransitionDurationMs);
            }
        },

        /**
         * Create the gallery layout
         */
        layoutRows: function() {
            var self = this;
            this.$node.find("." + self.options.galleryRowClass).each(function() {
                var $thisRow = $(this); 
                $thisRow.data('left', 0);

                var spaceRequired = 0;
                $('.gallery-item', $thisRow).each(function() {
                    spaceRequired += $(this).width();
                });

                $thisRow.data("width", spaceRequired);
                $thisRow.find("." + self.options.galleryRowInnerClass).width(spaceRequired);

            });
        },

        /**
         * creates navigation arrows for each row in the gallery and binds arrows for each row.
         */
        initialiseNavigation: function() {
            var self = this;
            this.$node.find("." + self.options.galleryRowClass).each(function() {
                var $this = $(this);
                self.createNavigationArrows($this);
                $this.on("click", "." + self.options.navigationArrowClass, function(e) {

                    var target = $(e.target).hasClass("left") ? $this.data('left') - self.options.navigationScrollDistance : $this.data('left') + self.options.navigationScrollDistance;

                    self.moveRow($this, target, true);
                    
                    return false;
                });
            });
        },

        /**
         * creates navigation arrows given a row parameter.
         * @param $row {jHTMLDivElement} the row upon which to create arrows.
         */
        createNavigationArrows: function($row) {
            var nav = { $container: $("<div>", {"class": this.options.navigationArrowContainerClass })};
            nav.$leftArrow = $("<a>", {
                "class": this.options.navigationArrowLeftClass,
                "href": "#",
                "data-icon": "ï„„"
            });
            nav.$rightArrow = $("<a>", {
                "class": this.options.navigationArrowRightClass,
                "href": "#",
                "data-icon": "ï„…"
            });
            nav.$leftArrow.add(nav.$rightArrow).appendTo(nav.$container);
            nav.$container.appendTo($row);
            $row.data("navigation", nav);
            this.updateNavigationArrowState($row);
        },

        /**
         * set the states for navigation arrows, active or inactive.
         */
        updateNavigationArrowState: function($row) {
            var nav = $row.data("navigation");
            if ( nav && nav.$leftArrow && nav.$rightArrow ) {

                var scrollLeftValue = $row.data('left');
                var canMoveRight = scrollLeftValue === 0 || ( (scrollLeftValue + $(document).width()) < $row.data("width") );
                
                nav.$leftArrow[scrollLeftValue === 0  ? "addClass" : "removeClass"]("inactive");
                nav.$rightArrow[ ! canMoveRight ? "addClass" : "removeClass"]("inactive");
            }
        },


        /**
         * Moves the position of the row
         */
        moveRow: function($row, newX, isEndOfEvent) {
            var $innerRow = $('.' + this.options.galleryRowInnerClass, $row);
            var rowWidth = $row.data("width");

            if (isEndOfEvent) {
                if (newX < 0) newX = 0;
                if (newX > rowWidth - $(window).width()) newX = rowWidth - $(window).width();
                $row.data('left', newX);
            }

            if (Modernizr.csstransforms3d) {
                $innerRow.css({
                    "-webkit-transform": "translate3d(" + -newX + "px, 0px, 0px)",
                    "-moz-transform": "translate3d(" + -newX + "px, 0px, 0px)",
                    "transform": "translate3d(" + -newX + "px, 0px, 0px)"
                });
            } else {
                $innerRow.css({ "left": -newX + "px" });
            }
            this.updateNavigationArrowState($row);
        },

        /**
         * initialises drag navigation for the given row.
         * @param $row {jHTMLElement} the row to bind dragging events to.
         */
        initialiseDragging: function($row) {
            var self = this;
            (function($row) {
                var mousedown = false, dragging = false, startPosX, startScrollLeft, touchStart;
                var $innerRow = $row.find("." + self.options.galleryRowInnerClass);
                $row.find("img, a");//.attr("draggable", "false"); // THis kills IE7??? removed for now...
               
                $row.bind("mousedown", function(e) {
                    mousedown = true;
                    if ( self.$usageIndicator ) self.destroyUsageIndicator();
                    startScrollLeft = $row.data('left');
                    startPosX = e.clientX;
                    return false;
                });
                // Bind to window so is caught if outside of the window etc...
                $row.bind("mouseup mouseleave", function(e) {
                    mousedown = false;
                    $row.removeClass("dragging");
                    if (dragging) {
                        var endPos = (startScrollLeft - (e.clientX - startPosX));
                        self.moveRow($row, endPos, true);
                    }
                    dragging = false;
                });
                $row.bind("mousemove", function(e) {
                    if ( mousedown ) {
                        if (!dragging) {
                            $row.addClass("dragging");
                            dragging = true;
                        }
                        var newPos = (startScrollLeft - (e.clientX - startPosX));
                        self.moveRow($row, newPos);
                        
                        // Check cursor has moved
                        if (Math.abs(e.clientX - startPosX)) {
                            var $clickEle = $(e.target);
                            if ( $clickEle.is("a") ) {
                                $clickEle.data("cancel", true);
                            } else {
                                $clickEle.closest("a").data("cancel", true);
                            }
                        }

                        return false;
                    }
                });
                
                // Touch events - for touch devices 
                $row.bind("touchswipe", function(e, phase, direction, distance, duration, terminalVelocity) {

                    //console.log(e, terminalVelocity);

                    if (phase === "START") {
                        startScrollLeft = $row.data('left');
                    }
                    if ((direction === "LEFT" || direction === "RIGHT") && distance) {
                        e.preventDefault();

                        var newPointerPos;
                        if (direction === "LEFT") newPointerPos = startScrollLeft + distance;
                        if (direction === "RIGHT") newPointerPos = startScrollLeft - distance;

                        if (phase === "MOVE") {
                            if (!dragging) {
                                $innerRow.addClass("preventAllAnimations");
                                dragging = true;
                            }
                            
                            self.moveRow($row, newPointerPos, false);
                        }

                        if (phase === "END") {
                            $innerRow.height(); // force reflow
                            $innerRow.removeClass("preventAllAnimations");

                            // Rough attempt at inertial scrolling
                            if (direction === "LEFT") newPointerPos += (200 * terminalVelocity);
                            if (direction === "RIGHT") newPointerPos -= (200 * terminalVelocity);

                            if (dragging) {
                                self.moveRow($row, newPointerPos, true);
                                dragging = false;
                            }
                        }
                    } 
                });
            })($row);
        }
    };

})(window.jQuery, window.TranslationManager.tr, window.Modernizr);


/* 
 #############################################################################
 Form Elements - v0.01
 
 dependencies
 
 Contains all custom form elements functionality
 
 #############################################################################
 */


var FormElements = (function () {

	// Public methods
	return {
		messages: {
			showMore: 'More search options',
			showLess: 'Less search options'
		},
		bindShowMore: function ($elements, $more) {
			var self = this;

			$elements.each(function () {
				var button = $(this);

				button.click(function (e) {
					e.preventDefault();
					setTimeout(function () {
						$more.slideToggle('slow', function () {
							if ($(this).css('display') == 'none') {
								button.find('span').text(self.messages.showMore);
								button.find('i').removeClass('icon icon-chevron-up').addClass('icon icon-chevron-down');
							} else {
								button.find('span').text(self.messages.showLess);
								button.find('i').removeClass('icon icon-chevron-down').addClass('icon icon-chevron-up');
							}
						});
					}, 100);


				});
			});

		},
		bindSelects: function ($elements) {

			$elements.each(function () {
				var selectDiv = $(this);
				//console.log(selectDiv.find(':selected').text());	
				if (selectDiv.find(':selected').length > 0)
					selectDiv.find('.value').text(selectDiv.find(':selected').text());
				else
					selectDiv.find('.value').text(selectDiv.find('option').first().text());
				selectDiv.change(function (e) {
					// this is a hack to display proper text in forms
					var aux = $(e.currentTarget);
					var text = "";
//					if($(e.currentTarget).parents("#form_tabs").length)
//						text = $(e.currentTarget).find('option:selected').text();
//					else
					text = $(e.currentTarget).find('option:selected').text();

					$(e.currentTarget).find('.value').text(text);
				});
			});

		},
		bindCheckboxes: function ($elements) {

			$elements.each(function () {
				var button = $(this);

				var isradio = $();

				button.click(function (e) {
					var buttonInput = button.find('input');
					if (button.hasClass("unique")) {
						var siblings = button.siblings(".unique");
						siblings.each(function () {
							var sibling = $(this);
							var siblinInput = sibling.find('input');
							siblinInput.prop("checked", false);
							sibling.removeClass('active');
						});
					}
					if (buttonInput.is(':checked')) {
						buttonInput.prop("checked", false);
						button.removeClass('active');
					} else {
						buttonInput.prop("checked", true);
						button.addClass('active');
					}
				});

			});

		},
		getCheckboxValues: function (input_name) {
			var values = [];
			$('input[type="checkbox"][name="' + input_name + '"]:checked , input[type="radio"][name="' + input_name + '"]:checked').each(function () {
				values.push($(this).val());
			});
			return values;
		},
		bindViewControls: function ($view, $grid, $list) {

			$grid.click(function (e) {
				e.preventDefault();
				$(".results").removeClass('list').addClass('grid');
				//$(".results").addClass('grid');
				$list.removeClass('active');
				$grid.addClass('active');
			});

			$list.click(function (e) {
				e.preventDefault();
				$(".results").removeClass('grid').addClass('list');
				//$(".results").addClass('list');
				$grid.removeClass('active');
				$list.addClass('active');
			});

		}
	};

})(jQuery);

/* 
 #############################################################################
 Primary navigation - v0.01
 
 dependencies
 - ScrollManager.js
 
 Contains all functionality to make the primary nav fixed to window, and the silde down menu
 #############################################################################
 */


var PrimaryNavigation = (function() {

	// Public methods
	return {
		// Fixed the navigation element via triggering CSS classes on users passes the 'showAfter' scrollY
		bindFixedNavigation: function(navElement, showAfter) {
			ScrollManager.addScrollSubscriber(
					{
						callbackFN: function(topScroll) {
							if (topScroll > showAfter && !navElement.hasClass('reveal')) {
								navElement.addClass('reveal').addClass('revealed');
							}
						},
						fixedThreshold: showAfter
					}
			);
			ScrollManager.addScrollSubscriber(
					{
						callbackFN: function(topScroll) {
							if (topScroll <= 1) {
								navElement.removeClass('reveal');
							}
						},
						fixedThreshold: 0
					}
			);
		},
		// Secondary nav
		bindSecondaryLinks: function($navContainer, options) {
			var config = $.extend({}, {
				useMask: true,
				listSelector: ".sec-nav-list",
				linksSelector: ".sec-nav-list li",
				sectionsSelector: ".page-section",
				headerLink: "Overview",
				footerLink: false,
				footerActiveFromEnd: 150,
				scrollSpeed: 800,
				scrollOffset: 120,
				activeOffset: 300
			}, options);


			var $navItems = $navContainer.find(config.linksSelector);
			$navItems.each(function(index) {
				var $el = $(this);
				if ($el.hasClass("internal-link")) {
					var id = $el.data("id");
					var linked = $(id);
					if (linked.length <= 0)
						$el.remove();
				}
			});

			var $secNavItems = $(config.linksSelector, $navContainer);
			var $listContainer = $(config.listSelector, $navContainer);
			var $secNavSections = $(config.sectionsSelector);
			var sectionActive = false;
			var $mask = false;



			ScrollManager.addScrollSubscriber(
					{
						callbackFN: function(topScroll, isAnimatedScroll) {
							if (isAnimatedScroll)
								return false;
							$activeSection = false;
							$secNavSections.each(function(i) {
								var $thisItem = $(this);

								var thisTop = $thisItem.offset().top - config.activeOffset;

								if (i === 0 && topScroll < thisTop)
									return false; // Exit loop if first item isn't visible yet (assumes order is correct...)

								if (topScroll >= thisTop) {
									$activeSection = $thisItem;
									return true;
								} else {
									return false;
								}
							});

							// Active section is false, no specific page-sections visible yet, so header if link exists...
							if (!$activeSection) {
								setActiveTab(config.headerLink);
							} else {
								// within config.footerActiveFromEnd from bottom of the page
								if ((topScroll + $(window).height() >= $(document).height() - config.footerActiveFromEnd)) {
									setActiveTab(config.footerLink);
								} else {
									setActiveTab($activeSection.data("id"));
								}
							}
						}
					}
			);

			$secNavItems.on("click", function(e) {


				var thisID = $(this).data("id") || false;
				setActiveTab(thisID);

				if (thisID) {
					e.preventDefault();

					var targetScrollY = 0;
					if (thisID === config.headerLink) {
						targetScrollY = 0;
					} else if (thisID === config.footerLink) {
						targetScrollY = $(document).height() - $(window).height();
					} else if ($secNavSections.filter('[data-id="' + thisID + '"]').length) {
						targetScrollY = $secNavSections.filter('[data-id="' + thisID + '"]').offset().top - config.scrollOffset;
					}

					ScrollManager.animatedScrollStart();
					$("html,body").stop().animate({scrollTop: targetScrollY}, config.scrollSpeed, "swing", function(evt) {
						ScrollManager.animatedScrollEnd();
					});
				}
			});

			function setActiveTab(sectionID) {
				var $activeTab;
				if (config.useMask) {
					if (!$mask) {
						$mask = $('<li class="mask" />');
						$listContainer.append($mask);
					}
					$activeTab = $secNavItems.filter('[data-id="' + sectionID + '"]');
					if ($activeTab && $activeTab.length) {
						$mask.css({"opacity": 1, "width": $activeTab.width(), "left": $activeTab.position().left});
					} else {
						$mask.css({"opacity": 0 /*, "left": -$mask.width() */});
					}
				} else {
					$secNavItems.removeClass("active");
					$activeTab = $secNavItems.filter('[data-id="' + sectionID + '"]');
					if ($activeTab && $activeTab.length) {
						$activeTab.addClass("active");
					}
				}
			}
		},
		// Navigation dropdown menu binding
		bindMainMenu: function($navContainer, options) {
			var config = $.extend({}, {
				bindResponse: false,
				isMobile: false,
				contentSelector: ".nav-content",
				pageWrapperSelector: ".outer-wrapper",
				slideTime: 666,
				fadeTime: 700,
				menuHeight: 70
			}, options);

			var $navContentArea = $('<div class="nav-content-area isoForceHardwardCalc" />');
			$navContainer.append($navContentArea);
			var $navListElement = $('.large-nav', $navContainer);
			var $breadCrumb = $('.breadcrumb', $navContainer);
			$(window).on("fonts-loaded", function() {
				var $activeLink = $('.active', $breadCrumb);
				var $activeIcon = $('.active i', $breadCrumb);
				if ($activeIcon.length && $activeLink.length) {
					$activeIcon.css({"left": $activeLink.position().left + ($activeLink.outerWidth(true) / 2) - 5}); // 5 = 50% width of indicator
				}
			});
			var $content;
			var menuOpen = false;
			var menuLoaded = false;

			$('.dropdownnav li', $navContainer).each(function() {
				var $navHolder = $(this);
				var $link = $('a', $navHolder);
				var $icon = $('a i', $navHolder);
				var $models;

				if (!$link.length)
					return true;

				if ($link.attr("href") == window.location.pathname)
					return false;

				var ajaxRequest = $.ajax({url: $link.attr("href"), dataType: 'html'});

				ajaxRequest.done(function(data, textStatus, jqXHR) {
					$content = $(config.contentSelector, data);
					$navContentArea.append(data);
					$models = $('.model-list', $content);
					$link.data('menu-height', $content.outerHeight(true));
					if ($content.outerHeight(true) < $(window).height()) {
						$link.data('menu-height', $(window).height());
					}
					menuLoaded = true;
				});

				$link.on('click', function(e) {
					e.preventDefault();
					if (menuLoaded) {
						var $navContent = $(config.contentSelector, $navContentArea);
						if (!menuOpen) {
							menuOpen = true;
							var heigh =  $(config.contentSelector).outerHeight(true);
							$link.data('menu-height', heigh);
							if (heigh < $(window).height()) {
								$link.data('menu-height', $(window).height());
							}
							ScrollManager.pauseScrollCallbacks();
							$navContainer.data("scrolly", $(window).scrollTop());
							$navHolder.addClass("active");
							$navListElement.addClass("nav-open");
							$icon.removeClass("icon icon icon-chevron-down").addClass("icon icon icon-chevron-up");
							$navContainer.addClass("revealed").addClass("menu-open");
							if (Modernizr.cssanimations) {
								$navContentArea.css("opacity",1);
								$navContentArea.css({"height": $link.data('menu-height')});
							} else {
								$navContentArea.css("opacity",1);
								$navContentArea.stop().animate({"height": $link.data('menu-height')}, config.slideTime);
							}
							setTimeout(function() {
								if (menuOpen) {
									window.scroll(0, 0);
									$navContainer.addClass("noFixed");
									$(config.pageWrapperSelector).css({"display": "none"});
								}
							}, config.slideTime);
						} else {
							setTimeout(function() {
								if (!menuOpen) {
									$navContainer.removeClass("noFixed");
									$navContainer.removeClass("forceFixed");
									$navHolder.removeClass("active");
									$navContent.css({"margin-top": 0});
									$icon.removeClass("icon icon icon-chevron-up").addClass("icon icon icon-chevron-down").height();
									$navContainer.removeClass("menu-open");
								}
							}, config.slideTime);

							$(config.pageWrapperSelector).css({"display": ""});
							$navListElement.removeClass("nav-open").height();
							$navContainer.addClass("forceFixed");
							$navContent.css({"margin-top": -($(window).scrollTop())});
							window.scroll(0, $navContainer.data("scrolly"));
							$(config.pageWrapperSelector).css({"height": "auto", "overflow": "visible"});
							ScrollManager.resumeScrollCallbacks();
							if (Modernizr.cssanimations) {
								$navContentArea.css("opacity",0);
								$navContentArea.css({"height": 0});
							} else {
								$navContentArea.stop().animate({"height": 0}, config.slideTime);
								$navContentArea.css("opacity",1);
							}
							menuOpen = false;
						}
						return false;
					}
				});
			});
		}

	};
}());




/* 
#############################################################################
    Video resizing - v0.01

    dependencies
    - jquery.debouncedresize.js

    Contains all functionality to resize youtube/vimeo embedded videos
#############################################################################
*/ 

var VideoResizing = (function () {

    // Public methods
    return {
        aspectRatio:2,
        // Fixed the navigation element via triggering CSS classes on users passes the 'showAfter' scrollY
        bindResizing:function($elements,$aspectRatio){
            var self=this;

            if($aspectRatio!==undefined){
                self.aspectRatio=$aspectRatio;
            }

            $(window).on('debouncedresize', function () {
        
                $elements.each(function(){
                    var width=parseInt($(this).width(),10);
                    $(this).height(width/self.aspectRatio);
                });

            }).trigger('debouncedresize');
        }
    };

})(jQuery);
;
(function (window, $, has) {

	var layers = [];
	var options = {
		"layerHeightPercentage": 0.65,
		"offsetPoints": [0, 25, 25, 50]
	};
	var dimensions = {};
	var scrollTop = null;
	var $window = $(window);
	var isFirefox = typeof InstallTrigger !== 'undefined';

	/**
	 * stores scroll and offset values and dispatches updates when needed.
	 * @param e {Event} the scroll event.
	 * @returns {Null} nothing.
	 */
	function globalScrollHandler(e, justUpdateScroll, aScrollTop) {
		var _scrollTop = aScrollTop || $window.scrollTop();

		if (_scrollTop !== scrollTop) {
			scrollTop = _scrollTop;

			if (justUpdateScroll)
				return;

			$.each(layers, function () {
				var layer = this;
				var elementData = getElementData(layer.$node);
				var additionalOffset = layer.getAdditionalOffset(elementData.positionOnPage);
				var offset = layer.$node.offset().top - scrollTop;

				if (elementData.elementIsInView) {

					if (layer.$parallaxLayer) {
						layer.$parallaxLayer.css({
							"transform": "translateY(" + offset + "px)",
							"visibility": "visible"
						});
					}
					
					var imageY = (-offset) + additionalOffset;
					if(layer.$parallaxLayer.attr("id") === "fotter-paralax"){
						imageY = Math.min(imageY,-80);
					}
					
					
					layer.$parallaxImage.css({
						"transform": "translateY(" + (imageY) + "px)",
						"visibility": "visible"
					});

				} else {
					layer.$parallaxImage.add(layer.$parallaxLayer).css("visibility", "hidden");
				}
			});
		}
	}

	function globalResizeHandler() {
		var width = $window.width();
		var height = $window.height();
		if (width !== dimensions.width || height !== dimensions.height) {

			dimensions = {
				width: width,
				height: height,
				layerHeight: height * options.layerHeightPercentage
			};

			$.each(layers, function (i) {
				
				var layerH = dimensions.layerHeight;
				if(!isNaN(this.data.factor))
					layerH *= this.data.factor;
				this.offset = this.$node.offset();
				
				
					

				this.$node.add(this.$node.parent()).css({
					"height": layerH,
					"width": width
				});

				if (this.$parallaxLayer) {
					this.$parallaxLayer.css({
						"height": layerH,
						"width": width
					});
				}

				this.$parallaxImage.css({"width": width, "height": height});

				if (i === layers.length - 1)
					globalScrollHandler();
			});
		}
	}

	/**
	 * registers background layers.
	 * @param $backgroundLayers {jHTMLElements} a collection of background layer elements.
	 * @returns {jContext} the jQuery context for chaining.
	 */
	function registerParallaxLayer($backgroundLayers) {
		return (has.touch) ? null : $backgroundLayers.each(function (i) {
			var $this = $(this).hasClass(".background-layer") ? $(this) : $(this).find(".background-layer");
			var $moveIntoParallax = $(this).find(".move-into-parallax");
			var width = $this.data("width");
			var height = $this.data("height");
			var factor = parseFloat($this.data("factor"));
			var id = $this.data("id")
			var src = $this.css("background-image").replace(window.HelperFunctions.cssBackgroundImageRegex, "$1");
			var parallaxLayerDelegateClass = "parallax-layer-";


			if (width && height && src) {

				var layer = {
					data: {"width": width, "height": height, "src": src, factor: factor},
					$node: $this,
					offset: $this.offset(),
					getAdditionalOffset: makeOffsetPoints(options.offsetPoints)
				};

				$.extend(layer, createParallaxLayer(layer));
				if (id && layer.$parallaxLayer)
					layer.$parallaxLayer.attr("id", id);


				if ($moveIntoParallax.length && layer.$parallaxLayer) {

					layer.$parallaxLayer.append(
							$moveIntoParallax
							.removeClass("move-into-parallax")
							.addClass("in-parallax-layer")
							);
				}

				layers.push(layer);

				// set parallax layer delegate for image toggle.
				parallaxLayerDelegateClass += layers.length;
				layer.$node.data("delegateElementSelector", "." + parallaxLayerDelegateClass);
				layer.$parallaxImage.addClass(parallaxLayerDelegateClass);

				$this.css("background-image", "");
			}

			if (i === $backgroundLayers.length - 1) {
				globalResizeHandler();
			}
		});
	}

	function createParallaxLayer(layer) {
		var $parallaxLayer = $("<div>", {"class": "parallax-layer"}).appendTo($("body"));

		var $parallaxImage = $("<div>", {
			"class": "parallax-image",
			"css": {
				"background-image": "url(" + layer.data.src + ")"
			}
		}).appendTo($parallaxLayer ? $parallaxLayer : layer.$node);

		return {"$parallaxLayer": $parallaxLayer, "$parallaxImage": $parallaxImage};
	}

	function makeOffsetPoints(points) {
		var a = points[0], b = points[1], c = points[2], d = points[3];
		return function (t) {
			return (1 - t) * (1 - t) * (1 - t) * a + 3 * (1 - t) * (1 - t) * t * b + 3 * (1 - t) * t * t * c + t * t * t * d;
		};
	}

	function getElementData(element) {
		var offset = element.offset();
		var height = element.height();
		var offsetTop = offset.top - dimensions.height - 50;
		var offsetBottom = offset.top + height + 50;

//        console.log(
//          scrollTop, offsetTop, offsetBottom,
//          scrollTop > offsetTop,
//          scrollTop < offsetBottom
//        )

		return {
			elementIsInView: scrollTop > offsetTop && scrollTop < offsetBottom,
			positionOnPage: Math.round(((scrollTop - offsetTop) / (offsetBottom - offsetTop)) * 100) / 100
		};
	}

	// handlers.
	$window.bind("scroll", globalScrollHandler);
  ScrollManager.addScrollSubscriber(
        {
            callbackFN: function (topScroll) {
                //console.log("CB");
                globalScrollHandler(this, false, topScroll);
            }
        }
    );


	$window.bind("resize", globalResizeHandler);

	window.Development = window.Development || {};
	window.Development.backgroundLayerScroll = registerParallaxLayer;

})(window, window.jQuery, window.Modernizr);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Gets And structure all the cars List
 * @returns {CarlistData}
 */
var Carslist = function () {
	var self = this;
	//cars
	this.cars = null;
	this.queryMeta = null;

	// parsed car data
	this.maker = [];
	this.model = [];
	this.country =[];
	this.makerModel = {
		"Any Make": []
	};
	this.fuelType = [];
	this.bodyTypes = [];
	this.basicColours = ['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'pink', 'brown', 'black', 'grey', 'white', 'silver', 'gold'];
	this.colours = [];
	this.doors = [];
	this.prices = {};
	this.years = {};
	this.mileages = {};



	this.parseCars = function (cars) {
		self.cars = cars;
		for (var i = 0, max = self.cars.length; i < max; i++) {
			self.cars[i] = self.standarizeData(self.cars[i]);
			var car = self.cars[i];
			self.parseMakesModel(car);
			self.parseBodyType(car);
			self.parsePrice(car);
			self.parseCountry(car);
			self.parseColour(car);
			self.parseMileage(car);
			self.parseYear(car);
			self.parseDoors(car);
		}
		// transform prices, years, mileages from object to array
		self.prices = Object.numKeys(self.prices);
		self.years = Object.numKeys(self.years);
		self.mileages = Object.numKeys(self.mileages);
		self.sortAll();
	};

	this.appendCars = function (cars) {
		var appendCars = cars;
		var start = self.cars.length;
		for (var i = start, max = start + appendCars.length; i < max; i++) {
			self.cars[i] = self.standarizeData(appendCars[i - start]);
		}
	};

	this.parseMakesModel = function (car) {
		if (!car.Make || !car.Model)
			return;
		var makerName = car.Make.toUpperCase();
		var modelName = car.Model.toUpperCase();
		if (!self.makerModel[makerName]) {
			self.maker.push(makerName);
			self.makerModel[makerName] = [];
		}
		if ($.inArray(modelName, self.model) === -1)
			self.model.push(modelName);
		if ($.inArray(modelName, self.makerModel["Any Make"]) === -1)
			self.makerModel["Any Make"].push(modelName);
		if ($.inArray(modelName, self.makerModel[makerName]) === -1)
			self.makerModel[makerName].push(modelName);
	};


	this.parseCountry = function(car){
		if(!car.Location)
			return;
		var country = car.Location;
		if ($.inArray(country, self.country) === -1)
			self.country.push(country);
	};
	
	
	this.parseBodyType = function (car) {
		if (!car.BodyType)
			return;
		var BodyType = car.BodyType.toUpperCase();
		if ($.inArray(BodyType, self.bodyTypes) === -1)
			self.bodyTypes.push(BodyType);
	};

	this.parsePrice = function (car) {
		var price = parseInt(car.Price);
		if (isNaN(price) || price < 0)
			return;
		self.prices[price] = true;
	};


	this.parseYear = function (car) {
		var year = parseInt(car.Year);
		if (isNaN(year) || year < 0)
			return;
		self.years[year] = true;
	};

	this.parseColour = function (car) {
		if (!car.Colour)
			return;
		var colourPart = car.Colour.split(" ");
		$.each(colourPart, function (index, value) {
			//console.log(value)
			var color = value.toLowerCase();
			if ($.inArray(color, self.basicColours) !== -1
					&& $.inArray(color, self.colours) === -1) {
				self.colours.push(color);
			}
		});
	};

	this.parseMileage = function (car) {
		var mileage = parseInt(car.Mileage);
		if (isNaN(mileage) || mileage < 0)
			return;
		self.mileages[mileage] = true;
	};


	this.sortAll = function () {
		self.maker.sort();
		self.country.sort();
		self.model.sort();
		self.fuelType.sort();
		self.bodyTypes.sort();
		self.colours.sort();
		self.doors.sort(sortNumber);
		self.prices.sort(sortNumber);
		self.years.sort(sortNumber);
		self.mileages.sort(sortNumber);
		for (var make in self.makerModel) {
			self.makerModel[make].sort();
		}
	};

//	this.parseFuelType = function (car) {
//		var fuel = car.FuelType.toUpperCase();
//		if ($.inArray(fuel, fuelType) === -1)
//			bodyTypes.push(car.FuelType.toUpperCase());
//	};

	this.parseDoors = function (car) {
		var doorsNumber = parseInt(car.Doors) ;
		if (!isNaN(doorsNumber) && $.inArray(doorsNumber, self.doors) === -1)
			self.doors.push(doorsNumber);
	};

	
	/**
	 * This methos standarize the data coming from solar so it can be used normaly by javascript.
	 * Data from solar does't come in an stardard format. i.e: "null" instead null.
	 * Change thimbnail fro default size. 
	 * @param {type} car
	 * @returns {unresolved}
	 */
	this.standarizeData = function (car) {
		for (var i in car) {
			if (car[i] === "null")
				car[i] = null;
		}
		// year
		if (!car.Year)
			car.Year = 0;
		if (!car.Price)
			car.Price = 0;
		if (car.Mileage===null || car.Mileage==="null")
			car.Mileage = -1;
		else{
			car.miles = Math.floor(parseInt(car.Mileage) * 0.621371);
		}
			
		
		
		if (!car.thumbnail) {
			car.thumbnail = 'https://development.toyota.ie/inc/images/noimage.jpg';
		} else {
			var thumbnails = car.thumbnail.split(",");
			if (thumbnails.length)
				car.thumbnail = thumbnails[0];
			car.thumbnail = car.thumbnail.replace('_thumbnail.jpg', '_default.jpg');
		}

		car.PriceF = formatNumber(car.Price);
		car.MileageF = formatNumber(car.Mileage);
		car.milesF =formatNumber(car.miles);
		return car;
	};



	this.reset = function () {
		self.cars = null;
		self.queryMeta = null;
		self.maker = [];
		self.model = [];
		self.makerModel = {
			"Any Make": []
		};
		self.fuelType = [];
		self.bodyTypes = [];
		self.basicColours = ['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'pink', 'brown', 'black', 'grey', 'white', 'silver', 'gold'];
		self.colours = [];
		self.doors = [];
		self.yearMin = 999999999999;
		self.yearMax = 0;
		self.priceMin = 999999999999;
		self.priceMax = 0;
		self.mileageMin = 999999999999;
		self.mileageMax = 0;
	};

};

function sortNumber(a, b) {
	return a - b;
}


function formatNumber(num) {
	if(num)
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
	else
		return num;
}

// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
if (!Object.numKeys) {
	Object.numKeys = (function () {
		'use strict';
		var hasOwnProperty = Object.prototype.hasOwnProperty,
				hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
				dontEnums = [
					'toString',
					'toLocaleString',
					'valueOf',
					'hasOwnProperty',
					'isPrototypeOf',
					'propertyIsEnumerable',
					'constructor'
				],
				dontEnumsLength = dontEnums.length;

		return function (obj) {
			if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
				throw new TypeError('Object.keys called on non-object');
			}

			var result = [], prop, i;

			for (prop in obj) {
				if (hasOwnProperty.call(obj, prop)) {
					var num = parseInt(prop);
						if(!isNaN(num))
							result.push(num);
				}
			}

			if (hasDontEnumBug) {
				for (i = 0; i < dontEnumsLength; i++) {
					if (hasOwnProperty.call(obj, dontEnums[i])) {
						var num = parseInt(dontEnums[i]);
						if(!isNaN(num))
							result.push(num);
					}
				}
			}
			return result;
		};
	}());
}



// Production steps of ECMA-262, Edition 5, 15.4.4.14
// Reference: http://es5.github.io/#x15.4.4.14
// https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(searchElement, fromIndex) {

    var k;

    // 1. Let O be the result of calling ToObject passing
    //    the this value as the argument.
    if (this == null) {
      throw new TypeError('"this" is null or not defined');
    }

    var O = Object(this);

    // 2. Let lenValue be the result of calling the Get
    //    internal method of O with the argument "length".
    // 3. Let len be ToUint32(lenValue).
    var len = O.length >>> 0;

    // 4. If len is 0, return -1.
    if (len === 0) {
      return -1;
    }

    // 5. If argument fromIndex was passed let n be
    //    ToInteger(fromIndex); else let n be 0.
    var n = +fromIndex || 0;

    if (Math.abs(n) === Infinity) {
      n = 0;
    }

    // 6. If n >= len, return -1.
    if (n >= len) {
      return -1;
    }

    // 7. If n >= 0, then Let k be n.
    // 8. Else, n<0, Let k be len - abs(n).
    //    If k is less than 0, then let k be 0.
    k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

    // 9. Repeat, while k < len
    while (k < len) {
      var kValue;
      // a. Let Pk be ToString(k).
      //   This is implicit for LHS operands of the in operator
      // b. Let kPresent be the result of calling the
      //    HasProperty internal method of O with argument Pk.
      //   This step can be combined with c
      // c. If kPresent is true, then
      //    i.  Let elementK be the result of calling the Get
      //        internal method of O with the argument ToString(k).
      //   ii.  Let same be the result of applying the
      //        Strict Equality Comparison Algorithm to
      //        searchElement and elementK.
      //  iii.  If same is true, return k.
      if (k in O && O[k] === searchElement) {
        return k;
      }
      k++;
    }
    return -1;
  };
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//  name of the callbak returned by the server using JsonP
var callbackfunction = "solrCallBack";

/**
 * Query Solr
 * @param {type} $form
 * @returns {CarssQuery}
 */
var CarsQuery = function ($form) {
	var self = this;
	//settings
	this.pagination = 10;
	this.url = location.protocol+"//usedcarstest.lexus.ie/api/solr?queryString=";
	this.queryString = "q=*:*&wt=json&indent=true";
	this.fullQueryString = "q=*:*&wt=json&indent=true&start=0&rows=999999";

	// dealer id
	this.dealerID = $("#searchComponent").data("id");
	console.log(this.dealerID);
	// add an exception for development.toyota.ie to query cogans
	if (this.dealerID === 9999)
		this.dealerID = null;
	if (this.dealerID)
		this.queryString += "&fq=" + encodeURIComponent("DealerId:" + this.dealerID);

	// pagination parameter
	this.start = 0;
	this.end = self.pagination;
	this.sortedQuery = false;
	//query callbacks
	this.fullQueryCallBack;
	this.sortQueryCallback;
	this.nextPageCallBack;
	this.initialQueryCallBack;
	//Search Form Elements
	this.$container = $("#car_filter");
	this.$make = this.$container.find("select[name='Make']");
	this.$model = this.$container.find("select[name='Model']");
	this.$country = this.$container.find("select[name='Country']");
	this.$priceMin = this.$container.find("select[name='Price-min']");
	this.$priceMax = this.$container.find("select[name='Price-max']");
	this.$body = this.$container.find("select[name='BodyType']");
	this.$doors = this.$container.find("select[name='Doors']");
	this.$yearFrom = this.$container.find("select[name='Year-from']");
	this.$yearTo = this.$container.find("select[name='Year-to']");
	this.$mileageMin = this.$container.find("select[name='Mileage-min']");
	this.$mileageMax = this.$container.find("select[name='Mileage-max']");
	this.$transmission = this.$container.find(".transmission .checkboxes");
	this.$fuel = this.$container.find(".fueltype .checkboxes");
	this.$colours = this.$container.find(".colours.checkboxes");
	this.$nct = this.$container.find(".nct .checkboxes");
	this.$dealers = this.$container.find("select[name='Dealer']");

	// controllers
	this.$search = this.$container.find(".perform-search");
	this.$sortField = $("#carSort");
	this.$sortDirection = $("#sortDirection");
	this.$showmore = $("#showMoreCars");
	//query string
	this.filters = null;
	this.query = null;
	this.lastQuery = null;
	this.directionfield = null;

	// ###################### Getter & Setter ############################ 
	this.setPageSize = function (size) {
		self.pagination = size;
	};
	this.setresultView = function (resultsView) {
		self.resultsView = resultsView;
	};
	this.getDirectionField = function () {
		return self.directionfield;
	};


	// ###################### QUERIES ############################ 
	// Queries all cars from solr 
	this.initialQuery = function () {
		self.sortedQuery = false;
		self.start = 0;
		self.end = 999999;
		self.ajaxCall(self.fullQueryString, self.initialQueryCallBack);
		// reset the query to match innitial state only 10 elements
		self.end = self.pagination;
		self.lastQuery = self.fullQueryString;
	};
	//get a new list of cars starting by row 0 anding in pagination
	this.fullQuery = function () {
		self.sortedQuery = false;
		self.start = 0;
		self.end = self.pagination;
		self.buildQuery();
		if (self.lastQuery !== self.query) {
			self.lastQuery = self.query;
			self.ajaxCall(self.query, self.fullQueryCallBack);
		} else {
			//alert("No changes detected in your search.");
		}
	};

	//get a new list of cars starting by row 0 ending in current pagination, and ordered by price, year or KM
	this.sortQuery = function () {
		self.sortedQuery = true;
		self.start = 0;
		//self.end = self.end;
		self.buildOrderedQuery();
		if (self.lastQuery !== self.query) {
			self.lastQuery = self.query;
			self.ajaxCall(self.query, self.sortQueryCallback);
		}
	};

	// get the nex page of the current filters
	this.nextPageQuery = function () {
		self.start += self.pagination;
		self.end += self.pagination;

		if (self.sortedQuery)
			self.buildOrderedQuery();
		else
			self.buildQuery();

		if (self.lastQuery !== self.query) {
			self.lastQuery = self.query;
			self.ajaxCall(self.query, self.nextPageCallBack);
		}
	};
	
	



	// maka any custom query
	this.customQuery = function (start, end, sorted, callback) {
		self.start = start;
		self.end = end;
		if (sorted)
			self.buildOrderedQuery();
		else
			self.buildQuery();
		if (self.lastQuery !== self.query) {
			self.lastQuery = self.query;
			self.ajaxCall(self.query, callback);
		}
	};




// ###################### Bindings ############################
	// binds events, Only read custom events generated by the views
	// never listen standart events.
	this.bindings = function () {
		//click SearchButton
		self.$search.on("SearchButton", function (e) {
			self.fullQuery();
		});

		//click SortBy
		self.$sortField.on("SortBy", function (e) {
			self.sortQuery();
		});

		//click ChangeOrder
		self.$sortDirection.on("ChangeOrder", function (e) {
			self.sortQuery();
		});

		// click ShowMore = next page
		self.$showmore.on("ShowMore", function (e) {
			e.preventDefault();
			self.nextPageQuery();
		});
	};

	// ###################### Set Query CallBacks ############################
	this.handleinitialQuery = function (callback) {
		self.initialQueryCallBack = callback;
	};
	this.handleFullQuery = function (callback) {
		self.fullQueryCallBack = callback;
	};

	this.handleSortQuery = function (callback) {
		self.sortQueryCallback = callback;
	};

	this.handleNextPageQuery = function (callback) {
		self.nextPageCallBack = callback;
	};




	// ###################### SOLR AJAX CALL ############################
	this.ajaxCall = function (queryUrl, callback, error) {
		var successCB = function (data) {
			/** make all reques look the same
			 * data = {
			 *		reponse{
			 *			docs:[]
			 *		},
			 *		resonseHeader{
			 *		}
			 *	}
			 **/
			if (data.docs) {
				data = {
					response: data,
					header: null
				};
			}

			if (callback)
				callback(data);
		};

		var errorCB = function (xhr, status, error) {
			alert("Sorry, an unknown error occurred and we can't load the cars.");
			if (error){
				//console.log(error);
			}	
		};

		var finalurl = self.url + encodeURIComponent(queryUrl);
		$.ajax({
			dataType: 'jsonp',
			jsonpCallback: callbackfunction,
			async: true,
			url: finalurl,
			success: successCB,
			error: errorCB,
			cache: true
		}).error(errorCB);


	};

	// reset pagination values
	this.resetpagination = function () {
		self.start = 0;
		self.end = self.pagination;
	};


	// ###################### Build the query ############################
	// Build the quewry string reading values from the SearchView
	this.buildQuery = function () {
		var search = '';
		//make
		var make = self.$make.val();
		if (!make)
			make = "Any Make";
		if (make !== '' && make !== "Any Make")
			search += '&fq=' + encodeURIComponent('Make:"' + make + '"');
		// model
		var model = self.$model.val();
		if (!model)
			model = "Any Model";
		if (model !== '' && model !== "Any Model")
			search += '&fq=' + encodeURIComponent('Model_exact:"' + model + '"');
		// country
		var country = self.$country.val();
		if (!country)
			country = "Any County";
		if (country !== '' && country !== "Any County")
			search += '&fq=' + encodeURIComponent('Location:*' + country + "*");
		// bodyType
		var bodytype = self.$body.val();
		if (!bodytype)
			bodytype = 'All body types';
		if (bodytype !== 'All body types')
			search += '&fq=' + encodeURIComponent('BodyType:"' + bodytype + '"');
		// doornumber
		var doors = self.$doors.val();
		if (!doors)
			doors = 'All doors';
		if (doors !== 'All doors')
			search += '&fq=' + encodeURIComponent('Doors:' + doors);

		// dealer
		var dealer = self.$dealers.val();
		if (!dealer)
			dealer = 'All dealers';
		if (dealer !== 'All dealers')
			search += '&fq=' + encodeURIComponent('DealerId:' + dealer);

		//year
		var yearfrom = self.$yearFrom.val();
		var yearto = self.$yearTo.val();
		if (yearfrom === null)
			yearfrom = 'From';
		if (yearto === null)
			yearfrom = 'To';
		if (yearfrom !== 'From' || yearto !== 'To') {
			yearfrom = (yearfrom !== 'From') ? yearfrom : '*';
			yearto = (yearto !== 'To') ? yearto : '*';
			search += '&fq=' + encodeURIComponent('Year:[' + yearfrom + ' TO ' + yearto + ']');
		}
		//price
		var pricemin = self.$priceMin.val();
		var pricemax = self.$priceMax.val();
		if (pricemin === null)
			pricemin = 'Min';
		if (pricemax === null)
			pricemax = 'Max';
		if (pricemin !== 'Min' || pricemax !== 'Max') {
			pricemin = (pricemin !== 'Min') ? pricemin : '*';
			pricemax = (pricemax !== 'Max') ? pricemax : '*';
			search += '&fq=' + encodeURIComponent('Price:[' + pricemin + ' TO ' + pricemax + ']');
		}
		//mileage
		var mileagemin = self.$mileageMin.val();
		var mileagemax = self.$mileageMax.val();
		if (mileagemin === null)
			mileagemin = 'Min';
		if (mileagemax === null)
			mileagemax = 'Max';
		if (mileagemin !== 'Min' && mileagemax !== 'Max') {
			mileagemin = (mileagemin !== 'Min') ? mileagemin : '*';
			mileagemax = (mileagemax !== 'Max') ? mileagemax : '*';
			search += '&fq=' + encodeURIComponent('Mileage:[' + mileagemin + ' TO ' + mileagemax + ']');
		}
		//colour
		var colourFilter = "";
		$.each(self.$colours.find('.active'), function () {
			if (colourFilter === "")
				colourFilter = $(this).find('input[type=checkbox]').val();
			else
				colourFilter += '+OR+' + $(this).find('input[type=checkbox]').val();
		});
		if (colourFilter !== "") {
			search += '&fq=' + encodeURIComponent('Colour:(' + colourFilter + ')');
		}
		//transmission¸
		var transmissionFilter = '';
		$.each($('#car_filter .transmission .active'), function () {
			var val = $(this).find('input[type=checkbox]').val();
			if (transmissionFilter === "")
				transmissionFilter = val;
			else
				transmissionFilter += '+OR+' + val;
		});
		if (transmissionFilter !== "") {
			search += '&fq=' + encodeURIComponent("Transmission:(" + transmissionFilter + ")");
		}
		//############################## NCT #####################
//		var nctFilter = '';
//		$.each($('#car_filter .nct .active'), function () {
//			var val = $(this).find('input[type=checkbox]').val();
//			if (nctFilter === "")
//				nctFilter = val;
//			else
//				nctFilter += '+OR+' + val;
//		});
//		if (nctFilter !== "") {
//			search += '&fq=' + encodeURIComponent("NCT:(" + nctFilter + ")");
//		}
		//Fuel¸
		var fuelFilter = '';
		$.each($('#car_filter .fueltype .active'), function () {
			var val = $(this).find('input[type=checkbox]').val();
			var name = $(this).find('input[type=checkbox]').attr('name');
			if (fuelFilter === "")
				fuelFilter = val;
			else
				fuelFilter += '+OR+' + val;
		});
		if (fuelFilter !== "") {
			search += '&fq=' + encodeURIComponent("FuelType:(" + fuelFilter + ")");
		}

		self.filters = search;


		var queryUrl = null;
		queryUrl = (self.filters) ? self.queryString + "&" + self.filters : self.queryString;

		queryUrl += "&start=" + self.start + "&rows=" + (self.end - self.start);

		self.query = queryUrl;
		return queryUrl;
	};

	// Appends 0rder parameters to the query
	this.buildOrderedQuery = function () {
		self.buildQuery();
		self.directionfield = self.$sortField.val();
		if(!self.directionfield)
			return;
		var $icon = self.$sortDirection.find("> .icon");
		var direction = $icon.hasClass("icon-chevron-up") ? "ASC" : "DESC";
		self.query += "&sort=" + encodeURIComponent(self.directionfield) + "+" + direction;
	};

	this.bindings();
};

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var carRender = function (car, index) {
	var year = (car.Year) ? car.Year : "Year-NA";
	var make = (car.Make) ? car.Make : "";
	var model = (car.Model) ? car.Model : "";
	var variant = (car.Variant) ? car.Variant : "";
	var price = (car.Price) ? "&euro;" + car.PriceF : "POA";
	if (make || model)
		model += "<br/>";
	var mileage = (car.Mileage === -1) ? "Mileage-NA" : car.milesF + "M (" + car.MileageF + "Km)";
	var clear =(index%2 === 0)?" clear_left":"";
	
	var carOutput = '<div class="result ' + car.layouth + clear +'" data-date="' + car.Year + '" data-price="' + car.Price + '" data-mileage="' + car.Mileage + '">' + 
			'<div class="image"><a href="/pages/assured-used-car-details/' + car.carID + '"><img src="' + car.thumbnail.replace('http:','https:') + '" alt="' + car.carID + '" ' +
			'width="400" height="300"></a></div>' +
			'<div class="content">' +
			'<div class="padding">' +
			'<h5><a href="/pages/assured-used-car-details/' + car.carID + '"><span><span class="reults_model">' + make + ' ' + model +"</span><span class='reults_variant'>"+ variant + '</span></span></a></h5>' +
			'<div class="price mobile_price">' + price + '</div>' +
			'<div class="details cardetails">';

	carOutput += '<span><span class="year ussuredCarValue" style="text-transform:capitalize">' + year + '</span></span>';
	carOutput += '<span><span class="bull">&nbsp;&bull;</span> <span class="mileage ussuredCarValue" style="text-transform:capitalize">' + mileage + '</span></span>';


	if (car.FuelType)
		carOutput += '<span><span class="bull">&nbsp;&bull;</span> <span class="ussuredCarValue">' + car.FuelType + '</span></span>';
	if (car.FuelType || car.Colour || car.Transmission)
		carOutput += '<br />';
	if (car.Colour)
		carOutput += '<span><span class="ussuredCarValue">' + car.Colour + '</span></span>';
	if (car.Transmission)
		carOutput += '<span><span class="bull">&nbsp;&bull;</span> <span class="ussuredCarValue">' + car.Transmission + '</span></span>';
	if (car.Location)
		carOutput += '<span><span class="bull">&nbsp;&bull;</span> <span class="ussuredCarValue" style="text-transform:capitalize">' + car.Location + '</span></span>';

	carOutput += '<a href="/pages/assured-used-car-details/' + car.carID + '" class="viewBtn view-car">' +
		'<i class="icon icon-chevron-right"></i>' +
		'<span>&nbsp; explore</span>' +
		'</a>' ;

	
	carOutput += '</div>' +
			'<div class="clear"></div>' +
			'</div>' +
			'<div class="price">' + price + '</div>' +
			//'<a href="/pages/assured-used-car-details/' + car.carID + '" class="uiBtn redBtn hasIcon hasIconRight smBtn view-car">' +
			//'<span>View car</span>' +
			//'<i class="icon icon-chevron-right"></i>' +
			//'</a>' +
			'</div>' +
			'<div class="clear"></div>' +
			'</div>';


//	if((index+1)%2 === 0)
//			carOutput += "<div style='font-size:0;clear:both'>&nbsp;</div>";
	return carOutput;
};


var CarsResulstView = function () {
	var self = this;
	// ################# initialization #################
	// DOM elements
	this.$container = $("#loadcars");
	this.$itemsWrapper = this.$container.find("#resultsAsuredUsedCars");
	this.$itemsContainer = this.$container.find(".results");
	this.$loading = this.$container.find("#loading");
	this.$showmore = $("#showMoreCars");
	this.$showing = $("#numvisible");
	this.$total = $("#numtotal");
	this.$pendingCars = $("#pendingCars");
	this.$sort = $("#carSort");
	this.$sortdir = $("#sortDirection");
	this.isMobile = (!$("html.no-touch").length);
	// model
	this.carsList = null;
	// view setting
	this.pageSize = 10;
	this.currentposition = 0;
	this.total = 0;
	this.sortField = null;
	this.sortAscending = true;

	// ################# setters #################
	this.setCarsList = function (carList) {
		self.carsList = carList;
	};
	this.setPageSize = function (size) {
		self.pageSize = size;
	};
	this.setDirectionField = function (directionfield) {
		this.$itemsWrapper.removeClass();
		this.$itemsWrapper.addClass(directionfield);
	};




	// ################# render methods #################

	// reset and render
	this.render = function () {
		self.$loading.hide();
		self.$itemsWrapper.css({opacity: '0'});
		self.$itemsContainer.empty();
		self.currentposition = 0;
		self.renderNextPage();
		self.renderTotal(self.total);
	};

	// render next x elements
	this.renderNextPage = function () {
		var cars = self.carsList.cars;
		var max = Math.min(self.currentposition + self.pageSize, cars.length);
//		var scrollid = new Date().getTime() + "_car";
//		self.$itemsContainer.append("<div id='" + scrollid + "'></div>");
		for (var i = self.currentposition; i < max; i++) {
			var car = cars[i];
			// layouth fix
			car.layouth = ((self.currentposition % 2) === 1) ? "right" : "";
			car.$elem = $(carRender(car,i));
			self.$itemsContainer.append(car.$elem);
			car.$elem.show();
			car.$elem.css("opacity", 1);
			self.currentposition++;

		}
		self.animateContainer();
//		$.smoothScroll({scrollTarget: '#'+scrollid});
		if (max === 0)
			self.renderEmpty();
	};

	// render the "showing x of total" section
	this.renderTotal = function (total) {
		self.total = total;
		self.showing = self.currentposition;
		self.$showing.text(self.showing);
		self.$total.text(self.total);
		this.$pendingCars.text("( "+(self.total - self.showing)+" )");
		if (self.showing >= self.total)
			self.$showmore.hide();
		else
			self.$showmore.show();
		document.cookie = "serach_auc_showing" + "=" + self.showing;
		window.serach_auc_showing = self.showing;
	};
	
	
	this.getShowingNumber  = function (){
		
		var last = getCookie("serach_auc_showing");
		if(last)
			window.serach_auc_total = parseInt(last);
		else
			window.serach_auc_total = this.pageSize;	
		return window.serach_auc_total;
	}
	
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		var object = null;
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ')
				c = c.substring(1);
			if (c.indexOf(name) == 0)
				object = c.substring(name.length, c.length);
		}
		if (object)
			object = JSON.parse(object);
		return object;
	}

	// rendes a no result message
	this.renderEmpty = function () {
		self.$itemsContainer.empty();
		self.$itemsContainer.html(
				'<h3 class="results heading2 center-text no-models">'
				+ 'No models found.</h3>');
	};

	this.renderLoading = function () {
		self.$itemsContainer.empty();
		self.$loading.show();
	};

	this.animateContainer = function () {
		// add little delay to get the $itemsContainer heigth properly.. otherwise it returns 0
		window.setTimeout(function () {
			var h = self.$itemsContainer.outerHeight() + 15;
			//self.$itemsWrapper.stop().animate({height: h}, 1000, "linear");
			self.$itemsWrapper.height(h);
			self.$itemsWrapper.stop().animate({opacity: '1'}, 500);
		}, 400);
	};

	this.setNewDirection = function (e) {
		var $icon = self.$sortdir.find("i");
		self.sortAscending = $icon.hasClass("icon-chevron-up");
		if (!self.sortAscending) {
			$icon.removeClass("icon-chevron-down");
			$icon.addClass("icon-chevron-up");
			if ($(".lt-ie8").length > 0)
				$icon.html("︽");
		} else {
			$icon.removeClass("icon-chevron-up");
			$icon.addClass("icon-chevron-down");
			if ($(".lt-ie8").length > 0)
				$icon.html("︾");
		}
		// set the new direction after change interface;
		self.sortAscending = !self.sortAscending;
		return self.sortAscending;
	};

	this.scrollTop = function () {
		$.smoothScroll({scrollTarget: ".search_result"});
	};

	// ################# Event Input Output #################

	// binds dom events to internal actions
	this.bindEvents = function () {
		//return top smooth
		$('.return_top').on('click', function (e) {
			e.preventDefault();
			$.smoothScroll({
				scrollTarget: '#car_filter'
			});
		});



		//resize continer on winresize
		$(window).resize(self.animateContainer);

		$(".show-list , .show-grid").click(function () {
			setTimeout(function () {
				self.animateContainer();
			}, 100);
		});
		
//		$("body").on("click",".result a",function(e){
//			e.preventDefault();
//			var $iframe = $("<iframe id='fulliframe'  src='"+$(this).attr("href")+"' />");
//			$("body").append($iframe);
//			$("body").height($iframe.find("body").height());
//			$("body").css("overflow","hidden");
//		});
		
	};

	// binds dom events to internal actions that generates a custom event captured by the model
	this.bindOutputEvents = function () {

		//click SortBy
		self.$sort.on("change", function () {
			self.sortField = self.$sort.val();
			self.renderLoading();
			self.$sort.trigger("SortBy");
		});

		//click ChangeOrder
		self.$sortdir.on("click", function (e) {
			e.preventDefault();
			self.setNewDirection();
			if (!self.$sort.val())
				return;
			self.renderLoading();
			self.$sortdir.trigger("ChangeOrder");
		});

		// click ShowMore = next page
		self.$showmore.click(function (e) {
			e.preventDefault();
			self.$showmore.trigger("ShowMore");
		});
	};




	window.setInterval(function () {
		var h = self.$itemsContainer.outerHeight() + 15;
		self.$itemsWrapper.height(h);
	}, 1000);
};
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Search Form View
 * @returns {CarsSearchView}
 */
var CarsSearchView = function () {
	var self = this;
	// ################# initialization #################
	// dealer id
	this.dealerID = $("#searchComponent").data("id");
	// left form
	this.$container = $("#car_filter");
	this.$make = this.$container.find("select[name='Make']");
	this.$model = this.$container.find("select[name='Model']");
	this.$country = this.$container.find("select[name='Country']");
	this.$priceMin = this.$container.find("select[name='Price-min']");
	this.$priceMax = this.$container.find("select[name='Price-max']");
	this.$body = this.$container.find("select[name='BodyType']");
	this.$doors = this.$container.find("select[name='Doors']");
	this.$yearFrom = this.$container.find("select[name='Year-from']");
	this.$yearTo = this.$container.find("select[name='Year-to']");
	this.$mileageMin = this.$container.find("select[name='Mileage-min']");
	this.$mileageMax = this.$container.find("select[name='Mileage-max']");
	this.$transmission = this.$container.find(".transmission .checkboxes");
	this.$fuel = this.$container.find(".fueltype .checkboxes");
	this.$colours = this.$container.find(".colours.checkboxes");
	this.$nct = this.$container.find(".nct .checkboxes");
	this.$dealers = this.$container.find("select[name='Dealer']");

	//buttons
	this.$search = this.$container.find(".perform-search");
	this.$clear = this.$container.find(".clear-fields");
	this.$clear.hide();
	// sort buttons
	this.$sort = $("#carSort");
	this.$sortdir = $("#sortDirection");
	
	// model
	this.carList = null;
	// view setting
	this.priceInterval = 10000;
	this.mileageInterval = 25000;
	this.yearInterval = 1;


	//last state
	this.lastState = restoreFromCookie();
	//removes county from DEFAULT_QUERY if the form has no county field
	if(!this.$country.length){
		delete DEFAULT_QUERY.country;
	}
	
	this.$country;
	// ################# setters #################
	this.setCarsList = function (carList) {
		self.carList = carList;
	};

	// ################# render methods #################


	this.ClearAllFields = function () {
		self.lastState = DEFAULT_QUERY;
		var checkboxes = this.$container.find(".checkbox:not(.unique)");
		checkboxes.find("input").prop("checked", "checked");
		checkboxes.click();
		var uniqueActiveCheckboxes = this.$container.find(".checkbox.unique.active");
		uniqueActiveCheckboxes.click();
		self.render();
		self.$search.click();
		self.$clear.hide();
	};

	this.render = function () {
		if (!self.carList)
			throw new UserException("carList not initialized, use setCarlis() first;");
		self.renderMakeSelect();
		self.renderModelSelect();
		self.renderPriceSelects();
		self.renderCountrySelect();
		self.renderBodySelect();
		self.renderDoorsSelect();
		self.renderYearSelects();
		self.renderTransmission();
		self.renderFuelType();
		self.renderNCT();
		self.renderMileageSelects();
		self.renderColourCheckBoxes();
		self.renderSort();
		var last = JSON.stringify(self.lastState);
		var def = JSON.stringify(DEFAULT_QUERY);
		if( last !== def)
			self.$clear.show();
	};

	this.renderMakeSelect = function (event) {
		self.$make.empty();
		self.$make.append('<option value="Any Make" >Any Make</option>');
		var lastEstate = self.lastState.make;
		for (var i = 0, max = self.carList.maker.length; i < max; i++) {
			var maker = self.carList.maker[i];
			var selected = (lastEstate === maker) ? "selected" : "";
			self.$make.append('<option value="' + maker + '" ' + selected + '>' + maker + '</option>');
		}
		self.$make.find("option:selected").change();
	};
	

	this.renderModelSelect = function () {
		self.$model.empty();
		self.$model.append('<option value="Any Model" selected>Any Model</option>');

		var $selected = self.$make.find("option:selected");
		var maker = ($selected.length) ? $selected.text() : "Any Make";
		var models = self.carList.makerModel[maker];
		var lastEstate = self.lastState.model;
		for (var i = 0, max = models.length; i < max; i++) {
			var model = self.carList.makerModel[maker][i];
			var selected = (lastEstate === model) ? "selected" : "";
			self.$model.append('<option value="' + model + '" ' + selected + '>' + model + '</option>');
		}
		self.$model.find("option:selected").change();
	};

	this.renderPriceSelects = function () {
		self.$priceMin.empty();
		self.$priceMin.append('<option value="Min" selected>Min</option>');
		self.$priceMax.empty();
		self.$priceMax.append('<option value="Max" selected>Max</option>');

		var values = getMinMaxArray(self.carList.prices, 20);
		var lastMinEstate = self.lastState.priceMin;
		var lastMaxEstate = self.lastState.priceMax;
		for (var i = 0, max = values.length; i < max; i++) {
			var v = (i < (max / 2))
					? Math.floor(values[i] / 1000) * 1000
					: Math.ceil(values[i] / 1000) * 1000;
			var vf = formatNumber(v);
			var st = "" + v;
			var minSelected = (lastMinEstate === st) ? "selected" : "";
			var maxSelected = (lastMaxEstate === st) ? "selected" : "";
			self.$priceMin.append('<option value="' + v + '" ' + minSelected + '>' + vf + '</option>');
			self.$priceMax.append('<option value="' + v + '" ' + maxSelected + '>' + vf + '</option>');
		}
		self.$priceMin.find("option:selected").change();
		self.$priceMax.find("option:selected").change();
	};

	this.renderCountrySelect = function () {
		var lastEstate = self.lastState.country;
		var $lastCounty = self.$country.find("option[value='" + lastEstate + "']");
		$lastCounty.prop('selected', 'selected');
		$lastCounty.change();
	};

	this.renderBodySelect = function () {
		self.$body.empty();
		self.$body.append('<option value="All body types" selected>All body types</option>');
		var lastEstate = self.lastState.body;
		for (var i = 0, max = self.carList.bodyTypes.length; i < max; i++) {
			var body = self.carList.bodyTypes[i];
			var selected = (lastEstate === body) ? "selected" : "";
			self.$body.append('<option value="' + body + '" ' + selected + '>' + body + '</option>');
		}
		self.$body.find("option:selected").change();
	};

	this.renderDealers= function () {
		if(!this.dealerID){
			var lastEstate = self.lastState.dealer;
			var $lastDealer = self.$dealers.find("option[value='" + lastEstate + "']");
			$lastDealer.prop('selected', 'selected');
			$lastDealer.change();
		}else{
			this.dealer.css('display','none');
		}
	};



	this.renderDoorsSelect = function () {
		self.$doors.empty();
		self.$doors.append('<option value="All doors"">All doors</option>');
		self.$doors.append('<option value="' + 2 + '"">' + 2 + '</option>');
		self.$doors.append('<option value="' + 3 + '"">' + 3 + '</option>');
		self.$doors.append('<option value="' + 4 + '"">' + 4 + '</option>');
		self.$doors.append('<option value="' + 5 + '"">' + 5 + '</option>');

		var lastEstate = self.lastState.doors;
		var $lastDoor = self.$doors.find("option[value='" + lastEstate + "']");
		$lastDoor.prop('selected', 'selected');
		$lastDoor.change();
	};

	this.renderYearSelects = function () {
		self.$yearFrom.empty();
		self.$yearFrom.append('<option value="From" selected>From</option>');
		self.$yearTo.empty();
		self.$yearTo.append('<option value="To" selected>To</option>');

		//var values = getMinMaxArray(self.carList.years, 10);
		var values = self.carList.years;
		var lastMinEstate = self.lastState.yearFrom;
		var lastMaxEstate = self.lastState.yearTo;
		for (var i = 0, max = values.length; i < max; i++) {
			var v = values[i];
			if (!v)
				continue;
			var st = "" + v;
			var minSelected = (lastMinEstate === st) ? "selected" : "";
			var maxSelected = (lastMaxEstate === st) ? "selected" : "";
			self.$yearFrom.append('<option value="' + v + '" ' + minSelected + '>' + v + '</option>');
			self.$yearTo.append('<option value="' + v + '" ' + maxSelected + '>' + v + '</option>');
		}
		self.$yearFrom.find("option:selected").change();
		self.$yearTo.find("option:selected").change();
	};



	this.renderMileageSelects = function () {
		self.$mileageMin.empty();
		self.$mileageMin.append('<option value="Min" selected>Min</option>');
		self.$mileageMax.empty();
		self.$mileageMax.append('<option value="Max" selected>Max</option>');

		var values = getMinMaxArray(self.carList.mileages, 20);
		var lastMinEstate = self.lastState.mileageMin;
		var lastMaxEstate = self.lastState.mileageMax;
		for (var i = 0, max = values.length; i < max; i++) {
			var v = (i < (max / 2))
					? Math.floor(values[i] / 1000) * 1000
					: Math.ceil(values[i] / 1000) * 1000;
			var vf = formatNumber(v);
			var st = "" + v;
			var minSelected = (lastMinEstate === st) ? "selected" : "";
			var maxSelected = (lastMaxEstate === st) ? "selected" : "";
			self.$mileageMin.append('<option value="' + v + '" ' + minSelected + '>' + vf + '</option>');
			self.$mileageMax.append('<option value="' + v + '" ' + maxSelected + '>' + vf + '</option>');
		}
		self.$mileageMin.find("option:selected").change();
		self.$mileageMax.find("option:selected").change();
	};

	this.renderColourCheckBoxes = function () {
		self.$colours.empty();
		for (var i = 0, max = self.carList.colours.length; i < max; i++) {
			var colour = self.carList.colours[i];
			self.$colours.append(
					'<div class="checkbox colour">' +
					'<div class="the-checkbox">' +
					'<div class="colour-block" style="background: ' + colour.toLowerCase() + ';"></div>' +
					'</div>' +
					'<input type="checkbox" name="' + colour + '" id="' + colour + '" value="' + colour + '">' +
					'<label for="' + colour + '">' + colour.toUpperCase() + '</label>' +
					'</div>'
					);
		}
		self.$colours.append(
				'<div class="checkbox colour">' +
				'<div class="the-checkbox">' +
				'<div class="colour-block" style="background:white;"></div>' +
				'</div>' +
				'<input type="checkbox" name="OTHER" id="OTHER" value="*">' +
				'<label for="' + this + '">OTHER</label>' +
				'</div>'
				);
		FormElements.bindCheckboxes(self.$colours.find(".checkbox"));

		var checkedList = self.lastState.colours.split(":");
		for (var i = 0, max = checkedList.length; i < max; i++) {
			var color = checkedList[i];
			if (!color)
				continue;
			var $input = self.$colours.find("input[value='" + color + "']");
			//not mark inputs as checked id state is not cheked it is schanged by formelement automatically
			//$input.prop("checked","checked");
			var parent = $input.parents(".checkbox");
			parent.click();
		}
	};



	this.renderTransmission = function () {
		var checkedList = self.lastState.transmission.split(":");
		for (var i = 0, max = checkedList.length; i < max; i++) {
			var last = checkedList[i];
			if (!last)
				continue;
			var $input = self.$transmission.find("input[value='" + last + "']");
			//not mark inputs as checked id state is not cheked it is schanged by formelement automatically
			//$input.prop("checked","checked");
			var parent = $input.parents(".checkbox");
			parent.click();
		}
	};

	this.renderFuelType = function () {
		var checkedList = self.lastState.fuel.split(":");
		for (var i = 0, max = checkedList.length; i < max; i++) {
			var last = checkedList[i];
			if (!last)
				continue;
			var $input = self.$fuel.find("input[value='" + last + "']");
			//not mark inputs as checked id state is not cheked it is schanged by formelement automatically
			//$input.prop("checked","checked");
			var parent = $input.parents(".checkbox");
			parent.click();
		}
	};

	this.renderNCT = function () {
		var checkedList = self.lastState.nct.split(":");
		for (var i = 0, max = checkedList.length; i < max; i++) {
			var last = checkedList[i];
			if (!last)
				continue;
			var $input = self.$nct.find("input[value='" + last + "']");
			//not mark inputs as checked id state is not cheked it is schanged by formelement automatically
			//$input.prop("checked","checked");
			var parent = $input.parents(".checkbox");
			parent.click();
		}
	};
	
	this.renderSort = function(){
		var lastEstatecarSort = self.lastState.carSort;
		if(lastEstatecarSort){
			this.$sort.val(lastEstatecarSort);
			this.$sort.change();
			//console.log(this.$sort.val());
		}	
		var lastEstatesortDirection = self.lastState.sortDirection;
		if(lastEstatesortDirection){
			this.$sortdir.find("> i").attr("class",lastEstatesortDirection);
		}
		if(self.lastState.showList){
			$(".show-list").click();
		}else{
			$(".show-grid").click();
		}
	};



	this.resetMax = function ($minSelect, $maxSelect, strictLess) {
		var min = parseInt($minSelect.val());
		var options = $maxSelect.find("option");
		var $default = options.eq(0);
		if (isNaN(min)) {
			//default is selected
			min = -99999;
		}
		for (var i = 0, m = options.length; i < m; i++) {
			var $op = options.eq(i);
			var val = $op.val();
			var max = parseInt(val);
			var less = (strictLess) ? max < min : max <= min;
			if (!isNaN(max) && less)
				$op.prop("disabled", true);
			else
				$op.prop("disabled", false);
		}
		$maxSelect.val($default.val()).change();
	};


	

	//triggers a select automatically
	this.setSelect = function ($elem, val) {

		var target = $elem.find('option[value="' + val + '"]');
		if (target.length) {
			var selected = $elem.find("option:selected");
			selected.prop("selected", false);
			target.prop('selected', 'selected');
			$elem.change();
		}
	};

	this.setCheckboxes = function ($elem, val) {

		var target = $elem.find('option[value="' + val + '"]');
		if (target.length) {
			var selected = $elem.find("option:selected");
			selected.prop("selected", false);
			target.prop('selected', 'selected');
			$elem.change();
		}
	};

	this.showClear = function(){
		this.$clear.show();
	};

	// ###################### store & restore state from cookie ############################

	this.saveToCookie = function () {
		//selects
		var state = {};
		state.make = this.$make.find("option:selected").val();
		state.model = this.$model.find("option:selected").val();
		state.country = this.$country.find("option:selected").val();
		state.priceMin = this.$priceMin.find("option:selected").val();
		state.priceMax = this.$priceMax.find("option:selected").val();
		state.body = this.$body.find("option:selected").val();
		state.doors = this.$doors.find("option:selected").val();
		state.yearFrom = this.$yearFrom.find("option:selected").val();
		state.yearTo = this.$yearTo.find("option:selected").val();
		state.mileageMin = this.$mileageMin.find("option:selected").val();
		state.mileageMax = this.$mileageMax.find("option:selected").val();

		//checkboxes
		state.transmission = this.$transmission.find(".active input[type=checkbox]");
		state.fuel = this.$fuel.find(".active input[type=checkbox]");
		state.colours = this.$colours.find(".active input[type=checkbox]");
		state.nct = this.$nct.find(".active input[type=checkbox]");

		state.transmission = checkboxValues(state.transmission);
		state.fuel = checkboxValues(state.fuel);
		state.colours = checkboxValues(state.colours);
		state.nct = checkboxValues(state.nct);
		
		//sort buttons
		state.carSort = this.$sort.find("option:selected").val();
		state.sortDirection = this.$sortdir.find("> i").attr("class");
		
		// list or grid view
		state.showList = $(".show-list").hasClass("active");
		document.cookie = "search_auc_lexus" + "=" + JSON.stringify(state);
	};





	function restoreFromCookie() {
		var state = getCookie("search_auc_lexus");
		if (!state)
			state = DEFAULT_QUERY;
		//console.log(state);
		return state;
	}



	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		var object = null;
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ')
				c = c.substring(1);
			if (c.indexOf(name) == 0)
				object = c.substring(name.length, c.length);
		}
		if (object)
			object = JSON.parse(object);
		return object;
	}


	function removeCookie(){
		document.cookie = "search_auc_lexus=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
	}

	function checkboxValues($cheboxes) {
		var list = "";
		$cheboxes.each(function (index) {
			var sep = (index) ? ":" : "";
			list += sep + $(this).val();
		});
		return list;
	}



	// ################# Event Input Output #################

	// binds dom events to internal actions
	this.bindEvents = function () {
		self.$make.change(function () {
			self.renderModelSelect();
		});

		self.$priceMin.change(function () {
			self.resetMax(self.$priceMin, self.$priceMax);
		});

		self.$mileageMin.change(function () {
			self.resetMax(self.$mileageMin, self.$mileageMax);
		});

		self.$yearFrom.change(function () {
			self.resetMax(self.$yearFrom, self.$yearTo, true);
		});


		self.$clear.click(function () {
			self.ClearAllFields();
			self.lastState = DEFAULT_QUERY;
			self.removeCookie();
		});
		
		self.$container.change(function () {
			self.$clear.show();
		});


		//saves cookie before leave page
		$(window).on('beforeunload', function () {
			self.saveToCookie();
		});

	};

	// binds dom events to internal actions that generates a custom event captured by the model
	this.bindOutputEvents = function () {
		//click SearchButton
		self.$search.click(function (e) {
			e.preventDefault();
			self.$search.trigger("SearchButton");
		});
	};

	//bind events

};

/**
 * Used to generate min and max selects
 * Returns on araay of aprox steps length with the values
 * in ascending order.
 * @param {type} array
 * @param {type} steps
 * @returns {array} return the array with the elements in the steps position.
 */
function getMinMaxArray(array, steps) {
	var step = Math.floor(array.length / steps);
	if (step === 0)
		step = 1;

	var result = [];


	// add elemet every step position
	var i = 0;
	var val = 0;
	for (var max = array.length; i < max; i += step) {
		var val = array[i];
		var repeated = result.indexOf(val);
		if (repeated === -1)
			result.push(val);
	}

	// ad the last elemet if not pressent
	if (val < array[array.length - 1]) {
		var val = array[array.length - 1];
		var repeated = result.indexOf(val);
		if (repeated === -1)
			result.push(val);
	}
	return result;
}


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


DEFAULT_QUERY = {  
   "make":"LEXUS",
   "model":"Any Model",
   "country":"Any County",
   "priceMin":"Min",
   "priceMax":"Max",
   "body":"All body types",
   "doors":"All doors",
   "yearFrom":"From",
   "yearTo":"To",
   "mileageMin":"Min",
   "mileageMax":"Max",
   "transmission":"",
   "fuel":"",
   "colours":"",
   "nct":"",
   "carSort":false,
   "sortDirection":0,
   "showList":false
};


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function InitAssuredUsedCars() {

	//check the search from is pressent in the page
	if ($("#car_filter").length === 0)
		return;


	// settings
	var pageSize = 10;

	// initialization
	var searchCarsList = new Carslist();
	var resultsCarsList = new Carslist();
	var query = new CarsQuery();
	var searchView = new CarsSearchView();
	var resultsView = new CarsResulstView();

	//setters
	query.setPageSize(pageSize);
	query.setresultView(resultsView);
	resultsView.setPageSize(pageSize);

	searchView.setCarsList(searchCarsList);
	resultsView.setCarsList(resultsCarsList);

	var total = 0;




	//first query: reset carlist,  render search menu, & render results.
	query.handleinitialQuery(function (data) {


		// the firs query has all cars to generate the menu
		searchCarsList.parseCars(data.response.docs);
		searchView.render();

		// bindings
		searchView.bindEvents();
		searchView.bindOutputEvents();
		resultsView.bindEvents();
		resultsView.bindOutputEvents();

		//execute query
		var showing = resultsView.getShowingNumber();
		var sorted = ($("#carSort").val());
		query.customQuery(0, showing, sorted, function (data) {
			var cars = data.response.docs;
			total = data.response.numFound;
			resultsView.scrollTop();
			resultsCarsList.reset();
			resultsCarsList.parseCars(cars);
			var prevPageSize = resultsView.pageSize;
			resultsView.setPageSize(showing);
			resultsView.render();
			resultsView.renderTotal(total);
			if (sorted)
				resultsView.setDirectionField(query.getDirectionField());
			resultsView.setPageSize(prevPageSize);
		});
	});




	var reRenderResults = function (cars, total) {
		resultsCarsList.reset();
		resultsCarsList.parseCars(cars);
		resultsView.render();
		resultsView.renderTotal(total);
	};

	// called on search: reset carlist, reset resultview & render results.
	query.handleFullQuery(function (data) {
		total = data.response.numFound;
		resultsView.scrollTop();
		reRenderResults(data.response.docs, total);
	});



	// called on reorder: reset carlist, reset resultview & render results.
	query.handleSortQuery(function (data) {
		total = data.response.numFound;
		reRenderResults(data.response.docs, total);
		resultsView.setDirectionField(query.getDirectionField());
	});

	// called on  show more: dont reset anything, instead append to carList & append to results.
	query.handleNextPageQuery(function (data) {
		var total = data.response.numFound;
		resultsCarsList.appendCars(data.response.docs);
		resultsView.renderNextPage();
		resultsView.renderTotal(total);
	});

	query.initialQuery();

}


;(function($, Modernizr) {

	$.fn.contentToggle = function(__options) {
		return this.each(function() {
			return ContentToggle.init($(this), __options);
		});
	};

	var ContentToggle = {
		options: {
			toggleButtonClass: "toggleBtn",
            toggleClassOnChange: ["inactiveClass", "activeClass"],
            transitionDuration: Modernizr.csstransitions ? 500 : 0
		},
		init: function($content, _options) {
			if ( ! $content.data("content-toggle-state") ) {
				var state = {
					$this: $content,
					options: $.extend(true, this.options, _options, {}),
					isDefaultState: true,
					$altElements: $content.find("[data-alt]"),
					$altImgElements: $content.find("[data-alt-img]"),
					$altBlock: $content.find(".alt-block"),
					$defaultBlock: $content.find(".default-block")
				};
				$content.data("content-toggle-state", state);
				state.$toggleButton = state.$this.find("." + state.options.toggleButtonClass);
				state.$altBlock.css("display", "none");
				state.$toggleButton.bind("click", $.proxy(this.toggle, state));
			}
		},
		toggle: function() {

			var self = this;

			if ( this.$altElements.length ) {
				this.$altElements.each(function() {
					var $this = $(this);
					var defaultValue = $this.html();
					var alt = $this.data("alt");

					crossFadeAnimate({
						element: $this,
						newHtmlValue: alt,
						duration: self.options.transitionDuration,
						callback: function() {
							$this.data("alt", defaultValue);
						}
					});
				});
			}

			if ( this.$altImgElements.length ) {
				this.$altImgElements.each(function() {
					var $this = $(this);
					var $delegate = $($this.data("delegateElementSelector"));
					var currentSrc = ($delegate.length ? $delegate.css("background-image") : $this.css("background-image")).replace(window.HelperFunctions.cssBackgroundImageRegex, "$1");
					var altSrc = $this.data("alt-img");

					crossFadeAnimate({
						"element": $delegate.length ? $delegate : $this,
						"cssProperty": ["background-image", "url(" + altSrc + ")"],
						"duration": self.options.transitionDuration,
						"callback": function() {
							$this.data("alt-img", currentSrc);
						}
					});
				});
			}

			crossFadeAnimate({
				element: this[this.isDefaultState ? "$defaultBlock" : "$altBlock"],
				newElement: this[this.isDefaultState ? "$altBlock" : "$defaultBlock"],
				duration: self.options.transitionDuration,
				setDisplayNone: true,
				callback: function() {

					var altText = self.$toggleButton.data("alt-text"),
						newAltText;

					if ( altText ) {
						newAltText = self.$toggleButton.html();
						self.$toggleButton.html(altText);
						self.$toggleButton.data("alt-text", newAltText);
					}

					if ( self.options.toggleClassOnChange instanceof Array ) {
						self.$toggleButton[!self.isDefaultState? "addClass" : "removeClass"](self.options.toggleClassOnChange[1]);
						self.$toggleButton[self.isDefaultState? "addClass" : "removeClass"](self.options.toggleClassOnChange[0]);
					}
				}
			});

			this.isDefaultState = !this.isDefaultState;

			return false;
		}
	};

	function crossFadeAnimate(options) {

		options.element.add(options.newElement)
			.css("transition", "opacity " + options.duration + "ms ease-in-out" );

		setTimeout(function() {
			options.element.css("opacity", "0");

			setTimeout(function() {
				if ( options.cssProperty ) {
					options.element.css.apply(options.element, options.cssProperty);
				}
				if ( options.newHtmlValue ) {
					options.element.html(options.newHtmlValue);
				}

				if ( options.setDisplayNone ) {
					options.element.css("display", "none");
				}

				if ( options.newElement ) {
					options.newElement.css({
						"opacity": "0",
						"display": "block"
					});

					setTimeout(function() {
						options.newElement.css("opacity", "1");
					}, 10);
				} else {
					options.element.css("opacity", "1");
				}

				setTimeout(options.callback, options.duration);
			}, options.duration);
		}, 10);
	}

})(window.jQuery, window.Modernizr);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function submitForm(form, succescallback) {
	var $form = $(form);
	var $model = $form.find("#model option:selected");

	if ($model.length && $model.data("other")) {
		$form.append("<input type='hidden' name='pdf' value='" + $model.data("other") + "' />");
	}


	var hostname = window.location.hostname;
	$form.append("<input type='hidden' name='site_url' value='" + hostname + "' />");

	$.post($form.attr('action'), $form.serialize(), function (data) {
		if (data.indexOf('sent') != -1) {
			if (succescallback)
				succescallback($form, data);
		} else {
			alert(data);
		}
	});
}


function showThanks(form) {
	var $form = $(form);
	$form.fadeOut();
	$form.parents(".form_container").find(".form_thanks").fadeIn();
}

function initForms() {

	if ($("#contactusform").length > 0) {
		FormElements.bindSelects($('#contactusform .select'));
		FormElements.bindCheckboxes($('#contactusform .checkbox'));
		$.validate({
			form: '#contactusform',
			modules: 'location, date, security',
			onError: function () {
				//alert('Please complete fields marked with  *');
			},
			onSuccess: function () {
				if (window.t1DataLayer) {
					$(window).trigger("workflowevent"); // trigger tag manager event
				}
				submitForm('#contactusform', function () {
					$("#contact_us_name").text($("#contactusform #first_name").val())
					showThanks('#contactusform');
					$.smoothScroll({
						scrollTarget: '#header_lexus'
					});

				});
				return false; // Will stop the submission of the form
			}
		});
	}


	if ($("#bookserviceform").length > 0) {
		FormElements.bindSelects($('#bookserviceform .select'));
		FormElements.bindCheckboxes($('#bookserviceform .checkbox'));
		$.validate({
			form: '#bookserviceform',
			modules: 'location, date, security',
			onError: function () {
				//alert('Please complete fields marked with  *');
			},
			onSuccess: function () {
				if (window.t1DataLayer) {
					$(window).trigger("workflowevent"); // trigger tag manager event
				}
				submitForm('#bookserviceform', function () {
					showThanks('#bookserviceform');
				});
				return false; // Will stop the submission of the form
			}
		});
	}

	if ($("#booktestdriveform").length > 0) {
		FormElements.bindSelects($('#booktestdriveform .select'));
		FormElements.bindCheckboxes($('#booktestdriveform .checkbox'));
		$.validate({
			form: '#booktestdriveform',
			modules: 'location, date, security',
			onError: function () {
//				alert('Please complete fields marked with  *');
			},
			onSuccess: function () {
				if (window.t1DataLayer) {
					var $model = $("#booktestdriveform").find("#model").val();
					window.t1DataLayer.event.modelname = $model;
					$(window).trigger("workflowevent"); // trigger tag manager event
				}
				submitForm('#booktestdriveform', function () {
					showThanks('#booktestdriveform');
				});
				return false; // Will stop the submission of the form
			}
		});
	}

	if ($("#newsletterform").length > 0) {
		FormElements.bindSelects($('#newsletterform .select'));
		FormElements.bindCheckboxes($('#newsletterform .checkbox'));
		$.validate({
			form: '#newsletterform',
			modules: 'location, date, security',
			onError: function () {
//				alert('Please complete fields marked with  *');
			},
			onSuccess: function () {
				submitForm('#newsletterform', function () {
					showThanks('#newsletterform');
				});
				return false; // Will stop the submission of the form
			}
		});
	}

	if ($("#requestbrochure").length > 0) {
		FormElements.bindSelects($('#requestbrochure .select'));
		FormElements.bindCheckboxes($('#requestbrochure .checkbox'));
		$.validate({
			form: '#requestbrochure',
			modules: 'location, date, security',
			onError: function () {
//				alert('Please complete fields marked with  *');
			},
			onSuccess: function () {
				if (window.t1DataLayer) {
					var $model = $("#requestbrochure").find("#model").val();
					window.t1DataLayer.event.modelname = $model;
					$(window).trigger("workflowevent"); // trigger tag manager event
				}
				submitForm('#requestbrochure', function () {
					showThanks('#requestbrochure');

					if ($("#selecttype option:selected").val() == 'pdfDownload') {
						$file = encodeURIComponent($('.brochure-pdf option:selected').data("other"));
						//console.log(window.location);
						if (!$("html.no-touch").length) {
							$file = $file + "&mobile=true";
						}
						if (location.href.indexOf('assuredusedcars.toyota.ie') == -1)
							window.location = "/ajax/download/?brochurefile=" + $file;
						else
							window.location = "/forms/download/?brochurefile=" + $file;
					}
				});
				return false; // Will stop the submission of the form
			}
		});
	}
	
	
	$("#selecttype").change(function () {
		if ($(this).val() === "Post") {

			$("#postbrochureAdress").fadeIn();

			$("#postbrochureAdress #city,#postbrochureAdress #street").attr("data-validation", "required");
			$("#postbrochureAdress #state").attr("data-validation", "select");

			$("#selecttype_icon").html('<i class="icon icon-envelope-alt"></i>');
		} else if ($(this).val() === "Ebrochure") {
			$("#postbrochureAdress").fadeOut();
			$("#postbrochureAdress  #city,#postbrochureAdress  #street,#postbrochureAdress  #state").attr("data-validation", "");
			$("#selecttype_icon").html('@');
		} else {
			$("#postbrochureAdress").fadeOut();
			$("#postbrochureAdress  #city,#postbrochureAdress  #street,#postbrochureAdress  #state").attr("data-validation", "");
			$("#selecttype_icon").html('<i class="icon icon-file-pdf"></i>');
		}

	});


	$("select.data-other").change(function () {
		$el = $(this);
		$prev = $el.prev(".data-other-fill");
		$aux = $el.find(":selected").data("other");
		$prev.val($aux);
	});
}

$(function () {

	initForms();

});





/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var directionsService;

try {
	directionsService = new google.maps.DirectionsService();
} catch (e) {
	console.log("error with google maps library");
}

function resizeMap(containerSelector) {
	var $conatiner = $(containerSelector);
	var $map = $conatiner.find(".gmap");
	if ($map.length) {
		var id = $map.attr("id");
		var map = window[id + "_map"];
		var x = map.getZoom();
		var c = map.getCenter();
		google.maps.event.trigger(map, 'resize');
		map.setZoom(x);
		map.setCenter(c);
	}
}



function getRoute($container) {
	var $getRouteMap = $container.find(".gmap");
	var id = $getRouteMap.attr("id");
	var map = window[id + "_map"];
	var markers = window[id + "_markers"];

	if ($getRouteMap.length) {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(setPosition);
		} else {
			$container.find('.map_errors').text("Geolocation is not supported by this browser.");
		}
	}

	function setPosition(position) {
		var usrLatitude = position.coords.latitude;
		var usrLongitude = position.coords.longitude;
		var usrPosition = new google.maps.LatLng(usrLatitude, usrLongitude);
		calcRoute(usrPosition);
	}

	function calcRoute(start) {
		
		directionsDisplay = new google.maps.DirectionsRenderer();
		directionsDisplay.setMap(map);
		//console.log('start:'+start)
		//console.log('end:'+end)
		var request = {
			origin: start,
			destination: map.getCenter(),
			travelMode: google.maps.DirectionsTravelMode.DRIVING
		};
		directionsService.route(request, function (response, status) {
			if (status == "ZERO_RESULTS") {
				$(".map_errors").html("Some error occurred");
			}
			if (status == google.maps.DirectionsStatus.OK) {
				//console.log(response);
				deletemarkers();
				directionsDisplay.setDirections(response);

				var totalDistance = 0;
				var totalDuration = 0;
				var legs = response.routes[0].legs;
				for (var i = 0; i < legs.length; ++i) {
					totalDistance += legs[i].distance.value;
				}

			}
		});
	}


	function deletemarkers() {
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(null);
		}
	}
}


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*!
 * Bootstrap v3.3.2 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

/*!
 * Generated using the Bootstrap Customizer (http://getbootstrap.com/customize/?id=cf0f62fd3c1dc5d4bb04)
 * Config saved to config.json and https://gist.github.com/cf0f62fd3c1dc5d4bb04
 */
if (typeof jQuery === 'undefined') {
	throw new Error('Bootstrap\'s JavaScript requires jQuery')
}
+function ($) {
	'use strict';
	var version = $.fn.jquery.split(' ')[0].split('.')
	if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1)) {
		throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher')
	}
}(jQuery);

/* ========================================================================
 * Bootstrap: carousel.js v3.3.2
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
	'use strict';

	// CAROUSEL CLASS DEFINITION
	// =========================

	var Carousel = function (element, options) {
		this.$element = $(element)
		this.$indicators = this.$element.find('.carousel-indicators')
		this.options = options
		this.paused =
				this.sliding =
				this.interval =
				this.$active =
				this.$items = null

		this.options.keyboard && this.$element.on('keydown.bs.carousel', $.proxy(this.keydown, this))

		this.options.pause == 'hover' && !('ontouchstart' in document.documentElement) && this.$element
				.on('mouseenter.bs.carousel', $.proxy(this.pause, this))
				.on('mouseleave.bs.carousel', $.proxy(this.cycle, this))
	}

	Carousel.VERSION = '3.3.2'

	Carousel.TRANSITION_DURATION = 600

	Carousel.DEFAULTS = {
		interval: 5000,
		pause: 'hover',
		wrap: true,
		keyboard: true
	}

	Carousel.prototype.keydown = function (e) {
		if (/input|textarea/i.test(e.target.tagName))
			return
		switch (e.which) {
			case 37:
				this.prev();
				break
			case 39:
				this.next();
				break
			default:
				return
		}

		e.preventDefault()
	}

	Carousel.prototype.cycle = function (e) {
		e || (this.paused = false)

		this.interval && clearInterval(this.interval)

		this.options.interval
				&& !this.paused
				&& (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

		return this
	}

	Carousel.prototype.getItemIndex = function (item) {
		this.$items = item.parent().children('.item')
		return this.$items.index(item || this.$active)
	}

	Carousel.prototype.getItemForDirection = function (direction, active) {
		var activeIndex = this.getItemIndex(active)
		var willWrap = (direction == 'prev' && activeIndex === 0)
				|| (direction == 'next' && activeIndex == (this.$items.length - 1))
		if (willWrap && !this.options.wrap)
			return active
		var delta = direction == 'prev' ? -1 : 1
		var itemIndex = (activeIndex + delta) % this.$items.length
		return this.$items.eq(itemIndex)
	}

	Carousel.prototype.to = function (pos) {
		var that = this
		var activeIndex = this.getItemIndex(this.$active = this.$element.find('.item.active'))

		if (pos > (this.$items.length - 1) || pos < 0)
			return

		if (this.sliding)
			return this.$element.one('slid.bs.carousel', function () {
				that.to(pos)
			}) // yes, "slid"
		if (activeIndex == pos)
			return this.pause().cycle()

		return this.slide(pos > activeIndex ? 'next' : 'prev', this.$items.eq(pos))
	}

	Carousel.prototype.pause = function (e) {
		e || (this.paused = true)

		if (this.$element.find('.next, .prev').length && $.support.transition) {
			this.$element.trigger($.support.transition.end)
			this.cycle(true)
		}

		this.interval = clearInterval(this.interval)

		return this
	}

	Carousel.prototype.next = function () {
		if (this.sliding)
			return
		return this.slide('next')
	}

	Carousel.prototype.prev = function () {
		if (this.sliding)
			return
		return this.slide('prev')
	}

	Carousel.prototype.slide = function (type, next) {
		var $active = this.$element.find('.item.active')
		var $next = next || this.getItemForDirection(type, $active)
		var isCycling = this.interval
		var direction = type == 'next' ? 'left' : 'right'
		var that = this

		if ($next.hasClass('active'))
			return (this.sliding = false)

		var relatedTarget = $next[0]
		var slideEvent = $.Event('slide.bs.carousel', {
			relatedTarget: relatedTarget,
			direction: direction
		})
		this.$element.trigger(slideEvent)
		if (slideEvent.isDefaultPrevented())
			return

		this.sliding = true

		isCycling && this.pause()

		if (this.$indicators.length) {
			this.$indicators.find('.active').removeClass('active')
			var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
			$nextIndicator && $nextIndicator.addClass('active')
		}

		var slidEvent = $.Event('slid.bs.carousel', {relatedTarget: relatedTarget, direction: direction}) // yes, "slid"
		if ($.support.transition && this.$element.hasClass('slide')) {
			$next.addClass(type)
			$next[0].offsetWidth // force reflow
			$active.addClass(direction)
			$next.addClass(direction)
			$active
					.one('bsTransitionEnd', function () {
						$next.removeClass([type, direction].join(' ')).addClass('active')
						$active.removeClass(['active', direction].join(' '))
						that.sliding = false
						setTimeout(function () {
							that.$element.trigger(slidEvent)
						}, 0)
					})
					.emulateTransitionEnd(Carousel.TRANSITION_DURATION)
		} else {
			$active.removeClass('active')
			$next.addClass('active')
			this.sliding = false
			this.$element.trigger(slidEvent)
		}

		isCycling && this.cycle()

		return this
	}


	// CAROUSEL PLUGIN DEFINITION
	// ==========================

	function Plugin(option) {
		return this.each(function () {
			var $this = $(this)
			var data = $this.data('bs.carousel')
			var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
			var action = typeof option == 'string' ? option : options.slide

			if (!data)
				$this.data('bs.carousel', (data = new Carousel(this, options)))
			if (typeof option == 'number')
				data.to(option)
			else if (action)
				data[action]()
			else if (options.interval)
				data.pause().cycle()
		})
	}

	var old = $.fn.carousel

	$.fn.carousel = Plugin
	$.fn.carousel.Constructor = Carousel


	// CAROUSEL NO CONFLICT
	// ====================

	$.fn.carousel.noConflict = function () {
		$.fn.carousel = old
		return this
	}


	// CAROUSEL DATA-API
	// =================

	var clickHandler = function (e) {
		var href
		var $this = $(this)
		var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) // strip for ie7
		if (!$target.hasClass('carousel'))
			return
		var options = $.extend({}, $target.data(), $this.data())
		var slideIndex = $this.attr('data-slide-to')
		if (slideIndex)
			options.interval = false

		Plugin.call($target, options)

		if (slideIndex) {
			$target.data('bs.carousel').to(slideIndex)
		}

		e.preventDefault()
	}

	$(document)
			.on('click.bs.carousel.data-api', '[data-slide]', clickHandler)
			.on('click.bs.carousel.data-api', '[data-slide-to]', clickHandler)

	$(window).on('load', function () {
		$('[data-ride="carousel"]').each(function () {
			var $carousel = $(this)
			Plugin.call($carousel, $carousel.data())
		})
	})

}(jQuery);

/* ========================================================================
 * Bootstrap: transition.js v3.3.2
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
	'use strict';

	// CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
	// ============================================================

	function transitionEnd() {
		var el = document.createElement('bootstrap')

		var transEndEventNames = {
			WebkitTransition: 'webkitTransitionEnd',
			MozTransition: 'transitionend',
			OTransition: 'oTransitionEnd otransitionend',
			transition: 'transitionend'
		}

		for (var name in transEndEventNames) {
			if (el.style[name] !== undefined) {
				return {end: transEndEventNames[name]}
			}
		}

		return false // explicit for ie8 (  ._.)
	}

	// http://blog.alexmaccaw.com/css-transitions
	$.fn.emulateTransitionEnd = function (duration) {
		var called = false
		var $el = this
		$(this).one('bsTransitionEnd', function () {
			called = true
		})
		var callback = function () {
			if (!called)
				$($el).trigger($.support.transition.end)
		}
		setTimeout(callback, duration)
		return this
	}

	$(function () {
		$.support.transition = transitionEnd()

		if (!$.support.transition)
			return

		$.event.special.bsTransitionEnd = {
			bindType: $.support.transition.end,
			delegateType: $.support.transition.end,
			handle: function (e) {
				if ($(e.target).is(this))
					return e.handleObj.handler.apply(this, arguments)
			}
		}
	})

}(jQuery);



/* 
	 #############################################################################
	 adepted carousel
	 #############################################################################
	 */

function offerCarousel(id) {
	var $container = $(id);
	if(!$container.length)
		return;
	var $inner = $('<div class="carousel-inner " >');
	var $left = $('<a class="carousel-arrow left icon icon-angle-left" href="#"><i class="icon icon-chevron-left"></i></a>');
	var $rigth = $('<a class="carousel-arrow right icon icon-angle-right" href="#" ><i class="icon icon-chevron-right"></i></a>');
	var $nav = $('<ol class="carousel-nav carousel-indicators"></ol>');
	var $items = $container.find(".grid_6");
	
	if ($items.length <= 2)
		return;

	
	if ($(window).width() >= 768 ) {
		$container.append($inner);
		setRows();
		$container.append($left);
		$container.append($rigth);
		$container.append($nav);
		setRowWidth();
		initCarousel();
		setRowHeigth();
		$(window).resize(function () {
			setRowWidth();
			setRowHeigth();
		});
		var $img = $inner.find("img");
		var x = window.setInterval(function () {
			var loaded = true;
			$img.each(function () {
				loaded = loaded & IsImageLoaded(this);
			});
			if (loaded) {
				setRowHeigth();
				window.clearInterval(x);
			}
		}, 100);
	}else{
		
	}

	function setRows() {
		$items.detach();
		for (var i = 0, max = $items.length; i < max; i += 2) {
			var $row = $("<div class='row item'></div>");
			$row.append($items.eq(i));
			$row.append($items.eq(i + 1));
			if (i === 0)
				$row.addClass("active");
			$inner.append($row);
		}
	}
	function setRowWidth() {
		var $rows = $container.find(".row");
		var w = $inner.width();
		$rows.width(w);
	}

	function setRowHeigth() {
		var $rows = $container.find(".row");
		var h = 0;
		$rows.each(function () {
			h = Math.max(h, $(this).outerHeight());
		});
		$inner.height(h);
	}

	function initCarousel() {
		var $carousel = $container.carousel({
			interval: 5000
		});

		var $rows = $container.find(".row");
		$rows.each(function (i) {
			var active = (i === 0) ? "class='active'" : "";
			$nav.append('<li data-target="' + id + '" data-slide-to="' + i + '" ' + active + '></li>');
		});

		$left.click(function (e) {
			e.preventDefault();
			$carousel.carousel("prev");
		});
		$rigth.click(function (e) {
			e.preventDefault();
			$carousel.carousel("next");
		});
		$nav.find("li").click(function (e) {
			e.preventDefault();
			$carousel.carousel(parseInt($(this).data("slide-to")));
		});
	}


	function IsImageLoaded(img) {
		// During the onload event, IE correctly identifies any images that
		// weren’t downloaded as not complete. Others should too. Gecko-based
		// browsers act like NS4 in that they report this incorrectly.
		if (!img.complete) {
			return false;
		}

		// However, they do have two very useful properties: naturalWidth and
		// naturalHeight. These give the true size of the image. If it failed
		// to load, either of these should be zero.

		if (typeof img.naturalWidth !== "undefined" && img.naturalWidth === 0) {
			return false;
		}

		// No other way of checking: assume it’s ok.
		return true;
	}
}


; (function ($, tr, Modernizr) {

    // default settings
    var settings = {
        translations: {},
        fadeTime: 450,
        columns: 4,
        allowAllFilterByDefault: false // false if seconary content layer used for view all interaction
    };
    
    var methods = {
        init: function (options) {
            return this.each(function () {
                // Construct instance
                var instance = this;
                var $this = $(this);
                instance.instanceSettings = $.extend(true, {}, settings, options);
                instance.viewAll = instance.instanceSettings.allowAllFilterByDefault;

                var $filterLinks = $('.filter-bar .fb-item', $this);
                var $activeFilter = $filterLinks.filter("[data-default]").length ? $filterLinks.filter("[data-default]").first() : $filterLinks.first();
                var $viewAllLink = $filterLinks.filter("[data-viewall]").length ? $filterLinks.filter("[data-viewall]").first() : false;
                var $contentBLocksConatiner = $('.fc-group-container', $this);
                var $contentBlocks = $('.fc-group', $contentBLocksConatiner);
                var $activeContent = $contentBlocks.filter($activeFilter.data('section-filter'));
                var $crossGroupFilters = $('.filter-bar .fbg-item', $this);


                $crossGroupFilters.each(function () {
                    var $eLink = $(this);
                    if ($eLink.data('item-filter')) {
                        $($eLink.data('item-filter'), $contentBlocks).hide();
                    }
                    $eLink.data("offtext", $eLink.text());

                    $eLink.on("click", function (e) {
                        e.preventDefault();
                        var wasActive = $eLink.hasClass($eLink.data("onclass"));
                        $eLink.toggleClass($eLink.data("onclass") + " " + $eLink.data("offclass"));
                        
                        var $filteredItems = $($eLink.data('item-filter'), $contentBlocks);
                        $contentBLocksConatiner.css({"min-height": $contentBLocksConatiner.height() });
                        $contentBlocks.addClass("preventAllAnimations");
                        $contentBlocks.css({"opacity": 0, "display": "none" }).height(); // .height to trigger reflow and apply 'preventAllAnimations'...

                        if (wasActive) {
                            $filteredItems.hide();
                            $eLink.text($eLink.data("offtext"));
                        } else {
                            $filteredItems.show();  
                            $eLink.text($eLink.data("ontext"));                          
                        }
                        
                        $activeContent.css({"display": "" }).height();
                        $contentBlocks.removeClass("preventAllAnimations");
                        $activeContent.css({"opacity": 1 });
                        $contentBLocksConatiner.css({"min-height": 0 });
                        clearRowStarts($contentBlocks, instance.instanceSettings.columns);
                    });
                });

                $filterLinks.on("click", function (e) {
                    e.preventDefault();
					var $contentBlocks = $('.fc-group', $contentBLocksConatiner);
                    $contentBlocks.css({"display": "" });
                    $activeFilter = $(this);
                    var isViewAll = $activeFilter.attr('data-viewall') === "";
                    if ((!isViewAll || instance.viewAll) && $activeFilter.data('section-filter')) {
                        $filterLinks.removeClass("active");
                        $activeFilter.addClass("active");

                        // Select the new content group
                        $activeContent = $contentBlocks.filter($activeFilter.data('section-filter'));
                        // Set container min-height to prevent jumo
                        $contentBLocksConatiner.css({"min-height": $contentBLocksConatiner.height() });
                        // Prevent animations on fadeout
                        $contentBlocks.addClass("preventAllAnimations");
                        $contentBlocks.css({"opacity": 0, "display": "none" }).height(); // .height to trigger reflow and apply 'preventAllAnimations'...
                            
                        $activeContent.css({"display": "" }).height();
                        $contentBlocks.removeClass("preventAllAnimations");
                        // Position & fade in new content
                        $activeContent.css({"opacity": 1 });
                        $contentBLocksConatiner.css({"min-height": 0 });
                        clearRowStarts($contentBlocks, instance.instanceSettings.columns);
                    }
                });

                $this.on("moveToSecondary", function (e) {
                    if ($viewAllLink.length) {
                        $activeFilter = $viewAllLink;
                        $filterLinks.removeClass("active");
                        $activeFilter.addClass("active");
                        $activeContent = $contentBlocks; // show all...
                        $contentBlocks.addClass("preventAllAnimations");
                        $contentBlocks.css({"opacity": 1, "display": "" });
                        $contentBlocks.removeClass("preventAllAnimations");
                        clearRowStarts($contentBlocks, instance.instanceSettings.columns);
                        instance.viewAll=true;
                    }
                });
                $this.on("moveFromSecondary", function (e) {
                    $activeFilter = $filterLinks.filter("[data-default]").length ? $filterLinks.filter("[data-default]").first() : $filterLinks.first();
                    $filterLinks.removeClass("active");
                    $activeFilter.addClass("active");
                    $activeContent = $contentBlocks.filter($activeFilter.data('section-filter'));
                    $contentBlocks.addClass("preventAllAnimations");
                    $contentBlocks.css({"opacity": 0, "display": "none" });
                    $activeContent.css({"opacity": 1, "display": ""  });
                    $contentBlocks.removeClass("preventAllAnimations");
                    clearRowStarts($contentBlocks, instance.instanceSettings.columns);
                    instance.viewAll=false;
                });
                
                $activeFilter.addClass("active");
                clearRowStarts($contentBlocks, instance.instanceSettings.columns);
                $contentBlocks.not($activeContent).css({"opacity": 0, "display": "none" });

                function clearRowStarts($itemContainers, colCount) {
                    $itemContainers.each(function () {
                        var visCount = 0;
                        $('.item', $(this)).each(function (i) {
                            $thisItem = $(this);
                            if ($thisItem.is(":visible")) {
                                $thisItem.css({"clear": visCount % colCount === 0 ? "left": "" });
                                visCount++;
                            }
                        });
                    });
                }
            });
        }
    };


    $.fn.FilterableContent = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.plugin SecondaryContent');
        }
    };

})(window.jQuery, window.TranslationManager.tr, window.Modernizr);




// Add local translations
TranslationManager.addLocalTranslations({
	"showAll": "View all",
	"showAllWithCount": "View all {0}",
	"showLess": "Show less",
	"backTo": "Back to",
	"readDetails": "Read Details",
	"close": "Close",
	"save": "Save",
	"share": "Share"
});

$(function() {

//	// Bind secondary content layer (for view all)
//	$('.SecContentContainer').each(function() {
//		$(this).SecondaryContent({
//			"sectionIDs": $(this).data("id").split("/"),
//			"mode": $(this).data("contenttype"),
//			"cGroupName": $(this).data("name"),
//			"translations": TranslationManager.getLocalTranslations()
//		})
//	});

//	// old hack to align .fc-group-name with the first item
//	$(".fc-group").each(function(){
//		$t = $(this);
//		var $name = $t.find(".fc-group-name").detach();
//		var $item = $t.find(".item").first();
//		$item.append($name);
//	});

	// Bind filterable content
	$('.filterable-content').FilterableContent({});

	// Bind read more
	$('.expansion-content').ReadMore({});
	

	var timeoutRef = false;

	// Colourbox bind for accessory items
	$('.cb-ajax-json').bind('click', function(e) {
		var $item = $(this);

		// Show the loading spinner over the item
		$item.LoadingOverlay();

		if (timeoutRef)
			clearTimeout(timeoutRef);

		timeoutRef = setTimeout(function() {

			$.ajax({
				type: "GET",
				url: $item.data('link'),
				dataType: "json",
				success: function(response) {

					// Create HTML from the JSON data
					var $content = $('<article class="feature-cbv" />');
					if (response.image && response.image.url && response.image.width && response.image.height)
						$content.append('<img src="' + response.image.url + '" alt="' + response.image.name + '" width="' + response.image.width + '" height="' + response.image.height + '" />');
					$content.append('<div class="share-tools open-top"><div class="tool-box"><a href="#save" class="share-item">' + TranslationManager.tr("save") + '</a><a href="#share" class="share-item">' + TranslationManager.tr("share") + '</a></div></div>');
					if (response.name)
						$content.append('<h1 class="title2">' + response.name + '</h1>');
					if (response.description)
						$content.append('<div class="body-content clearfix">' + response.description + '</div<');

					// Launch coloutbox with generated HTML
					$.colorbox({
						close: '<a class="closelink"><span>' + TranslationManager.tr("close") + '</span> <i data-icon="&#xf00d;"></i></a>',
						transition: 'none',
						width: response.image.width + 20 || 500,
						className: 'feature-overlay colourbox-content',
						html: $content,
						onComplete: function(e) {
							CommonEffects.bindShareTools($content);
						}
					});

					// Remove loading spinner
					$item.LoadingOverlay('removeLoadingOverlay');
				},
				error: function(xhr) {
					$.colorbox.close();
					$item.LoadingOverlay('removeLoadingOverlay');
					alert("could not load content, error: " + xhr.status);
				}
			});
		}, 1000);
	});

});


(function ($, tr, Modernizr) {

	// default settings
	var settings = {
		translations: {},
		fadeTime: 450,
		openContentWidth: "470",
		arrowIndent: 30,
		gutterWidth: 10,
		visualParentOffsetSelector: ".item",
		replacementTextTranslationKey: "readDetails",
		closeTextTranslationKey: "close"
	};

	var methods = {
		init: function (options) {
			return this.each(function () {
				// Construct instance
				var instance = this;
				var $this = $(this);
				if ($this.data("readMoreBound"))
					return false;
				$this.data("readMoreBound", true);
				instance.instanceSettings = $.extend(true, {}, settings, options);

				var readMoreText = tr(instance.instanceSettings.replacementTextTranslationKey);
				var closeText = tr(instance.instanceSettings.closeTextTranslationKey);

				// Insert read more text and hide expansion content
				var $readMoreLink = $('<div class="read-more-container"><a href="#" class="readMore"><span>' + readMoreText + '</span> <i class="icon icon-info-sign"></i></a></div>');
				$this.before($readMoreLink);
				$this.hide();

				$('a', $readMoreLink).on("click", function (e) {
					e.preventDefault();
					e.stopPropagation();
					var $thisReadMoreLink = $(this);
					$(window).trigger("closemodals");

					if (!$thisReadMoreLink.data('isOpen')) {
						$thisReadMoreLink.data('isOpen', true).addClass("active");

						var winW = $(window).width();
						var width = Math.min( winW - 20,instance.instanceSettings.openContentWidth);
						// Construct HTML
						var $closeLink = $('<a href="#" class="closelink"><span>' + closeText + '</span> <i class="icon icon icon-remove"></i></a>');
						var $overlayContent = $('<div class="readmore-content"></div>').css({"z-index": 9, "width": width});
						var $overlayInner = $('<div class="rmc-inner">' + $this.html() + '</div>').prepend($closeLink);
						var $overlayPointer = $('<span class="arrow up"></span>').css({"margin-left": instance.instanceSettings.arrowIndent});
						$overlayContent.append($overlayPointer);
						$overlayContent.append($overlayInner);

						// Add to page
						$thisReadMoreLink.after($overlayContent).height();
						$overlayContent.css({"opacity": 1});
						// Adjust to go above more link if needed
						var positionNeeded = $overlayContent.offset().top + $overlayContent.height();
						var currentLowerVisPoint = $(window).scrollTop() + $(window).height();
						if (positionNeeded > currentLowerVisPoint || $('body').height() < positionNeeded) {
							$overlayContent.css({"bottom": $readMoreLink.height()});
							$overlayPointer.removeClass('up').addClass('down');
							$overlayContent.append($overlayPointer);
						} else {
							$overlayContent.css({"top": $readMoreLink.height()});
						}

						// Adjust to go left of link if needed
						var $positionParent = $thisReadMoreLink.closest(instance.instanceSettings.visualParentOffsetSelector);
						if (!$positionParent.length)
							$positionParent = $thisReadMoreLink.parent();
						var spaceInRow = $positionParent.offsetParent().width() - $positionParent.position().left;
						if (spaceInRow < instance.instanceSettings.openContentWidth) {
							$overlayContent.css({"left": spaceInRow - instance.instanceSettings.openContentWidth - instance.instanceSettings.gutterWidth});
							$overlayPointer.css({"margin-left": (instance.instanceSettings.openContentWidth - spaceInRow) + instance.instanceSettings.arrowIndent});
						}
						
						// adjust in smalls screens
						if(width < instance.instanceSettings.openContentWidth){
							$overlayContent.css({"left": -10});
						}
							

						// Prevent clicks inside triggering body click
						$overlayContent.click(function (e) {
							e.stopPropagation();
						});

						$(window).one("closemodals", function (e) {
							closeLayer();
						});

						// Close click once only bind
						$closeLink.one("click", function (e) {
							e.preventDefault();
							closeLayer();
						});

					}
					function closeLayer() {
						$thisReadMoreLink.data('isOpen', false).removeClass("active");
						$closeLink.unbind();
						if ($overlayContent && $overlayContent.length) {
							$overlayContent.remove();
							$overlayContent = null;
						}
					}
				});
			});
		}
	};


	$.fn.ReadMore = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.plugin SecondaryContent');
		}
	};

})(window.jQuery, window.TranslationManager.tr, window.Modernizr);




/*!
	jQuery ColorBox v1.4.6 - 2013-03-19
	(c) 2013 Jack Moore - jacklmoore.com/colorbox
	license: http://www.opensource.org/licenses/mit-license.php
*/
(function ($, document, window) {
	var
	// Default settings object.
	// See http://jacklmoore.com/colorbox for details.
	defaults = {
		transition: "fade",
		speed: 250,
		width: false,
		initialWidth: "0",
		innerWidth: false,
		maxWidth: false,
		height: false,
		initialHeight: "0",
		innerHeight: false,
		maxHeight: false,
		scalePhotos: true,
		scrolling: true,
		inline: false,
		html: false,
		iframe: false,
		fastIframe: true,
		photo: false,
		href: false,
		title: false,
		rel: false,
		opacity: 0.5,
		preloading: true,
		className: false,
		
		// alternate image paths for high-res displays
		retinaImage: false,
		retinaUrl: false,
		retinaSuffix: '@2x.$1',

		// internationalization
		current: "image {current} of {total}",
		previous: "previous",
		next: "next",
		close: "close",
		xhrError: "This content failed to load.",
		imgError: "This image failed to load.",

		open: false,
		returnFocus: true,
		reposition: true,
		loop: true,
		slideshow: false,
		slideshowAuto: true,
		slideshowSpeed: 2500,
		slideshowStart: "start slideshow",
		slideshowStop: "stop slideshow",
		photoRegex: /\.(gif|png|jp(e|g|eg)|bmp|ico)((#|\?).*)?$/i,

		onOpen: false,
		onLoad: false,
		onComplete: false,
		onCleanup: false,
		onClosed: false,
		overlayClose: true,
		escKey: true,
		arrowKey: true,
		top: false,
		bottom: false,
		left: false,
		right: false,
		fixed: false,
		data: undefined
	},
	
	// Abstracting the HTML and event identifiers for easy rebranding
	colorbox = 'colorbox',
	prefix = 'cbox',
	boxElement = prefix + 'Element',
	
	// Events
	event_open = prefix + '_open',
	event_load = prefix + '_load',
	event_complete = prefix + '_complete',
	event_cleanup = prefix + '_cleanup',
	event_closed = prefix + '_closed',
	event_purge = prefix + '_purge',
	
	// Special Handling for IE
	isIE = !$.support.leadingWhitespace, // IE6 to IE8
	isIE6 = isIE && !window.XMLHttpRequest, // IE6
	event_ie6 = prefix + '_IE6',

	// Cached jQuery Object Variables
	$overlay,
	$box,
	$wrap,
	$content,
	$topBorder,
	$leftBorder,
	$rightBorder,
	$bottomBorder,
	$related,
	$window,
	$loaded,
	$loadingBay,
	$loadingOverlay,
	$title,
	$current,
	$slideshow,
	$next,
	$prev,
	$close,
	$groupControls,
	$events = $('<a/>'),
	
	// Variables for cached values or use across multiple functions
	settings,
	interfaceHeight,
	interfaceWidth,
	loadedHeight,
	loadedWidth,
	element,
	index,
	photo,
	open,
	active,
	closing,
	loadingTimer,
	publicMethod,
	div = "div",
	className,
	requests = 0,
	init;

	// ****************
	// HELPER FUNCTIONS
	// ****************
	
	// Convience function for creating new jQuery objects
	function $tag(tag, id, css) {
		var element = document.createElement(tag);

		if (id) {
			element.id = prefix + id;
		}

		if (css) {
			element.style.cssText = css;
		}

		return $(element);
	}
	
	// Get the window height using innerHeight when available to avoid an issue with iOS
	// http://bugs.jquery.com/ticket/6724
	function winheight() {
		return window.innerHeight ? window.innerHeight : $(window).height();
	}

	// Determine the next and previous members in a group.
	function getIndex(increment) {
		var
		max = $related.length,
		newIndex = (index + increment) % max;
		
		return (newIndex < 0) ? max + newIndex : newIndex;
	}

	// Convert '%' and 'px' values to integers
	function setSize(size, dimension) {
		return Math.round((/%/.test(size) ? ((dimension === 'x' ? $window.width() : winheight()) / 100) : 1) * parseInt(size, 10));
	}
	
	// Checks an href to see if it is a photo.
	// There is a force photo option (photo: true) for hrefs that cannot be matched by the regex.
	function isImage(settings, url) {
		return settings.photo || settings.photoRegex.test(url);
	}

	function retinaUrl(settings, url) {
		return settings.retinaUrl && window.devicePixelRatio > 1 ? url.replace(settings.photoRegex, settings.retinaSuffix) : url;
	}

	function trapFocus(e) {
		if ('contains' in $box[0] && !$box[0].contains(e.target)) {
			e.stopPropagation();
			$box.focus();
		}
	}

	// Assigns function results to their respective properties
	function makeSettings() {
		var i,
			data = $.data(element, colorbox);
		
		if (data === null) {
			settings = $.extend({}, defaults);
//			if (console && console.log) {
//				//console.log('Error: cboxElement missing settings object');
//			}
		} else {
			settings = $.extend({}, data);
		}
		
		for (i in settings) {
			if ($.isFunction(settings[i]) && i.slice(0, 2) !== 'on') { // checks to make sure the function isn't one of the callbacks, they will be handled at the appropriate time.
				settings[i] = settings[i].call(element);
			}
		}
		
		settings.rel = settings.rel || element.rel || $(element).data('rel') || 'nofollow';
		settings.href = settings.href || $(element).attr('href');
		settings.title = settings.title || element.title;
		
		if (typeof settings.href === "string") {
			settings.href = $.trim(settings.href);
		}
	}

	function trigger(event, callback) {
		// for external use
		$(document).trigger(event);

		// for internal use
		$events.trigger(event);

		if ($.isFunction(callback)) {
			callback.call(element);
		}
	}

	// Slideshow functionality
	function slideshow() {
		var
		timeOut,
		className = prefix + "Slideshow_",
		click = "click." + prefix,
		clear,
		set,
		start,
		stop;
		
		if (settings.slideshow && $related[1]) {
			clear = function () {
				clearTimeout(timeOut);
			};

			set = function () {
				if (settings.loop || $related[index + 1]) {
					timeOut = setTimeout(publicMethod.next, settings.slideshowSpeed);
				}
			};

			start = function () {
				$slideshow
					.html(settings.slideshowStop)
					.unbind(click)
					.one(click, stop);

				$events
					.bind(event_complete, set)
					.bind(event_load, clear)
					.bind(event_cleanup, stop);

				$box.removeClass(className + "off").addClass(className + "on");
			};
			
			stop = function () {
				clear();
				
				$events
					.unbind(event_complete, set)
					.unbind(event_load, clear)
					.unbind(event_cleanup, stop);
				
				$slideshow
					.html(settings.slideshowStart)
					.unbind(click)
					.one(click, function () {
						publicMethod.next();
						start();
					});

				$box.removeClass(className + "on").addClass(className + "off");
			};
			
			if (settings.slideshowAuto) {
				start();
			} else {
				stop();
			}
		} else {
			$box.removeClass(className + "off " + className + "on");
		}
	}

	function launch(target) {
		if (!closing) {
			
			element = target;
			
			makeSettings();
			
			$related = $(element);
			
			index = 0;
			
			if (settings.rel !== 'nofollow') {
				$related = $('.' + boxElement).filter(function () {
					var data = $.data(this, colorbox),
						relRelated;

					if (data) {
						relRelated =  $(this).data('rel') || data.rel || this.rel;
					}
					
					return (relRelated === settings.rel);
				});
				index = $related.index(element);
				
				// Check direct calls to ColorBox.
				if (index === -1) {
					$related = $related.add(element);
					index = $related.length - 1;
				}
			}
			
			$overlay.css({
				opacity: parseFloat(settings.opacity),
				cursor: settings.overlayClose ? "pointer" : "auto",
				visibility: 'visible'
			}).show();
			

			if (className) {
				$box.add($overlay).removeClass(className);
			}
			if (settings.className) {
				$box.add($overlay).addClass(settings.className);
			}
			className = settings.className;

			$close.html(settings.close).show();

			if (!open) {
				open = active = true; // Prevents the page-change action from queuing up if the visitor holds down the left or right keys.
				
				// Show colorbox so the sizes can be calculated in older versions of jQuery
				$box.css({visibility:'hidden', display:'block'});
				
				$loaded = $tag(div, 'LoadedContent', 'width:0; height:0; overflow:hidden').appendTo($content);

				// Cache values needed for size calculations
				interfaceHeight = $topBorder.height() + $bottomBorder.height() + $content.outerHeight(true) - $content.height();//Subtraction needed for IE6
				interfaceWidth = $leftBorder.width() + $rightBorder.width() + $content.outerWidth(true) - $content.width();
				loadedHeight = $loaded.outerHeight(true);
				loadedWidth = $loaded.outerWidth(true);
				
				
				// Opens inital empty ColorBox prior to content being loaded.
				settings.w = setSize(settings.initialWidth, 'x');
				settings.h = setSize(settings.initialHeight, 'y');
				publicMethod.position();

				if (isIE6) {
					$window.bind('resize.' + event_ie6 + ' scroll.' + event_ie6, function () {
						$overlay.css({width: $window.width(), height: winheight(), top: $window.scrollTop(), left: $window.scrollLeft()});
					}).trigger('resize.' + event_ie6);
				}
				
				slideshow();

				trigger(event_open, settings.onOpen);
				
				$groupControls.add($title).hide();

				$box.focus();
				
				// Confine focus to the modal
				// Uses event capturing that is not supported in IE8-
				if (document.addEventListener) {

					document.addEventListener('focus', trapFocus, true);
					
					$events.one(event_closed, function () {
						document.removeEventListener('focus', trapFocus, true);
					});
				}

				// Return focus on closing
				if (settings.returnFocus) {
					$events.one(event_closed, function () {
						$(element).focus();
					});
				}
			}
			
			load();
		}
	}

	// ColorBox's markup needs to be added to the DOM prior to being called
	// so that the browser will go ahead and load the CSS background images.
	function appendHTML() {
		if (!$box && document.body) {
			init = false;

			$window = $(window);
			$box = $tag(div).attr({
				id: colorbox,
				'class': isIE ? prefix + (isIE6 ? 'IE6' : 'IE') : '',
				role: 'dialog',
				tabindex: '-1'
			}).hide();
			$overlay = $tag(div, "Overlay", isIE6 ? 'position:absolute' : '').hide();
			$loadingOverlay = $tag(div, "LoadingOverlay").add($tag(div, "LoadingGraphic"));
			$wrap = $tag(div, "Wrapper");
			$content = $tag(div, "Content").append(
				$title = $tag(div, "Title"),
				$current = $tag(div, "Current"),
				$prev = $tag('button', "Previous"),
				$next = $tag('button', "Next"),
				$slideshow = $tag('button', "Slideshow"),
				$loadingOverlay,
				$close = $tag('button', "Close")
			);
			
			$wrap.append( // The 3x3 Grid that makes up ColorBox
				$tag(div).append(
					$tag(div, "TopLeft"),
					$topBorder = $tag(div, "TopCenter"),
					$tag(div, "TopRight")
				),
				$tag(div, false, 'clear:left').append(
					$leftBorder = $tag(div, "MiddleLeft"),
					$content,
					$rightBorder = $tag(div, "MiddleRight")
				),
				$tag(div, false, 'clear:left').append(
					$tag(div, "BottomLeft"),
					$bottomBorder = $tag(div, "BottomCenter"),
					$tag(div, "BottomRight")
				)
			).find('div div').css({'float': 'left'});
			
			$loadingBay = $tag(div, false, 'position:absolute; width:9999px; visibility:hidden; display:none');
			
			$groupControls = $next.add($prev).add($current).add($slideshow);

			$(document.body).append($overlay, $box.append($wrap, $loadingBay));
		}
	}

	// Add ColorBox's event bindings
	function addBindings() {
		function clickHandler(e) {
			// ignore non-left-mouse-clicks and clicks modified with ctrl / command, shift, or alt.
			// See: http://jacklmoore.com/notes/click-events/
			if (!(e.which > 1 || e.shiftKey || e.altKey || e.metaKey)) {
				e.preventDefault();
				launch(this);
			}
		}

		if ($box) {
			if (!init) {
				init = true;

				// Anonymous functions here keep the public method from being cached, thereby allowing them to be redefined on the fly.
				$next.click(function () {
					publicMethod.next();
				});
				$prev.click(function () {
					publicMethod.prev();
				});
				$close.click(function () {
					publicMethod.close();
				});
				$overlay.click(function () {
					if (settings.overlayClose) {
						publicMethod.close();
					}
				});
				
				// Key Bindings
				$(document).bind('keydown.' + prefix, function (e) {
					var key = e.keyCode;
					if (open && settings.escKey && key === 27) {
						e.preventDefault();
						publicMethod.close();
					}
					if (open && settings.arrowKey && $related[1] && !e.altKey) {
						if (key === 37) {
							e.preventDefault();
							$prev.click();
						} else if (key === 39) {
							e.preventDefault();
							$next.click();
						}
					}
				});

				if ($.isFunction($.fn.on)) {
					// For jQuery 1.7+
					$(document).on('click.'+prefix, '.'+boxElement, clickHandler);
				} else {
					// For jQuery 1.3.x -> 1.6.x
					// This code is never reached in jQuery 1.9, so do not contact me about 'live' being removed.
					// This is not here for jQuery 1.9, it's here for legacy users.
					$('.'+boxElement).live('click.'+prefix, clickHandler);
				}
			}
			return true;
		}
		return false;
	}

	// Don't do anything if ColorBox already exists.
	if ($.colorbox) {
		return;
	}

	// Append the HTML when the DOM loads
	$(appendHTML);


	// ****************
	// PUBLIC FUNCTIONS
	// Usage format: $.fn.colorbox.close();
	// Usage from within an iframe: parent.$.fn.colorbox.close();
	// ****************
	
	publicMethod = $.fn[colorbox] = $[colorbox] = function (options, callback) {
		var $this = this;
		
		options = options || {};
		
		appendHTML();

		if (addBindings()) {
			if ($.isFunction($this)) { // assume a call to $.colorbox
				$this = $('<a/>');
				options.open = true;
			} else if (!$this[0]) { // colorbox being applied to empty collection
				return $this;
			}
			
			if (callback) {
				options.onComplete = callback;
			}
			
			$this.each(function () {
				$.data(this, colorbox, $.extend({}, $.data(this, colorbox) || defaults, options));
			}).addClass(boxElement);
			
			if (($.isFunction(options.open) && options.open.call($this)) || options.open) {
				launch($this[0]);
			}
		}
		
		return $this;
	};

	publicMethod.position = function (speed, loadedCallback) {
		var
		css,
		top = 0,
		left = 0,
		offset = $box.offset(),
		scrollTop,
		scrollLeft;
		
		$window.unbind('resize.' + prefix);

		// remove the modal so that it doesn't influence the document width/height
		$box.css({top: -9e4, left: -9e4});

		scrollTop = $window.scrollTop();
		scrollLeft = $window.scrollLeft();

		if (settings.fixed && !isIE6) {
			offset.top -= scrollTop;
			offset.left -= scrollLeft;
			$box.css({position: 'fixed'});
		} else {
			top = scrollTop;
			left = scrollLeft;
			$box.css({position: 'absolute'});
		}

		// keeps the top and left positions within the browser's viewport.
		if (settings.right !== false) {
			left += Math.max($window.width() - settings.w - loadedWidth - interfaceWidth - setSize(settings.right, 'x'), 0);
		} else if (settings.left !== false) {
			left += setSize(settings.left, 'x');
		} else {
			left += Math.round(Math.max($window.width() - settings.w - loadedWidth - interfaceWidth, 0) / 2);
		}
		
		if (settings.bottom !== false) {
			top += Math.max(winheight() - settings.h - loadedHeight - interfaceHeight - setSize(settings.bottom, 'y'), 0);
		} else if (settings.top !== false) {
			top += setSize(settings.top, 'y');
		} else {
			top += Math.round(Math.max(winheight() - settings.h - loadedHeight - interfaceHeight, 0) / 2);
		}

		$box.css({top: offset.top, left: offset.left, visibility:'visible'});

		// setting the speed to 0 to reduce the delay between same-sized content.
		speed = ($box.width() === settings.w + loadedWidth && $box.height() === settings.h + loadedHeight) ? 0 : speed || 0;
		
		// this gives the wrapper plenty of breathing room so it's floated contents can move around smoothly,
		// but it has to be shrank down around the size of div#colorbox when it's done.  If not,
		// it can invoke an obscure IE bug when using iframes.
		$wrap[0].style.width = $wrap[0].style.height = "9999px";
		
		function modalDimensions(that) {
			$topBorder[0].style.width = $bottomBorder[0].style.width = $content[0].style.width = (parseInt(that.style.width,10) - interfaceWidth)+'px';
			$content[0].style.height = $leftBorder[0].style.height = $rightBorder[0].style.height = (parseInt(that.style.height,10) - interfaceHeight)+'px';
		}

		css = {width: settings.w + loadedWidth + interfaceWidth, height: settings.h + loadedHeight + interfaceHeight, top: top, left: left};

		if(speed===0){ // temporary workaround to side-step jQuery-UI 1.8 bug (http://bugs.jquery.com/ticket/12273)
			$box.css(css);
		}
		$box.dequeue().animate(css, {
			duration: speed,
			complete: function () {
				modalDimensions(this);
				
				active = false;
				
				// shrink the wrapper down to exactly the size of colorbox to avoid a bug in IE's iframe implementation.
				$wrap[0].style.width = (settings.w + loadedWidth + interfaceWidth) + "px";
				$wrap[0].style.height = (settings.h + loadedHeight + interfaceHeight) + "px";
				
				if (settings.reposition) {
					setTimeout(function () {  // small delay before binding onresize due to an IE8 bug.
						$window.bind('resize.' + prefix, publicMethod.position);
					}, 1);
				}

				if (loadedCallback) {
					loadedCallback();
				}
			},
			step: function () {
				modalDimensions(this);
			}
		});
	};

	publicMethod.resize = function (options) {
		if (open) {
			options = options || {};
			
			if (options.width) {
				settings.w = setSize(options.width, 'x') - loadedWidth - interfaceWidth;
			}
			if (options.innerWidth) {
				settings.w = setSize(options.innerWidth, 'x');
			}
			$loaded.css({width: settings.w});
			
			if (options.height) {
				settings.h = setSize(options.height, 'y') - loadedHeight - interfaceHeight;
			}
			if (options.innerHeight) {
				settings.h = setSize(options.innerHeight, 'y');
			}
			if (!options.innerHeight && !options.height) {
				$loaded.css({height: "auto"});
				settings.h = $loaded.height();
			}
			$loaded.css({height: settings.h});
			
			publicMethod.position(settings.transition === "none" ? 0 : settings.speed);
		}
	};

	publicMethod.prep = function (object) {
		if (!open) {
			return;
		}
		
		var callback, speed = settings.transition === "none" ? 0 : settings.speed;

		$loaded.empty().remove(); // Using empty first may prevent some IE7 issues.

		$loaded = $tag(div, 'LoadedContent').append(object);
		
		function getWidth() {
			settings.w = settings.w || $loaded.width();
			settings.w = settings.mw && settings.mw < settings.w ? settings.mw : settings.w;
			return settings.w;
		}
		function getHeight() {
			settings.h = settings.h || $loaded.height();
			settings.h = settings.mh && settings.mh < settings.h ? settings.mh : settings.h;
			return settings.h;
		}
		
		$loaded.hide()
		.appendTo($loadingBay.show())// content has to be appended to the DOM for accurate size calculations.
		.css({width: getWidth(), overflow: settings.scrolling ? 'auto' : 'hidden'})
		.css({height: getHeight()})// sets the height independently from the width in case the new width influences the value of height.
		.prependTo($content);
		
		$loadingBay.hide();
		
		// floating the IMG removes the bottom line-height and fixed a problem where IE miscalculates the width of the parent element as 100% of the document width.
		
		$(photo).css({'float': 'none'});

		callback = function () {
			var total = $related.length,
				iframe,
				frameBorder = 'frameBorder',
				allowTransparency = 'allowTransparency',
				complete;
			
			if (!open) {
				return;
			}
			
			function removeFilter() {
				if (isIE) {
					$box[0].style.removeAttribute('filter');
				}
			}
			
			complete = function () {
				clearTimeout(loadingTimer);
				$loadingOverlay.hide();
				trigger(event_complete, settings.onComplete);
			};
			
			if (isIE) {
				//This fadeIn helps the bicubic resampling to kick-in.
				if (photo) {
					$loaded.fadeIn(100);
				}
			}
			
			$title.html(settings.title).add($loaded).show();
			
			if (total > 1) { // handle grouping
				if (typeof settings.current === "string") {
					$current.html(settings.current.replace('{current}', index + 1).replace('{total}', total)).show();
				}
				
				$next[(settings.loop || index < total - 1) ? "show" : "hide"]().html(settings.next);
				$prev[(settings.loop || index) ? "show" : "hide"]().html(settings.previous);
				
				if (settings.slideshow) {
					$slideshow.show();
				}
				
				// Preloads images within a rel group
				if (settings.preloading) {
					$.each([getIndex(-1), getIndex(1)], function(){
						var src,
							img,
							i = $related[this],
							data = $.data(i, colorbox);

						if (data && data.href) {
							src = data.href;
							if ($.isFunction(src)) {
								src = src.call(i);
							}
						} else {
							src = $(i).attr('href');
						}

						if (src && isImage(data, src)) {
							src = retinaUrl(data, src);
							img = new Image();
							img.src = src;
						}
					});
				}
			} else {
				$groupControls.hide();
			}
			
			if (settings.iframe) {
				iframe = $tag('iframe')[0];
				
				if (frameBorder in iframe) {
					iframe[frameBorder] = 0;
				}
				
				if (allowTransparency in iframe) {
					iframe[allowTransparency] = "true";
				}

				if (!settings.scrolling) {
					iframe.scrolling = "no";
				}
				
				$(iframe)
					.attr({
						src: settings.href,
						name: (new Date()).getTime(), // give the iframe a unique name to prevent caching
						'class': prefix + 'Iframe',
						allowFullScreen : true, // allow HTML5 video to go fullscreen
						webkitAllowFullScreen : true,
						mozallowfullscreen : true
					})
					.one('load', complete)
					.appendTo($loaded);
				
				$events.one(event_purge, function () {
					iframe.src = "//about:blank";
				});

				if (settings.fastIframe) {
					$(iframe).trigger('load');
				}
			} else {
				complete();
			}
			
			if (settings.transition === 'fade') {
				$box.fadeTo(speed, 1, removeFilter);
			} else {
				removeFilter();
			}
		};
		
		if (settings.transition === 'fade') {
			$box.fadeTo(speed, 0, function () {
				publicMethod.position(0, callback);
			});
		} else {
			publicMethod.position(speed, callback);
		}
	};

	function load () {
		var href, setResize, prep = publicMethod.prep, $inline, request = ++requests;
		
		active = true;
		
		photo = false;
		
		element = $related[index];
		
		makeSettings();
		
		trigger(event_purge);
		
		trigger(event_load, settings.onLoad);
		
		settings.h = settings.height ?
				setSize(settings.height, 'y') - loadedHeight - interfaceHeight :
				settings.innerHeight && setSize(settings.innerHeight, 'y');
		
		settings.w = settings.width ?
				setSize(settings.width, 'x') - loadedWidth - interfaceWidth :
				settings.innerWidth && setSize(settings.innerWidth, 'x');
		
		// Sets the minimum dimensions for use in image scaling
		settings.mw = settings.w;
		settings.mh = settings.h;
		
		// Re-evaluate the minimum width and height based on maxWidth and maxHeight values.
		// If the width or height exceed the maxWidth or maxHeight, use the maximum values instead.
		if (settings.maxWidth) {
			settings.mw = setSize(settings.maxWidth, 'x') - loadedWidth - interfaceWidth;
			settings.mw = settings.w && settings.w < settings.mw ? settings.w : settings.mw;
		}
		if (settings.maxHeight) {
			settings.mh = setSize(settings.maxHeight, 'y') - loadedHeight - interfaceHeight;
			settings.mh = settings.h && settings.h < settings.mh ? settings.h : settings.mh;
		}
		
		href = settings.href;
		
		loadingTimer = setTimeout(function () {
			$loadingOverlay.show();
		}, 100);
		
		if (settings.inline) {
			// Inserts an empty placeholder where inline content is being pulled from.
			// An event is bound to put inline content back when ColorBox closes or loads new content.
			$inline = $tag(div).hide().insertBefore($(href)[0]);

			$events.one(event_purge, function () {
				$inline.replaceWith($loaded.children());
			});

			prep($(href));
		} else if (settings.iframe) {
			// IFrame element won't be added to the DOM until it is ready to be displayed,
			// to avoid problems with DOM-ready JS that might be trying to run in that iframe.
			prep(" ");
		} else if (settings.html) {
			prep(settings.html);
		} else if (isImage(settings, href)) {

			href = retinaUrl(settings, href);

			$(photo = new Image())
			.addClass(prefix + 'Photo')
			.bind('error',function () {
				settings.title = false;
				prep($tag(div, 'Error').html(settings.imgError));
			})
			.one('load', function () {
				var percent;

				if (request !== requests) {
					return;
				}

				if (settings.retinaImage && window.devicePixelRatio > 1) {
					photo.height = photo.height / window.devicePixelRatio;
					photo.width = photo.width / window.devicePixelRatio;
				}

				if (settings.scalePhotos) {
					setResize = function () {
						photo.height -= photo.height * percent;
						photo.width -= photo.width * percent;
					};
					if (settings.mw && photo.width > settings.mw) {
						percent = (photo.width - settings.mw) / photo.width;
						setResize();
					}
					if (settings.mh && photo.height > settings.mh) {
						percent = (photo.height - settings.mh) / photo.height;
						setResize();
					}
				}
				
				if (settings.h) {
					photo.style.marginTop = Math.max(settings.mh - photo.height, 0) / 2 + 'px';
				}
				
				if ($related[1] && (settings.loop || $related[index + 1])) {
					photo.style.cursor = 'pointer';
					photo.onclick = function () {
						publicMethod.next();
					};
				}
				
				if (isIE) {
					photo.style.msInterpolationMode = 'bicubic';
				}
				
				setTimeout(function () { // A pause because Chrome will sometimes report a 0 by 0 size otherwise.
					prep(photo);
				}, 1);
			});
			
			setTimeout(function () { // A pause because Opera 10.6+ will sometimes not run the onload function otherwise.
				photo.src = href;
			}, 1);
		} else if (href) {
			$loadingBay.load(href, settings.data, function (data, status) {
				if (request === requests) {
					prep(status === 'error' ? $tag(div, 'Error').html(settings.xhrError) : $(this).contents());
				}
			});
		}
	}
		
	// Navigates to the next page/image in a set.
	publicMethod.next = function () {
		if (!active && $related[1] && (settings.loop || $related[index + 1])) {
			index = getIndex(1);
			launch($related[index]);
		}
	};
	
	publicMethod.prev = function () {
		if (!active && $related[1] && (settings.loop || index)) {
			index = getIndex(-1);
			launch($related[index]);
		}
	};

	// Note: to use this within an iframe use the following format: parent.$.fn.colorbox.close();
	publicMethod.close = function () {
		if (open && !closing) {
			
			closing = true;
			
			open = false;
			
			trigger(event_cleanup, settings.onCleanup);
			
			$window.unbind('.' + prefix + ' .' + event_ie6);
			
			$overlay.fadeTo(200, 0);
			
			$box.stop().fadeTo(300, 0, function () {
			
				$box.add($overlay).css({'opacity': 1, cursor: 'auto'}).hide();
				
				trigger(event_purge);
				
				$loaded.empty().remove(); // Using empty first may prevent some IE7 issues.
				
				setTimeout(function () {
					closing = false;
					trigger(event_closed, settings.onClosed);
				}, 1);
			});
		}
	};

	// Removes changes ColorBox made to the document, but does not remove the plugin
	// from jQuery.
	publicMethod.remove = function () {
		$([]).add($box).add($overlay).remove();
		$box = null;
		$('.' + boxElement)
			.removeData(colorbox)
			.removeClass(boxElement);

		$(document).unbind('click.'+prefix);
	};

	// A method for fetching the current element ColorBox is referencing.
	// returns a jQuery object.
	publicMethod.element = function () {
		return $(element);
	};

	publicMethod.settings = defaults;

}(jQuery, document, window));

(function($) {

    // global settings
    var settings = {
        isDebug: false,
        loadEffectCSSClass: "cl-loadeffect",
        overlayOppacity: 0.65,
        maskElement: true,
        iconTypeCSSClass: "loader-dark-bg", // loader-light, loader-white-bg, loader-dark, loader-dark-bg
        overlayBackgroundColor: "#fff",
        loaderTopOffset: 'auto', // auto, or an int
        loaderRightOffset: 'auto', // auto, or an int
        loaderBottomOffset: 'auto', // auto, or an int
        loaderLeftOffset: 'auto' // auto, or an int
    };

    var loadingTimer,
    loadingIntervalActive = false,
    loadingFrame = 1;

    var methods = {

        init: function(options) {

            var ls = $.extend({}, settings, options); // local settings
            return this.each(function() {

                $this = $(this);
                $elementToMask = $(this);

                var $backgroundDiv = $('<div class="cl-updateProgressBackground tidyAfterLoad"></div>');
                if (ls.overlayBackgroundColor.length > 0) {
                    $backgroundDiv.css({ "background-color": ls.overlayBackgroundColor });
                }
                var $updateProgressDiv = $('<div class="cl-updateProgressLoader tidyAfterLoad"></div>');
                var $IconContainer = $('<div class="iconContainer"></div>');
                var $IconSprite = $('<div class="iconSprite"></div>');

                $IconSprite.addClass(ls.iconTypeCSSClass);

                $updateProgressDiv.append($IconContainer.append($IconSprite));

                // Searchs for an element within the '$this' with a class of loadEffectCSSClass (default "loadeffect")
                // Applys loading overlay to that div if found, otherwise applies to '$this' itself (legacy code for another project
                if ($('.' + ls.loadEffectCSSClass, $this).length) {
                    $elementToMask = $('.' + ls.loadEffectCSSClass, $this);
                } else {
                    $elementToMask.addClass(ls.loadEffectCSSClass).addClass('cl-removeLFClass');
                }

                // By default the CSS will center loader, can be overridden
                if (ls.loaderTopOffset != 'auto') {
                    $updateProgressDiv.css({ "top": ls.loaderTopOffset + "px", "margin-top": 0 });
                }
                if (ls.loaderRightOffset != 'auto') {
                    $updateProgressDiv.css({ "right": ls.loaderRightOffset + "px", "left": "auto" });
                }
                if (ls.loaderBottomOffset != 'auto') {
                    $updateProgressDiv.css({ "bottom": ls.loaderBottomOffset + "px", "top": "", "margin-bottom": "" });
                }
                if (ls.loaderLeftOffset != 'auto') {
                    $updateProgressDiv.css({ "left": ls.loaderLeftOffset + "px", "margin-top": 0 });
                }

                // Switch mode based on settings, dont apply mask if icon only is true
                if (ls.maskElement) {
                    // Apply styles to mask
                    $backgroundDiv.css({ "opacity": ls.overlayOppacity, "height": $elementToMask.outerHeight(), "width": $elementToMask.outerWidth() });
                    // Insert load mask and spinner
                    $elementToMask.prepend($updateProgressDiv).prepend($backgroundDiv);
                } else {
                    // Insert spinner
                    $elementToMask.prepend($updateProgressDiv);
                }

                // spin up the spinner if its not already running for another elements
                if (!loadingIntervalActive) {
                    loadingIntervalActive = true;
                    loadingTimer = setInterval(function() { animatedLoading(); }, 66);
                }

                function animatedLoading() {
                    // Selects and updates all loaders on page
                    var $loadIcons = $('.cl-updateProgressLoader .iconContainer .iconSprite');
                    if ($loadIcons.length) {
                        $loadIcons.css('background-position', '0 ' + (loadingFrame * -40) + 'px');
                        loadingFrame = (loadingFrame + 1) % 12;
                    } else {
                        loadingIntervalActive = false;
                        clearInterval(loadingTimer);
                    }
                }
            });
        },

        removeLoadingOverlay: function(options) {
            var ls = $.extend({}, settings, options); // local settings
            return this.each(function() {
                $this = $(this);

                // Remove all the inserted elements
                $('.tidyAfterLoad', $this).remove();
                // Remove all loading applied classes
                $('.cl-removeLFClass', $this).removeClass('cl-removeLFClass').removeClass(ls.loadEffectCSSClass);
                $this.removeClass('cl-removeLFClass').removeClass(ls.loadEffectCSSClass);

                // Clear the interval if no more loaders present in the whole page
                if ($('.tidyAfterLoad').length < 1) {
                    loadingIntervalActive = false;
                    clearInterval(loadingTimer);
                }
            });

        }
    };



    $.fn.LoadingOverlay = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.plugin');
        }
    };

})(jQuery);    
/**
 * Created by marlon.jerez on 21/01/2016.
 */




$(function() {
    var $header_l = $("#header_lexus.animated");
    if($header_l.length == 0)
        return;

    var $body=$("body");
    var current = "relative";
    var animating = false;
    var lastScroll = 0;

    function header_lexus(topScroll){
        var slideDown = (topScroll - lastScroll) > 0;
        if(topScroll <= 100 && current != "relative"){
            current = "relative";
            $header_l.removeClass("fixed showing");
            $body.removeClass("header_fixed");
            $header_l.removeClass("animate");
        }else if(topScroll > 100 && topScroll <= 250 &&  current != "fixed"){
            current = "fixed";
            $header_l.removeClass("showing").addClass("fixed");
            $body.addClass("header_fixed");
            if(!slideDown){
                $header_l.addClass("animate");
            }else{
                $header_l.removeClass("animate");
            }
        }else if(topScroll > 250 && current != "showing"){
            current = "showing";
            $header_l.addClass("fixed showing");
            $body.addClass("header_fixed");
            $header_l.addClass("animate");
        }


        lastScroll = topScroll;
    }
    ScrollManager.addScrollSubscriber({callbackFN: header_lexus});
});

/* 
 * The MIT License
 *
 * Copyright (c) 2012 James Allardice
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// Defines the global Placeholders object along with various utility methods
(function (global) {

    "use strict";

    // Cross-browser DOM event binding
    function addEventListener(elem, event, fn) {
        if (elem.addEventListener) {
            return elem.addEventListener(event, fn, false);
        }
        if (elem.attachEvent) {
            return elem.attachEvent("on" + event, fn);
        }
    }

    // Check whether an item is in an array (we don't use Array.prototype.indexOf so we don't clobber any existing polyfills - this is a really simple alternative)
    function inArray(arr, item) {
        var i, len;
        for (i = 0, len = arr.length; i < len; i++) {
            if (arr[i] === item) {
                return true;
            }
        }
        return false;
    }

    // Move the caret to the index position specified. Assumes that the element has focus
    function moveCaret(elem, index) {
        var range;
        if (elem.createTextRange) {
            range = elem.createTextRange();
            range.move("character", index);
            range.select();
        } else if (elem.selectionStart) {
            elem.focus();
            elem.setSelectionRange(index, index);
        }
    }

    // Attempt to change the type property of an input element
    function changeType(elem, type) {
        try {
            elem.type = type;
            return true;
        } catch (e) {
            // You can't change input type in IE8 and below
            return false;
        }
    }

    // Expose public methods
    global.Placeholders = {
        Utils: {
            addEventListener: addEventListener,
            inArray: inArray,
            moveCaret: moveCaret,
            changeType: changeType
        }
    };

}(this));

(function (global) {

    "use strict";

    var validTypes = [
            "text",
            "search",
            "url",
            "tel",
            "email",
            "password",
            "number",
            "textarea"
        ],

        // The list of keycodes that are not allowed when the polyfill is configured to hide-on-input
        badKeys = [

            // The following keys all cause the caret to jump to the end of the input value
            27, // Escape
            33, // Page up
            34, // Page down
            35, // End
            36, // Home

            // Arrow keys allow you to move the caret manually, which should be prevented when the placeholder is visible
            37, // Left
            38, // Up
            39, // Right
            40, // Down

            // The following keys allow you to modify the placeholder text by removing characters, which should be prevented when the placeholder is visible
            8, // Backspace
            46 // Delete
        ],

        // Styling variables
        placeholderStyleColor = "#ccc",
        placeholderClassName = "placeholdersjs",
        classNameRegExp = new RegExp("(?:^|\\s)" + placeholderClassName + "(?!\\S)"),

        // These will hold references to all elements that can be affected. NodeList objects are live, so we only need to get those references once
        inputs, textareas,

        // The various data-* attributes used by the polyfill
        ATTR_CURRENT_VAL = "data-placeholder-value",
        ATTR_ACTIVE = "data-placeholder-active",
        ATTR_INPUT_TYPE = "data-placeholder-type",
        ATTR_FORM_HANDLED = "data-placeholder-submit",
        ATTR_EVENTS_BOUND = "data-placeholder-bound",
        ATTR_OPTION_FOCUS = "data-placeholder-focus",
        ATTR_OPTION_LIVE = "data-placeholder-live",
        ATTR_MAXLENGTH = "data-placeholder-maxlength",

        // Various other variables used throughout the rest of the script
        test = document.createElement("input"),
        head = document.getElementsByTagName("head")[0],
        root = document.documentElement,
        Placeholders = global.Placeholders,
        Utils = Placeholders.Utils,
        hideOnInput, liveUpdates, keydownVal, styleElem, styleRules, placeholder, timer, form, elem, len, i;

    // No-op (used in place of public methods when native support is detected)
    function noop() {}

    // Avoid IE9 activeElement of death when an iframe is used.
    // More info:
    // http://bugs.jquery.com/ticket/13393
    // https://github.com/jquery/jquery/commit/85fc5878b3c6af73f42d61eedf73013e7faae408
    function safeActiveElement() {
        try {
            return document.activeElement;
        } catch (err) {}
    }

    // Hide the placeholder value on a single element. Returns true if the placeholder was hidden and false if it was not (because it wasn't visible in the first place)
    function hidePlaceholder(elem, keydownValue) {
        var type,
            maxLength,
            valueChanged = (!!keydownValue && elem.value !== keydownValue),
            isPlaceholderValue = (elem.value === elem.getAttribute(ATTR_CURRENT_VAL));

        if ((valueChanged || isPlaceholderValue) && elem.getAttribute(ATTR_ACTIVE) === "true") {
            elem.removeAttribute(ATTR_ACTIVE);
            elem.value = elem.value.replace(elem.getAttribute(ATTR_CURRENT_VAL), "");
            elem.className = elem.className.replace(classNameRegExp, "");

            // Restore the maxlength value
            maxLength = elem.getAttribute(ATTR_MAXLENGTH);
            if (parseInt(maxLength, 10) >= 0) { // Old FF returns -1 if attribute not set (see GH-56)
                elem.setAttribute("maxLength", maxLength);
                elem.removeAttribute(ATTR_MAXLENGTH);
            }

            // If the polyfill has changed the type of the element we need to change it back
            type = elem.getAttribute(ATTR_INPUT_TYPE);
            if (type) {
                elem.type = type;
            }
            return true;
        }
        return false;
    }

    // Show the placeholder value on a single element. Returns true if the placeholder was shown and false if it was not (because it was already visible)
    function showPlaceholder(elem) {
        var type,
            maxLength,
            val = elem.getAttribute(ATTR_CURRENT_VAL);
        if (elem.value === "" && val) {
            elem.setAttribute(ATTR_ACTIVE, "true");
            elem.value = val;
            elem.className += " " + placeholderClassName;

            // Store and remove the maxlength value
            maxLength = elem.getAttribute(ATTR_MAXLENGTH);
            if (!maxLength) {
                elem.setAttribute(ATTR_MAXLENGTH, elem.maxLength);
                elem.removeAttribute("maxLength");
            }

            // If the type of element needs to change, change it (e.g. password inputs)
            type = elem.getAttribute(ATTR_INPUT_TYPE);
            if (type) {
                elem.type = "text";
            } else if (elem.type === "password") {
                if (Utils.changeType(elem, "text")) {
                    elem.setAttribute(ATTR_INPUT_TYPE, "password");
                }
            }
            return true;
        }
        return false;
    }

    function handleElem(node, callback) {

        var handleInputsLength, handleTextareasLength, handleInputs, handleTextareas, elem, len, i;

        // Check if the passed in node is an input/textarea (in which case it can't have any affected descendants)
        if (node && node.getAttribute(ATTR_CURRENT_VAL)) {
            callback(node);
        } else {

            // If an element was passed in, get all affected descendants. Otherwise, get all affected elements in document
            handleInputs = node ? node.getElementsByTagName("input") : inputs;
            handleTextareas = node ? node.getElementsByTagName("textarea") : textareas;

            handleInputsLength = handleInputs ? handleInputs.length : 0;
            handleTextareasLength = handleTextareas ? handleTextareas.length : 0;

            // Run the callback for each element
            for (i = 0, len = handleInputsLength + handleTextareasLength; i < len; i++) {
                elem = i < handleInputsLength ? handleInputs[i] : handleTextareas[i - handleInputsLength];
                callback(elem);
            }
        }
    }

    // Return all affected elements to their normal state (remove placeholder value if present)
    function disablePlaceholders(node) {
        handleElem(node, hidePlaceholder);
    }

    // Show the placeholder value on all appropriate elements
    function enablePlaceholders(node) {
        handleElem(node, showPlaceholder);
    }

    // Returns a function that is used as a focus event handler
    function makeFocusHandler(elem) {
        return function () {

            // Only hide the placeholder value if the (default) hide-on-focus behaviour is enabled
            if (hideOnInput && elem.value === elem.getAttribute(ATTR_CURRENT_VAL) && elem.getAttribute(ATTR_ACTIVE) === "true") {

                // Move the caret to the start of the input (this mimics the behaviour of all browsers that do not hide the placeholder on focus)
                Utils.moveCaret(elem, 0);

            } else {

                // Remove the placeholder
                hidePlaceholder(elem);
            }
        };
    }

    // Returns a function that is used as a blur event handler
    function makeBlurHandler(elem) {
        return function () {
            showPlaceholder(elem);
        };
    }

    // Functions that are used as a event handlers when the hide-on-input behaviour has been activated - very basic implementation of the "input" event
    function makeKeydownHandler(elem) {
        return function (e) {
            keydownVal = elem.value;

            //Prevent the use of the arrow keys (try to keep the cursor before the placeholder)
            if (elem.getAttribute(ATTR_ACTIVE) === "true") {
                if (keydownVal === elem.getAttribute(ATTR_CURRENT_VAL) && Utils.inArray(badKeys, e.keyCode)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    return false;
                }
            }
        };
    }
    function makeKeyupHandler(elem) {
        return function () {
            hidePlaceholder(elem, keydownVal);

            // If the element is now empty we need to show the placeholder
            if (elem.value === "") {
                elem.blur();
                Utils.moveCaret(elem, 0);
            }
        };
    }
    function makeClickHandler(elem) {
        return function () {
            if (elem === safeActiveElement() && elem.value === elem.getAttribute(ATTR_CURRENT_VAL) && elem.getAttribute(ATTR_ACTIVE) === "true") {
                Utils.moveCaret(elem, 0);
            }
        };
    }

    // Returns a function that is used as a submit event handler on form elements that have children affected by this polyfill
    function makeSubmitHandler(form) {
        return function () {

            // Turn off placeholders on all appropriate descendant elements
            disablePlaceholders(form);
        };
    }

    // Bind event handlers to an element that we need to affect with the polyfill
    function newElement(elem) {

        // If the element is part of a form, make sure the placeholder string is not submitted as a value
        if (elem.form) {
            form = elem.form;

            // If the type of the property is a string then we have a "form" attribute and need to get the real form
            if (typeof form === "string") {
                form = document.getElementById(form);
            }

            // Set a flag on the form so we know it's been handled (forms can contain multiple inputs)
            if (!form.getAttribute(ATTR_FORM_HANDLED)) {
                Utils.addEventListener(form, "submit", makeSubmitHandler(form));
                form.setAttribute(ATTR_FORM_HANDLED, "true");
            }
        }

        // Bind event handlers to the element so we can hide/show the placeholder as appropriate
        Utils.addEventListener(elem, "focus", makeFocusHandler(elem));
        Utils.addEventListener(elem, "blur", makeBlurHandler(elem));

        // If the placeholder should hide on input rather than on focus we need additional event handlers
        if (hideOnInput) {
            Utils.addEventListener(elem, "keydown", makeKeydownHandler(elem));
            Utils.addEventListener(elem, "keyup", makeKeyupHandler(elem));
            Utils.addEventListener(elem, "click", makeClickHandler(elem));
        }

        // Remember that we've bound event handlers to this element
        elem.setAttribute(ATTR_EVENTS_BOUND, "true");
        elem.setAttribute(ATTR_CURRENT_VAL, placeholder);

        // If the element doesn't have a value and is not focussed, set it to the placeholder string
        if (hideOnInput || elem !== safeActiveElement()) {
            showPlaceholder(elem);
        }
    }

    Placeholders.nativeSupport = test.placeholder !== void 0;

    if (!Placeholders.nativeSupport) {

        // Get references to all the input and textarea elements currently in the DOM (live NodeList objects to we only need to do this once)
        inputs = document.getElementsByTagName("input");
        textareas = document.getElementsByTagName("textarea");

        // Get any settings declared as data-* attributes on the root element (currently the only options are whether to hide the placeholder on focus or input and whether to auto-update)
        hideOnInput = root.getAttribute(ATTR_OPTION_FOCUS) === "false";
        liveUpdates = root.getAttribute(ATTR_OPTION_LIVE) !== "false";

        // Create style element for placeholder styles (instead of directly setting style properties on elements - allows for better flexibility alongside user-defined styles)
        styleElem = document.createElement("style");
        styleElem.type = "text/css";

        // Create style rules as text node
        styleRules = document.createTextNode("." + placeholderClassName + " { color:" + placeholderStyleColor + "; }");

        // Append style rules to newly created stylesheet
        if (styleElem.styleSheet) {
            styleElem.styleSheet.cssText = styleRules.nodeValue;
        } else {
            styleElem.appendChild(styleRules);
        }

        // Prepend new style element to the head (before any existing stylesheets, so user-defined rules take precedence)
        head.insertBefore(styleElem, head.firstChild);

        // Set up the placeholders
        for (i = 0, len = inputs.length + textareas.length; i < len; i++) {
            elem = i < inputs.length ? inputs[i] : textareas[i - inputs.length];

            // Get the value of the placeholder attribute, if any. IE10 emulating IE7 fails with getAttribute, hence the use of the attributes node
            placeholder = elem.attributes.placeholder;
            if (placeholder) {

                // IE returns an empty object instead of undefined if the attribute is not present
                placeholder = placeholder.nodeValue;

                // Only apply the polyfill if this element is of a type that supports placeholders, and has a placeholder attribute with a non-empty value
                if (placeholder && Utils.inArray(validTypes, elem.type)) {
                    newElement(elem);
                }
            }
        }

        // If enabled, the polyfill will repeatedly check for changed/added elements and apply to those as well
        timer = setInterval(function () {
            for (i = 0, len = inputs.length + textareas.length; i < len; i++) {
                elem = i < inputs.length ? inputs[i] : textareas[i - inputs.length];

                // Only apply the polyfill if this element is of a type that supports placeholders, and has a placeholder attribute with a non-empty value
                placeholder = elem.attributes.placeholder;
                if (placeholder) {
                    placeholder = placeholder.nodeValue;
                    if (placeholder && Utils.inArray(validTypes, elem.type)) {

                        // If the element hasn't had event handlers bound to it then add them
                        if (!elem.getAttribute(ATTR_EVENTS_BOUND)) {
                            newElement(elem);
                        }

                        // If the placeholder value has changed or not been initialised yet we need to update the display
                        if (placeholder !== elem.getAttribute(ATTR_CURRENT_VAL) || (elem.type === "password" && !elem.getAttribute(ATTR_INPUT_TYPE))) {

                            // Attempt to change the type of password inputs (fails in IE < 9)
                            if (elem.type === "password" && !elem.getAttribute(ATTR_INPUT_TYPE) && Utils.changeType(elem, "text")) {
                                elem.setAttribute(ATTR_INPUT_TYPE, "password");
                            }

                            // If the placeholder value has changed and the placeholder is currently on display we need to change it
                            if (elem.value === elem.getAttribute(ATTR_CURRENT_VAL)) {
                                elem.value = placeholder;
                            }

                            // Keep a reference to the current placeholder value in case it changes via another script
                            elem.setAttribute(ATTR_CURRENT_VAL, placeholder);
                        }
                    }
                } else if (elem.getAttribute(ATTR_ACTIVE)) {
                    hidePlaceholder(elem);
                    elem.removeAttribute(ATTR_CURRENT_VAL);
                }
            }

            // If live updates are not enabled cancel the timer
            if (!liveUpdates) {
                clearInterval(timer);
            }
        }, 100);
    }

    Utils.addEventListener(global, "beforeunload", function () {
        Placeholders.disable();
    });

    // Expose public methods
    Placeholders.disable = Placeholders.nativeSupport ? noop : disablePlaceholders;
    Placeholders.enable = Placeholders.nativeSupport ? noop : enablePlaceholders;

}(this));

(function ($) {

    "use strict";

    var originalValFn = $.fn.val,
        originalPropFn = $.fn.prop;

    if (!Placeholders.nativeSupport) {

        $.fn.val = function (val) {
            var originalValue = originalValFn.apply(this, arguments),
                placeholder = this.eq(0).data("placeholder-value");
            if (val === undefined && this.eq(0).data("placeholder-active") && originalValue === placeholder) {
                return "";
            }
            return originalValue;
        };

        $.fn.prop = function (name, val) {
            if (val === undefined && this.eq(0).data("placeholder-active") && name === "value") {
                return "";
            }
            return originalPropFn.apply(this, arguments);
        };
    }

}(jQuery));
