<?php

/*
 * queris the cartell api and returs responsed formattesd as json
 * // to test uses car reg 06D52678
 * //IMPORTANT, DONT USE  === TO CHECK VALUES COMPARED ON simpleXML object
 */

error_reporting(E_ALL ^ E_NOTICE);

global $config, $curl_config, $api, $jsonResult, $debug;

$debug = false;

// ################# empty query #####################
if (count($_GET) === 0) {
	header("HTTP/1.0 400 Request Error");
	echo '{"Error":"at least one parameter is required"}';
	return;
}

// ################# configuration #####################
include 'restclient.php';
$config = array(
	user => "Radical_servicetell@toyota.ie",
	pass => "TOY5ervicetell2015",
	reg_api => array(
		url => "https://www.cartell.ie/secure/xml/findvehicle/",
		defparams => array(
			servicename => "XML_Toyota_Servicetell_VRM",
			xmltype => "rawxml",
			readingtype => "kilometers"
		)
	),
	services_api => array(
		url => "https://www.cartell.ie/secure/xml/servicetell",
		defparams => array()
	),
	menu_api => array(
		url => "https://www.cartell.ie/secure/xml/servicetell/GetMenuOptions",
		defparams => array()
	)
);
$curl_config = array(
	CURLOPT_HTTPAUTH => CURLAUTH_ANY,
	CURLOPT_SSL_VERIFYPEER => false,
	CURLOPT_CONNECTTIMEOUT => 0
);

// ################# INITIALISE REST CLIENT #####################
function my_xml_decoder($response) {
	global $debug;
	$xml = (substr($response, strpos($response, "\r\n\r\n") + 4));
	//echo $response."\n";
	return new SimpleXMLElement($xml);
}

$api = new RestClient();
$api->set_option("username", $config["user"]);
$api->set_option("password", $config["pass"]);
$api->set_option('curl_options', $curl_config);
$api->set_option('format', "xml");
$api->register_decoder('xml', "my_xml_decoder");

function makeQuery($userparams, $config, $path) {
	global $api, $debug;
	$api->set_option('base_url', $config["url"]);
	//set request prameters
	$defparams = $config["defparams"];
	$params = array_merge($userparams, $defparams);
	//make query to get the service code
	$result = $api->get($path, $params);
	if ($debug)
		var_dump($result);
	if ($result->info->http_code == 200) {
		return $result->decode_response();
	} else {
		return null;
	}
}

function queryRegistration($reg) {
	global $config;
	$userparams = array('registration' => $reg);
	$reg_result = makeQuery($userparams, $config["reg_api"], "");
	if ($reg_result && $reg_result->Vehicle) {
		$reg_result = $reg_result->Vehicle;
	}
	return $reg_result;
}

function queryService($servicecode) {
	global $config;
	$userparams = array("ServiceCode" => $servicecode);
	$products = makeQuery($userparams, $config["services_api"], "");
	//var_dump($products);
	if ($products && $products->Products) {
		$products = $products->Products;
	}
	return $products;
}

function queryMenu($userparams) {
	global $config;
	return makeQuery($userparams, $config["menu_api"], "");
}

// ################# QUERIES #####################
$reg = filter_input(INPUT_GET, 'registration', FILTER_SANITIZE_SPECIAL_CHARS);
$sercode = filter_input(INPUT_GET, 'ServiceCode', FILTER_SANITIZE_SPECIAL_CHARS);
$menuOptions = filter_input(INPUT_GET, 'GetMenuOptions', FILTER_SANITIZE_SPECIAL_CHARS);
if ($sercode) {
	// ################# SERVICE query #####################
	$serv = queryService($sercode);
	if (!$serv->Error) {
		include 'db.php';
		$db = new Database();
		$lookupID = $db->store_lookup(null, $serv, $_GET);
	}
	$jsonResult = array(
		"service" => $serv,
		"lookupID" => $lookupID
	);
} else if ($reg) {
	// ################# REGISTRATION query #####################
	$vehicle = queryRegistration($reg);
	if ($vehicle && $vehicle->ServiceCode) {
		$serv = queryService((string) $vehicle->ServiceCode);
		if (!$serv->Error) {
			// insert data into db
			include 'db.php';
			$db = new Database();
			$lookupID = $db->store_lookup($vehicle, $serv);
		}
		$jsonResult = array(
			"service" => $serv,
			"vehicle" => $vehicle,
			"lookupID" => $lookupID
		);
	} else {
		//this is and error response
		$jsonResult = $vehicle;
	}
} else if ($menuOptions) {
	// ################# MENU query #####################
	unset($_GET["GetMenuOptions"]);
	$menu = queryMenu($_GET);
	$jsonResult = $menu;
}


// ################# menu query #####################

if ($jsonResult && !$debug) {
	//header('Content-Type: application/json');
	if ($jsonResult->Error)
		header("HTTP/1.0 400 Request Error");
	echo json_encode($jsonResult);
}else {
	header("HTTP/1.0 400 Request Error");
	echo '{"Error":"Server error please try later or contact admin"}';
}




