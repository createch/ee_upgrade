<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Database {

	private function storeIntoDB($form, $data) {
		$mysqli = new mysqli("localhost", "forms_user", "u2t56uEe0a6", "forms_data_capture");

		if (mysqli_connect_errno()) {
			echo "Connect failed: %s\n", mysqli_connect_error();
			return false;
		}

		if (!$this->tableExists($mysqli, $form)) {
			echo "Table doesn't exists ";
			$mysqli->close();
			return false;
		}

		if ($this->checkTableColumns($mysqli, $form, $data)) {

			$insert_data = "INSERT INTO " . $form . " (";
			$i = 0;
			foreach ($data as $key => $value) {
				$insert_data.=$key;
				$i++;
				if ($i < count($data))
					$insert_data.=",";
			}
			$insert_data.=") VALUES (";
			$i = 0;
			foreach ($data as $key => $value) {
				//$value = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING); // NOT USED AS VALUES ARE NOT FROM POST ARRAY
				$insert_data.="'$value'";
				$i++;
				if ($i < count($data))
					$insert_data.=",";
			}
			$insert_data.=")";
			//var_dump($insert_data);
			if (!$mysqli->query($insert_data)) {
				echo $insert_data . "\n";
				echo "some error occurred:\n" . $mysqli->error;
				$mysqli->close();
				return false;
			}
			$id = $mysqli->insert_id;
			$mysqli->close();
			return $id;
		}
	}

	private function tableExists($mysqli, $table) {
		$res = $mysqli->query("SHOW TABLES LIKE '$table'");
		return $res->num_rows > 0;
	}

	private function checkTableColumns($mysqli, $table, $data) {

		$result = $mysqli->query("SHOW COLUMNS FROM $table");

		$dbcolumns = array();

		while ($column = $result->fetch_assoc()) {

			$dbcolumns[] = $column['Field'];
		}

		$extracols = array_diff(array_keys($data), $dbcolumns);


		if (count($extracols) > 0) {

			$alter = '';
			foreach ($extracols as $extracol) {
				$type = 'VARCHAR(255)';
				if (gettype($data[$extracol]) == 'integer' || gettype($data[$extracol]) == 'double')
					$type = 'INTEGER';
				if ($extracol == 'query')
					$type = 'TEXT';
				//echo "ALTER TABLE contacts ADD $extracol $type;";
				$alter .= "ALTER TABLE $table ADD $extracol $type;\n";
			}

			//echo $alter;
			return false;
		}else {

			return true;
		}
	}

	public function store_lookup($car, $serv,$get=null) {
		if(!$serv){
			throw new Exception("SERVICE CODE REQUIERD");
		}
		$data = null;
		if ($car) {
			$data = array(
				'car_reg' => mysql_real_escape_string((string) $car->Registration),
				'cartell_service_code' => mysql_real_escape_string((string) $car->ServiceCode),
				'cartell_products' => mysql_real_escape_string($this->productToCSV($serv)),
				'cartell_make' => mysql_real_escape_string((string) $car->Make),
				'cartell_model' => mysql_real_escape_string((string) $car->Model),
				'cartell_description' => mysql_real_escape_string((string) $car->Description),
				'cartell_engine_capacity' => mysql_real_escape_string((string) $car->EngineCapacity),
				'cartell_transmission' =>mysql_real_escape_string( (string) $car->Transmission),
				'cartell_fuel_type' => mysql_real_escape_string((string) $car->FuelType),
				'cartell_chassis_number' => mysql_real_escape_string((string) $car->ChassisNumber),
				'cartell_first_registration_date' => mysql_real_escape_string((string) $car->FirstRegistrationDate),
				'usr_ip' =>mysql_real_escape_string( $_SERVER['REMOTE_ADDR']),
			);
		}else{
			$fuel = "other";
			$model_fuel = strtoupper($get["model"]);
			if(strpos($model_fuel,"DIESEL"))
					$fuel = "DIESEL";
			if(strpos($model_fuel,"PETROL"))
					$fuel = "PETROL";
			$model = str_replace(array("DIESEL","PETROL"), "", $model_fuel);
			$data = array(
				'cartell_service_code' => mysql_real_escape_string((string) $get["ServiceCode"]),
				'cartell_products' => mysql_real_escape_string($this->productToCSV($serv)),
				'cartell_make' =>mysql_real_escape_string( $get["make"]),
				'cartell_model' => mysql_real_escape_string($model),
				'cartell_description' => mysql_real_escape_string($get["model"]." ".$get["body"]),
				'cartell_engine_capacity' =>mysql_real_escape_string( $get["engine"]),
				'cartell_fuel_type' =>mysql_real_escape_string( $fuel),
				'usr_ip' => mysql_real_escape_string($_SERVER['REMOTE_ADDR']),
			);
		}

		//var_dump($data);
		return $this->storeIntoDB("cartell_service_lookup", $data);
	}

	public function store_form() {
		
	}

	private function productToCSV($serv) {
		$csv = "";
		$produscts = $serv->Product;
		foreach ($produscts as $key => $product) {
			$name = (string) $product->Name;
			$cat = (string) $product->Category;
			$p = (string) $product->Pricing;
			$csv .= $name . " ; " . $cat . " ; " . $p . "\n";
		}
		return $csv;
	}

}
