<?php
	date_default_timezone_set("Europe/Dublin");
	ini_set('memory_limit', '-1');
	set_time_limit(0);

	$processed=0;

	$newCarIDs=array();

	function deleteDocs(){

		$data_string = '<delete><query>*:*</query></delete>';

		$verbose = fopen('php://temp', 'rw+');                                                                               
		 
		$ch = curl_init('http://84.51.229.197:8080/solr/used-cars/update');                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_STDERR, $verbose);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/xml',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
		 
		$result = curl_exec($ch);

		rewind($verbose);
		$verboseLog = stream_get_contents($verbose);

		var_dump($verboseLog);
		var_dump($result);

		curl_close($ch);
		flush();

	}

	function commitChanges(){
	
		$verbose = fopen('php://temp', 'rw+');                                                                               
		 
		$ch = curl_init('http://84.51.229.197:8080/solr/used-cars/update?stream.body=%3Ccommit/%3E');                                                                                                                                                                                                         
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_STDERR, $verbose);                                                                                                                
		 
		$result = curl_exec($ch);

		rewind($verbose);
		$verboseLog = stream_get_contents($verbose);

		var_dump($verboseLog);
		var_dump($result);

		curl_close($ch);
		flush();

	}

	deleteDocs();

	commitChanges();

?>
