<?php
	date_default_timezone_set("Europe/Dublin");
	ini_set('memory_limit', '-1');
	set_time_limit(0);

	$processed=0;

	$newCarIDs=array();

	function sendToSolr($data){

		$data_string = json_encode($data);

		//var_dump($data_string);

		$verbose = fopen('php://temp', 'rw+');                                                                               
		 
		$ch = curl_init('http://84.51.229.197:8080/solr/used-cars/update/json');                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_STDERR, $verbose);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
		 
		$result = curl_exec($ch);

		rewind($verbose);
		$verboseLog = stream_get_contents($verbose);

		var_dump($verboseLog);
		var_dump($result);

		curl_close($ch);
		flush();

	}

	function getJsonError($error){

		switch ($error) {
	        case JSON_ERROR_NONE:
	            return ' - No errors';
	        break;
	        case JSON_ERROR_DEPTH:
	            return ' - Maximum stack depth exceeded';
	        break;
	        case JSON_ERROR_STATE_MISMATCH:
	            return ' - Underflow or the modes mismatch';
	        break;
	        case JSON_ERROR_CTRL_CHAR:
	            return ' - Unexpected control character found';
	        break;
	        case JSON_ERROR_SYNTAX:
	            return ' - Syntax error, malformed JSON';
	        break;
	        case JSON_ERROR_UTF8:
	            return ' - Malformed UTF-8 characters, possibly incorrectly encoded';
	        break;
	        default:
	            return ' - Unknown error';
	        break;
    	}
	}

	function readPage($url){

		global $newCarIDs;

		global $processed;

		try{
			$content=file_get_contents($url);
			$page=json_decode(trim($content));
			$error=json_last_error();

			if($error!=JSON_ERROR_NONE) throw new Exception('Json parsing error. Error:'.getJsonError($error).'\nData:'.$content);

			$items=$page[0]->Cars;

			echo "processing page :".$url."\n";

			if(count($items)){
				foreach ($items as $item) {
					$newCarIDs[]=$item->carID;
					$item->Price=intval($item->Price);
				}
			}

			sendToSolr($items);

			$processed++;
			echo "processed ".$processed."\n";

			if($page[0]->NextPageURL!='LastPage') {

				//sleep (2);
				readPage($page[0]->NextPageURL);

			}else{
				echo " all cars have been processed";
			}


		}catch(Exception $e){
			echo "###################Exception######################:".$e->getMessage();
			$message = $e->getMessage();
			// In case any of our lines are larger than 70 characters, we should use wordwrap()
			$message = wordwrap($message, 70, "\r\n");
			mail('psoprovici@toyota.ie, salvo.vaccarino@radical.ie', 'Solr Script Error', $message);
			exit(0);
		}

	}

	function deleteDoc($id){

		echo "deleting CarId: ".$id;

		$deleteURL="";

		$data_string = '<delete><id>'.$id.'</id></delete>';

		$verbose = fopen('php://temp', 'rw+');                                                                               
		 
		$ch = curl_init('http://84.51.229.197:8080/solr/used-cars/update');                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_STDERR, $verbose);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/xml',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
		 
		$result = curl_exec($ch);

		rewind($verbose);
		$verboseLog = stream_get_contents($verbose);

		var_dump($verboseLog);
		var_dump($result);

		curl_close($ch);
		flush();

	}


	function reloadCollection(){

		$verbose = fopen('php://temp', 'rw+');                                                                               
		 
		$ch = curl_init('http://84.51.229.197:8080/solr/admin/cores?action=RELOAD&core=used-cars');                                                                                                                                                                                                         
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_STDERR, $verbose);                                                                                                                
		 
		$result = curl_exec($ch);

		rewind($verbose);
		$verboseLog = stream_get_contents($verbose);

		var_dump($verboseLog);
		var_dump($result);

		curl_close($ch);
		flush();

	}



	function deleteMissingDocs(){

		global $newCarIDs;

		$url="http://84.51.229.197:8080/solr/used-cars/select?q=*:*&fl=carID&wt=json&indent=true&rows=10000000";
		
		$page=json_decode(trim(@file_get_contents($url)));

		foreach ($page->response->docs as $car) {
			# code...
			if(!in_array($car->carID, $newCarIDs)) deleteDoc($car->carID);
		}

	}

	function commitChanges(){
	
		$verbose = fopen('php://temp', 'rw+');                                                                               
		 
		$ch = curl_init('http://84.51.229.197:8080/solr/used-cars/update?stream.body=%3Ccommit/%3E');                                                                                                                                                                                                         
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_STDERR, $verbose);                                                                                                                
		 
		$result = curl_exec($ch);

		rewind($verbose);
		$verboseLog = stream_get_contents($verbose);

		var_dump($verboseLog);
		var_dump($result);

		curl_close($ch);
		flush();

	}

	$url="http://toyotasurvey.force.com/auc/AUC_Used_Car_List_Page";
	
	readPage($url);
	deleteMissingDocs();

	commitChanges();
	reloadCollection();

?>
