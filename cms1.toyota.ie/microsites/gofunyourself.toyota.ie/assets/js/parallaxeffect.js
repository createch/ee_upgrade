var parallexeffect=(function(){
		o={
			currentPos:0,
			effects:[],

			init:function(){
				var me=this;

				me.loop();

				return me;
			},
			loop:function(){
				var me=this;
				
				var theInt=setTimeout(function(){
					if(parseInt($(window).width())>720){
						me.updateScrollPos();
						me.doEffects();
					}
					me.loop();

				},50);
			},
			updateScrollPos:function(){
				this.currentPos=$(document).scrollTop();
			},
			percentageOf:function($el){
				var p=(this.currentPos-$el.offset().top)/parseInt($el.height());
				if(p<0){
					p=0;
				} else if(p>1){
					p=1;
				}
				return p;
			},
			percentageOfWindow:function($el){
				var p=1-($el.offset().top-this.currentPos)/parseInt($(window).height());
				if(p<0){
					p=0;
				} else if(p>1){
					p=1;
				}
				return p;
			},
			addEffect:function(obj){
				this.effects.push(obj);
			},
			// doEffects:function(){
			// 	for(var q=0;q<this.effects.length;q++){
			// 		var effect=this.effects[q];
			// 		var perc=this.percentageOf(effect.container);

			// 		var effectPerc=(perc-effect.time.start)/effect.time.end;

			// 		if(effectPerc<0){
			// 			effectPerc=0;
			// 		} else if(effectPerc>1){
			// 			effectPerc=1;
			// 		}

			// 		for(var w=0;w<effect.change.length;w++){
			// 			var change=effect.change[w];

			// 			effect.object.css(change.type,change.from+((change.to-change.from)*effectPerc));
			// 		}
			// 	}
			// },
			doEffects:function(){
				for(var q=0;q<this.effects.length;q++){
					var effect=this.effects[q];
					var perc=this.percentageOfWindow(effect.object);

					var effectPerc=(perc-effect.time.start)/effect.time.end;

					if(effectPerc<0){
						effectPerc=0;
					} else if(effectPerc>1){
						effectPerc=1;
					}

					for(var w=0;w<effect.change.length;w++){
						var change=effect.change[w];

						effect.object.css(change.type,change.from+((change.to-change.from)*effectPerc));
					}
				}
			}
		};

		return o.init();
	})();




	//----

	parallexeffect.addEffect({
		object:$('#prize-pics .image_aygo_white'),
		time:{ start:0.1, end:0.4 },
		change:[
			{ type:'opacity', from:0, to:1 },
			{ type:'marginLeft', from:-250, to:0 }
		]
	});
	parallexeffect.addEffect({
		object:$('#prize-pics .image_aygo_black'),
		time:{ start:0.1, end:0.4 },
		change:[
			{ type:'opacity', from:0, to:1 },
			{ type:'marginLeft', from:250, to:0 }
		]
	});
	parallexeffect.addEffect({
		object:$('#prize-pics .image_haribo1'),
		time:{ start:0.2, end:0.3 },
		change:[
			{ type:'opacity', from:0, to:1 },
			{ type:'marginTop', from:100, to:0 },
			{ type:'marginLeft', from:100, to:0 }
		]
	});
	parallexeffect.addEffect({
		object:$('#prize-pics .image_haribo2'),
		time:{ start:0.2, end:0.3 },
		change:[
			{ type:'opacity', from:0, to:1 },
			{ type:'marginTop', from:100, to:0 },
			{ type:'marginLeft', from:-100, to:0 }
		]
	});
	parallexeffect.addEffect({
		object:$('#prize-pics .image_jumper1'),
		time:{ start:0.2, end:0.3 },
		change:[
			{ type:'opacity', from:0, to:1 },
			{ type:'marginTop', from:50, to:0 },
			{ type:'marginLeft', from:100, to:0 }
		]
	});
	parallexeffect.addEffect({
		object:$('#prize-pics .image_jumper2'),
		time:{ start:0.2, end:0.3 },
		change:[
			{ type:'opacity', from:0, to:1 },
			{ type:'marginTop', from:50, to:0 },
			{ type:'marginLeft', from:-100, to:0 }
		]
	});
	parallexeffect.addEffect({
		object:$('#prize-pics .image_taytoman'),
		time:{ start:0.4, end:0.5 },
		change:[
			{ type:'marginTop', from:120, to:0 }
		]
	});
	parallexeffect.addEffect({
		object:$('#prize-pics .image_spacehopper1'),
		time:{ start:0, end:0.2 },
		change:[
			{ type:'marginTop', from:200, to:0 }
		]
	});
	parallexeffect.addEffect({
		object:$('#prize-pics .image_spacehopper2'),
		time:{ start:0.05, end:0.25 },
		change:[
			{ type:'marginTop', from:200, to:0 }
		]
	});

	