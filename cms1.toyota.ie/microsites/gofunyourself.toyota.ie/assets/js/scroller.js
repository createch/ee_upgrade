var scroller=(function(){
		o={
			$container:$('#locked-area .inside'),
			colors:['orange','blue','teal','yellow'],
			options:[
				
				{ value:'Give us your best attempt at rapping', h2Class:'rapping' },
				{ value:'Hop on one leg for as long as you can', h2Class:'hop' },
				{ value:'Do your best celebrity impression', h2Class:'impression' },
				{ value:'Give us your best sexy bear impression', h2Class:'bear' },
				{ value:'Tell us a joke', h2Class:'joke' },
				{ value:'Juggle 3 unusual objects', h2Class:'juggle' },
				{ value:'Sing a song in a queue', h2Class:'song' },
				{ value:'Do the cinnamon challenge', h2Class:'cinnamon' },
				{ value:'Pretend to be your favourite animal', h2Class:'animal' },
				{ value:'Give us a Next Top Model catwalk', h2Class:'catwalk' }
			],
			spinning:false,
			speed:0,
			answer:null,

			init:function(){
				var me=this;
				
				$('.buttonbig,#spin-again').click(function(){
					lockscreen.open(function(){
						me.start();
						me.spin();
					});
				});


				return me;
			},
			createOptions:function(){
				this.$container.html('<div class="scroller"><div class="arrow-left">&nbsp;</div><div class="arrow-right">&nbsp;</div><ul></ul></div>');
				this.$container.find('.scroller').prepend('<div class="indicator"><div class="padding"><div class="icon_hourglass"><img src="assets/images/icon_hourglass.png" alt="Waiting icon" /></div>We’re choosing your challenge...</div></div>');

				this.addOptions();

				this.positionArrows();
			},
			addOptions:function(){
				for(var q=0;q<this.options.length*2;q++){
					var top=0;
					if(q!=0){
						top=q*parseInt(this.$container.find('.scroller ul li:first').height());
					}
					this.$container.find('.scroller ul').append('<li class="'+this.colors[q%this.colors.length]+'" style="top: '+top+'px;" rel="'+this.options[q%this.options.length].h2Class+'"><div class="padding">'+this.options[q%this.options.length].value+'</div></li>');
				}
			},
			positionArrows:function(){
				var arrowTop=(parseInt($('body').height())/2)-parseInt($('#locked-area').css('padding-top'))-50;
				this.$container.find('.arrow-left').css('top',arrowTop);
				this.$container.find('.arrow-right').css('top',arrowTop);
			},
			getHighest:function(){
				var highest=this.$container.find('.scroller ul li:first');

				this.$container.find('.scroller ul li').each(function(){
					if(parseInt($(this).css('top'))<parseInt(highest.css('top'))){
						highest=$(this);
					}
				});
				return highest;
			},
			getLowest:function(){
				var lowest=this.$container.find('.scroller ul li:first');

				this.$container.find('.scroller ul li').each(function(){
					if(parseInt($(this).css('top'))>parseInt(lowest.css('top'))){
						lowest=$(this);
					}
				});
				return lowest;
			},
			start:function(){
				this.createOptions();
				this.loop();
			},
			spin:function(){
				this.speed=Math.floor(Math.random() * (100 - 70 + 1)) + 70;
				this.startSpin();
			},
			loop:function(){
				var me=this;
				
				var theInt=setTimeout(function(){
					if(me.spinning){
						me.$container.find('.scroller ul li').each(function(){

							var newTop=parseInt($(this).css('top'));
							
							$(this).css({
								top:newTop+me.speed
							});
						});

						lowest=me.getLowest();
						if(parseInt(lowest.css('top'))>(parseInt($(window).height())+parseInt(lowest.height()))){
							lowest.css({
								top:parseInt(me.getHighest().css('top'))-parseInt(lowest.height())
							});
						}

						me.speed-=1;
						if(me.speed<=0){
							me.speed=0;
							me.stopSpin();
						}
					}

					me.loop();

				},50);
			},
			startSpin:function(){
				this.spinning=true;
				this.$container.find('.scroller ul').addClass('spinning');
				this.$container.find('.scroller .indicator').fadeIn('slow');
			},
			stopSpin:function(){
				this.spinning=false;
				this.$container.find('.scroller ul').removeClass('spinning');
				this.$container.find('.scroller .indicator').fadeOut('slow');
				this.getAnswer();

				var originalBG=this.answer.css('background');
				this.answer.css({
					background:'white',
					color:'black'
				});

				$('h2.non-picked,.wheres-the-button').hide();
				$('#picked-challenge h4').show();
				$('#your-challenge h2').hide();
				$('h2.'+this.answer.attr('rel')).show();
				$('.before-spin').attr('style','display:none !important');
				$('.after-spin').attr('style','display:block !important');
				

				setTimeout(function(){
					lockscreen.close();
					

					$('html, body').animate({
				        scrollTop: $("#your-challenge").offset().top
				    }, 1000);
				},2000);
			},
			getAnswer:function(){
				var arrowTop=parseInt($('.arrow-left').css('top'));
				var closest=this.$container.find('.scroller ul li:first');

				this.$container.find('.scroller ul li').each(function(){
					var d1=parseInt($(this).css('top'))-arrowTop;
					var d2=parseInt(closest.css('top'))-arrowTop;

					if(Math.sqrt(d1*d1)<Math.sqrt(d2*d2)){
						closest=$(this);
					}
				});

				this.answer=closest;
			}

		};

		return o.init();
	})();