var lockscreen=(function(){
		o={
			showing:false,

			init:function(){
				var me=this;
				$(window).resize(function(){
					if(me.showing){
						me.resizeBody();
					}
				});
				return me;
			},
			open:function(fc){
				this.showing=true;
				this.resizeBody();
				$(document).scrollTop(0);
				$('#locked-area').fadeIn("slow",function(){
					fc();
				});
			},
			close:function(){
				this.showing=false;
				this.resetBody();
				$('#locked-area').fadeOut("slow");
			},
			resizeBody:function(){
				$('#locked-area').css({
					height:$(window).height(),
					width:$(window).width(),
					paddingTop:parseInt($('.primary-nav-outer').height())-21
				});

				$('.body').css({
					height:parseInt($(window).height())-parseInt($('.body').offset().top),
					width:$(window).width(),
					overflow:'hidden'
				});
			},
			resetBody:function(){
				$('.body').css({
					height:'initial',
					width:'initial',
					overflow:'initial'
				});
			}
		};

		return o.init();
	})();