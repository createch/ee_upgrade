$(document).ready(function(){
	InstagramVideos=(function(){
	    o={
	        $el:$('.instagram-videos'),
	        videos:[],
	        perPage:4,
	        current:0,

	        init:function(){
	        	var me=this;
	        	me.load();

	        	$('#latest-entries .more').click(function(e){
	        		e.preventDefault();
	        		me.nextPage();
	        	});

	        	$(window).resize(function(){
	        		me.resizeVideos();
	        	});

	            return me;
	        },

	        load:function(){
	        	var me=this;

	        	$.ajax({
	        		url 	:'http://rdappshost.com/toyota/gofunyourself/',
	        		cache 	:false,
	        		async 	:true,
	        		type 	:'POST',
	        		dataType:'jsonp',
	        		jsonpCallback: 'content'
				}).done(function (r) {
				    if(r.status=='success'){
    					me.videos=r.data.videos;
    					me.clear();
    					me.show();
    				} else {
    					
    				}
				});


	        },

	        show:function(){
	        	var till=this.videos.length>this.perPage?this.perPage:this.videos.length;
	        	for(var q=0;q<till;q++){
	        		this.make(this.videos[q]);
	        		this.current=q+1;
	        	}
	        },

	        nextPage:function(){
	        	var till=this.videos.length>(this.perPage+this.current)?(this.perPage+this.current):this.videos.length;
	        	for(var q=this.current;q<till;q++){
	        		this.make(this.videos[q]);
	        		this.current=q+1;
	        	}
	        },

	        clear:function(){
	        	this.current=0;
	        	this.$el.html('');
	        },
	        make:function(video){

	        	var html='<div class="entry grid_6">'+
						'<div class="info">'+
							'<h4>'+video.user_full_name+'</h4>'+
							'<div class="handle">@'+video.user_username+'</div>'+
							'<div class="timestamp">'+this.timeSince(video.created_time)+'</div>'+
						'</div>'+
						'<div class="instagram-video">';

						if(video.type=='video'){
							html+='<video id="video_'+video.instagram_id+'" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" width="auto" height="auto" poster="'+video.image_standard_res+'" data-setup="{}">'+
								'<source src="'+video.video_standard_res+'" type="video/mp4" />'+
								'<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>'+
							'</video>';
						} else if(video.source=='twitter'){
							html+='<img src="assets/php/i.php?i='+video.media_url+'" alt="'+video.caption+'" />';
						} else {
							html+='<img src="'+video.image_standard_res+'" alt="'+video.caption+'" />';
						}
						
					html+='</div>'+
					'</div>';

				this.$el.append(html);
				this.resizeVideos();

				if(video.type=='video'){
					videojs('video_'+video.instagram_id, { }, function(){
						  
						});
				}

				if(!this.isMore()){
					$('#latest-entries .show-more').hide();
				}
	        },

	        isMore:function(){
	        	if(this.current+1>=this.videos.length){
	        		return false;
	        	} else {
	        		return true;
	        	}
	        },

	        resizeVideos:function(){

	        	$('.instagram-video').each(function(){
		        	$(this).css({
						height:parseInt($(this).css('width'))
					});
				});
	        },

	        timeSince:function(date){
	        	var seconds = Math.floor(((new Date().getTime()/1000) - date));

			    var interval = Math.floor(seconds / 31536000);

			    if (interval >= 1) {
			        return interval + " years";
			    }
			    interval = Math.floor(seconds / 2592000);
			    if (interval >= 1) {
			        return interval + " months";
			    }
			    interval = Math.floor(seconds / 86400);
			    if (interval >= 1) {
			        return interval + " days";
			    }
			    interval = Math.floor(seconds / 3600);
			    if (interval >= 1) {
			        return interval + " hours";
			    }
			    interval = Math.floor(seconds / 60);
			    if (interval >= 1) {
			        return interval + " minutes";
			    }
			    return Math.floor(seconds) + " seconds";
	        }
	    };

	    return o.init();
	})();
});
