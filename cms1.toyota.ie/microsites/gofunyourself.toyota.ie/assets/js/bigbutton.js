$(document).ready(function(){
	resizeButton();
	resizePrizePics();
	$(window).resize(function(){
		resizeButton();
		resizePrizePics();
	});

	flash();
});

function resizeButton(){
	$('.button-container').each(function(){
		var b=$(this).find('.buttonbig');
		b.height(parseInt(b.width()));
		b.find('.text').css({
			fontSize: (parseInt(b.width())/481)*105
		});
	});
}

function resizePrizePics(){
		var b=$('#prize-pics');
		b.height(parseInt(b.width())/2);
}

function flash () {
	$('.buttonbig .top .flash').fadeToggle(1000,function(){
		flash();
	});
}

