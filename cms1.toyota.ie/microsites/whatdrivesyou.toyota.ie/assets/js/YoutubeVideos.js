var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var players=new Array();

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
	//setTimeout(function(){event.target.playVideo();}, 3000, event);
	//event.target.playVideo();
}

function onPlayerError(event) {
	//setTimeout(function(){event.target.playVideo();}, 3000, event);
	//console.log(event)
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.

var pauseFlag = false;
function onPlayerStateChange(event) {
	//console.log(event.target.d.id)
	videoID=event.target.d.id;
	if (event.data == YT.PlayerState.PLAYING) {
        ga('send', 'event', 'Videos', 'Play', 'VideoId: '+videoID, 0);
        pauseFlag = true;
    }
    // track when user clicks to Pause
    if (event.data == YT.PlayerState.PAUSED && pauseFlag) {
        ga('send', 'event', 'Videos', 'Pause', 'VideoId: '+videoID, 0);
        pauseFlag = false;
    }
    // track when video ends
    if (event.data == YT.PlayerState.ENDED) {
        ga('send', 'event', 'Videos', 'Ended', 'VideoId: '+videoID, 0);
    }
}


function createVideo(vid,width,height){
    var player = new YT.Player(vid, {
      height: height,
      width: width,
      videoId: vid,
      playerVars: { 'autoplay': 1, 'controls': 2 , 'enablejsapi':1 , 'theme': 'light'},
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange,
        'onError':onPlayerError
      }
    });

    players.push(player)

}

$(document).ready(function(){
	$('.youtube').each(function(i,e){
		$(this).append('<img src="http://img.youtube.com/vi/'+$(this).attr('rel')+'/maxresdefault.jpg" alt="Youtube video thumbnail #'+i+'" /><span class="ui-sprite play-icon-large"></span>');
		
		$(this).click(function(){
			//$(this).html('<iframe width="100%" style="height:'+(parseInt($(this).width())/16)*9+'px;" class="video" src="//www.youtube.com/embed/'+$(this).attr('rel')+'?autoplay=1" frameborder="0" allowfullscreen></iframe>');
			vid=$(this).attr('rel')
			//width=$(this).width()+'px';
			width='100%';
			height=parseInt($(this).width()/16)*9+'px';
			createVideo(vid,width,height)
		});

		VideoResizing.bindResizing($('.youtube'), 16 / 9);
	});

	$('#videoLink').click(function(e){
		if($(this).hasClass('embed-video')){
			e.preventDefault();
			
			$(this).find('.play-icon-large').remove();
			$(this).append('<div class="grid_8 centered"><iframe width="100%" style="height:'+(parseInt($(this).width())/16)*9+'px;" class="video" src="//www.youtube.com/embed/'+$(this).attr('href').replace('https://www.youtube.com/watch?v=','')+'?autoplay=1" frameborder="0" allowfullscreen></iframe></div>');
		

			VideoResizing.bindResizing($('.video'), 16 / 9);
			parallaxContentResize();

		}
	});

	parallaxContentResize();
	$(window).resize(function(){
		parallaxContentResize();
	});

	window.location.hash=hash;

});


function parallaxContentResize(){
	$('.parallax-layer').each(function(i,e){

		if(parseInt($(this).height())<parseInt($(this).find('.container_12').height())){
			var newH=$(this).find('.container_12').height();
			newH+=(marginBottom=30);
			$(this).height(newH);
			$('.parallax:nth-child('+(i+1)+')').height(newH);
			$('.parallax:nth-child('+(i+1)+') .bg-par').height(newH);
		}
	});

	$('.parallax').each(function(i,e){

		if(parseInt($(this).height())<parseInt($(this).find('.move-into-parallax').height())){
			var newH=$(this).find('.move-into-parallax').height();
			newH+=(marginBottom=30);
			$(this).height(newH);
			$(this).find('.background-layer').height(newH);
		}
	});
}
