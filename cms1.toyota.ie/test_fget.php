<?php
/**
 * Created by PhpStorm.
 * User: marlon.jerez
 * Date: 06/12/2016
 * Time: 10:06
 */

//echo @file_get_contents("http://www.toyota.ie/api/page/models/index.json");


if(isset($_GET["sleep"])){
    sleep(7);
        echo "done after 7 secs";
}else{


    $handle = curl_init();

    curl_setopt($handle, CURLOPT_URL,"http://development.toyota.ie/test_fget.php?sleep=true" );
    curl_setopt($handle, CURLOPT_POST, false);
    curl_setopt($handle, CURLOPT_BINARYTRANSFER, false);
    curl_setopt($handle, CURLOPT_HEADER, true);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($handle, CURLOPT_TIMEOUT, 5);
    curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);

    $response = curl_exec($handle);
    $hlength  = curl_getinfo($handle, CURLINFO_HEADER_SIZE);
    $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
    $body     = substr($response, $hlength);

    $curl_err = curl_errno($handle);

    if($curl_err == CURLE_OPERATION_TIMEDOUT){
        throw new Exception("URL CALL TIMEOUT");
    }

    // If HTTP response is not 200, throw exception
    if ($httpCode != 200) {
        throw new Exception($httpCode);
    }

    echo $body;

}




?>