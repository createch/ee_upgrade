/*
 * touchswipe: special jQuery event that for standard touch events to provide additional args
 *
 */
(function ($) {

    var $event = $.event,
	$special,
    touchStart = 0,
    currentTouch = 0,
    previousTouch = 0,
    currentCoOrds = { x: 0, y: 0 },
    startCoOrds = { x: 0, y: 0 },
    previousCoOrds = { x: 0, y: 0 };

    $special = $event.special.touchswipe = {
        setup: function () {
            $(this).on("touchstart touchmove touchend touchcancel", $special.handler);
        },
        teardown: function () {
            $(this).off("touchstart touchmove touchend touchcancel", $special.handler);
        },
        handler: function (event, execAsap) {

            var phase, terminalVelocity;

            switch (event.originalEvent.type) {
                case "touchstart":
                    touchStart = event.timeStamp;
                    previousTouch = event.timeStamp;
                    currentTouch = event.timeStamp;
                    startCoOrds.x = event.originalEvent.targetTouches[0].pageX;
                    startCoOrds.y = event.originalEvent.targetTouches[0].pageY;
                    currentCoOrds.x = event.originalEvent.targetTouches[0].pageX;
                    currentCoOrds.y = event.originalEvent.targetTouches[0].pageY;
                    phase = "START";
                    break;
                case "touchend":
                    phase = "END";
                    terminalVelocity = Math.abs(getDistance(previousCoOrds, currentCoOrds) / (previousTouch - currentTouch));
                    break;
                case "touchcancel":
                    phase = "CANCEL";
                    break;
                case "touchmove":
                    phase = "MOVE";
                    previousCoOrds.x = currentCoOrds.x;
                    previousCoOrds.y = currentCoOrds.y;
                    previousTouch = currentTouch;
                    currentTouch = event.timeStamp;
                    currentCoOrds.x = event.originalEvent.targetTouches[0].pageX;
                    currentCoOrds.y = event.originalEvent.targetTouches[0].pageY;
            }

            var context = this;
		    var args = Array.prototype.slice.call(arguments);
            var direction = getDirection(startCoOrds, currentCoOrds);
            var distance = getDistance(startCoOrds, currentCoOrds);
            var duration = (event.timeStamp - touchStart);
		    var dispatch = function () {
		        // set correct event type
		        event.type = "touchswipe";
		        $event.dispatch.apply(context, args);
		    };

            args.push(phase);
            args.push(direction);
            args.push(distance);
            args.push(duration);
            args.push(terminalVelocity);

            dispatch();
        }
    };

    function getAngle(startPoint, endPoint) {
        var x = startPoint.x - endPoint.x;
        var y = endPoint.y - startPoint.y;
        var r = Math.atan2(y, x); //radians
        var angle = Math.round(r * 180 / Math.PI); //degrees

        if (angle < 0) {
            angle = 360 - Math.abs(angle);
        }

        return angle;
    }

    function getDirection(startPoint, endPoint) {
        var angle = getAngle(startPoint, endPoint);

        if ((angle <= 45) && (angle >= 0)) {
            return "LEFT";
        } else if ((angle <= 360) && (angle >= 315)) {
            return "LEFT";
        } else if ((angle >= 135) && (angle <= 225)) {
            return "RIGHT";
        } else if ((angle > 45) && (angle < 135)) {
            return "DOWN";
        } else {
            return "UP";
        }
    }

    function getDistance(startPoint, endPoint) {
        return Math.round(Math.sqrt(Math.pow(endPoint.x - startPoint.x, 2) + Math.pow(endPoint.y - startPoint.y, 2)));
    }

})(jQuery);