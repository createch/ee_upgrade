;(function () {

    // ensure window.Development exists as an object.
    window.Development = typeof window.Development === "object" ? window.Development : {};

    /* 
    #############################################################################
    Work in progress elements - not finished yet...

    #############################################################################
    */

    window.Development.galleryCarousel = function (galleryContainer, options) {

        var config = $.extend({}, {
            innerContainerSelector: ".carousel-inner",
            itemSelector: ".carousel-item",
            gridWidth: 960,
            minHeight: 100,
            mouseUpMouseMoveTarget: $(window),
            mouseDownSelector: ""
        }, options);

        // private sub functions
        var methods = {
            updateBounds: function (instance) {
                instance.guttersWidth = ($(window).width() - instance.config.gridWidth) / 2;
                instance.itemsInGutter = Math.ceil(instance.guttersWidth / instance.itemsWith);
                instance.visibleInGrid = Math.floor(instance.config.gridWidth / instance.itemsWith);
                instance.startLeft = -(instance.itemsInGutter * instance.itemsWith);
                instance.maxLeft = -(instance.itemsWith * (instance.itemsCount - 1));
            },
            mouseDown: function (instance, e) {
                instance.isHandleDown = true;
                instance.$this.removeClass("transition");
                instance.startMousePos = e.clientX;
            },
            mouseUp: function (instance, e) {
                if (instance.isHandleDown) {
                    instance.isHandleDown = false;
                    instance.currentLeft = instance.workingX;
                    instance.$this.addClass("transition");
                    if (instance.currentLeft > 0) {
                        methods.slideGallery(instance, 0);
                        instance.currentLeft = 0;
                    } else if (instance.currentLeft < instance.maxLeft) {
                        methods.slideGallery(instance, instance.maxLeft);
                        instance.currentLeft = instance.maxLeft;
                    }
                }
                return false;
            },
            mouseMove: function (instance, e) {
                if (instance.isHandleDown) {
                    var newPosX = instance.currentLeft - (instance.startMousePos - e.clientX);
                    methods.slideGallery(instance, newPosX);
                }
            },
            slideGallery: function (instance, newPosX) {
                instance.workingX = newPosX;
                //console.log("slideGallery s", instance.currentLeft)
                if (Modernizr.csstransforms3d) {
                    instance.$innerContainer.css({
                        "-webkit-transform": "translate3d(" + instance.workingX + "px, 0px, 0px)",
                        "-moz-transform": "translate3d(" + instance.workingX + "px, 0px, 0px)",
                        "transform": "translate3d(" + instance.workingX + "px, 0px, 0px)"
                    });
                } else {
                    instance.$innerContainer.css({ "left": instance.workingX + "px" });
                }
            }
        };

        // Init
        $(galleryContainer).each(function () {
            var $this = $(this);
            var instance = $this.data("galleryCarousel", {});

            instance.$this = $this;
            instance.isHandleDown = false;
            instance.currentLeft = 0;
            instance.workingX = 0;
            instance.config = config;
            instance.$innerContainer = $(instance.config.innerContainerSelector, instance.$this);
            instance.$outerContainer = instance.$this.parents(instance.config.mouseDownSelector).first();
            instance.$items = $(instance.config.itemSelector, instance.$this);
            instance.itemsWith = instance.$items.first().outerWidth(true);
            instance.itemsCount = instance.$items.length;
            instance.containerWidth = instance.itemsWith * instance.itemsCount;
            instance.containerHeight = instance.config.minHeight;

            // Find the tallest item
            instance.$items.each(function () {
                instance.containerHeight = Math.max(instance.containerHeight, $(this).outerHeight(true));
            });

            // Configure bounds & resize method
            methods.updateBounds(instance);
            instance.$this.on('debouncedresize', function () { methods.updateBounds(instance); });

            // Bind mouse events
            instance.$outerContainer.on('selectstart dragstart', function () { return false; });
            instance.$outerContainer.on('mousedown', function (e) { methods.mouseDown(instance, e); });
            instance.config.mouseUpMouseMoveTarget.on('mouseup', function (e) { methods.mouseUp(instance, e); });
            instance.config.mouseUpMouseMoveTarget.on('mousemove', function (e) { methods.mouseMove(instance, e); });

            // Configure containers
            instance.$outerContainer.css({ "cursor": "move" });
            instance.$this.css({ "position": "relative", "width": instance.config.gridWidth, "height": instance.containerHeight }).addClass("carousel-active");
            instance.$innerContainer.css({ "position": "absolute", "left": 0, "width": instance.containerWidth });

            // Set initial position
            instance.currentLeft = instance.startLeft;
            methods.slideGallery(instance, instance.startLeft);

        });
    };


} ());