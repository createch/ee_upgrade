
/* 
#############################################################################
    Scroll manager - v1.0

    dependencies
    - HelperFunctions.js

    Unifies scroll type events across devices, provides some efficiency for 
    threshold type subscriptions
#############################################################################
*/ 

var ScrollManager = (function () {

    // privates
    var scrollSubscribers = [];
    var lastScrollY = 0;
    var enableCallBacks = true;
    var animatedScroll = false;

    // Handle scroll
    $(window).on('scroll', function (e) {
        ScrollManager.updateScrollSubscribers($(window).scrollTop());
    });

    // Trigger scroll on touch event also
    if (Modernizr.touch) {
        $(document).on({
            'touchstart touchmove touchend touchcancel': function (e) {
                ScrollManager.updateScrollSubscribers(window.pageYOffset);
            }
        });
    }

    // Fire scroll functions once fonts have loaded...
    $(window).on("fonts-loaded", function () {
        ScrollManager.updateScrollSubscribers($(window).scrollTop());
    });

    // Public scroll manager functions
    return {

        /*
        // Add a new callback to scroll events, subscriberData argument format:

        subscriberData {
        // Call back function - required
        callbackFN: function (scrollY) {   },
        // Call execution context object - optional
        context: $(this),
        // Interger - optional, callback will only be triggered when scolling past / above this position
        fixedThreshold: 0
        }
        */
        addScrollSubscriber: function (subscriberData) {
            if (subscriberData.callbackFN && HelperFunctions.isFunction(subscriberData.callbackFN)) {
                scrollSubscribers.push(subscriberData);
                subscriberData.callbackFN.apply(subscriberData.context || this, [lastScrollY]);
            }
        },

        animatedScrollStart: function () {
            animatedScroll = true;
        },

        animatedScrollEnd: function () {
            animatedScroll = false;
        },

        // Pauses the subscriber callbacks, called when displaying overlays to prevent out of context events firing
        pauseScrollCallbacks: function () {
            enableCallBacks = false;
        },

        // Resumes the subscriber callbacks
        resumeScrollCallbacks: function () {
            enableCallBacks = true;
            ScrollManager.updateScrollSubscribers($(window).scrollTop());
        },

        // Causes all subscriber callbacks to be executed
        updateScrollSubscribers: function (positionY) {
            if (enableCallBacks) {

                lastScrollY = positionY;
                $.each(scrollSubscribers, function (i, l) {
                    var context = l.context || this;
                    if (!l.fixedThreshold && l.fixedThreshold != 0) {
                        l.callbackFN.apply(context, [positionY, animatedScroll]);
                    } else if ((!l.lastExecution && l.lastExecution != 0) || l.lastExecution <= l.fixedThreshold && positionY > l.fixedThreshold || l.lastExecution >= l.fixedThreshold && positionY <= l.fixedThreshold) {
                        l.lastExecution = positionY;
                        l.callbackFN.apply(context, [positionY, animatedScroll]);
                    }
                });

            }
        }
    };
} ());


