﻿
		(function ($, window, document) {
			"use strict";

			var div = document.createElement("div"),
					divStyle = div.style,
					prefixes = [
						"O",
						"o",
						"ms",
						"Webkit",
						"Moz"
					],
					prefix,
					i = prefixes.length,
					properties = [
						"transform",
						"transformOrigin",
						"transformStyle",
						"transition",
						"transitionDelay",
						"transitionDuration",
						"transitionProperty",
						"transitionTimingFunction",
						"perspective",
						"perspectiveOrigin",
						"perspectiveOriginX",
						"perspectiveOriginY"
								//"backfaceVisibility"
					],
					property,
					j = prefixes.length;

			// Find the right prefix
			while (i--) {
				if (prefixes[i] + leadingUppercase(properties[0]) in divStyle) {
					prefix = prefixes[i];
					continue;
				}
			}

			// This browser is not compatible with transforms
			if (!prefix) {
				return;
			}

			// Build cssHooks for each property
			while (j--) {
				property = prefix + leadingUppercase(properties[j]);

				if (property in divStyle) {

					// px isn't the default unit of this property
					$.cssNumber[properties[j]] = true;

					// populate cssProps
					$.cssProps[properties[j]] = property;

					// MozTranform requires a complete hook because "px" is required in translate
					if (property === "MozTransform") {
						$.cssHooks[properties[j]] = {
							get: function (elem, computed) {
								return (computed ?
										// remove "px" from the computed matrix
										$.css(elem, property).split("px").join("") :
										elem.style[property]
										);
							},
							set: function (elem, value) {
								// add "px" to matrices
								if (/matrix\([^)p]*\)/.test(value))
									value = value.replace(/matrix((?:[^,]*,){4})([^,]*),([^)]*)/, "matrix$1$2px,$3px");
								elem.style[property] = value;
							}
						};
					}


				}
			}

			function leadingUppercase(word) {
				return word.slice(0, 1).toUpperCase() + word.slice(1);
			}

		})(jQuery, window, document);