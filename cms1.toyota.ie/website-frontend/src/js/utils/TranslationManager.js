
/* 
#############################################################################
    Translation manager - v0.01

    Unfified store of all translations needed by Javascript for localisation

    03/09/2013 - Added tr method for getting the translation of a string via function.
#############################################################################
*/ 

var TranslationManager = (function () {

    // Default English Translations
    var translations = {
        "showAll": "View all",
        "showAllWithCount": "View all ({0})",
        "showLess": "Show less",
        "backTo": "Back to",
        "readDetails": "Read Details",
        "close": "Close",
        "save": "Save",
        "share": "Share"
    };

    // Public
    return {

        addLocalTranslations: function (localisedTranslations) {
            translations = $.extend(true, {}, translations, localisedTranslations);
            return translations;
        },

        getLocalTranslations: function (localisedTranslations) {
            return translations;
        },

        tr: function(text) {
            return translations[text] || text;
        }
    };
} ());