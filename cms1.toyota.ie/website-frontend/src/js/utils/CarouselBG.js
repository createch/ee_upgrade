/*
 #############################################################################
 CarouselBG - v1.00
 #############################################################################
 */


var CarouselBG = (function () {

    // Public methods
    return {

        move: null,
        addCarousel: function ($carousel, options) {

            var self = this;

            var config = $.extend({}, {
                slideInterval: 3500,
                transitionTime: 1000,
                addArrows: true
            }, options);

            // Buggy in IE7 and 8 so currently disabled
            if ($('html').hasClass('lt-ie9'))
                return false;


            var $content = $carousel.find('.block');
            var $link = $carousel.find('.carousel-link');
            var $bg = $carousel.find('.carousel-bg');

            $bg.each(function () {
                var $this = $(this);
                var imageList = [];
                if (typeof $this.data("carousel-assets") != "undefined" && $this.data("carousel-assets") != null)
                    imageList = $this.data("carousel-assets").split('|');
                var intervalRef;

                if (imageList.length > 1) {
                    $this.data("carouselBG", {
                        currentIndex: 0,
                        itemsCount: imageList.length,
                        $carouselContainer: $this,
                        itemArray: [],
                        animationInProgress: false,
                        tidyTimer: false,
                        content: $content,
                        link: $link,
                        currentContent: null
                    });

                    var instance = $this.data("carouselBG");

                    instance.move = function (index) {
                        if (!instance.animationInProgress) {

                            // validate index..
                            if (index >= instance.itemsCount) {
                                index = 0;
                            } else if (index < 0) {
                                index = instance.itemsCount - 1;
                            }

                            instance.animationInProgress = true;
                            instance.currentIndex = index;
                            instance.itemArray.removeClass("active").eq(instance.currentIndex).addClass("active");
                            $('li', instance.$carouselNav).removeClass("active");
                            $('li', instance.$carouselNav).eq(instance.currentIndex).addClass("active");
                            if ($('.thumbLink').length > 0) {
                                $('.thumbLink').removeClass("active");
                                $('.thumbLink').eq(instance.currentIndex).addClass("active");
                            }
                            var $prevCont;
                            if (instance.currentContent === null) {
                                $prevCont = instance.content.eq(instance.currentIndex - 1);
                                instance.currentContent = $prevCont;
                            } else {
                                $prevCont = instance.currentContent;
                            }

                            var $nextCont = instance.content.eq(index);
                            $prevCont.removeClass("active");
                            $nextCont.addClass("active");
                            var $l = $nextCont.data("carousel-link");
                            if ($l && instance.link) {
                                instance.link.show();
                                instance.link.attr("href", $l);
                            } else {
                                instance.link.hide();
                                instance.link.attr("href", "javascript:void(0)");
                            }


                            if (!Modernizr.csstransitions) {
                                instance.itemArray.not(instance.itemArray.eq(instance.currentIndex)).hide();
                                instance.itemArray.eq(instance.currentIndex).show();
                                $prevCont.hide();
                                $nextCont.show();
                            }

                            instance.currentContent = $nextCont;

                            instance.lockTimer = setTimeout(function () {
                                instance.animationInProgress = false;
                            }, config.transitionTime);
                        }
                    };

                    self.move = instance.move;


                    instance.cycle = function () {
                        intervalRef = setInterval(function () {
                            instance.move(instance.currentIndex + 1);
                        }, config.slideInterval);
                    };

                    // Create elements for each image
                    $(imageList).each(function (index, image) {
                        var $image = $('<div style="background-image: url(' + image + ');" class="coverBG carousel-bg-item" />');
                        if (index === 0)
                            $image.addClass("active");
                        instance.itemArray.push($image);
                    });

                    // Transform JS array to $ object collection
                    instance.itemArray = $(instance.itemArray).map(function () {
                        return this.toArray();
                    });

                    // Append images, remove the no-js BG image inline style
                    instance.$carouselContainer.append(instance.itemArray).css({"background-image": ""}).addClass("active");

                    // Background scaling fix - must be applied after the elements are in the page...
                    if (!Modernizr.backgroundsize) {
                        CommonEffects.scaleBGs(instance.itemArray);
                    }

                    // Create nav container
                    instance.$navContainer = $('<div class="carousel-nav-container" />');
                    instance.$carouselContainer.prepend(instance.$navContainer);

                    var thumblinkList = [];

                    if (typeof $this.data("carousel-thumbnails") != "undefined" && $this.data("carousel-thumbnails") != null) {

                        instance.$thumbnailNav = $('<div class="thumbnails-container" ></div>');

                        instance.$wrapper = $('<div class="thumbnails-wrapper"></div>');

                        thumbnailsList = $this.data("carousel-thumbnails").split('|');

                        for (var i = 0, len = thumbnailsList.length; i < len; i++) {

                            var $thumbLink = $('<div class="thumbLink" />').append($('<a href="#" ><img src="' + thumbnailsList[i] + '" /></a>'));

                            if (i === 0) $thumbLink.addClass("active");
                            $thumbLink.on("click", function (e) {
                                clearInterval(intervalRef);
                                instance.move($(this).index());
                                e.preventDefault();
                            });

                            instance.$wrapper.append($thumbLink);
                            thumblinkList.push($thumbLink);

                        }

                        instance.$wrapper.append("<div class='clear' />");

                        instance.$thumbnailNav.append(instance.$wrapper);
                        instance.$navContainer.parent().parent().append(instance.$thumbnailNav);

                    }

                    // Create, bind and append navigation
                    instance.$carouselNav = $('<ul class="carousel-nav" />');
                    for (var i = 0, len = instance.itemsCount; i < len; i++) {
                        var $navLink = $('<li />').append($('<a href="#" />'));
                        if (i === 0)
                            $navLink.addClass("active");
                        $navLink.on("click", function (e) {
                            clearInterval(intervalRef);
                            instance.move($(this).index());
                            e.preventDefault();
                        });
                        instance.$carouselNav.append($navLink);
                    }

                    instance.$navContainer.prepend(instance.$carouselNav);


                    // Create & bind arrows
                    if (config.addArrows) {
                        instance.$leftArrow = $('<a class="carousel-arrow left icon icon-angle-left" href="" data-icon="&#xf104;" />').on("click", function (e) {
                            clearInterval(intervalRef);
                            instance.move(instance.currentIndex - 1);
                            e.preventDefault();
                        });
                        instance.$rightArrow = $('<a class="carousel-arrow right icon icon-angle-right" href="" data-icon="&#xf105;" />').on("click", function (e) {
                            clearInterval(intervalRef);
                            instance.move(instance.currentIndex + 1);
                            e.preventDefault();
                        });
                        instance.$navContainer.prepend(instance.$leftArrow).prepend(instance.$rightArrow);
                    }

                    instance.move(0);
                    instance.cycle();


                    var scrollThumbWidth = 0;
                    var scrollCurrentPos = 0;

                    function tumbnailsStart() {
                        var left = 0;
                        for (var i = 0; i < thumblinkList.length; i++) {
                            var item = thumblinkList[i];
                            var w = item.outerWidth(true);
                            item.css("left", left + "px");
                            left += w;
                        }
                        scrollThumbWidth = left;
                        var wrapperW = instance.$wrapper.width();
                        if (scrollThumbWidth > wrapperW) {
                            instance.$thumbnailNav.find(".carousel-arrow").show();
                        } else {
                            instance.$thumbnailNav.find(".carousel-arrow").hide();
                        }
                        //reset the position
                        instance.$wrapper.css("left", "0");
                        instance.$thumbnailNav.find(".carousel-arrow.left").hide();
                        scrollCurrentPos = 0;
                    };

                    // Create & bind arrows
                    if (instance.$thumbnailNav && instance.$thumbnailNav.length && instance.$wrapper) {

                        instance.$thumnailsLeftArrow = $('<a class="carousel-arrow left icon icon-angle-left" href="" data-icon="&#xf104;" />').on("click", function (e) {
                            var w = instance.$wrapper.width();
                            var next_post = scrollCurrentPos + w;

                            //hide left button
                            var next_next = next_post + w;
                            if (next_next >= 0) {
                                next_post = 0;
                                instance.$thumbnailNav.find(".carousel-arrow.left").hide();
                            }
                            //show right button
                            instance.$thumbnailNav.find(".carousel-arrow.right").show();
                            scrollCurrentPos = next_post;
                            instance.$wrapper.animate({
                                left: next_post + "px"
                            }, 1000, function () {
                                // Animation complete.
                            });
                            e.preventDefault();
                        });


                        instance.$thumnailsRightArrow = $('<a class="carousel-arrow right icon icon-angle-right" href="" data-icon="&#xf105;" />').on("click", function (e) {

                            var w = instance.$wrapper.width();
                            var next_post = scrollCurrentPos - w;

                            // hide right button
                            var next_next = next_post - w;
                            if (next_next <= -scrollThumbWidth) {
                                instance.$thumbnailNav.find(".carousel-arrow.right").hide();
                            }

                            //show left button
                            instance.$thumbnailNav.find(".carousel-arrow.left").show();
                            scrollCurrentPos = next_post;
                            instance.$wrapper.animate({
                                left: next_post + "px"
                            }, 1000, function () {
                                // Animation complete.
                            });
                            e.preventDefault();
                        });

                        instance.$thumbnailNav.prepend(instance.$thumnailsLeftArrow).prepend(instance.$thumnailsRightArrow);

                        tumbnailsStart();
                        $(window).resize(tumbnailsStart);
                    }

                } else if (imageList.length > 0) {
                    $bg.css("background-image", "url(" + imageList[0] + ")");
                }
            });
        }

    };
}());



