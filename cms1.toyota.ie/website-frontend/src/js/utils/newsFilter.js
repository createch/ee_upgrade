/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
;
(function($) {

	var $allBtn, $toyotaBtn, $dealerBtn;
	var $allBtnMore, $toyotaBtnMore, $dealerBtnMore;
	var $allItems, $toyotaItems, $dealerItems;

	var $container, $itemsContainer, $itemsContainerWrapper;

	var increment = 8;

	var currentView;

	var animate = true;





	function resetView(viewname) {
		if (viewname === currentView)
			return;
		if (viewname === ".fc-group")
			transform_grid($itemsContainer, "grid_3", "grid_6", 4);
		else
			transform_grid($itemsContainer, "grid_6", "grid_3", 4);
		currentView = viewname;
		setShowMore(viewname);
		setCurrentFilterButton(viewname);
		allPos = toyotaPos = dealerPos = 0;
		$allItems.each(function() {
			var $elem = $(this);
			hide($elem);
		});

	}

	function show($elem, i) {
		gridStak(i, $elem);
		$elem.css("display", "block");
		if (animate)
			$elem.animate({"opacity": 1}, 200, "linear");
		else
			$elem.css({"opacity": 1});
		animateContainer();
	}

	function hide($elem) {
		$elem.css("opacity", "0");
		$elem.css("display", "none");
	}
	function setShowMore(viewname) {
		$allBtnMore.css("display", "none");
		$toyotaBtnMore.css("display", "none");
		$dealerBtnMore.css("display", "none");
		$container.find(".showMore[data-section-filter='" + viewname + "']").show();
	}

	function setCurrentFilterButton(viewname) {
		$allBtn.removeClass("active");
		$toyotaBtn.removeClass("active");
		$dealerBtn.removeClass("active");
		$container.find(".filter-control[data-section-filter='" + viewname + "']").addClass("active");
	}


	var allPos = 0;
	function showAll() {
		var start = allPos;
		var end = Math.min(allPos + increment, $allItems.length);
		resetView(".fc-group");
		setTimeout(function() {
			for (var i = start; i < end; i++) {
				var $elem = $allItems.eq(i);
				show($elem, i);
			}
			if (end >= $allItems.length)
				$allBtnMore.hide();
		}, 200);

	}


	var toyotaPos = 0;
	function showToyota() {
		var start = toyotaPos;
		var end = Math.min(toyotaPos + increment, $allItems.length);
		resetView(".toyota-group");
		setTimeout(function() {
			for (var i = start; i < end; i++) {
				var $elem = $toyotaItems.eq(i);
				show($elem, i);
			}
			if (end >= $toyotaItems.length)
				$toyotaBtnMore.hide();
		}, 200);
	}

	var dealerPos = 0;
	function showDealer() {
		var start = dealerPos;
		var end = Math.min(dealerPos + increment, $allItems.length);
		resetView(".local-group");
		setTimeout(function() {
			for (var i = start; i < end; i++) {
				var $elem = $dealerItems.eq(i);
				show($elem, i);
			}
			if (end >= $dealerItems.length)
				$dealerBtnMore.hide();
		}, 200);
	}


	function gridStak(i, $elem) {
		$elem.removeClass("m2 m4");
		var m4 = (i) % 4;
		var m2 = (i) % 2;
		if (m4 === 0) {
			$elem.addClass("m4");
		} else if (m2 === 0) {
			$elem.addClass("m2");
		}
	}

	function transform_grid($wrapper, grid_x, grid_y, num) {
		var $gridElems = $wrapper.find("." + grid_x);
		//adds an exception within offer so only news get resized offers are all grid_6
		var offesr = $gridElems.find(".offer-spotlight");
		var isOffer = offesr.length > 0;
		if(isOffer)
			return;

		var top = Math.min($gridElems.length, num);
		for (var i = 0; i < top; i++) {
			$gridElems.eq(i).addClass(grid_y);
			$gridElems.eq(i).removeClass(grid_x);
		}
	}


	function animateContainer() {
		var h = $itemsContainer.height();
		$itemsContainerWrapper.stop().animate({height: h+30}, 500, "linear");
	}






	// add grid 6 to the first 4 eleemntonly in desktop
	var maximized = false;
	function maximize() {
		if ($("body").outerWidth() >= 992 && !maximized) {
			maximized = true;
			transform_grid($(".fc-group-container"), "grid_3", "grid_6", 4);
		}
	}

	function minimize() {
		initShowMore();
		if (maximized) {
			transform_grid($(".fc-group-container"), "grid_6", "grid_3", 1000);
			maximized = false;
		}
	}

	function setanimate() {
		if ($("body").outerWidth() > 900)
			animate = true;
		else
			animate = false;

	}



	$.fn.NewsFilter = function() {
		// disable anable animations
		setanimate();
		$(window).resize(setanimate);

		// init container and elemets
		$container = this;
		$itemsContainer = $container.find(".fc-group-container");
		$itemsContainerWrapper = $container.find(".fc-group-container-wrapper");
		$allItems = $container.find(".fc-group");
		$toyotaItems = $container.find(".toyota-group");
		$dealerItems = $container.find(".local-group");


		// inti filter buttons
		$allBtn = $($container.find(".filter-control[data-section-filter='.fc-group']"));
		$toyotaBtn = $($container.find(".filter-control[data-section-filter='.toyota-group']"));
		$dealerBtn = $($container.find(".filter-control[data-section-filter='.local-group']"));
		// init showMore buttons
		$allBtnMore = $($container.find(".showMore[data-section-filter='.fc-group']"));
		$toyotaBtnMore = $($container.find(".showMore[data-section-filter='.toyota-group']"));
		$dealerBtnMore = $($container.find(".showMore[data-section-filter='.local-group']"));

		
		//
		if($toyotaItems.length === 0 || $dealerItems.length === 0){
			$allBtn.parents(".filter-bar ").hide();
		}


		// show all first view
		showAll();


		//clickFilter binds.
		$allBtn.click(function(e) {
			e.preventDefault();
			showAll();
		});
		$toyotaBtn.click(function(e) {
			e.preventDefault();
			showToyota();
		});
		$dealerBtn.click(function(e) {
			e.preventDefault();
			showDealer();
		});


		//clickShowMore binds.
		$allBtnMore.click(function(e) {
			allPos += increment;
			e.preventDefault();
			showAll();
		});
		$toyotaBtnMore.click(function(e) {
			toyotaPos += increment;
			e.preventDefault();
			showToyota();
		});
		$dealerBtnMore.click(function(e) {
			dealerPos += increment;
			e.preventDefault();
			showDealer();
		});


		//return top smooth
		$('.return_top').on('click', function(e) {
			e.preventDefault();
			$.smoothScroll({
				scrollTarget: '#filterable_section'
			});
		});


	};

})(window.jQuery);


