
/* 
 #############################################################################
 Common effects - v1.00
 
 dependencies
 - ScrollManager.js
 - HelperFunctions.js
 
 Contains common functions to apply UI transitions & effects
 #############################################################################
 */

var CommonEffects = (function () {

	// Close share models
	$(window).on("closemodals", function (e) {
		$(".share-tools").removeClass("open");
		$(".tool-box", $(".share-tools:not(.always-open)")).css({"display": "none"});
	});

	$(document).on('click.closemodals', function (e) {
		$(window).trigger("closemodals");
	});

	// Public methods
	return {
		fixedScaledHeader: function ($contentWrapper, $headerContainer, heightOffset, minHeight) {
			// Add padding to wrapper to enable covers to show through
			$(window).on('debouncedresize', function () {
				if (($(window).height()) < minHeight) {
					$contentWrapper.removeClass('fixed-header').css({
						"padding-top": 0
					});
				} else {
					var offSet = ($(window).height() - minHeight) > heightOffset ? heightOffset : $(window).height() - minHeight;
					$contentWrapper.addClass('fixed-header').css({
						"padding-top": $(window).height() - offSet
					});
				}
			}).trigger('debouncedresize');
			HelperFunctions.matchViewportSize($headerContainer, heightOffset, minHeight);
		},
		fixedScaledFooter: function ($contentWrapper, $footerContainer, heightOffset, minHeight) {

			// Triggers a class when 50% down the document switching the static element behind the content from header to footer
			var scrollYThreshold = ($(document).height() / 2) - ($(window).height());

			$(window).on("fonts-loaded", function () {
				scrollYThreshold = ($(document).height() / 2) - ($(window).height());
			});

			ScrollManager.addScrollSubscriber(
					{
						callbackFN: function (topScroll) {
							//console.log("scrollYThreshold: " + scrollYThreshold + " , topScroll: " + topScroll);
							if (topScroll >= scrollYThreshold) {
								$contentWrapper.addClass("show-footer");
							} else if (topScroll < scrollYThreshold) {
								$contentWrapper.removeClass("show-footer");
							}
						}
					}
			);

			// Add padding to wrapper to enable covers to show through
			$(window).on('debouncedresize', function () {
				if ($(window).height() < minHeight) {
					$contentWrapper.removeClass('fixed-footer').css({
						"padding-bottom": 0
					});
				} else {
					$contentWrapper.addClass('fixed-footer').css({
						"padding-bottom": $(window).height() - heightOffset
					});
				}
			}).trigger('debouncedresize');
			// Scale the footer
			HelperFunctions.matchViewportSize($footerContainer, heightOffset, minHeight);
		},
		footerPopup: function ($ctaElement, distanceFromFooterToShow) {

			// Calling .height() triggers reflow so first class changes take effect...
			$ctaElement.addClass('binding bound').height();
			$ctaElement.removeClass('binding');
			$('.master-wrapper').append($ctaElement);

			ScrollManager.addScrollSubscriber(
					{
						callbackFN: function (topScroll) {
							// In bounds?
							if (topScroll > ($(document).height() - $(window).height() - distanceFromFooterToShow)) {
								$ctaElement.addClass("active");
							} else {
								$ctaElement.removeClass("active");
							}
						}
					}
			);
		},
		readMoreRows: function ($elements, translations) {
			$elements.each(function (i) {
				var heighAdjust = +0;
				var $outerWrapper = $(this);
				var $readMoreLink = $('.read-more-trigger', $outerWrapper);
				var $rowsContainer = $('.read-more-rows-container', $outerWrapper);
				var initialRows = $outerWrapper.data('initial-rows') || 1;
				var closedHeight = $('.spotlight-row', $rowsContainer).first().height() + heighAdjust;

				// If only 1 row or no read more text dont proceed
				if ($('.spotlight-row', $rowsContainer).length < 2 || !$readMoreLink.length)
					return;

				var openHeight = $rowsContainer.first().height();
				var totalItems = $('.spotlight', $rowsContainer).length;

				var str = translations.showAll + " (" + totalItems + ")";
				$readMoreLink.text(str);
				$rowsContainer.css({"height": closedHeight, "min-height": closedHeight, "overflow": "hidden"}).height();
				$rowsContainer.css({"transition": "height 0.66s ease"});
				
				
				//this is a hack to solve rendering problems only in ajax rendering
				var resize = window.setInterval(function () {
					var closedHeight = $('.spotlight-row', $rowsContainer).first().height();
					$rowsContainer.css({"height": closedHeight, "min-height": closedHeight, "overflow": "hidden"}).height();
					$rowsContainer.css({"transition": "height 0.66s ease"});
				}, 300);

				$readMoreLink.on("click", function (e) {
					clearInterval(resize);
					openHeight = $('.slide-up-element', $rowsContainer).first().height();
					e.preventDefault();
					if ($rowsContainer.hasClass("open")) {
						$rowsContainer.css({"height": closedHeight});
						$readMoreLink.text(str);
					} else {
						$rowsContainer.css({"height": openHeight});
						$readMoreLink.text(translations.showLess);
					}
					$rowsContainer.toggleClass("open");
				});


				

				// Reset heights once fonts loaded
				$(window).on("fonts-loaded", function () {
					closedHeight = $('.spotlight-row', $rowsContainer).first().height() + heighAdjust;
					$rowsContainer.css({"height": closedHeight, "min-height": closedHeight, "overflow": "hidden"}).height();
					$rowsContainer.css({"transition": "height 0.66s ease"});

					// Slide up elements as the user scrolls
					if ($("body").outerWidth(true) >= 768) {
						CommonEffects.slideUpOnView($('.slide-up-container'), false);
					}
				});
			});
		},
		wrapBy: function (div, num, wrap) {
			$.each(div, function (key, obj) {
				stack = $(obj).children();
				for (var i = 0; i < stack.length; i += num) {
					stack.slice(i, i + num).wrapAll(wrap);
				}
				$('.wrapper').append('<div class="clear"></div>');
			});
		},
		bindShareTools: function ($context) {
			$(".share-tools", $context).bind("click", function (e) {
				e.stopPropagation();
				var $this = $(this);
				var wasOpen = $this.hasClass("open");
				$(window).trigger("closemodals");
				if (!wasOpen) {
					$(".tool-box", $this).css({"display": "block"}).height();
					$this.addClass("open");
				}
			});
		},
		bindQuickSpecDislaimer: function ($context) {
			// Quick spec bindings
			$('.quick-spec-disclaimer', $context).each(function () {
				var $this = $(this);
				var $text = $('.disclaimer-text', $this);
				var $openLink = $('<a href="#" class="open"></a>').text($this.data('open-text'));
				$this.append($openLink);
				var $closeLink = $('<a href="#" class="close"></a>').text($this.data('close-text'));
				$text.append($closeLink).hide();

				$openLink.on("click", function (e) {
					e.preventDefault();
					$openLink.hide();
					$text.show();
				});

				$closeLink.on("click", function (e) {
					e.preventDefault();
					$openLink.show();
					$text.hide();
				});

			});
		},
		parraliseBG: function ($elements, height, startRange, endRange) {

			// Disabled on touch devices
			if (!Modernizr.touch) {
				$elements.each(function () {
					var $element = $(this);

					var onScroll = function (topScroll) {
						var element = this;
						element.height();
						var offset = element.offset();
						var offsetTop = offset.top - $(window).height() - 50; // +height; // restore and it wont start till fully in view
						var offsetBottom = offset.top + height + 50;


						// console.log("offset: " + offset + " offsetTop: " + offsetTop + " offsetBottom: " + offsetBottom);

						// In view?
						if (topScroll > offsetTop && topScroll < offsetBottom) {
							var relScroll = topScroll - offsetTop;
							var range = ($(window).height() + height);
							var fraction = relScroll / range;
							var newY = (1 - fraction) * (startRange);
							if (Modernizr.csstransforms3d) {
								element.css({
									"-webkit-transform": "translate3d(0px, " + ((endRange + newY)) + "px, 0px)",
									"-moz-transform": "translate3d(0px, " + (endRange + newY) + "px, 0px)",
									"transform": "translate3d(0px, " + (endRange + newY) + "px, 0px)"
								});
							} else {
								element.css({"top": (newY + endRange) + "px"});

							}
						}
					};
					onScroll.apply($element, [$element.offset().top - 1]);
					onScroll.apply($element, [$element.offset().top]);

					ScrollManager.addScrollSubscriber(
							{
								context: $element,
								callbackFN: onScroll
							}
					);
				});
			}
		},
		parralax: function ($elements, height, startRange, endRange) {

			// Disabled on touch devices
			if (!Modernizr.touch) {
				$elements.each(function () {
					var $element = $(this);

					var onScroll = function (topScroll) {
						var element = this;
						element.height();
						var offset = element.offset();
						var offsetTop = offset.top - $(window).height() - 50; // +height; // restore and it wont start till fully in view
						var offsetBottom = offset.top + height + 50;


						//console.log("offset: " + offset + " offsetTop: " + offsetTop + " offsetBottom: " + offsetBottom);

						// In view?
						if (topScroll > offsetTop && topScroll < offsetBottom) {
							var relScroll = topScroll - offsetTop;
							var range = ($(window).height() + height);
							var fraction = relScroll / range;
							var newY = (1 - fraction) * (startRange);
							if (Modernizr.csstransforms3d) {
								element.css({
									"-webkit-transform": "translate3d(0px, " + ((endRange + newY)) + "px, 0px)",
									"-moz-transform": "translate3d(0px, " + (endRange + newY) + "px, 0px)",
									"transform": "translate3d(0px, " + (endRange + newY) + "px, 0px)"
								});
							} else {
								element.css({"top": (newY + endRange) + "px"});

							}
						}
					};
					onScroll.apply($element, [$element.offset().top - 1]);
					onScroll.apply($element, [$element.offset().top]);

					ScrollManager.addScrollSubscriber(
							{
								context: $element,
								callbackFN: onScroll
							}
					);
				});
			}
		},
		slideUpOnView: function ($elements, onceOnly) {

			// Disabled on touch devices
			if (!Modernizr.touch) {
				$elements.each(function () {
					var container = $(this);
					var element = $('.slide-up-element', container).first();
					var _slideUpVisibleRatio = 0.40;
					container.css({"min-height": container.height()}).addClass("active");

					ScrollManager.addScrollSubscriber(
							{
								callbackFN: function (topScroll) {
									var containerHeight = container.height();
									var elementOffset = container.offset();
									var offsetTop = elementOffset.top - $(window).height(); // +height; // restore and it wont start till fully in view
									var offsetBottom = elementOffset.top + containerHeight;

									// In view?
									if (topScroll > offsetTop && topScroll < offsetBottom) {

										var relScroll = topScroll - offsetTop;
										var range = ($(window).height() + containerHeight);
										var fraction = Math.min((relScroll / range) / _slideUpVisibleRatio, 1);
										var newY = containerHeight - (containerHeight * fraction);

										if (Modernizr.csstransforms3d) {
											element.css({
												"-webkit-transform": "translate3d(0px, " + (newY) + "px, 0px)",
												"-moz-transform": "translate3d(0px, " + (newY) + "px, 0px)",
												"transform": "translate3d(0px, " + (newY) + "px, 0px)"
											});
										} else {
											element.css({"top": (newY) + "px"});

										}
									} else {

										if (Modernizr.csstransforms3d) {
											element.css({
												"-webkit-transform": "translate3d(0px, " + (-containerHeight) + "px, 0px)",
												"-moz-transform": "translate3d(0px, " + (-containerHeight) + "px, 0px)",
												"transform": "translate3d(0px, " + (-containerHeight) + "px, 0px)"
											});
										} else {
											element.css({"top": (-containerHeight) + "px"});

										}
									}
								}
							}
					);
				});
			}

		},
		toggleClassWhenVisible: function ($elements, completeVisibilty, osTop, osBottom, cssClass) {

			// Disabled on touch devices
			if (!Modernizr.touch) {
				$elements.each(function () {
					var element = $(this);
					var _className = cssClass || "visible";
					var _completeVisibilty = completeVisibilty || true;
					var _offsetTop = osTop || 150;
					var _offsetBottom = osBottom || 0;
					var height = element.height();
					ScrollManager.addScrollSubscriber(
							{
								callbackFN: function (topScroll) {
									var offset = element.offset();
									var offsetTop = offset.top - $(window).height();
									if (_completeVisibilty)
										offsetTop += height;
									if (!element.hasClass('wasVisible'))
										offsetTop += _offsetTop;
									var offsetBottom = offset.top + height;
									if (!element.hasClass('wasVisible'))
										offsetBottom += _offsetBottom;

									// In view?
									if (topScroll > offsetTop && topScroll < offsetBottom) {
										element.addClass(_className).addClass('wasVisible');
									} else {
										element.removeClass(_className);
									}
								},
								context: element
							}
					);
				});
			}
		},
		scaleBGs: function ($bgElements) {
			$bgElements.each(function (index, image) {
				var $coverBG = $(this);
				var BGPath = $coverBG.css("backgroundImage").replace(window.HelperFunctions.cssBackgroundImageRegex, "$1");
				var replacementImage = new Image();
				$coverBG.css({"background-image": "", "overflow": "hidden"}).prepend($(replacementImage));

				replacementImage.onload = function () {

					$(window).on('debouncedresize', function () {
						var $replacementImage = $(replacementImage);
						$replacementImage.css({"width": "auto", "height": "auto"});

						var imageAspect = $replacementImage.width() / $replacementImage.height();
						var containerHeight = parseInt($coverBG.outerHeight(true), 10);
						var containerWidth = parseInt($coverBG.outerWidth(true), 10);
						var containerAspect = containerWidth / containerHeight;
						var tHeight;
						var tWidth;
						var tYoffset;
						var tXoffset;
						var delta;

						if (imageAspect > containerAspect) {
							delta = Math.floor(containerHeight * imageAspect - containerWidth);
							tHeight = containerHeight;
							tWidth = "auto";
							tYoffset = 0;
							tXoffset = -delta;
						} else {
							delta = Math.floor(containerWidth / imageAspect - containerHeight);
							tHeight = "auto";
							tWidth = containerWidth;
							tYoffset = -delta;
							tXoffset = 0;
						}

						$replacementImage.css({"height": tHeight, "width": tWidth, "margin-top": tYoffset, "margin-left": tXoffset}).addClass('bg-cover-replacement');
						$replacementImage.removeAttr("width").removeAttr("height");


					}).trigger('debouncedresize');
				};
				replacementImage.src = BGPath;

				//console.log(BGPath)
			});
		}
	};
}());



