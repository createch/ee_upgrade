(function ($) {

	// default settings
	var settings = {
		mode: "move", // "ajax, gallery or move"
		ajaxContentBindingTypes: [],
		ajaxContentSelector: ".ajaxable",
		slideTime: 700,
		fadeTime: 700,
		gallerySlideTime: 700,
		toolbarWidth: 73,
		itemSelector: ".secContentTrigger",
		ajaxLoaderPath: "images/ajax-loader.gif",
		cGroupName: "",
		sectionIDs: ["SecondaryContent"],
		translations: {}
	};

	//adjust toolbar for mobile
	function adjustToolbarW() {
		var w = $(document).outerWidth();
		if (w < 768)
			settings.toolbarWidth = 0;
		else
			settings.toolbarWidth = 73;
	}
	$(window).resize(adjustToolbarW);
	adjustToolbarW();

	// Cross instance vars
	var htmlCreated, $slideInContainer, $slideInContent, $parentContent, slideOpen;

	var methods = {
		init: function (options) {

			return this.each(function () {
				// Merge settings and defaults
				var instanceSettings = $.extend(true, {}, settings, options);

				// Construct context data object
				var $this = $(this);
				$this.data('SecondaryContent', {
					$this: $this,
					options: instanceSettings
				});

				var instance = $this.data('SecondaryContent');
				instance.$items = instance.$this.find(instance.options.itemSelector);

				// Create the slider HTML components
				instance.screenWidth = $(window).width();
				instance.screenHeight = $(window).height();
				if (!htmlCreated) {
					methods.createHTML.apply(instance.$this);
				}

				instance.$items.on("click", function (e) {
					e.preventDefault();

					var $theLink = $(this);
					if ($theLink.data("cancel")) {
						$theLink.data("cancel", false);
					} else {
						var target = (instance.$items.data("target"))?instance.$items.data("target"):instance.$items.attr("id");
						window.location.hash = "/" + instance.options.sectionIDs.join("/") + "/" + target;
					}
				});

				var listenArray = instance.options.sectionIDs.slice(0);
				listenArray.push("*");

				var openfunc = function myFunc(pathNames) {
					//console.log("open");
					var $item = instance.$this.find('#' + pathNames[pathNames.length - 1]);
					// hack to avoid duplacate id. the element is uding data-target instead of id
					if (!$item.length)
						$item = instance.$this.find('*[data-target="' + pathNames[pathNames.length - 1] + '"]');

					// BACKGROUND LAYER SCROLL MOVE LINKS OUTSIDE CONTAINER - TEMP FALLBACK
					if (!$item.length)
						$item = $('#' + pathNames[pathNames.length - 1]);

					if (slideOpen) {
						methods.showContent.apply(instance.$this, [$item]);
					} else {
						methods.openSecondaryContentLayer.apply(instance.$this, [$item]);	
					}

					var defferedObject = $.Deferred();
					return defferedObject.promise();
					//defferedObject.resolve();
				};

				AddressEmitter.on(listenArray, openfunc);

				AddressEmitter.on(instance.options.sectionIDs, function myFunc(pathNames) {

					if (slideOpen) {
						methods.close.apply(instance.$this);
						slideOpen = false;
					}
					var defferedObject = $.Deferred();
					return defferedObject.promise();
					//defferedObject.resolve();
				});

				AddressEmitter.on(["*"], function myFunc(pathNames) {

					if (slideOpen) {
						methods.close.apply(instance.$this);
						slideOpen = false;
					}
					var defferedObject = $.Deferred();
					return defferedObject.promise();
					//defferedObject.resolve();
				});


				//ig current location.hash is = to the secondaricontent binded then we open the secondary content
				var hash = window.location.hash;
				var secContent = instance.options.sectionIDs.join("/");
				if (hash)
					hash = hash.substring(2);
				if (hash && hash.startsWith(secContent)) {
					window.setTimeout(function () {
						if(slideOpen)
							return;
						//console.log("hash");
						window.location.hash = "";
						//use window settimeout becouse there is a problem with adress emitter
						//not workign properly (not emittin events inmediatelly)
						window.setTimeout(function () {
							//console.log("timeout");
							var target = (instance.$items.data("target"))?instance.$items.data("target"):instance.$items.attr("id");
							window.location.hash = "/" + instance.options.sectionIDs.join("/") + "/" + target;
						}, 100);
					}, 100);

				}
			});
		},
		createHTML: function () {
			var instance = $(this).data('SecondaryContent');
			if (!instance)
				return false;

			/* HACKS FOR USER TESTING - NOT PRODUCTION CODE...  */
			var closeIconClass = "";
			var varientCssClass = "v1";
			var varientType = getParameterByName("v");
			if (varientType) {
				varientCssClass = "v" + varientType;
				if (varientType == "2" || varientType == "3") {
					closeIconClass = "icon icon icon-chevron-left";
				}
				if (varientType == "4") {
					closeIconClass = "icon icon icon-angle-left";
				}
				if (varientType == "4") {
					closeIconClass = "icon icon icon-angle-left";
				}


				if (varientType == "5a") {
					closeIconClass = "icon icon icon-angle-left uiBtn redBtn";
				}
				if (varientType == "5b") {
					closeIconClass = "icon icon icon-angle-left uiBtn darkBtn";
				}
				if (varientType == "5c") {
					closeIconClass = "icon icon icon-angle-left uiBtn greyBtn";
				}

			}

			$slideInContainer = $('<div class="page-transition-slider ' + varientCssClass + '">' +
					'<div class="content-wrapper">' +
					'</div>' +
					'<div class="tool-bar">' +
					'<a href="#" class="close-link">' +
					'<span class="small-logo"></span>' +
					'<div class="cb-close">' +
					'<i class="icon icon icon-angle-left close-icon ' + closeIconClass + '" data-icon="&#xf00d;"></i>' +
					'<span href="#" class="back-text"></span>' +
					'</div>' +
					'</a>' +
					'</div>' +
					'</div>');

			$slideInContent = $('.content-wrapper', $slideInContainer);
			$slideInContent.css({"min-height": $(window).height(), "width": instance.screenWidth - instance.options.toolbarWidth, "opacity": 0});
			$(window).on('debouncedresize orientationchange', function () {
				instance.screenWidth = $(window).width();
				instance.screenHeight = $(window).height();
				$slideInContent.css({"min-height": $(window).height(), "width": instance.screenWidth - instance.options.toolbarWidth});
				var $content = $slideInContent.children(":first");
				$slideInContent.height(instance.screenHeight);
			});
			$parentContent = $('.outer-wrapper, .page-wrapper');
			slideOpen = false;
			htmlCreated = true;
			$('body').append($slideInContainer);

			function getParameterByName(name) {
				name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
				var regexS = "[\\?&]" + name + "=([^&#]*)";
				var regex = new RegExp(regexS);
				var results = regex.exec(window.location.href);
				if (results === null)
					return "";
				else
					return decodeURIComponent(results[1].replace(/\+/g, " "));
			}
		},
		openSecondaryContentLayer: function ($item) {
			var instance = $(this).data('SecondaryContent');
			if (!instance)
				return false;

			slideOpen = true;

			// Slide in the overlay
			if (Modernizr.csstransitions) {
				$slideInContainer.css({"margin-left": 0});
			} else {
				$slideInContainer.stop().animate({"margin-left": 0}, instance.options.slideTime);
			}
			// Store the main content scroll position
			$parentContent.data("scrollY", $(window).scrollTop());

			setTimeout(function () { // timeout for animation duration...

				$parentContent.hide();
				$slideInContainer.addClass("focus");
				window.scrollTo(0, 0);

				methods.showContent.apply(instance.$this, [$item]);

				$("body").css("overflow", "hidden");
			}, instance.options.slideTime);

			if (instance.options.cGroupName) {
				$('.back-text', $slideInContainer).html(instance.options.translations.backTo + " <strong>" + instance.options.cGroupName + "</strong>");
			}

			$('.tool-bar', $slideInContainer).unbind("click").on("click", function (e) {
				e.preventDefault();
				window.location.hash = "/" + instance.options.sectionIDs.join("/");
			});

		},
		showContent: function ($item) {

			var instance = $(this).data('SecondaryContent');
			if (!instance)
				return false;

			switch (instance.options.mode) {
				case "ajax":
					methods.ajaxContent.apply(instance.$this, [$item]);
					break;
				case "gallery":
					methods.openGallery.apply(instance.$this, [$item]);
					break;
				case "move":
					methods.moveContent.apply(instance.$this, [$item]);
					break;
				default:
					methods.ajaxContent.apply(instance.$this, [$item]);
			}
		},
		close: function () {
			window.location.hash = "";
			var instance = $(this).data('SecondaryContent');
			if (!instance)
				return false;

			$slideInContent.css({"opacity": 0, "display": "block"});
			setTimeout(function () {
				if (instance.galleryOpen) {
					methods.destroyGallery.apply(instance.$this, [instance]);
				}

				if (instance.$placeHolder) {
					instance.$placeHolder.replaceWith(instance.$this);
					instance.$this.trigger("moveFromSecondary");
				}
				$("body").css("overflow", "initial");
				$slideInContent.empty();
				$slideInContainer.removeClass("focus");
				$parentContent.show();
				window.scrollTo(0, $parentContent.data("scrollY"));
				if (Modernizr.csstransitions) {
					$slideInContainer.css({"margin-left": "100%"});
				} else {
					$slideInContainer.stop().animate({"margin-left": "100%"}, instance.options.slideTime);
				}
			}, Modernizr.csstransitions ? instance.options.fadeTime : 0);
		},
		openGallery: function ($item) {

			var instance = $(this).data('SecondaryContent');
			if (!instance)
				return false;

			instance.$activeItem = $item;

			if (!instance.galleryOpen) {
				instance.galleryOpen = true;
				instance.activeItemIndex = null;

				// Create and add size to containers
				instance.galleryHooks = {
					$galContainer: $('<div class="fs-gallery" />'),
					$galInnerContainer: $('<div class="fs-gallery-in preventAllAnimations" />')
				};
				instance.galleryHooks.$galContainer.append(instance.galleryHooks.$galInnerContainer);
				instance.galleryHooks.$galInnerContainer.width(Math.max((instance.screenWidth - instance.options.toolbarWidth) * instance.$items.length));
				instance.galleryHooks.$galContainer.on('selectstart dragstart', function () {
					return false;
				});

				$.each(instance.$items, function (index, value) {
					// Create a HTML chunk for each gallery item but dont add the image yet
					var $thisItem = $(value);
					var aURL = $thisItem.attr("href");
					var $itemHTML = $('<div class="fs-item coverBG" />');
					$itemHTML.data("details", {
						type: $thisItem.hasClass("video-item") ? "video" : "image",
						loaded: false,
						URL: $thisItem.attr("href")
					}).width((instance.screenWidth - instance.options.toolbarWidth));
					instance.galleryHooks.$galInnerContainer.append($itemHTML);

					// Add a background loader spinner
					$itemHTML.css({"background": "url(" + instance.options.ajaxLoaderPath + ") no-repeat center center"});
				});

				// Append the items
				$slideInContent.html(instance.galleryHooks.$galContainer);
				$slideInContent.css({"opacity": 1});

				// Create, bind and append arrows
				instance.galleryHooks.$leftArrow = $('<a class="carousel-arrow left icon icon-angle-left" href="#" data-icon="&#xf104;" />').on("click", function (e) {
					e.preventDefault();
					if ($(this).hasClass("disabled"))
						return;
					methods.galleryMoveLeft.apply(instance.$this, [instance]);
				});
				instance.galleryHooks.$rightArrow = $('<a class="carousel-arrow right icon icon-angle-right" href="#" data-icon="&#xf105;" />').on("click", function (e) {
					e.preventDefault();
					if ($(this).hasClass("disabled"))
						return;
					methods.galleryMoveRight.apply(instance.$this, [instance]);
				});
				instance.galleryHooks.$galContainer.prepend(instance.galleryHooks.$leftArrow).prepend(instance.galleryHooks.$rightArrow);

				// Create, bind and append navigation
				instance.galleryHooks.$carouselNav = $('<ul class="carousel-nav" />');
				for (var i = 0, len = instance.$items.length; i < len; i++) {
					var $navLink = $('<li />').append($('<a href="#" />'));
					if (i == instance.$items.index($item))
						$navLink.addClass("active");
					$navLink.on("click", function (e) {
						var $nextItem = instance.$items.eq($(this).index());
						methods.renderGalleryItem.apply(instance.$this, [instance.$items.index($nextItem)]);
						e.preventDefault();
					});
					instance.galleryHooks.$carouselNav.append($navLink);
				}

				instance.galleryHooks.$galContainer.prepend(instance.galleryHooks.$carouselNav);

				// Keyboard nav
				$("body").on("keydown.sc_galleryNav", function (e) {
					// left arrow
					if ((e.keyCode || e.which) == 37) {
						methods.galleryMoveLeft.apply(instance.$this, [instance]);
					}
					// right arrow
					if ((e.keyCode || e.which) == 39) {
						methods.galleryMoveRight.apply(instance.$this, [instance]);
					}
				});

				// Bind the touch events
				if (Modernizr.touch) {

					// Touch events - for touch devices 
					instance.galleryHooks.$galContainer.on("touchswipe", function (e, phase, direction, distance, duration, terminalVelocity) {

						if (phase == "START") {
							instance.galleryHooks.$galInnerContainer.addClass("preventAllAnimations");
						}
						if ((direction == "LEFT" || direction == "RIGHT") && distance > 5) {
							e.preventDefault();

							var newPointerPos;
							if (direction == "LEFT")
								newPointerPos = distance + instance.Xposition;
							if (direction == "RIGHT")
								newPointerPos = instance.Xposition - distance;

							if (phase == "END") {
								instance.galleryHooks.$galInnerContainer.height(); // force reflow
								instance.galleryHooks.$galInnerContainer.removeClass("preventAllAnimations");

								if (terminalVelocity > 2.5 || distance > (instance.screenWidth / 3)) {
									if (direction === "LEFT") {
										var nextIndex = instance.activeItemIndex + 1 == instance.$items.length ? instance.activeItemIndex : instance.activeItemIndex + 1;
										var $nextItem = instance.$items.eq(nextIndex);
										methods.renderGalleryItem.apply(instance.$this, [instance.$items.index($nextItem)]);
									} else {
										var prevIndex = instance.activeItemIndex == 0 ? 0 : instance.activeItemIndex - 1;
										var $prevItem = instance.$items.eq(prevIndex);
										methods.renderGalleryItem.apply(instance.$this, [instance.$items.index($prevItem)]);
									}
								}
								methods.scrollToPosition.apply(instance.$this, [(instance.screenWidth - instance.options.toolbarWidth) * instance.activeItemIndex]);

							} else {
								methods.setX.apply(instance.$this, [newPointerPos]);
							}
						} else if (phase == "END") {
							instance.galleryHooks.$galInnerContainer.height(); // force reflow
							instance.galleryHooks.$galInnerContainer.removeClass("preventAllAnimations");

						}
					});

				}

				// Manage window resize
				$slideInContent.css({"height": $(window).height()});
				$(window).on('debouncedresize.sc_galleryResize orientationchange.sc_galleryResize', function () {
					$slideInContent.css({"height": $(window).height()});
					instance.galleryHooks.$galInnerContainer.addClass("preventAllAnimations");
					instance.galleryHooks.$galInnerContainer.width(Math.max((instance.screenWidth - instance.options.toolbarWidth) * instance.$items.length));
					$('.fs-item', instance.galleryHooks.$galContainer).width((instance.screenWidth - instance.options.toolbarWidth));
					methods.scrollToPosition.apply(instance.$this, [(instance.screenWidth - instance.options.toolbarWidth) * instance.activeItemIndex]);
				});

				// Display the chosen item
				methods.renderGalleryItem.apply(instance.$this, [instance.$items.index($item)]);
			} else {
				methods.renderGalleryItem.apply(instance.$this, [instance.$items.index($item)]);
			}
		},
		destroyGallery: function (instance) {
			instance.galleryHooks.$leftArrow.off();
			instance.galleryHooks.$rightArrow.off();
			$('li', instance.galleryHooks.$carouselNav).off();
			instance.galleryHooks.$galContainer.off();
			$("body").off("keydown.sc_galleryNav");
			$slideInContent.css({"height": ""});
			$(window).off("debouncedresize.sc_galleryResize orientationchange.sc_galleryResize");
			instance.galleryOpen = false;
			instance.galleryHooks = {};
		},
		galleryMoveLeft: function (instance) {
			if (instance.activeItemIndex > 0) {
				var $prevItem = instance.$items.eq(instance.activeItemIndex - 1);
				methods.renderGalleryItem.apply(instance.$this, [instance.$items.index($prevItem)]);
			}
		},
		galleryMoveRight: function (instance) {
			if (instance.activeItemIndex + 1 < instance.$items.length) {
				var $nextItem = instance.$items.eq(instance.activeItemIndex + 1);
				methods.renderGalleryItem.apply(instance.$this, [instance.$items.index($nextItem)]);
			}
		},
		renderGalleryItem: function (itemIndexToShow) {
			var instance = $(this).data('SecondaryContent');
			if (!instance)
				return false;

			if (instance.activeItemIndex != itemIndexToShow) {
				instance.activeItemIndex = itemIndexToShow;

				// update arrow states
				instance.galleryHooks.$leftArrow.removeClass("disabled");
				instance.galleryHooks.$rightArrow.removeClass("disabled");
				if (instance.activeItemIndex + 1 == instance.$items.length) { // last...
					instance.galleryHooks.$rightArrow.addClass("disabled");
				} else if (instance.activeItemIndex == 0) { // first
					instance.galleryHooks.$leftArrow.addClass("disabled");
				}

				// Is it loaded - if not set load for when slide is ended
				if (instance.loadTimeout)
					clearTimeout(instance.loadTimeout);
				instance.loadTimeout = setTimeout(function () {
					var $itemHTML = $('.fs-item', instance.galleryHooks.$galInnerContainer).eq(itemIndexToShow);
					if (!$itemHTML.data('details').loaded)
						methods.loadGalleryItem.apply(instance.$this, [$itemHTML]);
				}, instance.options.gallerySlideTime);

				// Temp method to kill any existing videos...
				var $existingVideos = $('.fs-video-item', instance.galleryHooks.$galContainer).parent();
				$existingVideos.each(function ($videoToRemove) {
					$(this).empty().css({"background": "url(" + instance.options.ajaxLoaderPath + ") no-repeat center center"}).data("details").loaded = false;
				});

				$('li', instance.galleryHooks.$carouselNav).removeClass("active");
				$('li', instance.galleryHooks.$carouselNav).eq(instance.activeItemIndex).addClass("active");
			}

			methods.scrollToPosition.apply(instance.$this, [(instance.screenWidth - instance.options.toolbarWidth) * instance.activeItemIndex]);
		},
		scrollToPosition: function (newPosX) {
			var instance = $(this).data('SecondaryContent');
			if (!instance)
				return false;

			instance.Xposition = newPosX;
			methods.setX.apply(instance.$this, [newPosX]);
			instance.galleryHooks.$galInnerContainer.height(); // force reflow
			instance.galleryHooks.$galInnerContainer.removeClass("preventAllAnimations");
		},
		setX: function (newPosX) {
			var instance = $(this).data('SecondaryContent');
			if (!instance)
				return false;

			var nValue = newPosX * -1;

			if (Modernizr.csstransforms3d) {
				instance.galleryHooks.$galInnerContainer.css({
					"-webkit-transform": "translate3d(" + nValue + "px, 0px, 0px)",
					"-moz-transform": "translate3d(" + newPosX + "px, 0px, 0px)",
					"transform": "translate3d(" + nValue + "px, 0px, 0px)"
				});
			} else {
				if (!instance.galleryHooks.$galInnerContainer.hasClass("preventAllAnimations")) {
					instance.galleryHooks.$galInnerContainer.stop().animate({"left": nValue}, instance.options.slideTime);
				} else {
					instance.galleryHooks.$galInnerContainer.stop().css({"left": nValue});

				}
			}
		},
		loadGalleryItem: function ($itemToLoad) {

			var instance = $(this).data('SecondaryContent');
			if (!instance)
				return false;

			$itemToLoad.data("details").loaded = true;

			if ($itemToLoad.data("details").type == "image") {
				var ImageToLoad = new Image();
				$itemToLoad.append($(ImageToLoad).css({"opacity": 0}));

				ImageToLoad.onload = function () {

					var imageResize = function () {
						var $ImageToLoad = $(ImageToLoad);
						$itemToLoad.css({"background": ""});
						$ImageToLoad.css({"width": "auto", "height": "auto"});

						var imageAspect = $ImageToLoad.width() / $ImageToLoad.height(),
								containerHeight = parseInt($itemToLoad.outerHeight(true), 10),
								containerWidth = parseInt($itemToLoad.outerWidth(true), 10),
								containerAspect = containerWidth / containerHeight,
								tHeight,
								tWidth,
								tYoffset,
								tXoffset,
								delta;

						if (imageAspect > containerAspect) {
							delta = Math.floor(containerHeight * imageAspect - containerWidth);
							tHeight = containerHeight;
							tWidth = "auto";
							tYoffset = 0;
							tXoffset = -delta;
						} else {
							delta = Math.floor(containerWidth / imageAspect - containerHeight),
									tHeight = "auto";
							tWidth = containerWidth,
									tYoffset = -delta;
							tXoffset = 0;
						}

						$ImageToLoad.css({"height": tHeight, "width": tWidth, "margin-top": tYoffset / 2, "margin-left": tXoffset / 2}).addClass('bg-cover-replacement fs-gal-image');
						$ImageToLoad.removeAttr("width").removeAttr("height").css({"opacity": 1});
					};
					imageResize();
					$(window).on('debouncedresize', imageResize);

				};
				ImageToLoad.onError = function () {
					alert("Sorry, this content cannot be loaded");
				};
				ImageToLoad.src = $itemToLoad.data("details").URL;
			} else {
				// Assume video

				var ajaxRequest = $.ajax({url: $itemToLoad.data("details").URL, dataType: 'html'});

				setTimeout(function () {
					ajaxRequest.done(function (data, textStatus, jqXHR) {
						var $content = $(instance.options.ajaxContentSelector, data);
						if (!$content.length)
							$content = $(data).filter(instance.options.ajaxContentSelector);
						$content.addClass("fs-video-item");
						$itemToLoad.css({"background": ""});
						$itemToLoad.append($content);

					});
				}, 100);

				ajaxRequest.fail(function (data, textStatus, jqXHR) {
					alert("Sorry, this content cannot be loaded"); // alerts 200
				});
			}
		},
		ajaxContent: function ($theLink) {

			var instance = $(this).data('SecondaryContent');
			if (!instance)
				return false;

			var ajaxRequest = $.ajax({url: $theLink.attr("href"), dataType: 'html'});

			setTimeout(function () {
				ajaxRequest.done(function (data, textStatus, jqXHR) {
					var $content = $(instance.options.ajaxContentSelector, data);
					if (!$content.length)
						$content = $(data).filter(instance.options.ajaxContentSelector);
					//$slideInContent.html($content);
					$slideInContent.html(data);
					$slideInContent.height(instance.screenHeight);

					$slideInContent.css({"opacity": 1});
				});
			}, 100);

			ajaxRequest.fail(function (data, textStatus, jqXHR) {
				alert("Sorry, this content cannot be loaded");
			});
		},
		moveContent: function ($theLink) {
			var instance = $(this).data('SecondaryContent');
			if (!instance)
				return false;

			instance.$placeHolder = $('<div class="secContentPh" />');
			instance.$this.before(instance.$placeHolder);
			$slideInContent.append(instance.$this);
			instance.$this.trigger("moveToSecondary");
			$slideInContent.css({"opacity": 1});
			$slideInContent.height(instance.screenHeight);


		}

	};


	$.fn.SecondaryContent = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.plugin SecondaryContent');
		}
	};



})(jQuery);

if (typeof String.prototype.startsWith != 'function') {
	// see below for better implementation!
	String.prototype.startsWith = function (str) {
		return this.indexOf(str) == 0;
	};
}


