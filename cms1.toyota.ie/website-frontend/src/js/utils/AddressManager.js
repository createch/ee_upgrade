
var AddressEmitter = (function () {

    var addressEventEmitter = new EventEmitter2({
        wildcard: true
    });

    $.address.tracker(null); // disable address manager built in GA tracking...

    $.address.change(function (event) {


        if (!event.pathNames.length) event.pathNames.push("*");

        var subscribedEvents = addressEventEmitter.listeners(event.pathNames);
        var callbackArray = [];

        //console.log(addressEventEmitter.listeners(event.pathNames));

        // Directly call each subscribed function so we can get its return (a deferred obj)
        $.each(subscribedEvents, function (index, functionItem) {

            callbackArray.push(functionItem.apply(this, [event.pathNames]));
        });

        // Fired when all deferred objects have been resolved
        $.when.apply(window, callbackArray).always(
            function () {

            }
        );
    });

    // Public functions
    return {
        on: function (event, callback) {
            return addressEventEmitter.on(event, callback);
        },
        once: function (event, callback) {
            return addressEventEmitter.once(event, callback);
        }

    };

} ());
    
