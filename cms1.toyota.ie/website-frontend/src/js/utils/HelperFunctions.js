
/* 
#############################################################################
    Helper functions - v1.0

    Utility functions for use accross the site
#############################################################################
*/ 

var HelperFunctions = (function () {

    // Public HelperFunctions
    return {

        // Checks if the supplied arg is a function
        isFunction: function (object) {
            return !!(object && object.constructor && object.call && object.apply);
        },

        // Helper function like .net string.format, string to format is the context 
        stringFormat: function () {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function (match, number) {
                return typeof args[number] != 'undefined' ? args[number]: match;
            });
        },

        // Matches the supplied element dimentions to the viewport 
        matchViewportSize: function (element, heightOffset, minHeight, minWidth, maxHeight) {
            var _heightDif = heightOffset || 0,
            _minHeight = minHeight || 400,
            _minWidth = minWidth || 960;
            $(window).on('debouncedresize', function () {
                var tHeight = $(window).height() - _heightDif < _minHeight ? _minHeight : $(window).height() - _heightDif;
                var tWidth = $(window).width() < _minWidth ? _minWidth : $(window).width();
                element.css({ "width": tWidth, "height": tHeight });
            }).trigger('debouncedresize');
        },

        // Matches the supplied element dimentions to the parent element, or $parentOverride if defined
        matchParentSize: function ($element, $parentOverride) {
            var $targetElement = $parentOverride || $element.parent();
            $element.each(function () {
                var $theElement = $(this);
                $(window).on('debouncedresize', function () {
                    $theElement.css({ "width": $targetElement.outerWidth(true), "height": $targetElement.outerHeight(true) });
                }).trigger('debouncedresize');
            });
        },

        cssBackgroundImageRegex: /^url\(["']?(.+?)["']?\)$/
    };
} ());


jQuery.fn.extend({
    ensureLoad: function (handler) {
        return this.each(function () {
            if (this.complete) {
                handler.call(this);
            } else {
                $(this).load(handler);
            }
        });
    }
});