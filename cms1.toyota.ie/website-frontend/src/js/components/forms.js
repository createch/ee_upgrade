/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function submitForm(form, succescallback) {
	var $form = $(form);
	var $model = $form.find("#model option:selected");

	if ($model.length && $model.data("other")) {
		$form.append("<input type='hidden' name='pdf' value='" + $model.data("other") + "' />");
	}


	var hostname = window.location.hostname;
	$form.append("<input type='hidden' name='site_url' value='" + hostname + "' />");
	$form.find(".hiddencaptcha").prop("disabled",true);


	$.post($form.attr('action'),$form.serialize(), function (data) {
		if (data.indexOf('sent') != -1) {
			if (succescallback)
				succescallback($form, data);
			$(".page-transition-slider .content-wrapper ").scrollTop();
			$("body").scrollTop();
		} else {
			alert(data);
		}
	});
}


function showThanks(form) {
	var $form = $(form);
	$form.fadeOut();
	$form.parents(".form_container").find(".form_thanks").fadeIn();
}

function initForms() {

	if ($("#contactusform").length > 0) {
		FormElements.bindSelects($('#contactusform .select'));
		FormElements.bindCheckboxes($('#contactusform .checkbox'));
		$.validate({
			form: '#contactusform',
			modules: 'location, date, security',
			onError: function () {
				//alert('Please complete fields marked with  *');
			},
			onSuccess: function () {
				if (window.t1DataLayer) {
					$(window).trigger("workflowevent"); // trigger tag manager event
				}
				submitForm('#contactusform', function () {
					showThanks('#contactusform');
				});
				return false; // Will stop the submission of the form
			}
		});
	}


	if ($("#bookserviceform").length > 0) {
		FormElements.bindSelects($('#bookserviceform .select'));
		FormElements.bindCheckboxes($('#bookserviceform .checkbox'));
		$.validate({
			form: '#bookserviceform',
			modules: 'location, date, security',
			onError: function () {
				//alert('Please complete fields marked with  *');
			},
			onSuccess: function () {
				if (window.t1DataLayer) {
					$(window).trigger("workflowevent"); // trigger tag manager event
				}
				submitForm('#bookserviceform', function () {
					showThanks('#bookserviceform');
				});
				return false; // Will stop the submission of the form
			}
		});
	}

	if ($("#booktestdriveform").length > 0) {
		FormElements.bindSelects($('#booktestdriveform .select'));
		FormElements.bindCheckboxes($('#booktestdriveform .checkbox'));
		$.validate({
			form: '#booktestdriveform',
			modules: 'location, date, security',
			onError: function () {
//				alert('Please complete fields marked with  *');
			},
			onSuccess: function () {
				if (window.t1DataLayer) {
					var $model = $("#booktestdriveform").find("#model").val();
					window.t1DataLayer.event.modelname = $model;
					$(window).trigger("workflowevent"); // trigger tag manager event
				}
				submitForm('#booktestdriveform', function () {
					showThanks('#booktestdriveform');
				});
				return false; // Will stop the submission of the form
			}
		});
	}

	if ($("#newsletterform").length > 0) {
		FormElements.bindSelects($('#newsletterform .select'));
		FormElements.bindCheckboxes($('#newsletterform .checkbox'));
		$.validate({
			form: '#newsletterform',
			modules: 'location, date, security',
			onError: function () {
//				alert('Please complete fields marked with  *');
			},
			onSuccess: function () {
				submitForm('#newsletterform', function () {
					showThanks('#newsletterform');
				});
				return false; // Will stop the submission of the form
			}
		});
	}

	if ($("#requestbrochure").length > 0) {
		FormElements.bindSelects($('#requestbrochure .select'));
		FormElements.bindCheckboxes($('#requestbrochure .checkbox'));
		$.validate({
			form: '#requestbrochure',
			modules: 'location, date, security',
			onError: function () {
//				alert('Please complete fields marked with  *');
			},
			onSuccess: function () {
				if (window.t1DataLayer) {
					var $model = $("#requestbrochure").find("#model").val();
					window.t1DataLayer.event.modelname = $model;
					$(window).trigger("workflowevent"); // trigger tag manager event
				}
				submitForm('#requestbrochure', function () {
					showThanks('#requestbrochure');

					if ($("#selecttype option:selected").val() == 'pdfDownload') {
						$file = encodeURIComponent($('.brochure-pdf option:selected').data("other"));
						//console.log(window.location);
						if (!$("html.no-touch").length) {
							$file = $file + "&mobile=true";
						}
						if (location.host.indexOf('assuredusedcars') == -1)
							window.location = "/ajax/download/?brochurefile=" + $file;
						else
							window.location = "/forms/download/?brochurefile=" + $file;
					}
				});
				return false; // Will stop the submission of the form
			}
		});
	}
	
	
	$("#selecttype").change(function () {
		if ($(this).val() === "Post") {

			$("#postbrochureAdress").fadeIn();

			$("#postbrochureAdress #city,#postbrochureAdress #street").attr("data-validation", "required");
			$("#postbrochureAdress #state").attr("data-validation", "select");

			$("#selecttype_icon").html('<i class="icon icon-envelope-alt"></i>');
		} else if ($(this).val() === "Ebrochure") {
			$("#postbrochureAdress").fadeOut();
			$("#postbrochureAdress  #city,#postbrochureAdress  #street,#postbrochureAdress  #state").attr("data-validation", "");
			$("#selecttype_icon").html('@');
		} else {
			$("#postbrochureAdress").fadeOut();
			$("#postbrochureAdress  #city,#postbrochureAdress  #street,#postbrochureAdress  #state").attr("data-validation", "");
			$("#selecttype_icon").html('<i class="icon icon-file-pdf"></i>');
		}

	});


	$("select.data-other").change(function () {
		$el = $(this);
		$prev = $el.prev(".data-other-fill");
		$aux = $el.find(":selected").data("other");
		$prev.val($aux);
	});
}

$(function () {

	initForms();

});




