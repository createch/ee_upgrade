
/* 
#############################################################################
    Video resizing - v0.01

    dependencies
    - jquery.debouncedresize.js

    Contains all functionality to resize youtube/vimeo embedded videos
#############################################################################
*/ 

var VideoResizing = (function () {

    // Public methods
    return {
        aspectRatio:2,
        // Fixed the navigation element via triggering CSS classes on users passes the 'showAfter' scrollY
        bindResizing:function($elements,$aspectRatio){
            var self=this;

            if($aspectRatio!==undefined){
                self.aspectRatio=$aspectRatio;
            }

            $(window).on('debouncedresize', function () {
        
                $elements.each(function(){
                    var width=parseInt($(this).width(),10);
                    $(this).height(width/self.aspectRatio);
                });

            }).trigger('debouncedresize');
        }
    };

})(jQuery);