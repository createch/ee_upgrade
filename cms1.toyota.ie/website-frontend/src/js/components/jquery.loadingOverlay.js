(function($) {

    // global settings
    var settings = {
        isDebug: false,
        loadEffectCSSClass: "cl-loadeffect",
        overlayOppacity: 0.65,
        maskElement: true,
        iconTypeCSSClass: "loader-dark-bg", // loader-light, loader-white-bg, loader-dark, loader-dark-bg
        overlayBackgroundColor: "#fff",
        loaderTopOffset: 'auto', // auto, or an int
        loaderRightOffset: 'auto', // auto, or an int
        loaderBottomOffset: 'auto', // auto, or an int
        loaderLeftOffset: 'auto' // auto, or an int
    };

    var loadingTimer,
    loadingIntervalActive = false,
    loadingFrame = 1;

    var methods = {

        init: function(options) {

            var ls = $.extend({}, settings, options); // local settings
            return this.each(function() {

                $this = $(this);
                $elementToMask = $(this);

                var $backgroundDiv = $('<div class="cl-updateProgressBackground tidyAfterLoad"></div>');
                if (ls.overlayBackgroundColor.length > 0) {
                    $backgroundDiv.css({ "background-color": ls.overlayBackgroundColor });
                }
                var $updateProgressDiv = $('<div class="cl-updateProgressLoader tidyAfterLoad"></div>');
                var $IconContainer = $('<div class="iconContainer"></div>');
                var $IconSprite = $('<div class="iconSprite"></div>');

                $IconSprite.addClass(ls.iconTypeCSSClass);

                $updateProgressDiv.append($IconContainer.append($IconSprite));

                // Searchs for an element within the '$this' with a class of loadEffectCSSClass (default "loadeffect")
                // Applys loading overlay to that div if found, otherwise applies to '$this' itself (legacy code for another project
                if ($('.' + ls.loadEffectCSSClass, $this).length) {
                    $elementToMask = $('.' + ls.loadEffectCSSClass, $this);
                } else {
                    $elementToMask.addClass(ls.loadEffectCSSClass).addClass('cl-removeLFClass');
                }

                // By default the CSS will center loader, can be overridden
                if (ls.loaderTopOffset != 'auto') {
                    $updateProgressDiv.css({ "top": ls.loaderTopOffset + "px", "margin-top": 0 });
                }
                if (ls.loaderRightOffset != 'auto') {
                    $updateProgressDiv.css({ "right": ls.loaderRightOffset + "px", "left": "auto" });
                }
                if (ls.loaderBottomOffset != 'auto') {
                    $updateProgressDiv.css({ "bottom": ls.loaderBottomOffset + "px", "top": "", "margin-bottom": "" });
                }
                if (ls.loaderLeftOffset != 'auto') {
                    $updateProgressDiv.css({ "left": ls.loaderLeftOffset + "px", "margin-top": 0 });
                }

                // Switch mode based on settings, dont apply mask if icon only is true
                if (ls.maskElement) {
                    // Apply styles to mask
                    $backgroundDiv.css({ "opacity": ls.overlayOppacity, "height": $elementToMask.outerHeight(), "width": $elementToMask.outerWidth() });
                    // Insert load mask and spinner
                    $elementToMask.prepend($updateProgressDiv).prepend($backgroundDiv);
                } else {
                    // Insert spinner
                    $elementToMask.prepend($updateProgressDiv);
                }

                // spin up the spinner if its not already running for another elements
                if (!loadingIntervalActive) {
                    loadingIntervalActive = true;
                    loadingTimer = setInterval(function() { animatedLoading(); }, 66);
                }

                function animatedLoading() {
                    // Selects and updates all loaders on page
                    var $loadIcons = $('.cl-updateProgressLoader .iconContainer .iconSprite');
                    if ($loadIcons.length) {
                        $loadIcons.css('background-position', '0 ' + (loadingFrame * -40) + 'px');
                        loadingFrame = (loadingFrame + 1) % 12;
                    } else {
                        loadingIntervalActive = false;
                        clearInterval(loadingTimer);
                    }
                }
            });
        },

        removeLoadingOverlay: function(options) {
            var ls = $.extend({}, settings, options); // local settings
            return this.each(function() {
                $this = $(this);

                // Remove all the inserted elements
                $('.tidyAfterLoad', $this).remove();
                // Remove all loading applied classes
                $('.cl-removeLFClass', $this).removeClass('cl-removeLFClass').removeClass(ls.loadEffectCSSClass);
                $this.removeClass('cl-removeLFClass').removeClass(ls.loadEffectCSSClass);

                // Clear the interval if no more loaders present in the whole page
                if ($('.tidyAfterLoad').length < 1) {
                    loadingIntervalActive = false;
                    clearInterval(loadingTimer);
                }
            });

        }
    };



    $.fn.LoadingOverlay = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.plugin');
        }
    };

})(jQuery);    