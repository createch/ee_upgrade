
/* 
 #############################################################################
 Form Elements - v0.01
 
 dependencies
 
 Contains all custom form elements functionality
 
 #############################################################################
 */


var FormElements = (function () {

	// Public methods
	return {
		messages: {
			showMore: 'More search options',
			showLess: 'Less search options'
		},
		bindShowMore: function ($elements, $more) {
			var self = this;

			$elements.each(function () {
				var button = $(this);

				button.click(function (e) {
					e.preventDefault();
					setTimeout(function () {
						$more.slideToggle('slow', function () {
							if ($(this).css('display') == 'none') {
								button.find('span').text(self.messages.showMore);
								button.find('i').removeClass('icon icon-chevron-up').addClass('icon icon-chevron-down');
							} else {
								button.find('span').text(self.messages.showLess);
								button.find('i').removeClass('icon icon-chevron-down').addClass('icon icon-chevron-up');
							}
						});
					}, 100);


				});
			});

		},
		bindSelects: function ($elements) {

			$elements.each(function () {
				var selectDiv = $(this);
				//console.log(selectDiv.find(':selected').text());	
				if (selectDiv.find(':selected').length > 0)
					selectDiv.find('.value').text(selectDiv.find(':selected').text());
				else
					selectDiv.find('.value').text(selectDiv.find('option').first().text());
				selectDiv.change(function (e) {
					// this is a hack to display proper text in forms
					var aux = $(e.currentTarget);
					var text = "";
//					if($(e.currentTarget).parents("#form_tabs").length)
//						text = $(e.currentTarget).find('option:selected').text();
//					else
					text = $(e.currentTarget).find('option:selected').text();

					$(e.currentTarget).find('.value').text(text);
				});
			});

		},
		bindCheckboxes: function ($elements) {

			$elements.each(function () {
				var button = $(this);

				var isradio = $();

				button.click(function (e) {
					var buttonInput = button.find('input');
					if (button.hasClass("unique")) {
						var siblings = button.siblings(".unique");
						siblings.each(function () {
							var sibling = $(this);
							var siblinInput = sibling.find('input');
							siblinInput.prop("checked", false);
							sibling.removeClass('active');
						});
					}
					if (buttonInput.is(':checked')) {
						buttonInput.prop("checked", false);
						button.removeClass('active');
					} else {
						buttonInput.prop("checked", true);
						button.addClass('active');
					}
				});

			});

		},
		getCheckboxValues: function (input_name) {
			var values = [];
			$('input[type="checkbox"][name="' + input_name + '"]:checked , input[type="radio"][name="' + input_name + '"]:checked').each(function () {
				values.push($(this).val());
			});
			return values;
		},
		bindViewControls: function ($view, $grid, $list) {

			$grid.click(function (e) {
				e.preventDefault();
				$(".results").removeClass('list').addClass('grid');
				//$(".results").addClass('grid');
				$list.removeClass('active');
				$grid.addClass('active');
			});

			$list.click(function (e) {
				e.preventDefault();
				$(".results").removeClass('grid').addClass('list');
				//$(".results").addClass('list');
				$grid.removeClass('active');
				$list.addClass('active');
			});

		}
	};

})(jQuery);