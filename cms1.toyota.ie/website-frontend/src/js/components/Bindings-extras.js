$(function() {
	
	$("#more_filters").click(function() {
		$container = $("#car_filter");
		$container.toggleClass("open");
		
		if ($container.hasClass("open")) {
			$(this).find("text").text("Less search options");
			$container.find(".search-simple").toggleClass("open");
		} else {
			setTimeout(function(){
				$container.find(".search-simple").toggleClass("open");
			}, 800);
			
			$(this).find("text").text("More search options");
		}
	});

	//function takes two params - elements to watch, aspect ratio
	VideoResizing.bindResizing($('.video'), 16 / 9);

	FormElements.bindShowMore($('.show-search-detailed'), $('.search-detailed'));
	FormElements.bindSelects($('.select'));
	FormElements.bindCheckboxes($('.checkbox'));

	FormElements.bindViewControls($('.results'), $('.show-grid'), $('.show-list'));	
});