// Add local translations
TranslationManager.addLocalTranslations({
	"showAll": "View all",
	"showAllWithCount": "View all {0}",
	"showLess": "Show less",
	"backTo": "Back to",
	"readDetails": "Read Details",
	"close": "Close",
	"save": "Save",
	"share": "Share"
});

$(function() {

//	// Bind secondary content layer (for view all)
//	$('.SecContentContainer').each(function() {
//		$(this).SecondaryContent({
//			"sectionIDs": $(this).data("id").split("/"),
//			"mode": $(this).data("contenttype"),
//			"cGroupName": $(this).data("name"),
//			"translations": TranslationManager.getLocalTranslations()
//		})
//	});

//	// old hack to align .fc-group-name with the first item
//	$(".fc-group").each(function(){
//		$t = $(this);
//		var $name = $t.find(".fc-group-name").detach();
//		var $item = $t.find(".item").first();
//		$item.append($name);
//	});

	// Bind filterable content
	$('.filterable-content').FilterableContent({});

	// Bind read more
	$('.expansion-content').ReadMore({});
	

	var timeoutRef = false;

	// Colourbox bind for accessory items
	$('.cb-ajax-json').bind('click', function(e) {
		var $item = $(this);

		// Show the loading spinner over the item
		$item.LoadingOverlay();

		if (timeoutRef)
			clearTimeout(timeoutRef);

		timeoutRef = setTimeout(function() {

			$.ajax({
				type: "GET",
				url: $item.data('link'),
				dataType: "json",
				success: function(response) {

					// Create HTML from the JSON data
					var $content = $('<article class="feature-cbv" />');
					if (response.image && response.image.url && response.image.width && response.image.height)
						$content.append('<img src="' + response.image.url + '" alt="' + response.image.name + '" width="' + response.image.width + '" height="' + response.image.height + '" />');
					$content.append('<div class="share-tools open-top"><div class="tool-box"><a href="#save" class="share-item">' + TranslationManager.tr("save") + '</a><a href="#share" class="share-item">' + TranslationManager.tr("share") + '</a></div></div>');
					if (response.name)
						$content.append('<h1 class="title2">' + response.name + '</h1>');
					if (response.description)
						$content.append('<div class="body-content clearfix">' + response.description + '</div<');

					// Launch coloutbox with generated HTML
					$.colorbox({
						close: '<a class="closelink"><span>' + TranslationManager.tr("close") + '</span> <i data-icon="&#xf00d;"></i></a>',
						transition: 'none',
						width: response.image.width + 20 || 500,
						className: 'feature-overlay colourbox-content',
						html: $content,
						onComplete: function(e) {
							CommonEffects.bindShareTools($content);
						}
					});

					// Remove loading spinner
					$item.LoadingOverlay('removeLoadingOverlay');
				},
				error: function(xhr) {
					$.colorbox.close();
					$item.LoadingOverlay('removeLoadingOverlay');
					alert("could not load content, error: " + xhr.status);
				}
			});
		}, 1000);
	});

});
