; (function ($, tr, Modernizr) {

    // default settings
    var settings = {
        translations: {},
        fadeTime: 450,
        columns: 4,
        allowAllFilterByDefault: false // false if seconary content layer used for view all interaction
    };
    
    var methods = {
        init: function (options) {
            return this.each(function () {
                // Construct instance
                var instance = this;
                var $this = $(this);
                instance.instanceSettings = $.extend(true, {}, settings, options);
                instance.viewAll = instance.instanceSettings.allowAllFilterByDefault;

                var $filterLinks = $('.filter-bar .fb-item', $this);
                var $activeFilter = $filterLinks.filter("[data-default]").length ? $filterLinks.filter("[data-default]").first() : $filterLinks.first();
                var $viewAllLink = $filterLinks.filter("[data-viewall]").length ? $filterLinks.filter("[data-viewall]").first() : false;
                var $contentBLocksConatiner = $('.fc-group-container', $this);
                var $contentBlocks = $('.fc-group', $contentBLocksConatiner);
                var $activeContent = $contentBlocks.filter($activeFilter.data('section-filter'));
                var $crossGroupFilters = $('.filter-bar .fbg-item', $this);


                $crossGroupFilters.each(function () {
                    var $eLink = $(this);
                    if ($eLink.data('item-filter')) {
                        $($eLink.data('item-filter'), $contentBlocks).hide();
                    }
                    $eLink.data("offtext", $eLink.text());

                    $eLink.on("click", function (e) {
                        e.preventDefault();
                        var wasActive = $eLink.hasClass($eLink.data("onclass"));
                        $eLink.toggleClass($eLink.data("onclass") + " " + $eLink.data("offclass"));
                        
                        var $filteredItems = $($eLink.data('item-filter'), $contentBlocks);
                        $contentBLocksConatiner.css({"min-height": $contentBLocksConatiner.height() });
                        $contentBlocks.addClass("preventAllAnimations");
                        $contentBlocks.css({"opacity": 0, "display": "none" }).height(); // .height to trigger reflow and apply 'preventAllAnimations'...

                        if (wasActive) {
                            $filteredItems.hide();
                            $eLink.text($eLink.data("offtext"));
                        } else {
                            $filteredItems.show();  
                            $eLink.text($eLink.data("ontext"));                          
                        }
                        
                        $activeContent.css({"display": "" }).height();
                        $contentBlocks.removeClass("preventAllAnimations");
                        $activeContent.css({"opacity": 1 });
                        $contentBLocksConatiner.css({"min-height": 0 });
                        clearRowStarts($contentBlocks, instance.instanceSettings.columns);
                    });
                });

                $filterLinks.on("click", function (e) {
                    e.preventDefault();
					var $contentBlocks = $('.fc-group', $contentBLocksConatiner);
                    $contentBlocks.css({"display": "" });
                    $activeFilter = $(this);
                    var isViewAll = $activeFilter.attr('data-viewall') === "";
                    if ((!isViewAll || instance.viewAll) && $activeFilter.data('section-filter')) {
                        $filterLinks.removeClass("active");
                        $activeFilter.addClass("active");

                        // Select the new content group
                        $activeContent = $contentBlocks.filter($activeFilter.data('section-filter'));
                        // Set container min-height to prevent jumo
                        $contentBLocksConatiner.css({"min-height": $contentBLocksConatiner.height() });
                        // Prevent animations on fadeout
                        $contentBlocks.addClass("preventAllAnimations");
                        $contentBlocks.css({"opacity": 0, "display": "none" }).height(); // .height to trigger reflow and apply 'preventAllAnimations'...
                            
                        $activeContent.css({"display": "" }).height();
                        $contentBlocks.removeClass("preventAllAnimations");
                        // Position & fade in new content
                        $activeContent.css({"opacity": 1 });
                        $contentBLocksConatiner.css({"min-height": 0 });
                        clearRowStarts($contentBlocks, instance.instanceSettings.columns);
                    }
                });

                $this.on("moveToSecondary", function (e) {
                    if ($viewAllLink.length) {
                        $activeFilter = $viewAllLink;
                        $filterLinks.removeClass("active");
                        $activeFilter.addClass("active");
                        $activeContent = $contentBlocks; // show all...
                        $contentBlocks.addClass("preventAllAnimations");
                        $contentBlocks.css({"opacity": 1, "display": "" });
                        $contentBlocks.removeClass("preventAllAnimations");
                        clearRowStarts($contentBlocks, instance.instanceSettings.columns);
                        instance.viewAll=true;
                    }
                });
                $this.on("moveFromSecondary", function (e) {
                    $activeFilter = $filterLinks.filter("[data-default]").length ? $filterLinks.filter("[data-default]").first() : $filterLinks.first();
                    $filterLinks.removeClass("active");
                    $activeFilter.addClass("active");
                    $activeContent = $contentBlocks.filter($activeFilter.data('section-filter'));
                    $contentBlocks.addClass("preventAllAnimations");
                    $contentBlocks.css({"opacity": 0, "display": "none" });
                    $activeContent.css({"opacity": 1, "display": ""  });
                    $contentBlocks.removeClass("preventAllAnimations");
                    clearRowStarts($contentBlocks, instance.instanceSettings.columns);
                    instance.viewAll=false;
                });
                
                $activeFilter.addClass("active");
                clearRowStarts($contentBlocks, instance.instanceSettings.columns);
                $contentBlocks.not($activeContent).css({"opacity": 0, "display": "none" });

                function clearRowStarts($itemContainers, colCount) {
                    $itemContainers.each(function () {
                        var visCount = 0;
                        $('.item', $(this)).each(function (i) {
                            $thisItem = $(this);
                            if ($thisItem.is(":visible")) {
                                $thisItem.css({"clear": visCount % colCount === 0 ? "left": "" });
                                visCount++;
                            }
                        });
                    });
                }
            });
        }
    };


    $.fn.FilterableContent = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.plugin SecondaryContent');
        }
    };

})(window.jQuery, window.TranslationManager.tr, window.Modernizr);



