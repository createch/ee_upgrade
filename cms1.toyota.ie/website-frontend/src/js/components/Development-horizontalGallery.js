/**
 * Horizontally scrolling gallery
 * @author Luke Channings
 * @date 04/09/13
 * @lastModified 05/09/13
 */
(function($, tr, Modernizr) {

    // ensure window.Development exists as an object.
    window.Development = typeof window.Development === "object" ? window.Development : {};

    window.Development.horizontalGallery = function($rootGalleryNodes, options) {
        return $rootGalleryNodes.each(function() {
            var ins = $.extend(true, {}, HorizontalGallery, {});
            ($.extend(true, ins, { options: options, $node: $(this) })).init();
        });
    };

    var HorizontalGallery = {

        /**
         * initialises the horizontal gallery.
         */
        init: function() {
            var self = this;
            self.createUsageIndicator();
            self.layoutRows();
            self.$node.bind(self.options.destroyUsageIndicatorEvents, $.proxy(self.destroyUsageIndicator, self) );
            if ( ! Modernizr.touch ) self.initialiseNavigation();
            self.$node.find("." + self.options.galleryRowClass).each(function() {
                var $this = $(this);
                self.initialiseDragging($this);

                var tallestItemHeight = 100;
                $this.find(".item-content").each(function() {
                    if ($(this).height() > tallestItemHeight) tallestItemHeight = $(this).height();
                });
                $this.find(".item-content").height(tallestItemHeight);
            });
            self.$node.addClass("active");
        },

        // default options.
        options: {
            "usageIndicatorClass": "gallery-horizontal-indicator",
            "usageIndicatorTransitionDurationMs": 500,
            "galleryRowClass": "gallery-horizontal-row",
            "galleryRowInnerClass": "gallery-horizontal-row-inner",
            "destroyUsageIndicatorEvents": "touchstart mouseup",
            "navigationArrowContainerClass": "navigation-arrows",
            "navigationArrowClass": "gallery-nav-arrow",
            "navigationArrowLeftClass": "gallery-nav-arrow left icon icon-angle-left",
            "navigationArrowRightClass": "gallery-nav-arrow right icon icon-angle-right",
            "navigationScrollDistance": 500, // distance in px.
            "navigationScrollSpeedMs": 600,
            "navigationScrollEasing": "easeOutCirc"
        },

        /**
         * creates a usage indicator.
         */
        createUsageIndicator: function() {
            // create the indicator.
            this.$usageIndicator = $("<div>", {
                "class": this.options.usageIndicatorClass,
                "html": $("<p>", {
                    "text" : Modernizr.touch? tr("Drag to navigate") :  tr("Drag or use arrow controls")
                })
            }).appendTo(this.$node);
        },

        /**
         * transitions the indicator out and removes it from the DOM tree.
         */
        destroyUsageIndicator: function() {
            var self = this;
            self.$usageIndicator = $('.'+ this.options.usageIndicatorClass, self.$node);
            if (!self.usageRemoved) {
                self.usageRemoved = true;
                self.$usageIndicator.css("opacity", 0);
                setTimeout(function() {
                    self.$usageIndicator.remove();
                    self.$usageIndicator = undefined;
                    self.$node.unbind(self.options.destroyUsageIndicatorEvents, self.destroyUsageIndicator);
                }, self.options.usageIndicatorTransitionDurationMs);
            }
        },

        /**
         * Create the gallery layout
         */
        layoutRows: function() {
            var self = this;
            this.$node.find("." + self.options.galleryRowClass).each(function() {
                var $thisRow = $(this); 
                $thisRow.data('left', 0);

                var spaceRequired = 0;
                $('.gallery-item', $thisRow).each(function() {
                    spaceRequired += $(this).width();
                });

                $thisRow.data("width", spaceRequired);
                $thisRow.find("." + self.options.galleryRowInnerClass).width(spaceRequired);

            });
        },

        /**
         * creates navigation arrows for each row in the gallery and binds arrows for each row.
         */
        initialiseNavigation: function() {
            var self = this;
            this.$node.find("." + self.options.galleryRowClass).each(function() {
                var $this = $(this);
                self.createNavigationArrows($this);
                $this.on("click", "." + self.options.navigationArrowClass, function(e) {

                    var target = $(e.target).hasClass("left") ? $this.data('left') - self.options.navigationScrollDistance : $this.data('left') + self.options.navigationScrollDistance;

                    self.moveRow($this, target, true);
                    
                    return false;
                });
            });
        },

        /**
         * creates navigation arrows given a row parameter.
         * @param $row {jHTMLDivElement} the row upon which to create arrows.
         */
        createNavigationArrows: function($row) {
            var nav = { $container: $("<div>", {"class": this.options.navigationArrowContainerClass })};
            nav.$leftArrow = $("<a>", {
                "class": this.options.navigationArrowLeftClass,
                "href": "#",
                "data-icon": "ï„„"
            });
            nav.$rightArrow = $("<a>", {
                "class": this.options.navigationArrowRightClass,
                "href": "#",
                "data-icon": "ï„…"
            });
            nav.$leftArrow.add(nav.$rightArrow).appendTo(nav.$container);
            nav.$container.appendTo($row);
            $row.data("navigation", nav);
            this.updateNavigationArrowState($row);
        },

        /**
         * set the states for navigation arrows, active or inactive.
         */
        updateNavigationArrowState: function($row) {
            var nav = $row.data("navigation");
            if ( nav && nav.$leftArrow && nav.$rightArrow ) {

                var scrollLeftValue = $row.data('left');
                var canMoveRight = scrollLeftValue === 0 || ( (scrollLeftValue + $(document).width()) < $row.data("width") );
                
                nav.$leftArrow[scrollLeftValue === 0  ? "addClass" : "removeClass"]("inactive");
                nav.$rightArrow[ ! canMoveRight ? "addClass" : "removeClass"]("inactive");
            }
        },


        /**
         * Moves the position of the row
         */
        moveRow: function($row, newX, isEndOfEvent) {
            var $innerRow = $('.' + this.options.galleryRowInnerClass, $row);
            var rowWidth = $row.data("width");

            if (isEndOfEvent) {
                if (newX < 0) newX = 0;
                if (newX > rowWidth - $(window).width()) newX = rowWidth - $(window).width();
                $row.data('left', newX);
            }

            if (Modernizr.csstransforms3d) {
                $innerRow.css({
                    "-webkit-transform": "translate3d(" + -newX + "px, 0px, 0px)",
                    "-moz-transform": "translate3d(" + -newX + "px, 0px, 0px)",
                    "transform": "translate3d(" + -newX + "px, 0px, 0px)"
                });
            } else {
                $innerRow.css({ "left": -newX + "px" });
            }
            this.updateNavigationArrowState($row);
        },

        /**
         * initialises drag navigation for the given row.
         * @param $row {jHTMLElement} the row to bind dragging events to.
         */
        initialiseDragging: function($row) {
            var self = this;
            (function($row) {
                var mousedown = false, dragging = false, startPosX, startScrollLeft, touchStart;
                var $innerRow = $row.find("." + self.options.galleryRowInnerClass);
                $row.find("img, a");//.attr("draggable", "false"); // THis kills IE7??? removed for now...
               
                $row.bind("mousedown", function(e) {
                    mousedown = true;
                    if ( self.$usageIndicator ) self.destroyUsageIndicator();
                    startScrollLeft = $row.data('left');
                    startPosX = e.clientX;
                    return false;
                });
                // Bind to window so is caught if outside of the window etc...
                $row.bind("mouseup mouseleave", function(e) {
                    mousedown = false;
                    $row.removeClass("dragging");
                    if (dragging) {
                        var endPos = (startScrollLeft - (e.clientX - startPosX));
                        self.moveRow($row, endPos, true);
                    }
                    dragging = false;
                });
                $row.bind("mousemove", function(e) {
                    if ( mousedown ) {
                        if (!dragging) {
                            $row.addClass("dragging");
                            dragging = true;
                        }
                        var newPos = (startScrollLeft - (e.clientX - startPosX));
                        self.moveRow($row, newPos);
                        
                        // Check cursor has moved
                        if (Math.abs(e.clientX - startPosX)) {
                            var $clickEle = $(e.target);
                            if ( $clickEle.is("a") ) {
                                $clickEle.data("cancel", true);
                            } else {
                                $clickEle.closest("a").data("cancel", true);
                            }
                        }

                        return false;
                    }
                });
                
                // Touch events - for touch devices 
                $row.bind("touchswipe", function(e, phase, direction, distance, duration, terminalVelocity) {

                    //console.log(e, terminalVelocity);

                    if (phase === "START") {
                        startScrollLeft = $row.data('left');
                    }
                    if ((direction === "LEFT" || direction === "RIGHT") && distance) {
                        e.preventDefault();

                        var newPointerPos;
                        if (direction === "LEFT") newPointerPos = startScrollLeft + distance;
                        if (direction === "RIGHT") newPointerPos = startScrollLeft - distance;

                        if (phase === "MOVE") {
                            if (!dragging) {
                                $innerRow.addClass("preventAllAnimations");
                                dragging = true;
                            }
                            
                            self.moveRow($row, newPointerPos, false);
                        }

                        if (phase === "END") {
                            $innerRow.height(); // force reflow
                            $innerRow.removeClass("preventAllAnimations");

                            // Rough attempt at inertial scrolling
                            if (direction === "LEFT") newPointerPos += (200 * terminalVelocity);
                            if (direction === "RIGHT") newPointerPos -= (200 * terminalVelocity);

                            if (dragging) {
                                self.moveRow($row, newPointerPos, true);
                                dragging = false;
                            }
                        }
                    } 
                });
            })($row);
        }
    };

})(window.jQuery, window.TranslationManager.tr, window.Modernizr);