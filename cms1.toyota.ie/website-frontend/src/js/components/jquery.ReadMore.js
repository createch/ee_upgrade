
(function ($, tr, Modernizr) {

	// default settings
	var settings = {
		translations: {},
		fadeTime: 450,
		openContentWidth: "470",
		arrowIndent: 30,
		gutterWidth: 10,
		visualParentOffsetSelector: ".item",
		replacementTextTranslationKey: "readDetails",
		closeTextTranslationKey: "close"
	};

	var methods = {
		init: function (options) {
			return this.each(function () {
				// Construct instance
				var instance = this;
				var $this = $(this);
				if ($this.data("readMoreBound"))
					return false;
				$this.data("readMoreBound", true);
				instance.instanceSettings = $.extend(true, {}, settings, options);

				var readMoreText = tr(instance.instanceSettings.replacementTextTranslationKey);
				var closeText = tr(instance.instanceSettings.closeTextTranslationKey);

				// Insert read more text and hide expansion content
				var $readMoreLink = $('<div class="read-more-container"><a href="#" class="readMore"><span>' + readMoreText + '</span> <i class="icon icon-info-sign"></i></a></div>');
				$this.before($readMoreLink);
				$this.hide();

				$('a', $readMoreLink).on("click", function (e) {
					e.preventDefault();
					e.stopPropagation();
					var $thisReadMoreLink = $(this);
					$(window).trigger("closemodals");

					if (!$thisReadMoreLink.data('isOpen')) {
						$thisReadMoreLink.data('isOpen', true).addClass("active");

						var winW = $(window).width();
						var width = Math.min( winW - 20,instance.instanceSettings.openContentWidth);
						// Construct HTML
						var $closeLink = $('<a href="#" class="closelink"><span>' + closeText + '</span> <i class="icon icon icon-remove"></i></a>');
						var $overlayContent = $('<div class="readmore-content"></div>').css({"z-index": 9, "width": width});
						var $overlayInner = $('<div class="rmc-inner">' + $this.html() + '</div>').prepend($closeLink);
						var $overlayPointer = $('<span class="arrow up"></span>').css({"margin-left": instance.instanceSettings.arrowIndent});
						$overlayContent.append($overlayPointer);
						$overlayContent.append($overlayInner);

						// Add to page
						$thisReadMoreLink.after($overlayContent).height();
						$overlayContent.css({"opacity": 1});
						// Adjust to go above more link if needed
						var positionNeeded = $overlayContent.offset().top + $overlayContent.height();
						var currentLowerVisPoint = $(window).scrollTop() + $(window).height();
						if (positionNeeded > currentLowerVisPoint || $('body').height() < positionNeeded) {
							$overlayContent.css({"bottom": $readMoreLink.height()});
							$overlayPointer.removeClass('up').addClass('down');
							$overlayContent.append($overlayPointer);
						} else {
							$overlayContent.css({"top": $readMoreLink.height()});
						}

						// Adjust to go left of link if needed
						var $positionParent = $thisReadMoreLink.closest(instance.instanceSettings.visualParentOffsetSelector);
						if (!$positionParent.length)
							$positionParent = $thisReadMoreLink.parent();
						var spaceInRow = $positionParent.offsetParent().width() - $positionParent.position().left;
						if (spaceInRow < instance.instanceSettings.openContentWidth) {
							$overlayContent.css({"left": spaceInRow - instance.instanceSettings.openContentWidth - instance.instanceSettings.gutterWidth});
							$overlayPointer.css({"margin-left": (instance.instanceSettings.openContentWidth - spaceInRow) + instance.instanceSettings.arrowIndent});
						}
						
						// adjust in smalls screens
						if(width < instance.instanceSettings.openContentWidth){
							$overlayContent.css({"left": -10});
						}
							

						// Prevent clicks inside triggering body click
						$overlayContent.click(function (e) {
							e.stopPropagation();
						});

						$(window).one("closemodals", function (e) {
							closeLayer();
						});

						// Close click once only bind
						$closeLink.one("click", function (e) {
							e.preventDefault();
							closeLayer();
						});

					}
					function closeLayer() {
						$thisReadMoreLink.data('isOpen', false).removeClass("active");
						$closeLink.unbind();
						if ($overlayContent && $overlayContent.length) {
							$overlayContent.remove();
							$overlayContent = null;
						}
					}
				});
			});
		}
	};


	$.fn.ReadMore = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.plugin SecondaryContent');
		}
	};

})(window.jQuery, window.TranslationManager.tr, window.Modernizr);



