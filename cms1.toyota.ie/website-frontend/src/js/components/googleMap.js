/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var directionsService;

try {
	directionsService = new google.maps.DirectionsService();
} catch (e) {
	console.log("error with google maps library");
}

function resizeMap(containerSelector) {
	//hack to make the map render again
	setTimeout(function(){
		var $conatiner = $(containerSelector);
		var $map = $conatiner.find(".gmap");
		if ($map.length) {
			var id = $map.attr("id");
			var map = window[id + "_map"];
			var x = map.getZoom();
			var c = map.getCenter();
			google.maps.event.trigger(map, 'resize');
			map.setZoom(x);
			map.setCenter(c);
		}
	},100);
}



function getRoute($container) {
	var $getRouteMap = $container.find(".gmap");
	var id = $getRouteMap.attr("id");
	var map = window[id + "_map"];
	var markers = window[id + "_markers"];

	if ($getRouteMap.length) {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(setPosition);
		} else {
			$container.find('.map_errors').text("Geolocation is not supported by this browser.");
		}
	}

	function setPosition(position) {
		var usrLatitude = position.coords.latitude;
		var usrLongitude = position.coords.longitude;
		var usrPosition = new google.maps.LatLng(usrLatitude, usrLongitude);
		calcRoute(usrPosition);
	}

	function calcRoute(start) {
		
		directionsDisplay = new google.maps.DirectionsRenderer();
		directionsDisplay.setMap(map);
		//console.log('start:'+start)
		//console.log('end:'+end)
		var request = {
			origin: start,
			destination: map.getCenter(),
			travelMode: google.maps.DirectionsTravelMode.DRIVING
		};
		directionsService.route(request, function (response, status) {
			if (status == "ZERO_RESULTS") {
				$(".map_errors").html("Some error occurred");
			}
			if (status == google.maps.DirectionsStatus.OK) {
				//console.log(response);
				deletemarkers();
				directionsDisplay.setDirections(response);

				var totalDistance = 0;
				var totalDuration = 0;
				var legs = response.routes[0].legs;
				for (var i = 0; i < legs.length; ++i) {
					totalDistance += legs[i].distance.value;
				}

			}
		});
	}


	function deletemarkers() {
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(null);
		}
	}
}

