;
(function (window, $, has) {

	var layers = [];
	var options = {
		"layerHeightPercentage": 0.65,
		"offsetPoints": [0, 25, 25, 50]
	};
	var dimensions = {};
	var scrollTop = null;
	var $window = $(window);
	var isFirefox = typeof InstallTrigger !== 'undefined';

	/**
	 * stores scroll and offset values and dispatches updates when needed.
	 * @param e {Event} the scroll event.
	 * @returns {Null} nothing.
	 */
	function globalScrollHandler(e, justUpdateScroll, aScrollTop) {
		var _scrollTop = aScrollTop || $window.scrollTop();

		if (_scrollTop !== scrollTop) {
			scrollTop = _scrollTop;

			if (justUpdateScroll)
				return;

			$.each(layers, function () {
				var layer = this;
				var elementData = getElementData(layer.$node);
				var additionalOffset = layer.getAdditionalOffset(elementData.positionOnPage);
				var offset = layer.$node.offset().top - scrollTop;

				if (elementData.elementIsInView) {

					if (layer.$parallaxLayer) {
						layer.$parallaxLayer.css({
							"transform": "translateY(" + offset + "px)",
							"visibility": "visible"
						});
					}
					
					var imageY = (-offset) + additionalOffset;
					if(layer.$parallaxLayer.attr("id") === "fotter-paralax"){
						imageY = Math.min(imageY,-80);
					}
					
					
					layer.$parallaxImage.css({
						"transform": "translateY(" + (imageY) + "px)",
						"visibility": "visible"
					});

				} else {
					layer.$parallaxImage.add(layer.$parallaxLayer).css("visibility", "hidden");
				}
			});
		}
	}

	function globalResizeHandler() {
		var width = $window.width();
		var height = $window.height();
		if (width !== dimensions.width || height !== dimensions.height) {

			dimensions = {
				width: width,
				height: height,
				layerHeight: height * options.layerHeightPercentage
			};

			$.each(layers, function (i) {
				
				var layerH = dimensions.layerHeight;
				if(!isNaN(this.data.factor))
					layerH *= this.data.factor;
				this.offset = this.$node.offset();
				
				
					

				this.$node.add(this.$node.parent()).css({
					"height": layerH,
					"width": width
				});

				if (this.$parallaxLayer) {
					this.$parallaxLayer.css({
						"height": layerH,
						"width": width
					});
				}

				this.$parallaxImage.css({"width": width, "height": height});

				if (i === layers.length - 1)
					globalScrollHandler();
			});
		}
	}

	/**
	 * registers background layers.
	 * @param $backgroundLayers {jHTMLElements} a collection of background layer elements.
	 * @returns {jContext} the jQuery context for chaining.
	 */
	function registerParallaxLayer($backgroundLayers) {
		return (has.touch) ? null : $backgroundLayers.each(function (i) {
			var $this = $(this).hasClass(".background-layer") ? $(this) : $(this).find(".background-layer");
			var $moveIntoParallax = $(this).find(".move-into-parallax");
			var width = $this.data("width");
			var height = $this.data("height");
			var factor = parseFloat($this.data("factor"));
			var id = $this.data("id")
			var src = $this.css("background-image").replace(window.HelperFunctions.cssBackgroundImageRegex, "$1");
			var parallaxLayerDelegateClass = "parallax-layer-";


			if (width && height && src) {

				var layer = {
					data: {"width": width, "height": height, "src": src, factor: factor},
					$node: $this,
					offset: $this.offset(),
					getAdditionalOffset: makeOffsetPoints(options.offsetPoints)
				};

				$.extend(layer, createParallaxLayer(layer));
				if (id && layer.$parallaxLayer)
					layer.$parallaxLayer.attr("id", id);


				if ($moveIntoParallax.length && layer.$parallaxLayer) {

					layer.$parallaxLayer.append(
							$moveIntoParallax
							.removeClass("move-into-parallax")
							.addClass("in-parallax-layer")
							);
				}

				layers.push(layer);

				// set parallax layer delegate for image toggle.
				parallaxLayerDelegateClass += layers.length;
				layer.$node.data("delegateElementSelector", "." + parallaxLayerDelegateClass);
				layer.$parallaxImage.addClass(parallaxLayerDelegateClass);

				$this.css("background-image", "");
			}

			if (i === $backgroundLayers.length - 1) {
				globalResizeHandler();
			}
		});
	}

	function createParallaxLayer(layer) {
		var $parallaxLayer = $("<div>", {"class": "parallax-layer"}).appendTo($("body"));

		var $parallaxImage = $("<div>", {
			"class": "parallax-image",
			"css": {
				"background-image": "url(" + layer.data.src + ")"
			}
		}).appendTo($parallaxLayer ? $parallaxLayer : layer.$node);

		return {"$parallaxLayer": $parallaxLayer, "$parallaxImage": $parallaxImage};
	}

	function makeOffsetPoints(points) {
		var a = points[0], b = points[1], c = points[2], d = points[3];
		return function (t) {
			return (1 - t) * (1 - t) * (1 - t) * a + 3 * (1 - t) * (1 - t) * t * b + 3 * (1 - t) * t * t * c + t * t * t * d;
		};
	}

	function getElementData(element) {
		var offset = element.offset();
		var height = element.height();
		var offsetTop = offset.top - dimensions.height - 50;
		var offsetBottom = offset.top + height + 50;

//        console.log(
//          scrollTop, offsetTop, offsetBottom,
//          scrollTop > offsetTop,
//          scrollTop < offsetBottom
//        )

		return {
			elementIsInView: scrollTop > offsetTop && scrollTop < offsetBottom,
			positionOnPage: Math.round(((scrollTop - offsetTop) / (offsetBottom - offsetTop)) * 100) / 100
		};
	}

	// handlers.
	$window.bind("scroll", globalScrollHandler);
  ScrollManager.addScrollSubscriber(
        {
            callbackFN: function (topScroll) {
                //console.log("CB");
                globalScrollHandler(this, false, topScroll);
            }
        }
    );


	$window.bind("resize", globalResizeHandler);

	window.Development = window.Development || {};
	window.Development.backgroundLayerScroll = registerParallaxLayer;

})(window, window.jQuery, window.Modernizr);