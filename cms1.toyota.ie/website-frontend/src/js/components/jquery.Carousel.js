/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*!
 * Bootstrap v3.3.2 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

/*!
 * Generated using the Bootstrap Customizer (http://getbootstrap.com/customize/?id=cf0f62fd3c1dc5d4bb04)
 * Config saved to config.json and https://gist.github.com/cf0f62fd3c1dc5d4bb04
 */
if (typeof jQuery === 'undefined') {
	throw new Error('Bootstrap\'s JavaScript requires jQuery')
}
+function ($) {
	'use strict';
	var version = $.fn.jquery.split(' ')[0].split('.')
	if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1)) {
		throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher')
	}
}(jQuery);

/* ========================================================================
 * Bootstrap: carousel.js v3.3.2
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
	'use strict';

	// CAROUSEL CLASS DEFINITION
	// =========================

	var Carousel = function (element, options) {
		this.$element = $(element)
		this.$indicators = this.$element.find('.carousel-indicators')
		this.options = options
		this.paused =
				this.sliding =
				this.interval =
				this.$active =
				this.$items = null

		this.options.keyboard && this.$element.on('keydown.bs.carousel', $.proxy(this.keydown, this))

		this.options.pause == 'hover' && !('ontouchstart' in document.documentElement) && this.$element
				.on('mouseenter.bs.carousel', $.proxy(this.pause, this))
				.on('mouseleave.bs.carousel', $.proxy(this.cycle, this))
	}

	Carousel.VERSION = '3.3.2'

	Carousel.TRANSITION_DURATION = 600

	Carousel.DEFAULTS = {
		interval: 5000,
		pause: 'hover',
		wrap: true,
		keyboard: true
	}

	Carousel.prototype.keydown = function (e) {
		if (/input|textarea/i.test(e.target.tagName))
			return
		switch (e.which) {
			case 37:
				this.prev();
				break
			case 39:
				this.next();
				break
			default:
				return
		}

		e.preventDefault()
	}

	Carousel.prototype.cycle = function (e) {
		e || (this.paused = false)

		this.interval && clearInterval(this.interval)

		this.options.interval
				&& !this.paused
				&& (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

		return this
	}

	Carousel.prototype.getItemIndex = function (item) {
		this.$items = item.parent().children('.item')
		return this.$items.index(item || this.$active)
	}

	Carousel.prototype.getItemForDirection = function (direction, active) {
		var activeIndex = this.getItemIndex(active)
		var willWrap = (direction == 'prev' && activeIndex === 0)
				|| (direction == 'next' && activeIndex == (this.$items.length - 1))
		if (willWrap && !this.options.wrap)
			return active
		var delta = direction == 'prev' ? -1 : 1
		var itemIndex = (activeIndex + delta) % this.$items.length
		return this.$items.eq(itemIndex)
	}

	Carousel.prototype.to = function (pos) {
		var that = this
		var activeIndex = this.getItemIndex(this.$active = this.$element.find('.item.active'))

		if (pos > (this.$items.length - 1) || pos < 0)
			return

		if (this.sliding)
			return this.$element.one('slid.bs.carousel', function () {
				that.to(pos)
			}) // yes, "slid"
		if (activeIndex == pos)
			return this.pause().cycle()

		return this.slide(pos > activeIndex ? 'next' : 'prev', this.$items.eq(pos))
	}

	Carousel.prototype.pause = function (e) {
		e || (this.paused = true)

		if (this.$element.find('.next, .prev').length && $.support.transition) {
			this.$element.trigger($.support.transition.end)
			this.cycle(true)
		}

		this.interval = clearInterval(this.interval)

		return this
	}

	Carousel.prototype.next = function () {
		if (this.sliding)
			return
		return this.slide('next')
	}

	Carousel.prototype.prev = function () {
		if (this.sliding)
			return
		return this.slide('prev')
	}

	Carousel.prototype.slide = function (type, next) {
		var $active = this.$element.find('.item.active')
		var $next = next || this.getItemForDirection(type, $active)
		var isCycling = this.interval
		var direction = type == 'next' ? 'left' : 'right'
		var that = this

		if ($next.hasClass('active'))
			return (this.sliding = false)

		var relatedTarget = $next[0]
		var slideEvent = $.Event('slide.bs.carousel', {
			relatedTarget: relatedTarget,
			direction: direction
		})
		this.$element.trigger(slideEvent)
		if (slideEvent.isDefaultPrevented())
			return

		this.sliding = true

		isCycling && this.pause()

		if (this.$indicators.length) {
			this.$indicators.find('.active').removeClass('active')
			var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
			$nextIndicator && $nextIndicator.addClass('active')
		}

		var slidEvent = $.Event('slid.bs.carousel', {relatedTarget: relatedTarget, direction: direction}) // yes, "slid"
		if ($.support.transition && this.$element.hasClass('slide')) {
			$next.addClass(type)
			$next[0].offsetWidth // force reflow
			$active.addClass(direction)
			$next.addClass(direction)
			$active
					.one('bsTransitionEnd', function () {
						$next.removeClass([type, direction].join(' ')).addClass('active')
						$active.removeClass(['active', direction].join(' '))
						that.sliding = false
						setTimeout(function () {
							that.$element.trigger(slidEvent)
						}, 0)
					})
					.emulateTransitionEnd(Carousel.TRANSITION_DURATION)
		} else {
			$active.removeClass('active')
			$next.addClass('active')
			this.sliding = false
			this.$element.trigger(slidEvent)
		}

		isCycling && this.cycle()

		return this
	}


	// CAROUSEL PLUGIN DEFINITION
	// ==========================

	function Plugin(option) {
		return this.each(function () {
			var $this = $(this)
			var data = $this.data('bs.carousel')
			var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
			var action = typeof option == 'string' ? option : options.slide

			if (!data)
				$this.data('bs.carousel', (data = new Carousel(this, options)))
			if (typeof option == 'number')
				data.to(option)
			else if (action)
				data[action]()
			else if (options.interval)
				data.pause().cycle()
		})
	}

	var old = $.fn.carousel

	$.fn.carousel = Plugin
	$.fn.carousel.Constructor = Carousel


	// CAROUSEL NO CONFLICT
	// ====================

	$.fn.carousel.noConflict = function () {
		$.fn.carousel = old
		return this
	}


	// CAROUSEL DATA-API
	// =================

	var clickHandler = function (e) {
		var href
		var $this = $(this)
		var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) // strip for ie7
		if (!$target.hasClass('carousel'))
			return
		var options = $.extend({}, $target.data(), $this.data())
		var slideIndex = $this.attr('data-slide-to')
		if (slideIndex)
			options.interval = false

		Plugin.call($target, options)

		if (slideIndex) {
			$target.data('bs.carousel').to(slideIndex)
		}

		e.preventDefault()
	}

	$(document)
			.on('click.bs.carousel.data-api', '[data-slide]', clickHandler)
			.on('click.bs.carousel.data-api', '[data-slide-to]', clickHandler)

	$(window).on('load', function () {
		$('[data-ride="carousel"]').each(function () {
			var $carousel = $(this)
			Plugin.call($carousel, $carousel.data())
		})
	})

}(jQuery);

/* ========================================================================
 * Bootstrap: transition.js v3.3.2
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
	'use strict';

	// CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
	// ============================================================

	function transitionEnd() {
		var el = document.createElement('bootstrap')

		var transEndEventNames = {
			WebkitTransition: 'webkitTransitionEnd',
			MozTransition: 'transitionend',
			OTransition: 'oTransitionEnd otransitionend',
			transition: 'transitionend'
		}

		for (var name in transEndEventNames) {
			if (el.style[name] !== undefined) {
				return {end: transEndEventNames[name]}
			}
		}

		return false // explicit for ie8 (  ._.)
	}

	// http://blog.alexmaccaw.com/css-transitions
	$.fn.emulateTransitionEnd = function (duration) {
		var called = false
		var $el = this
		$(this).one('bsTransitionEnd', function () {
			called = true
		})
		var callback = function () {
			if (!called)
				$($el).trigger($.support.transition.end)
		}
		setTimeout(callback, duration)
		return this
	}

	$(function () {
		$.support.transition = transitionEnd()

		if (!$.support.transition)
			return

		$.event.special.bsTransitionEnd = {
			bindType: $.support.transition.end,
			delegateType: $.support.transition.end,
			handle: function (e) {
				if ($(e.target).is(this))
					return e.handleObj.handler.apply(this, arguments)
			}
		}
	})

}(jQuery);



/* 
	 #############################################################################
	 adepted carousel
	 #############################################################################
	 */

function offerCarousel(id) {
	var $container = $(id);
	if(!$container.length)
		return;
	var $inner = $('<div class="carousel-inner " >');
	var $left = $('<a class="carousel-arrow left icon icon-angle-left" href="" data-icon="&#xf104;" />');
	var $rigth = $('<a class="carousel-arrow right icon icon-angle-right" href="" data-icon="&#xf105;" />');
	var $nav = $('<ol class="carousel-nav carousel-indicators"></ol>');
	var $items = $container.find(".grid_6");
	
	if ($items.length <= 2)
		return;

	
	if ($(window).width() >= 768 ) {
		$container.append($inner);
		setRows();
		$container.append($left);
		$container.append($rigth);
		$container.append($nav);
		setRowWidth();
		initCarousel();
		setRowHeigth();
		$(window).resize(function () {
			setRowWidth();
			setRowHeigth();
		});
		var $img = $inner.find("img");
		var x = window.setInterval(function () {
			var loaded = true;
			$img.each(function () {
				loaded = loaded & IsImageLoaded(this);
			});
			if (loaded) {
				setRowHeigth();
				window.clearInterval(x);
			}
		}, 100);
	}else{
		
	}

	function setRows() {
		$items.detach();
		for (var i = 0, max = $items.length; i < max; i += 2) {
			var $row = $("<div class='row item'></div>");
			$row.append($items.eq(i));
			$row.append($items.eq(i + 1));
			if (i === 0)
				$row.addClass("active");
			$inner.append($row);
		}
	}
	function setRowWidth() {
		var $rows = $container.find(".row");
		var w = $inner.width();
		$rows.width(w);
	}

	function setRowHeigth() {
		var $rows = $container.find(".row");
		var h = 0;
		$rows.each(function () {
			h = Math.max(h, $(this).outerHeight());
		});
		$inner.height(h);
	}

	function initCarousel() {
		var $carousel = $container.carousel({
			interval: 5000
		});

		var $rows = $container.find(".row");
		$rows.each(function (i) {
			var active = (i === 0) ? "class='active'" : "";
			$nav.append('<li data-target="' + id + '" data-slide-to="' + i + '" ' + active + '></li>');
		});

		$left.click(function (e) {
			e.preventDefault();
			$carousel.carousel("prev");
		});
		$rigth.click(function (e) {
			e.preventDefault();
			$carousel.carousel("next");
		});
		$nav.find("li").click(function (e) {
			e.preventDefault();
			$carousel.carousel(parseInt($(this).data("slide-to")));
		});
	}


	function IsImageLoaded(img) {
		// During the onload event, IE correctly identifies any images that
		// weren’t downloaded as not complete. Others should too. Gecko-based
		// browsers act like NS4 in that they report this incorrectly.
		if (!img.complete) {
			return false;
		}

		// However, they do have two very useful properties: naturalWidth and
		// naturalHeight. These give the true size of the image. If it failed
		// to load, either of these should be zero.

		if (typeof img.naturalWidth !== "undefined" && img.naturalWidth === 0) {
			return false;
		}

		// No other way of checking: assume it’s ok.
		return true;
	}
}

