
/* 
 #############################################################################
 Primary navigation - v0.01
 
 dependencies
 - ScrollManager.js
 
 Contains all functionality to make the primary nav fixed to window, and the silde down menu
 #############################################################################
 */


var PrimaryNavigation = (function() {

	// Public methods
	return {
		// Fixed the navigation element via triggering CSS classes on users passes the 'showAfter' scrollY
		bindFixedNavigation: function(navElement, showAfter) {
			ScrollManager.addScrollSubscriber(
					{
						callbackFN: function(topScroll) {
							if (topScroll > showAfter && !navElement.hasClass('reveal')) {
								navElement.addClass('reveal').addClass('revealed');
							}
						},
						fixedThreshold: showAfter
					}
			);
			ScrollManager.addScrollSubscriber(
					{
						callbackFN: function(topScroll) {
							if (topScroll <= 1) {
								navElement.removeClass('reveal');
							}
						},
						fixedThreshold: 0
					}
			);
		},
		// Secondary nav
		bindSecondaryLinks: function($navContainer, options) {
			var config = $.extend({}, {
				useMask: true,
				listSelector: ".sec-nav-list",
				linksSelector: ".sec-nav-list li",
				sectionsSelector: ".page-section",
				headerLink: "Overview",
				footerLink: false,
				footerActiveFromEnd: 150,
				scrollSpeed: 800,
				scrollOffset: 120,
				activeOffset: 300
			}, options);


			var $navItems = $navContainer.find(config.linksSelector);
			$navItems.each(function(index) {
				var $el = $(this);
				if ($el.hasClass("internal-link")) {
					var id = $el.data("id");
					var linked = $(id);
					if (linked.length <= 0)
						$el.remove();
				}
			});

			var $secNavItems = $(config.linksSelector, $navContainer);
			var $listContainer = $(config.listSelector, $navContainer);
			var $secNavSections = $(config.sectionsSelector);
			var sectionActive = false;
			var $mask = false;



			ScrollManager.addScrollSubscriber(
					{
						callbackFN: function(topScroll, isAnimatedScroll) {
							if (isAnimatedScroll)
								return false;
							$activeSection = false;
							$secNavSections.each(function(i) {
								var $thisItem = $(this);

								var thisTop = $thisItem.offset().top - config.activeOffset;

								if (i === 0 && topScroll < thisTop)
									return false; // Exit loop if first item isn't visible yet (assumes order is correct...)

								if (topScroll >= thisTop) {
									$activeSection = $thisItem;
									return true;
								} else {
									return false;
								}
							});

							// Active section is false, no specific page-sections visible yet, so header if link exists...
							if (!$activeSection) {
								setActiveTab(config.headerLink);
							} else {
								// within config.footerActiveFromEnd from bottom of the page
								if ((topScroll + $(window).height() >= $(document).height() - config.footerActiveFromEnd)) {
									setActiveTab(config.footerLink);
								} else {
									setActiveTab($activeSection.data("id"));
								}
							}
						}
					}
			);

			$secNavItems.on("click", function(e) {


				var thisID = $(this).data("id") || false;
				setActiveTab(thisID);

				if (thisID) {
					e.preventDefault();

					var targetScrollY = 0;
					if (thisID === config.headerLink) {
						targetScrollY = 0;
					} else if (thisID === config.footerLink) {
						targetScrollY = $(document).height() - $(window).height();
					} else if ($secNavSections.filter('[data-id="' + thisID + '"]').length) {
						targetScrollY = $secNavSections.filter('[data-id="' + thisID + '"]').offset().top - config.scrollOffset;
					}

					ScrollManager.animatedScrollStart();
					$("html,body").stop().animate({scrollTop: targetScrollY}, config.scrollSpeed, "swing", function(evt) {
						ScrollManager.animatedScrollEnd();
					});
				}
			});

			function setActiveTab(sectionID) {
				var $activeTab;
				if (config.useMask) {
					if (!$mask) {
						$mask = $('<li class="mask" />');
						$listContainer.append($mask);
					}
					$activeTab = $secNavItems.filter('[data-id="' + sectionID + '"]');
					if ($activeTab && $activeTab.length) {
						$mask.css({"opacity": 1, "width": $activeTab.width(), "left": $activeTab.position().left});
					} else {
						$mask.css({"opacity": 0 /*, "left": -$mask.width() */});
					}
				} else {
					$secNavItems.removeClass("active");
					$activeTab = $secNavItems.filter('[data-id="' + sectionID + '"]');
					if ($activeTab && $activeTab.length) {
						$activeTab.addClass("active");
					}
				}
			}
		},
		// Navigation dropdown menu binding
		bindMainMenu: function($navContainer, options) {
			var config = $.extend({}, {
				bindResponse: false,
				isMobile: false,
				contentSelector: ".nav-content",
				pageWrapperSelector: ".outer-wrapper",
				slideTime: 666,
				fadeTime: 700,
				menuHeight: 70
			}, options);

			var $navContentArea = $('<div class="nav-content-area isoForceHardwardCalc" />');
			$navContainer.append($navContentArea);
			var $navListElement = $('.large-nav', $navContainer);
			var $breadCrumb = $('.breadcrumb', $navContainer);
			$(window).on("fonts-loaded", function() {
				var $activeLink = $('.active', $breadCrumb);
				var $activeIcon = $('.active i', $breadCrumb);
				if ($activeIcon.length && $activeLink.length) {
					$activeIcon.css({"left": $activeLink.position().left + ($activeLink.outerWidth(true) / 2) - 5}); // 5 = 50% width of indicator
				}
			});
			var $content;
			var menuOpen = false;
			var menuLoaded = false;

			$('.dropdownnav li', $navContainer).each(function() {
				var $navHolder = $(this);
				var $link = $('a', $navHolder);
				var $icon = $('a i', $navHolder);
				var $models;

				if (!$link.length)
					return true;

				if ($link.attr("href") == window.location.pathname)
					return false;

				var ajaxRequest = $.ajax({url: $link.attr("href"), dataType: 'html'});

				ajaxRequest.done(function(data, textStatus, jqXHR) {
					$content = $(config.contentSelector, data);
					$navContentArea.append(data);
					$models = $('.model-list', $content);
					$link.data('menu-height', $content.outerHeight(true));
					if ($content.outerHeight(true) < $(window).height()) {
						$link.data('menu-height', $(window).height());
					}
					menuLoaded = true;
				});

				$link.on('click', function(e) {
					e.preventDefault();
					if (menuLoaded) {
						var $navContent = $(config.contentSelector, $navContentArea);
						if (!menuOpen) {
							menuOpen = true;
							var heigh =  $(config.contentSelector).outerHeight(true);
							$link.data('menu-height', heigh);
							if (heigh < $(window).height()) {
								$link.data('menu-height', $(window).height());
							}
							ScrollManager.pauseScrollCallbacks();
							$navContainer.data("scrolly", $(window).scrollTop());
							$navHolder.addClass("active");
							$navListElement.addClass("nav-open");
							$icon.removeClass("icon icon icon-chevron-down").addClass("icon icon icon-chevron-up");
							$navContainer.addClass("revealed").addClass("menu-open");
							if (Modernizr.cssanimations) {
								$navContentArea.css("opacity",1);
								$navContentArea.css({"height": $link.data('menu-height')});
							} else {
								$navContentArea.css("opacity",1);
								$navContentArea.stop().animate({"height": $link.data('menu-height')}, config.slideTime);
							}
							setTimeout(function() {
								if (menuOpen) {
									window.scroll(0, 0);
									$navContainer.addClass("noFixed");
									$(config.pageWrapperSelector).css({"display": "none"});
								}
							}, config.slideTime);
						} else {
							setTimeout(function() {
								if (!menuOpen) {
									$navContainer.removeClass("noFixed");
									$navContainer.removeClass("forceFixed");
									$navHolder.removeClass("active");
									$navContent.css({"margin-top": 0});
									$icon.removeClass("icon icon icon-chevron-up").addClass("icon icon icon-chevron-down").height();
									$navContainer.removeClass("menu-open");
								}
							}, config.slideTime);

							$(config.pageWrapperSelector).css({"display": ""});
							$navListElement.removeClass("nav-open").height();
							$navContainer.addClass("forceFixed");
							$navContent.css({"margin-top": -($(window).scrollTop())});
							window.scroll(0, $navContainer.data("scrolly"));
							$(config.pageWrapperSelector).css({"height": "auto", "overflow": "visible"});
							ScrollManager.resumeScrollCallbacks();
							if (Modernizr.cssanimations) {
								$navContentArea.css("opacity",0);
								$navContentArea.css({"height": 0});
							} else {
								$navContentArea.stop().animate({"height": 0}, config.slideTime);
								$navContentArea.css("opacity",1);
							}
							menuOpen = false;
						}
						return false;
					}
				});
			});
		}

	};
}());


