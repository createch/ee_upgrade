
/* 
 ###########################################################################################################
 ########################## BINDINGS START  ################################################################
 ###########################################################################################################
 */
$(function () {

	/*
	 #############################################################################
	 Print buttons
	 #############################################################################
	 */

    $(".printpage").click(function () {
        window.print();
    });

	/* 
	 #############################################################################
	 Filterable content remove empty item from menu
	 #############################################################################
	 */
	$(".filterable-content").each(function (index) {
		var $container = $(this);
		$container.find(".fc-group").each(function () {
			var $group = $(this);
			var name = false;

			if ($group.hasClass("exterior-group"))
				name = "exterior-group";
			else if ($group.hasClass("interior-group"))
				name = "interior-group";
			else if ($group.hasClass("engines-group"))
				name = "engines-group";
			else if ($group.hasClass("safety-group"))
				name = "safety-group";

			var $itms = $group.find(".fc-group-content .item");
			if ($itms.length === 0) {
				var $control = $container.find(".fb-item[data-section-filter='." + name + "']");
				$control.parent().remove();
				$group.remove();
			}
		});
	});

	// horizontall gallery
	TranslationManager.addLocalTranslations({
		"backTo": "Back to"
	});

	window.localisedTranslations = TranslationManager.getLocalTranslations();
	/* 
	 #############################################################################
	 Primary navigation bindings
	 #############################################################################
	 */


	// limit breadcrumb for larger strings
	var bread_length = 0;
	var $bread_text = $(".primary-nav .breadcrumb > li > a  span");
	$bread_text.each(function () {
		bread_length += $(this).text().length;
	});
	if (bread_length > 55) {
		$bread_text.each(function () {
			$el = $(this);
			if ($el.text().length > 18)
				$el.text($el.text().substring(0, 15) + '...');
		});
	}



	PrimaryNavigation.bindFixedNavigation($('.primary-nav'), Math.min($(window).height() / 2));
	PrimaryNavigation.bindFixedNavigation($('.secondary-nav'), Math.min($(window).height() / 2));
	PrimaryNavigation.bindMainMenu($('.primary-nav'));
	PrimaryNavigation.bindSecondaryLinks($('.secondary-nav'));

	//close menu on wen open secondary content
	$("body").on("click", ".nav-content .secContentTrigger", function () {
		$("#main_menu_trigger").click();
		return true;
	});

	$("#main_menu_trigger").click(function () {
		if ($('.secondary-nav').length > 0) {
			if ($('.secondary-nav').css('display') == 'none')
				$('.secondary-nav').css('display', 'block');
			else
				$('.secondary-nav').css('display', 'none');
		}
	})


	/* 
	 #############################################################################
	 Scaled header & footer bindings
	 #############################################################################
	 */

	// Settings
	var fixedNavHeight = 70;
	var contentLenghtVisibleOnLoad = 80;
	var minHeroHeight = 620;
	var $masterWrapper = $('.master-wrapper');
	var $innerWrapper = $('.inner-wrapper');

	// Common element refrences
	var $headerElement = $('.header-container', $masterWrapper).first();
	var $footerElement = $('.footer-container', $masterWrapper).first();
	var $footerDynamicCTA = $('.dynamic-cta', $footerElement).first();

	// Bind footer popup CTA if present
	if ($footerDynamicCTA.length) {
		CommonEffects.footerPopup($footerDynamicCTA, $(window).height() / 1.5);
	}

	// If the header element has the static trigger class bind it
	if ($headerElement.hasClass('fx-header')) {
		CommonEffects.fixedScaledHeader($masterWrapper, $headerElement, contentLenghtVisibleOnLoad, minHeroHeight);
	}

	// If the footer element has the static trigger class bind it
	if ($footerElement.hasClass('fx-footer')) {
		CommonEffects.fixedScaledFooter($masterWrapper, $footerElement, fixedNavHeight, minHeroHeight);
	}

	// If we have both static header & footer, inner wrapper must be at least as tall as the viewport to hide the transition...
	if ($headerElement.hasClass('fx-header') && $footerElement.hasClass('fx-footer')) {
		$(window).on('debouncedresize', function () {
			$innerWrapper.css({"min-height": $(window).height()});
		}).trigger('debouncedresize');
	}

	// Secondary Content bindings
	if (window.location.hostname !== "assuredusedcars.toyota.ie") {
		$('.SecContentContainer').each(function () {
			$(this).SecondaryContent({
				"sectionIDs": $(this).data("id").split("/"),
				"mode": $(this).data("contenttype"),
				"cGroupName": $(this).data("name"),
				"translations": localisedTranslations
			});				
		});
	}



	/* 
	 #############################################################################
	 Generic visual enhancements, no core functionality
	 #############################################################################
	 */

	// Carousel BG
	$('.carousel').each(function () {
		CarouselBG.addCarousel($(this));
	});

	// immitates the background size propery in browser which dont support it
	if (!Modernizr.backgroundsize) {
		CommonEffects.scaleBGs($('.coverBG'));
	}

	CommonEffects.wrapBy($('.slide-up-element'), 4, "<div class='spotlight-row grid_row'><div class='wrapper'></div></div>");

	// Read more rows
	CommonEffects.readMoreRows($('.read-more-rows'), localisedTranslations);

	// remove view more button or the container if it is empty
	$(".spotlight-container").each(function () {
		var x = $(this).find(".more_about_item");
		if (x.length <= 0)
			$(this).hide();
		else if (x.length <= 4)
			$(this).find(".read-more-trigger").remove();
	});

	// Toggle a css class when items come into view
	CommonEffects.toggleClassWhenVisible($('.fadeIn'));

	// Background parralax
	CommonEffects.parraliseBG($('.bg-par-asset'), 422, -229, 0);

	if ($.fn.contentToggle) {
		$("[data-content-toggle]").contentToggle({
			toggleClassOnChange: ["blueBtn", "greyBtn"]
		});
	}

	// create background layers.
	if (Modernizr.csstransforms3d && Development.backgroundLayerScroll) {
		Development.backgroundLayerScroll($("[data-layer-parallax]"));
	}

	// offer spotlight binding.
	$(".offer-spotlight .full-terms, .offer-remote .full-terms").each(function () {
		var $text = $(this).prev(".terms");
		if ($text.length) {
			$(this).ReadMore({
				"translations": $.extend(localisedTranslations, {"link-text": $text.text()}),
				"replacementTextTranslationKey": "link-text"
			});
			$text.css("display", "none");
		}
	});


	// dinamically load content
	$(".ajax-content").html('<div id="#cboxLoadingGraphic">&nbsp;</div>');

	/*$(".ajax-content").load($('.ajax-content').attr('rel'), function() {
	 wrapGrid();
	 });*/

	// Hack for fonts loaded event, replace with real hook...
	setTimeout(function () {
		$(window).trigger("fonts-loaded");
	}, 250);
	setTimeout(function () {
		$(window).trigger("fonts-loaded");
	}, 500);
	setTimeout(function () {
		$(window).trigger("fonts-loaded");
	}, 1000);


	// share tools
	CommonEffects.bindShareTools($(document));
	// quick pick disclaimer
	CommonEffects.bindQuickSpecDislaimer($(document));
	//horixontal gallery
	Development.horizontalGallery($("[data-gallery-horizontal]"));







	/* 
	 #############################################################################
	 Parallax footer and header HEro
	 #############################################################################
	 */


	// Settings
	var fixedNavHeight = 70;
	var contentLenghtVisibleOnLoad = 80;
	var minHeroHeight = 620;
	var $masterWrapper = $('.master-wrapper');

	// Common element refrences
	var $headerElement = $('.header-hero', $masterWrapper).first();
	var $footerElement = $('.footer-hero', $masterWrapper).first();

	// If the header element has the static trigger class bind it
	if ($headerElement.hasClass('fx-header')) {
		CommonEffects.fixedScaledHeader($masterWrapper, $headerElement, contentLenghtVisibleOnLoad, minHeroHeight);
	}

	// If the footer element has the static trigger class bind it
	if ($footerElement.hasClass('fx-footer')) {
		CommonEffects.fixedScaledFooter($masterWrapper, $footerElement, fixedNavHeight, minHeroHeight);
	}

	// If we have both static header & footer, inner wrapper must be at least as tall as the viewport to hide the transition...
	if ($headerElement.hasClass('fx-header') && $footerElement.hasClass('fx-footer')) {
		$(window).on('debouncedresize', function () {
			$innerWrapper.css({"min-height": $(window).height()});
		}).trigger('debouncedresize');
	}

	// immitates the background size propery in browser which dont support it
	if (!Modernizr.backgroundsize) {
		CommonEffects.scaleBGs($('.coverBG'));
	}

	/* 
	 #############################################################################
	 forms
	 #############################################################################
	 */


	$(".contactform input , .contactform select , .contactform textarea").focusin(function () {
		var parent = $(this).parents("div[class^='grid_']");
		parent.first().find(".title1").css("color", "#00a0f0");
	});

	$(".contactform input ,  .contactform select , .contactform textarea").focusout(function () {
		var parent = $(this).parents("div[class^='grid_']");
		parent.first().find(".title1").css("color", "");
	});


	$("input.datepicker").datepicker({
		minDate: +3,
		maxDate: "+1M +20D",
		beforeShowDay: $.datepicker.noWeekends
	});

	$.formUtils.addValidator({
		name: 'select',
		validatorFunction: function (value, $el, config, language, $form) {
			var x = value !== "no_selection";
			return x;
		},
		errorMessage: 'Please select one valid option.',
		errorMessageKey: 'badEvenNumber'
	});


	/* 
	 #############################################################################
	 Grid_3 and grid_6fixes
	 #############################################################################
	 */




	// BINDINGS
	// fix offers grid
	// staff section in about us page
	grid_3_6_stack($(".staff , .fc-group-container"));
	grid_6_stack($(".container_12"));
	grid_more_about_stack($(".spotlight-container"));

	/* 
	 #############################################################################
	 NEWS and OFFERS, Make First 4 lemets grid_6 instead grid_3
	 turn it onto grid_3 when click on any filter button
	 #############################################################################
	 */


	$("#filter").NewsFilter();




	/* 
	 #############################################################################
	 back to menu
	 #############################################################################
	 */


	//smooth scloo to top
	$('.return_menu').on('click', function (e) {
		e.preventDefault();
		$.smoothScroll({
			scrollTarget: '#main_nav'
		});
	});





	/* 
	 #############################################################################
	 SECONDARY NAV
	 #############################################################################
	 */


	var $sec = $(".sec-nav-list");
	var secW = $sec.width();
	var $items = $sec.find("li");


	/* 
	 #############################################################################
	 social.
	 #############################################################################
	 */



	var $activeShare = null;
	$("body").click(function () {
		if ($activeShare) {
			$activeShare.find(".social_box_wrapper").hide();
		}
		$activeShare = null;
	});


	$(".share").click(function (e) {
		e.stopPropagation();
		if ($activeShare) {
			$activeShare.find(".social_box_wrapper").hide();
		}
		$activeShare = $(this);
		$(this).find(".social_box_wrapper").show();

		//hack to display the facebook share
		setTimeout(function () {
			var $span = $("#fbLike > span");
			var $iframe = $span.find("> iframe");
			$span.height(30);
			$iframe.height(30);
		}, 10);
	});


	/* 
	 #############################################################################
	 assured used car detail fix
	 #############################################################################
	 */

	var modelid = $("#assuredmodel");
	if (modelid.length === 1) {
		var bc = $(".breadcrumb span");
		bc.eq(2).text(modelid.text());
	}


	/* 
	 #############################################################################
	 google map secondary content redraw fix
	 #############################################################################
	 */

	$("body").on("click", ".tabtrigger[data-tab='pane-directions-tab']", function (e) {
		resizeMap("#getDirectionsMap");
	});

	$("body").on("click", ".getRoute", function (e) {
		e.preventDefault();
		getRoute($(this).parents(".container_12"));
	});



	/* 
	 #############################################################################
	 youtube videos uploads by users.... we don have control over the code
	 #############################################################################
	 */

	function resizeyoutube() {
		$("iframe[src*='youtube.com/']").each(function (i) {
			var el = $(this);
			el.css("width", "100%");
			var w = el.width();
			el.height(w / 1.778);
		});
	}
	resizeyoutube();
	$(window).resize(resizeyoutube);
	/* 
	 #############################################################################
	 fix some icons in ie7
	 #############################################################################
	 */
	$(".lt-ie8 .close-icon").html("x");
	$(".lt-ie8 .icon-angle-right").html("&raquo;");
	$(".lt-ie8 .icon-angle-left").html("&laquo;");
	$(".lt-ie8 .icon-chevron-down").html("︾");
	$(".lt-ie8 .icon-chevron-up").html("︽");



	/* 
	 #############################################################################
	 Assured Ased Cars 
	 #############################################################################
	 */

	//calculate price
	if ($('.finance_price').length > 0) {

		model = $('.finance_price').data('rel');

		$.post('//finance.toyota.ie/calculator.php?format=feed', {model_string: model},
		function (r) {
			//console.log(r)
			if (typeof r.error === "undefined")
				$('.finance_price').text('€' + r['Monthly Instalments']);
			else
				$('.flex_finance_block').css('display', 'none');
			$('.flex-quote-text').css('display', 'none');
		});

	}

	// change null string by not available text
	$(".ussuredCarValue").each(function () {
		var el = $(this);
		if (el.text() === "null") {
			el.parent().html("Not Available");
		}
	});

	// equal height colum on red call to action
	function resizeContact() {
		var assuredText = $(".assuredContact .text");
		var assuredFlex = $(".assuredFlex .text");
		if (assuredText.length && assuredFlex.length) {
			var h1 = assuredText.height();
			var h2 = assuredFlex.height();
			if (h2 > h1)
				assuredText.height(h2);
		}

	}

	resizeContact();
	$(window).resize(resizeContact);



	/*
	 #############################################################################
	 FEATURED USED CARS
	 #############################################################################
	 */
	if($("#featurd-used-cars").length){
        grid_more_about_stack($("#featurd-used-cars.spotlight-container"));
        CommonEffects.wrapBy($('#featurd-used-cars .slide-up-element'), 4, "<div class='spotlight-row grid_row'><div class='wrapper'></div></div>");
        // Read more rows
        var $fucR = $('#featurd-used-cars .read-more-rows');
        var $rowsContainer = $('.read-more-rows-container', $fucR);
        CommonEffects.readMoreRows($fucR, localisedTranslations);
        // remove view more button or the container if it is empty
        $("#featurd-used-cars.spotlight-container").each(function () {
            var x = $(this).find(".more_about_item");
            if (x.length <= 0)
                $(this).hide();
            else if (x.length <= 4)
                $(this).find(".read-more-trigger").remove();
        });
	}


    InitSearchBox();
    initFinanceBox();
	InitAssuredUsedCars();
    loadMap();

});
/* 
 ###########################################################################################################
 ############################## BINDINGS END ###############################################################
 ###########################################################################################################
 */





function wrapGrid() {
	stack = $(".groupby2 .grid_6");
	for (var i = 0; i < stack.length; i += 2) {
		stack.slice(i, i + 2).wrapAll("<div class='row'></div>");
	}
}


// add classes every 2 or 4 elements to succesive grid_3
// so they stak correctly even with diferrent sizes
function grid_3_6_stack($wrapper, onlyVisivle) {
	var aux = (onlyVisivle) ? ":visible" : "";
	$wrapper.each(function (index) {
		var $gridElems = $(this).find(".grid_3" + aux);
		for (var i = 2; i < $gridElems.length; i++) {
			var $elem = $gridElems.eq(i);
			$elem.removeClass("m2 m4");
			var m4 = (i) % 4;
			var m2 = (i) % 2;
			if (m4 === 0) {
				$elem.addClass("m4");
				continue;
			} else if (m2 === 0) {
				$elem.addClass("m2");
				continue;
			} else {

			}
		}
	});

}

function grid_6_stack($wrapper) {
	$wrapper.each(function (index) {
		var $gridElems = $(this).find("> .grid_6");
		for (var i = 2; i < $gridElems.length; i++) {
			var $elem = $gridElems.eq(i);
			$elem.removeClass("m2 m4");
			var m4 = (i) % 4;
			var m2 = (i) % 2;
			if (m4 === 0) {
				$elem.addClass("m4");
				continue;
			} else if (m2 === 0) {
				$elem.addClass("m2");
				continue;
			} else {

			}
		}
	});

}

function loadMap(elemId) {
	var selector = ".ajax-content";
	if (elemId) {
		selector = elemId;
	}
	$.each($(selector), function (key, value) {
		$(value).load($(value).data('rel'), function () {
			wrapGrid();
		});
	});
}

function grid_more_about_stack($wrapper, onlyVisivle) {
	var aux = (onlyVisivle) ? ":visible" : "";
	$wrapper.each(function (index) {
		var $gridElems = $(this).find(".more_about_item" + aux);
		for (var i = 2; i < $gridElems.length; i++) {
			var $elem = $gridElems.eq(i);
			$elem.removeClass("m2 m4");
			var m4 = (i) % 4;
			var m2 = (i) % 2;
			if (m4 === 0) {
				$elem.addClass("m4");
				continue;
			} else if (m2 === 0) {
				$elem.addClass("m2");
				continue;
			} else {

			}
		}
	});

}

$(function () {
	/* 
	 #############################################################################
	 Dealer Change requests
	 #############################################################################
	 */

	new offerCarousel("#promotions-carousel");
	new offerCarousel("#spotlight-carousel");
	new offerCarousel("#latest-offers-carousel");
	new offerCarousel("#latest-news-carousel");
	initSecondaryNav();
	$(window).resize(initSecondaryNav);


	function initSecondaryNav() {
		var $dropdown = null, $submenu = null;
		var gap = 350;

		var $container = $('.secondary-nav .grid_12');
		var $sec_nav_list = $container.find(".sec-nav-list");
		var $items = $sec_nav_list.find("li:not(.dropdown)").detach();
		var cont_w = $container.width();
		var items_w = 0;
		$sec_nav_list.empty();
		for (var i = 0, max = $items.length; i < max; i++) {
			var $elem = $items.eq(i);
			if (items_w + gap >= cont_w) {
				if (!$dropdown)
					createDropdown();
				$elem.detach().appendTo($submenu);
			} else {
				$sec_nav_list.append($elem);
				items_w += $elem.width();
			}
		}

		if ($dropdown)
			$sec_nav_list.append($dropdown);

		function createDropdown() {
			$dropdown = $('<li class="dropdown" style="display: inline-table;"><a href="#">More &nbsp;<i class="icon icon-chevron-down display-md"></i> </a></li>')
			$submenu = $('<ul class="dropdown-sub-menu"></ul>');
			$dropdown.append($submenu);
		}
	}


//	/// test te embed carsearch details.
//	var carSearchElems = null;
//	var $carSearch = $("#carsearch");
//	var $carDetails = $("#car_details");
//	$('body').on("click","#resultsAsuredUsedCars a",function(e){
//		e.preventDefault();
//		var $link = $(this);
//		var href = $link.attr("href");
//		$carDetails.load( href+" #car-details",function(){
//			$carSearch.css("display","none");
//		});
//	});
//
//	
//	$('body').on("click","#carback",function(e){
//		e.preventDefault();
//		$carDetails.empty();
//		$carSearch.attr("style","");
//		CarouselBG.addCarousel($carSearch.find(".carousel"));
//	});
});
