;(function($, Modernizr) {

	$.fn.contentToggle = function(__options) {
		return this.each(function() {
			return ContentToggle.init($(this), __options);
		});
	};

	var ContentToggle = {
		options: {
			toggleButtonClass: "toggleBtn",
            toggleClassOnChange: ["inactiveClass", "activeClass"],
            transitionDuration: Modernizr.csstransitions ? 500 : 0
		},
		init: function($content, _options) {
			if ( ! $content.data("content-toggle-state") ) {
				var state = {
					$this: $content,
					options: $.extend(true, this.options, _options, {}),
					isDefaultState: true,
					$altElements: $content.find("[data-alt]"),
					$altImgElements: $content.find("[data-alt-img]"),
					$altBlock: $content.find(".alt-block"),
					$defaultBlock: $content.find(".default-block")
				};
				$content.data("content-toggle-state", state);
				state.$toggleButton = state.$this.find("." + state.options.toggleButtonClass);
				state.$altBlock.css("display", "none");
				state.$toggleButton.bind("click", $.proxy(this.toggle, state));
			}
		},
		toggle: function() {

			var self = this;

			if ( this.$altElements.length ) {
				this.$altElements.each(function() {
					var $this = $(this);
					var defaultValue = $this.html();
					var alt = $this.data("alt");

					crossFadeAnimate({
						element: $this,
						newHtmlValue: alt,
						duration: self.options.transitionDuration,
						callback: function() {
							$this.data("alt", defaultValue);
						}
					});
				});
			}

			if ( this.$altImgElements.length ) {
				this.$altImgElements.each(function() {
					var $this = $(this);
					var $delegate = $($this.data("delegateElementSelector"));
					var currentSrc = ($delegate.length ? $delegate.css("background-image") : $this.css("background-image")).replace(window.HelperFunctions.cssBackgroundImageRegex, "$1");
					var altSrc = $this.data("alt-img");

					crossFadeAnimate({
						"element": $delegate.length ? $delegate : $this,
						"cssProperty": ["background-image", "url(" + altSrc + ")"],
						"duration": self.options.transitionDuration,
						"callback": function() {
							$this.data("alt-img", currentSrc);
						}
					});
				});
			}

			crossFadeAnimate({
				element: this[this.isDefaultState ? "$defaultBlock" : "$altBlock"],
				newElement: this[this.isDefaultState ? "$altBlock" : "$defaultBlock"],
				duration: self.options.transitionDuration,
				setDisplayNone: true,
				callback: function() {

					var altText = self.$toggleButton.data("alt-text"),
						newAltText;

					if ( altText ) {
						newAltText = self.$toggleButton.html();
						self.$toggleButton.html(altText);
						self.$toggleButton.data("alt-text", newAltText);
					}

					if ( self.options.toggleClassOnChange instanceof Array ) {
						self.$toggleButton[!self.isDefaultState? "addClass" : "removeClass"](self.options.toggleClassOnChange[1]);
						self.$toggleButton[self.isDefaultState? "addClass" : "removeClass"](self.options.toggleClassOnChange[0]);
					}
				}
			});

			this.isDefaultState = !this.isDefaultState;

			return false;
		}
	};

	function crossFadeAnimate(options) {

		options.element.add(options.newElement)
			.css("transition", "opacity " + options.duration + "ms ease-in-out" );

		setTimeout(function() {
			options.element.css("opacity", "0");

			setTimeout(function() {
				if ( options.cssProperty ) {
					options.element.css.apply(options.element, options.cssProperty);
				}
				if ( options.newHtmlValue ) {
					options.element.html(options.newHtmlValue);
				}

				if ( options.setDisplayNone ) {
					options.element.css("display", "none");
				}

				if ( options.newElement ) {
					options.newElement.css({
						"opacity": "0",
						"display": "block"
					});

					setTimeout(function() {
						options.newElement.css("opacity", "1");
					}, 10);
				} else {
					options.element.css("opacity", "1");
				}

				setTimeout(options.callback, options.duration);
			}, options.duration);
		}, 10);
	}

})(window.jQuery, window.Modernizr);