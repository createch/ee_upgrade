/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Gets And structure all the cars List
 * @returns {CarlistData}
 */
var Carslist = function () {
	var self = this;
	//cars
	this.cars = null;
	this.queryMeta = null;

	// parsed car data
	this.maker = [];
	this.model = [];
	this.country =[];
	this.makerModel = {
		"Any Make": []
	};
	this.fuelType = [];
	this.bodyTypes = [];
	this.basicColours = ['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'pink', 'brown', 'black', 'grey', 'white', 'silver', 'gold'];
	this.colours = [];
	this.doors = [];
	this.prices = {};
	this.years = {};
	this.mileages = {};



	this.parseCars = function (cars) {
		self.cars = cars;
		for (var i = 0, max = self.cars.length; i < max; i++) {
			self.cars[i] = self.standarizeData(self.cars[i]);
			var car = self.cars[i];
			self.parseMakesModel(car);
			self.parseBodyType(car);
			self.parsePrice(car);
			self.parseCountry(car);
			self.parseColour(car);
			self.parseMileage(car);
			self.parseYear(car);
			self.parseDoors(car);
		}
		// transform prices, years, mileages from object to array
		self.prices = Object.numKeys(self.prices);
		self.years = Object.numKeys(self.years);
		self.mileages = Object.numKeys(self.mileages);
		self.sortAll();
	};

	this.appendCars = function (cars) {
		var appendCars = cars;
		var start = self.cars.length;
		for (var i = start, max = start + appendCars.length; i < max; i++) {
			self.cars[i] = self.standarizeData(appendCars[i - start]);
		}
	};

	this.parseMakesModel = function (car) {
		if (!car.Make || !car.Model)
			return;
		var makerName = car.Make.toUpperCase();
		var modelName = car.Model.toUpperCase();
		if (!self.makerModel[makerName]) {
			self.maker.push(makerName);
			self.makerModel[makerName] = [];
		}
		if ($.inArray(modelName, self.model) === -1)
			self.model.push(modelName);
		if ($.inArray(modelName, self.makerModel["Any Make"]) === -1)
			self.makerModel["Any Make"].push(modelName);
		if ($.inArray(modelName, self.makerModel[makerName]) === -1)
			self.makerModel[makerName].push(modelName);
	};


	this.parseCountry = function(car){
		if(!car.Location)
			return;
		var country = car.Location;
		if ($.inArray(country, self.country) === -1)
			self.country.push(country);
	};
	
	
	this.parseBodyType = function (car) {
		if (!car.BodyType)
			return;
		var BodyType = car.BodyType.toUpperCase();
		if ($.inArray(BodyType, self.bodyTypes) === -1)
			self.bodyTypes.push(BodyType);
	};

	this.parsePrice = function (car) {
		var price = parseInt(car.Price);
		if (isNaN(price) || price < 0)
			return;
		self.prices[price] = true;
	};


	this.parseYear = function (car) {
		var year = parseInt(car.Year);
		if (isNaN(year) || year < 0)
			return;
		self.years[year] = true;
	};

	this.parseColour = function (car) {
		if (!car.Colour)
			return;
		var colourPart = car.Colour.split(" ");
		$.each(colourPart, function (index, value) {
			//console.log(value)
			var color = value.toLowerCase();
			if ($.inArray(color, self.basicColours) !== -1
					&& $.inArray(color, self.colours) === -1) {
				self.colours.push(color);
			}
		});
	};

	this.parseMileage = function (car) {
		var mileage = parseInt(car.Mileage);
		if (isNaN(mileage) || mileage < 0)
			return;
		self.mileages[mileage] = true;
	};


	this.sortAll = function () {
		self.maker.sort();
		self.country.sort();
		self.model.sort();
		self.fuelType.sort();
		self.bodyTypes.sort();
		self.colours.sort();
		self.doors.sort(sortNumber);
		self.prices.sort(sortNumber);
		self.years.sort(sortNumber);
		self.mileages.sort(sortNumber);
		for (var make in self.makerModel) {
			self.makerModel[make].sort();
		}
	};

//	this.parseFuelType = function (car) {
//		var fuel = car.FuelType.toUpperCase();
//		if ($.inArray(fuel, fuelType) === -1)
//			bodyTypes.push(car.FuelType.toUpperCase());
//	};

	this.parseDoors = function (car) {
		var doorsNumber = parseInt(car.Doors) ;
		if (!isNaN(doorsNumber) && $.inArray(doorsNumber, self.doors) === -1)
			self.doors.push(doorsNumber);
	};

	
	/**
	 * This methos standarize the data coming from solar so it can be used normaly by javascript.
	 * Data from solar does't come in an stardard format. i.e: "null" instead null.
	 * Change thimbnail fro default size. 
	 * @param {type} car
	 * @returns {unresolved}
	 */
	this.standarizeData = function (car) {
		for (var i in car) {
			if (car[i] === "null")
				car[i] = null;
		}
		// year
		if (!car.Year)
			car.Year = 0;
		if (!car.Price)
			car.Price = 0;
		if (car.Mileage===null || car.Mileage==="null")
			car.Mileage = -1;
		else{
			car.miles = Math.floor(parseInt(car.Mileage) * 0.621371);
		}
			
		
		
		if (!car.thumbnail) {
			car.thumbnail = 'https://development.toyota.ie/inc/images/noimage.jpg';
		} else {
			var thumbnails = car.thumbnail.split(",");
			if (thumbnails.length)
				car.thumbnail = thumbnails[0];
			car.thumbnail = car.thumbnail.replace('_thumbnail.jpg', '_default.jpg');
		}

		car.PriceF = formatNumber(car.Price);
		car.MileageF = formatNumber(car.Mileage);
		car.milesF =formatNumber(car.miles);
		return car;
	};



	this.reset = function () {
		self.cars = null;
		self.queryMeta = null;
		self.maker = [];
		self.model = [];
		self.makerModel = {
			"Any Make": []
		};
		self.fuelType = [];
		self.bodyTypes = [];
		self.basicColours = ['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'pink', 'brown', 'black', 'grey', 'white', 'silver', 'gold'];
		self.colours = [];
		self.doors = [];
		self.yearMin = 999999999999;
		self.yearMax = 0;
		self.priceMin = 999999999999;
		self.priceMax = 0;
		self.mileageMin = 999999999999;
		self.mileageMax = 0;
	};

};

function sortNumber(a, b) {
	return a - b;
}


function formatNumber(num) {
	if(num)
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
	else
		return num;
}

// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
if (!Object.numKeys) {
	Object.numKeys = (function () {
		'use strict';
		var hasOwnProperty = Object.prototype.hasOwnProperty,
				hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
				dontEnums = [
					'toString',
					'toLocaleString',
					'valueOf',
					'hasOwnProperty',
					'isPrototypeOf',
					'propertyIsEnumerable',
					'constructor'
				],
				dontEnumsLength = dontEnums.length;

		return function (obj) {
			if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
				throw new TypeError('Object.keys called on non-object');
			}

			var result = [], prop, i;

			for (prop in obj) {
				if (hasOwnProperty.call(obj, prop)) {
					var num = parseInt(prop);
						if(!isNaN(num))
							result.push(num);
				}
			}

			if (hasDontEnumBug) {
				for (i = 0; i < dontEnumsLength; i++) {
					if (hasOwnProperty.call(obj, dontEnums[i])) {
						var num = parseInt(dontEnums[i]);
						if(!isNaN(num))
							result.push(num);
					}
				}
			}
			return result;
		};
	}());
}



// Production steps of ECMA-262, Edition 5, 15.4.4.14
// Reference: http://es5.github.io/#x15.4.4.14
// https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(searchElement, fromIndex) {

    var k;

    // 1. Let O be the result of calling ToObject passing
    //    the this value as the argument.
    if (this == null) {
      throw new TypeError('"this" is null or not defined');
    }

    var O = Object(this);

    // 2. Let lenValue be the result of calling the Get
    //    internal method of O with the argument "length".
    // 3. Let len be ToUint32(lenValue).
    var len = O.length >>> 0;

    // 4. If len is 0, return -1.
    if (len === 0) {
      return -1;
    }

    // 5. If argument fromIndex was passed let n be
    //    ToInteger(fromIndex); else let n be 0.
    var n = +fromIndex || 0;

    if (Math.abs(n) === Infinity) {
      n = 0;
    }

    // 6. If n >= len, return -1.
    if (n >= len) {
      return -1;
    }

    // 7. If n >= 0, then Let k be n.
    // 8. Else, n<0, Let k be len - abs(n).
    //    If k is less than 0, then let k be 0.
    k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

    // 9. Repeat, while k < len
    while (k < len) {
      var kValue;
      // a. Let Pk be ToString(k).
      //   This is implicit for LHS operands of the in operator
      // b. Let kPresent be the result of calling the
      //    HasProperty internal method of O with argument Pk.
      //   This step can be combined with c
      // c. If kPresent is true, then
      //    i.  Let elementK be the result of calling the Get
      //        internal method of O with the argument ToString(k).
      //   ii.  Let same be the result of applying the
      //        Strict Equality Comparison Algorithm to
      //        searchElement and elementK.
      //  iii.  If same is true, return k.
      if (k in O && O[k] === searchElement) {
        return k;
      }
      k++;
    }
    return -1;
  };
}