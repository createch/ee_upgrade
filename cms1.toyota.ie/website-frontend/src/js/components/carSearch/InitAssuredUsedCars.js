/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function InitAssuredUsedCars() {

	//check the search from is pressent in the page
	if ($("#car_filter").length === 0)
		return;


	// settings
	var pageSize = 10;

	// initialization
	var searchCarsList = new Carslist();
	var resultsCarsList = new Carslist();
	var query = new CarsQuery();
	var searchView = new CarsSearchView();
	var resultsView = new CarsResulstView();

	//setters
	query.setPageSize(pageSize);
	query.setresultView(resultsView);
	resultsView.setPageSize(pageSize);

	searchView.setCarsList(searchCarsList);
	resultsView.setCarsList(resultsCarsList);

	var total = 0;




	//first query: reset carlist,  render search menu, & render results.
	query.handleinitialQuery(function (data) {


		// the firs query has all cars to generate the menu
		searchCarsList.parseCars(data.response.docs);
		searchView.render();

		// bindings
		searchView.bindEvents();
		searchView.bindOutputEvents();
		resultsView.bindEvents();
		resultsView.bindOutputEvents();

		//execute query
		var showing = Math.min(resultsView.getShowingNumber(),10);
		var sorted = ($("#carSort").val());
		query.customQuery(0, showing, sorted, function (data) {
			var cars = data.response.docs;
			total = data.response.numFound;
			resultsView.scrollTop();
			resultsCarsList.reset();
			resultsCarsList.parseCars(cars);
			var prevPageSize = resultsView.pageSize;
			resultsView.setPageSize(showing);
			resultsView.render();
			resultsView.renderTotal(total);
			if (sorted)
				resultsView.setDirectionField(query.getDirectionField());
			resultsView.setPageSize(prevPageSize);
		});
	});




	var reRenderResults = function (cars, total) {
		resultsCarsList.reset();
		resultsCarsList.parseCars(cars);
		resultsView.render();
		resultsView.renderTotal(total);
	};

	// called on search: reset carlist, reset resultview & render results.
	query.handleFullQuery(function (data) {
		total = data.response.numFound;
		resultsView.scrollTop();
		reRenderResults(data.response.docs, total);
	});



	// called on reorder: reset carlist, reset resultview & render results.
	query.handleSortQuery(function (data) {
		total = data.response.numFound;
		reRenderResults(data.response.docs, total);
		resultsView.setDirectionField(query.getDirectionField());
	});

	// called on  show more: dont reset anything, instead append to carList & append to results.
	query.handleNextPageQuery(function (data) {
		var total = data.response.numFound;
		resultsCarsList.appendCars(data.response.docs);
		resultsView.renderNextPage();
		resultsView.renderTotal(total);
	});

	query.initialQuery();

}



