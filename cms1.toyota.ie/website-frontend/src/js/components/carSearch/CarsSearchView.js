/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Search Form View
 * @returns {CarsSearchView}
 */
var CarsSearchView = function () {
    var self = this;
    // ################# initialization #################
    // dealer id
    this.delaerID = $("#searchComponent").data("id");
    // left form
    this.$container = $("#car_filter");
    this.$make = this.$container.find("select[name='Make']");
    this.$model = this.$container.find("select[name='Model']");
    this.$country = this.$container.find("select[name='Country']");
    this.$priceMin = this.$container.find("select[name='Price-min']");
    this.$priceMax = this.$container.find("select[name='Price-max']");
    this.$body = this.$container.find("select[name='BodyType']");
    this.$doors = this.$container.find("select[name='Doors']");
    this.$yearFrom = this.$container.find("select[name='Year-from']");
    this.$yearTo = this.$container.find("select[name='Year-to']");
    this.$mileageMin = this.$container.find("select[name='Mileage-min']");
    this.$mileageMax = this.$container.find("select[name='Mileage-max']");
    this.$transmission = this.$container.find(".transmission .checkboxes");
    this.$fuel = this.$container.find(".fueltype .checkboxes");
    this.$colours = this.$container.find(".colours.checkboxes");
    this.$nct = this.$container.find(".nct .checkboxes");
    //buttons
    this.$search = this.$container.find(".perform-search");
    this.$clear = this.$container.find(".clear-fields");
    this.$clear.hide();
    // sort buttons
    this.$sort = $("#carSort");
    this.$sortdir = $("#sortDirection");

    // model
    this.carList = null;
    // view setting
    this.priceInterval = 10000;
    this.mileageInterval = 25000;
    this.yearInterval = 1;


    //last state
    this.lastState = restoreFromCookie();
    //removes county from DEFAULT_QUERY if the form has no county field
    if (!this.$country.length) {
        delete DEFAULT_QUERY.country;
    }

    this.$country;
    // ################# setters #################
    this.setCarsList = function (carList) {
        self.carList = carList;
    };

    // ################# render methods #################


    this.ClearAllFields = function () {
        self.lastState = DEFAULT_QUERY;
        var checkboxes = this.$container.find(".checkbox:not(.unique)");
        checkboxes.find("input").prop("checked", "checked");
        checkboxes.click();
        var uniqueActiveCheckboxes = this.$container.find(".checkbox.unique.active");
        uniqueActiveCheckboxes.click();
        self.render();
        self.$search.click();
        self.$clear.hide();
    };

    this.render = function () {
        if (!self.carList)
            throw new UserException("carList not initialized, use setCarlis() first;");
        self.renderMakeSelect();
        self.renderModelSelect();
        self.renderPriceSelects();
        self.renderCountrySelect();
        self.renderBodySelect();
        self.renderDoorsSelect();
        self.renderYearSelects();
        self.renderTransmission();
        self.renderFuelType();
        self.renderNCT();
        self.renderMileageSelects();
        self.renderColourCheckBoxes();
        self.renderSort();
        var last = JSON.stringify(self.lastState);
        var def = JSON.stringify(DEFAULT_QUERY);
        if (last !== def)
            self.$clear.show();
    };

    this.renderMakeSelect = function (event) {
        self.$make.empty();
        self.$make.append('<option value="Any Make" selected>Any Make</option>');
        var lastEstate = self.lastState.make;
        for (var i = 0, max = self.carList.maker.length; i < max; i++) {
            var maker = self.carList.maker[i];
            var selected = (lastEstate === maker) ? "selected" : "";
            self.$make.append('<option value="' + maker + '" ' + selected + '>' + maker + '</option>');
        }
        self.$make.find("option:selected").change();
    };


    this.renderModelSelect = function () {
        self.$model.empty();
        self.$model.append('<option value="Any Model" selected>Any Model</option>');

        var $selected = self.$make.find("option:selected");
        var maker = ($selected.length) ? $selected.text() : "Any Make";
        var models = self.carList.makerModel[maker];
        var lastEstate = self.lastState.model;
        for (var i = 0, max = models.length; i < max; i++) {
            var model = self.carList.makerModel[maker][i];
            var selected = (lastEstate === model) ? "selected" : "";
            self.$model.append('<option value="' + model + '" ' + selected + '>' + model + '</option>');
        }
        self.$model.find("option:selected").change();
    };

    this.renderPriceSelects = function () {
        self.$priceMin.empty();
        self.$priceMin.append('<option value="Min" selected>Min</option>');
        self.$priceMax.empty();
        self.$priceMax.append('<option value="Max" selected>Max</option>');

        var values = getMinMaxArray(self.carList.prices, 20);
        var lastMinEstate = self.lastState.priceMin;
        var lastMaxEstate = self.lastState.priceMax;
        for (var i = 0, max = values.length; i < max; i++) {
            var v = (i < (max / 2))
                ? Math.floor(values[i] / 1000) * 1000
                : Math.ceil(values[i] / 1000) * 1000;
            var vf = formatNumber(v);
            var st = "" + v;
            var minSelected = (lastMinEstate === st) ? "selected" : "";
            var maxSelected = (lastMaxEstate === st) ? "selected" : "";
            self.$priceMin.append('<option value="' + v + '" ' + minSelected + '>' + vf + '</option>');
            self.$priceMax.append('<option value="' + v + '" ' + maxSelected + '>' + vf + '</option>');
        }
        self.$priceMin.find("option:selected").change();
        self.$priceMax.find("option:selected").change();
    };

    this.renderCountrySelect = function () {
        var lastEstate = self.lastState.country;
        var $lastCounty = self.$country.find("option[value='" + lastEstate + "']");
        $lastCounty.prop('selected', 'selected');
        $lastCounty.change();
    };

    this.renderBodySelect = function () {
        self.$body.empty();
        self.$body.append('<option value="All body types" selected>All body types</option>');
        var lastEstate = self.lastState.body;
        for (var i = 0, max = self.carList.bodyTypes.length; i < max; i++) {
            var body = self.carList.bodyTypes[i];
            var selected = (lastEstate === body) ? "selected" : "";
            self.$body.append('<option value="' + body + '" ' + selected + '>' + body + '</option>');
        }
        self.$body.find("option:selected").change();
    };


    this.renderDoorsSelect = function () {
        self.$doors.empty();
        self.$doors.append('<option value="All doors"">All doors</option>');
        self.$doors.append('<option value="' + 2 + '"">' + 2 + '</option>');
        self.$doors.append('<option value="' + 3 + '"">' + 3 + '</option>');
        self.$doors.append('<option value="' + 4 + '"">' + 4 + '</option>');
        self.$doors.append('<option value="' + 5 + '"">' + 5 + '</option>');

        var lastEstate = self.lastState.doors;
        var $lastDoor = self.$doors.find("option[value='" + lastEstate + "']");
        $lastDoor.prop('selected', 'selected');
        $lastDoor.change();
    };

    this.renderYearSelects = function () {
        self.$yearFrom.empty();
        self.$yearFrom.append('<option value="From" selected>From</option>');
        self.$yearTo.empty();
        self.$yearTo.append('<option value="To" selected>To</option>');

        //var values = getMinMaxArray(self.carList.years, 10);
        var values = self.carList.years;
        var lastMinEstate = self.lastState.yearFrom;
        var lastMaxEstate = self.lastState.yearTo;
        for (var i = 0, max = values.length; i < max; i++) {
            var v = values[i];
            if (!v)
                continue;
            var st = "" + v;
            var minSelected = (lastMinEstate === st) ? "selected" : "";
            var maxSelected = (lastMaxEstate === st) ? "selected" : "";
            self.$yearFrom.append('<option value="' + v + '" ' + minSelected + '>' + v + '</option>');
            self.$yearTo.append('<option value="' + v + '" ' + maxSelected + '>' + v + '</option>');
        }
        self.$yearFrom.find("option:selected").change();
        self.$yearTo.find("option:selected").change();
    };


    this.renderMileageSelects = function () {
        self.$mileageMin.empty();
        self.$mileageMin.append('<option value="Min" selected>Min</option>');
        self.$mileageMax.empty();
        self.$mileageMax.append('<option value="Max" selected>Max</option>');

        var values = getMinMaxArray(self.carList.mileages, 20);
        var lastMinEstate = self.lastState.mileageMin;
        var lastMaxEstate = self.lastState.mileageMax;
        for (var i = 0, max = values.length; i < max; i++) {
            var v = (i < (max / 2))
                ? Math.floor(values[i] / 1000) * 1000
                : Math.ceil(values[i] / 1000) * 1000;
            var vf = formatNumber(v);
            var st = "" + v;
            var minSelected = (lastMinEstate === st) ? "selected" : "";
            var maxSelected = (lastMaxEstate === st) ? "selected" : "";
            self.$mileageMin.append('<option value="' + v + '" ' + minSelected + '>' + vf + '</option>');
            self.$mileageMax.append('<option value="' + v + '" ' + maxSelected + '>' + vf + '</option>');
        }
        self.$mileageMin.find("option:selected").change();
        self.$mileageMax.find("option:selected").change();
    };

    this.renderColourCheckBoxes = function () {
        self.$colours.empty();
        for (var i = 0, max = self.carList.colours.length; i < max; i++) {
            var colour = self.carList.colours[i];
            self.$colours.append(
                '<div class="checkbox colour">' +
                '<div class="the-checkbox">' +
                '<div class="colour-block" style="background: ' + colour.toLowerCase() + ';"></div>' +
                '</div>' +
                '<input type="checkbox" name="' + colour + '" id="' + colour + '" value="' + colour + '">' +
                '<label for="' + colour + '">' + colour.toUpperCase() + '</label>' +
                '</div>'
            );
        }
        self.$colours.append(
            '<div class="checkbox colour">' +
            '<div class="the-checkbox">' +
            '<div class="colour-block" style="background:white;"></div>' +
            '</div>' +
            '<input type="checkbox" name="OTHER" id="OTHER" value="*">' +
            '<label for="' + this + '">OTHER</label>' +
            '</div>'
        );
        FormElements.bindCheckboxes(self.$colours.find(".checkbox"));

        var checkedList = self.lastState.colours.split(":");
        for (var i = 0, max = checkedList.length; i < max; i++) {
            var color = checkedList[i];
            if (!color)
                continue;
            var $input = self.$colours.find("input[value='" + color + "']");
            //not mark inputs as checked id state is not cheked it is schanged by formelement automatically
            //$input.prop("checked","checked");
            var parent = $input.parents(".checkbox");
            parent.click();
        }
    };


    this.renderTransmission = function () {
        var checkedList = self.lastState.transmission.split(":");
        for (var i = 0, max = checkedList.length; i < max; i++) {
            var last = checkedList[i];
            if (!last)
                continue;
            var $input = self.$transmission.find("input[value='" + last + "']");
            //not mark inputs as checked id state is not cheked it is schanged by formelement automatically
            //$input.prop("checked","checked");
            var parent = $input.parents(".checkbox");
            parent.click();
        }
    };

    this.renderFuelType = function () {
        var checkedList = self.lastState.fuel.split(":");
        for (var i = 0, max = checkedList.length; i < max; i++) {
            var last = checkedList[i];
            if (!last)
                continue;
            var $input = self.$fuel.find("input[value='" + last + "']");
            //not mark inputs as checked id state is not cheked it is schanged by formelement automatically
            //$input.prop("checked","checked");
            var parent = $input.parents(".checkbox");
            parent.click();
        }
    };

    this.renderNCT = function () {
        var checkedList = self.lastState.nct.split(":");
        for (var i = 0, max = checkedList.length; i < max; i++) {
            var last = checkedList[i];
            if (!last)
                continue;
            var $input = self.$nct.find("input[value='" + last + "']");
            //not mark inputs as checked id state is not cheked it is schanged by formelement automatically
            //$input.prop("checked","checked");
            var parent = $input.parents(".checkbox");
            parent.click();
        }
    };

    this.renderSort = function () {
        var lastEstatecarSort = self.lastState.carSort;
        if (lastEstatecarSort) {
            this.$sort.val(lastEstatecarSort);
            this.$sort.change();
            //console.log(this.$sort.val());
        }
        var lastEstatesortDirection = self.lastState.sortDirection;
        if (lastEstatesortDirection) {
            this.$sortdir.find("> i").attr("class", lastEstatesortDirection);
        }
        if (!self.lastState.showList) {
            $(".show-grid").click();
        }
    };


    this.resetMax = function ($minSelect, $maxSelect, strictLess) {
        var min = parseInt($minSelect.val());
        var options = $maxSelect.find("option");
        var $default = options.eq(0);
        if (isNaN(min)) {
            //default is selected
            min = -99999;
        }
        for (var i = 0, m = options.length; i < m; i++) {
            var $op = options.eq(i);
            var val = $op.val();
            var max = parseInt(val);
            var less = (strictLess) ? max < min : max <= min;
            if (!isNaN(max) && less)
                $op.prop("disabled", true);
            else
                $op.prop("disabled", false);
        }
        $maxSelect.val($default.val()).change();
    };


    //triggers a select automatically
    this.setSelect = function ($elem, val) {

        var target = $elem.find('option[value="' + val + '"]');
        if (target.length) {
            var selected = $elem.find("option:selected");
            selected.prop("selected", false);
            target.prop('selected', 'selected');
            $elem.change();
        }
    };

    this.setCheckboxes = function ($elem, val) {

        var target = $elem.find('option[value="' + val + '"]');
        if (target.length) {
            var selected = $elem.find("option:selected");
            selected.prop("selected", false);
            target.prop('selected', 'selected');
            $elem.change();
        }
    };

    this.showClear = function () {
        this.$clear.show();
    };

    // ###################### store & restore state from cookie ############################

    this.saveToCookie = function () {
        //selects
        var state = {};
        state.make = this.$make.find("option:selected").val();
        state.model = this.$model.find("option:selected").val();
        state.country = this.$country.find("option:selected").val();
        state.priceMin = this.$priceMin.find("option:selected").val();
        state.priceMax = this.$priceMax.find("option:selected").val();
        state.body = this.$body.find("option:selected").val();
        state.doors = this.$doors.find("option:selected").val();
        state.yearFrom = this.$yearFrom.find("option:selected").val();
        state.yearTo = this.$yearTo.find("option:selected").val();
        state.mileageMin = this.$mileageMin.find("option:selected").val();
        state.mileageMax = this.$mileageMax.find("option:selected").val();

        //checkboxes
        state.transmission = this.$transmission.find(".active input[type=checkbox]");
        state.fuel = this.$fuel.find(".active input[type=checkbox]");
        state.colours = this.$colours.find(".active input[type=checkbox]");
        state.nct = this.$nct.find(".active input[type=checkbox]");

        state.transmission = checkboxValues(state.transmission);
        state.fuel = checkboxValues(state.fuel);
        state.colours = checkboxValues(state.colours);
        state.nct = checkboxValues(state.nct);

        //sort buttons
        state.carSort = this.$sort.find("option:selected").val();
        state.sortDirection = this.$sortdir.find("> i").attr("class");

        // list or grid view
        state.showList = $(".show-list").hasClass("active");
        document.cookie = "serach_auc" + "=" + JSON.stringify(state);
    };


    function restoreFromCookie() {
        var state = getCookie("serach_auc");
        if (!state)
            state = DEFAULT_QUERY;
        //console.log(state);
        var fromUrl = parseHashParams();
        var result = mergeExistin(state, fromUrl);
        return result;
    }


    function mergeExistin(target, varArgs) { // .length of function is 2
        if (target == null) { // TypeError if undefined or null
            throw new TypeError('Cannot convert undefined or null to object');
        }
        var to = Object(target);

        for (var key in to) {
            if (to.hasOwnProperty(key) && typeof varArgs[key] != 'undefined') {
                to[key] = varArgs[key];
            }
        }

        if (typeof varArgs["make"] != 'undefined') {
            //removes a cookie from previous search;
            document.cookie = "search_auc_showing=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        }
        return to;
    };


    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        var object = null;
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1);
            if (c.indexOf(name) == 0)
                object = c.substring(name.length, c.length);
        }
        if (object)
            object = JSON.parse(object);
        return object;
    }


    function removeCookie() {
        document.cookie = "serach_auc=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
    }

    function checkboxValues($cheboxes) {
        var list = "";
        $cheboxes.each(function (index) {
            var sep = (index) ? ":" : "";
            list += sep + $(this).val();
        });
        return list;
    }


    // ################# Event Input Output #################

    // binds dom events to internal actions
    this.bindEvents = function () {
        self.$make.change(function () {
            self.renderModelSelect();
        });

        self.$priceMin.change(function () {
            self.resetMax(self.$priceMin, self.$priceMax);
        });

        self.$mileageMin.change(function () {
            self.resetMax(self.$mileageMin, self.$mileageMax);
        });

        self.$yearFrom.change(function () {
            self.resetMax(self.$yearFrom, self.$yearTo, true);
        });


        self.$clear.click(function () {
            self.ClearAllFields();
            self.lastState = DEFAULT_QUERY;
            removeCookie();
        });

        self.$container.change(function () {
            self.$clear.show();
        });


        //saves cookie before leave page
        $(window).on('beforeunload', function () {
            self.saveToCookie();
        });

    };

    // binds dom events to internal actions that generates a custom event captured by the model
    this.bindOutputEvents = function () {
        //click SearchButton
        self.$search.click(function (e) {
            e.preventDefault();
            removeHash();
            self.$search.trigger("SearchButton");
        });
    };

    //bind events

};

/**
 * Used to generate min and max selects
 * Returns on araay of aprox steps length with the values
 * in ascending order.
 * @param {type} array
 * @param {type} steps
 * @returns {array} return the array with the elements in the steps position.
 */
function getMinMaxArray(array, steps) {
    var step = Math.floor(array.length / steps);
    if (step === 0)
        step = 1;

    var result = [];


    // add elemet every step position
    var i = 0;
    var val = 0;
    for (var max = array.length; i < max; i += step) {
        var val = array[i];
        var repeated = result.indexOf(val);
        if (repeated === -1)
            result.push(val);
    }

    // ad the last elemet if not pressent
    if (val < array[array.length - 1]) {
        var val = array[array.length - 1];
        var repeated = result.indexOf(val);
        if (repeated === -1)
            result.push(val);
    }
    return result;
}


/**
 * returns an object wiht all the parameters of the search query string
 */
function parseHashParams() {
    var urlParams;
    var match,
        pl = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) {
            return decodeURIComponent(s.replace(pl, " "));
        },
        query = window.location.hash.substring(1);

    urlParams = {};
    while (match = search.exec(query))
        urlParams[decode(match[1])] = decode(match[2]);
    return urlParams;
}


function removeHash() {
    var scrollV;
    var scrollH;
    var loc = window.location;
    if ("pushState" in history)
        history.pushState("", document.title, loc.pathname + loc.search);
    else {
        // Prevent scrolling by storing the page's current scroll offset
        scrollV = document.body.scrollTop;
        scrollH = document.body.scrollLeft;

        loc.hash = "";

        // Restore the scroll offset, should be flicker free
        document.body.scrollTop = scrollV;
        document.body.scrollLeft = scrollH;
    }
}



