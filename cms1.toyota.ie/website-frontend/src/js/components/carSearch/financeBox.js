/**
 * Created by marlon.jerez on 14/03/2017.
 */

var initFinanceBox = function () {

    $finaceBox = $(".usedcFinanceBox");
    if ($finaceBox.length == 0)
        return;

    $finaceBox.find(".f_info").ReadMore({
        "replacementTextTranslationKey": " ",
        "openContentWidth":"280"
    });



    var $user_input = $('#used_f_car_deposit');
    var $car_price = $("#used_f_car_price");
    var $terms = $(".terms_table , .terms_title");
    var $financeAmount = $("#finance_amount");
    var $terms_wrapepr = $(".temrs_wrapper");

    var cleave = new Cleave($user_input, {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand',
        numeralPositiveOnly:true
    });

    var min_deposit = parseInt($user_input.data("min_deposit"),10);
    var max_deposit = parseInt($user_input.data("max_deposit"),10);
    var total = parseInt($car_price.data("price"),10);
    var base_url = $finaceBox.data("base-ulr");
    var DealerId = $finaceBox.data("dealer-id");
    var CarId = $finaceBox.data("car-id");

    $(".calculate_finance").click(function(e){
        e.preventDefault();
        var deposit = cleave.getRawValue();
        if(deposit < min_deposit){
            cleave.setRawValue(min_deposit);
            deposit = min_deposit;
        }

        if(deposit > max_deposit){
            cleave.setRawValue(max_deposit);
            deposit = max_deposit;
        }

        calcTerms(deposit);
        refactor_deposit_url(deposit);
    });



    function refactor_deposit_url(deposit) {

        var dealer_url =  "/ajax/forms?carID="+CarId+"&dealer="+DealerId+"&deposit="+deposit;
        var toyota_url = "//www.toyota.ie/used-cars/index.json#/iframe/";
        toyota_url += encodeURIComponent("https://assuredusedcars.toyota.ie/forms?form=contactus&carID="+CarId+"&dealer="+DealerId+"&deposit="+deposit);
        $("a.refactor_deposit").attr("href",dealer_url);
        $("a.refactor_deposit_toyota").attr("href",toyota_url);
    }



    //calcTerms(min_deposit);


    function calcTerms(deposit){
        $terms_wrapepr.removeClass("active");
        calcRow($("#m24"),deposit,24);
        calcRow($("#m36"),deposit,36);
        calcRow($("#m48"),deposit,48);
        calcRow($("#m60"),deposit,60);
        setTimeout(function () {
            //minimum animation time
            calcDone()
        },400)
    }

    var calcFinished = 0;
    var financeAmount  = 0;
    function calcDone(){
        calcFinished++;
        if(calcFinished >= 5){
            $financeAmount.text("€"+formatNumber(financeAmount));
            $terms_wrapepr.addClass("active");
            calcFinished = 0;
            financeAmount  = 0;
        }
    }

    function calcRow($row,deposit,months){
        $.ajax({
            dataType: 'json',
            async: true,
            url: base_url,
            data:{
                action:"getQuoteData",
                model_id:"USED-CAR",
                format:"feed",
                plan_type:"HP",
                road_price:total,
                terms_in_months:months,
                deposit:deposit
            },
            success: function(data){
                var montly = data["Monthly Instalments"];
                var weekly = data["Weekly Amount"];
                financeAmount  = data["Finance amount"];
                $row.find(".t_m_value").text("€"+formatNumber(montly));
                $row.find(".t_w_value").text("€"+formatNumber(weekly));
                calcDone();
            },
            error: function(){
                calcDone();
            },
            cache: true
        }).error(calcDone);
    }
};




/**
 * returns an object wiht all the parameters of the search query string
 */
function parseHashParams() {
    var urlParams;
    var match,
        pl = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) {
            return decodeURIComponent(s.replace(pl, " "));
        },
        query = window.location.hash.substring(1);

    urlParams = {};
    while (match = search.exec(query))
        urlParams[decode(match[1])] = decode(match[2]);
    return urlParams;
}




