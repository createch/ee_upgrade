function InitSearchBox() {

    $searbox = $("#usedcSearchBox");

    if ($searbox.length == 0)
        return;

    var query = new CarsQuery();
    var searchCarsList = new Carslist();
    var SearchBoxView =  new HomeSearchView($searbox);

    SearchBoxView.setCarsList(searchCarsList);

    query.handleinitialQuery(function (data) {

        // the firs query has all cars to generate the menu
        searchCarsList.parseCars(data.response.docs);
        SearchBoxView.render();

        // bindings
        SearchBoxView.bindEvents();
    });

    query.initialQuery();
}



var  HomeSearchView = function ($container) {
    var self = this;
    // ################# initialization #################
    this.$container = $("#usedcSearchBox");
    // dealer id
    this.delaerID = $container.data("dealer-id");
    // left form

    this.$make = this.$container.find("select[name='used_s_make']");
    this.$model = this.$container.find("select[name='used_s_model']");
    this.$year = this.$container.find("input[name='used_s_name']");

    this.$submit = this.$container.find(".used_search_btn");

    this.USEDCAR_URL = this.$submit.attr("href");






    // ################# setters #################
    this.setCarsList = function (carList) {
        self.carList = carList;
    };

    this.ClearAllFields = function () {
        self.lastState = DEFAULT_QUERY;
        var checkboxes = this.$container.find(".checkbox:not(.unique)");
        checkboxes.find("input").prop("checked", "checked");
        checkboxes.click();
        var uniqueActiveCheckboxes = this.$container.find(".checkbox.unique.active");
        uniqueActiveCheckboxes.click();
        self.render();
        self.$search.click();
        self.$clear.hide();
    };

    this.render = function () {
        if (!self.carList)
            throw new UserException("carList not initialized, use setCarlis() first;");
        self.renderMakeSelect();
        self.renderModelSelect();
        self.setSubmitUrl();
    };

    this.renderMakeSelect = function (event) {
        self.$make.empty();
        self.$make.append('<option value="Any Make">Any Car Make</option>');
        for (var i = 0, max = self.carList.maker.length; i < max; i++) {
            var maker = self.carList.maker[i];
            var selected = ( 'toyota' === maker.toLowerCase()) ? "selected" : "";
            self.$make.append('<option value="' + maker + '" ' + selected + '>' + maker + '</option>');
        }
        self.$make.find("option:selected").change();
    };


    this.renderModelSelect = function () {
        self.$model.empty();
        self.$model.append('<option value="Any Model" selected>Any Model</option>');

        var $selected = self.$make.find("option:selected");
        var maker = ($selected.length) ? $selected.text() : "Any Model";
        var models = self.carList.makerModel[maker];
        for (var i = 0, max = models.length; i < max; i++) {
            var model = self.carList.makerModel[maker][i];
            self.$model.append('<option value="' + model + '" >' + model + '</option>');
        }
        self.$model.find("option:selected").change();
    };


    // ################# Event Input Output #################

    this.setSubmitUrl = function () {
        var make = encodeURIComponent(self.$make.val());
        var model = encodeURIComponent(self.$model.val());
        var year = (self.$year.val())?"&yearFrom="+encodeURIComponent(self.$year.val()):"";
        var url = self.USEDCAR_URL+"#make="+make+"&model="+model+year;
        self.$submit.attr("href",url);
    };

    // binds dom events to internal actions
    this.bindEvents = function () {
        self.$make.change(function () {
            self.renderModelSelect();
            self.setSubmitUrl();
        });

        self.$model.change(function () {
            self.setSubmitUrl();
        });

        this.$year.change(function (e) {
            self.setSubmitUrl();
        });
    };


};