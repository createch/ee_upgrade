/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


DEFAULT_QUERY = {  
   "make":"TOYOTA",
   "model":"Any Model",
   "country":"Any County",
   "priceMin":"Min",
   "priceMax":"Max",
   "body":"All body types",
   "doors":"All doors",
   "yearFrom":"From",
   "yearTo":"To",
   "mileageMin":"Min",
   "mileageMax":"Max",
   "transmission":"",
   "fuel":"",
   "colours":"",
   "nct":"",
   "carSort":false,
   "sortDirection":0,
   "showList":true
};

