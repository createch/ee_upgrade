/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var carRender = function (car, index) {
	var year = (car.Year) ? car.Year : "Year-NA";
	var make = (car.Make) ? car.Make : "";
	var model = (car.Model) ? car.Model : "";
	var variant = (car.Variant) ? car.Variant : "";
	var price = (car.Price) ? "&euro;" + car.PriceF : "POA";
	if (make || model)
		model += "<br/>";
	var mileage = (car.Mileage === -1) ? "Mileage-NA" : car.milesF + "M (" + car.MileageF + "Km)";
	var clear =(index%2 === 0)?" clear_left":"";
	
	var carOutput = '<div class="result ' + car.layouth + clear +'" data-date="' + car.Year + '" data-price="' + car.Price + '" data-mileage="' + car.Mileage + '">' + 
			'<div class="image"><a href="/pages/assured-used-car-details/' + car.carID + '"><img src="' + car.thumbnail.replace('http://','//') + '" alt="' + car.carID + '" ' +
			'width="400" height="300"></a></div>' +
			'<div class="content">' +
			'<div class="padding">' +
			'<h5><a href="/pages/assured-used-car-details/' + car.carID + '"><span><span class="reults_model">' + make + ' ' + model +"</span><span class='reults_variant'>"+ variant + '</span></span></a></h5>' +
			'<div class="price mobile_price">' + price + '</div>' +
			'<div class="details cardetails">';

	carOutput += '<span><span class="year ussuredCarValue" style="text-transform:capitalize">' + year + '</span></span>';
	carOutput += '<span><span class="bull">&nbsp;&bull;</span> <span class="mileage ussuredCarValue" style="text-transform:capitalize">' + mileage + '</span></span>';

	if (car.FuelType || car.Colour || car.Transmission)
		carOutput += '<br />';
	if (car.FuelType)
		carOutput += '<span><span class="ussuredCarValue">' + car.FuelType + '</span></span>';
	if (car.Colour)
		carOutput += '<span><span class="bull">&nbsp;&bull;</span> <span class="ussuredCarValue">' + car.Colour + '</span></span>';
	if (car.Transmission)
		carOutput += '<span><span class="bull">&nbsp;&bull;</span> <span class="ussuredCarValue">' + car.Transmission + '</span></span>';
	if (car.Location)
		carOutput += '<br/><span><span class="ussuredCarValue" style="text-transform:capitalize">' + car.Location + '</span></span>';

	
	carOutput += '</div>' +
			'<div class="clear"></div>' +
			'</div>' +
			'<div class="price">' + price + '</div>' +
			'<a href="/pages/assured-used-car-details/' + car.carID + '" class="uiBtn redBtn hasIcon hasIconRight smBtn view-car">' +
			'<span>View car</span>' +
			'<i class="icon icon-chevron-right"></i>' +
			'</a>' +
			'</div>' +
			'<div class="clear"></div>' +
			'</div>';


//	if((index+1)%2 === 0)
//			carOutput += "<div style='font-size:0;clear:both'>&nbsp;</div>";
	return carOutput;
};


var CarsResulstView = function () {
	var self = this;
	// ################# initialization #################
	// DOM elements
	this.$container = $("#loadcars");
	this.$itemsWrapper = this.$container.find("#resultsAsuredUsedCars");
	this.$itemsContainer = this.$container.find(".results");
	this.$loading = this.$container.find("#loading");
	this.$showmore = $("#showMoreCars");
	this.$showing = $("#numvisible");
	this.$total = $("#numtotal");
	this.$sort = $("#carSort");
	this.$sortdir = $("#sortDirection");
	this.isMobile = (!$("html.no-touch").length);
	// model
	this.carsList = null;
	// view setting
	this.pageSize = 10;
	this.currentposition = 0;
	this.total = 0;
	this.sortField = null;
	this.sortAscending = true;

	// ################# setters #################
	this.setCarsList = function (carList) {
		self.carsList = carList;
	};
	this.setPageSize = function (size) {
		self.pageSize = size;
	};
	this.setDirectionField = function (directionfield) {
		this.$itemsWrapper.removeClass();
		this.$itemsWrapper.addClass(directionfield);
	};




	// ################# render methods #################

	// reset and render
	this.render = function () {
		self.$loading.hide();
		self.$itemsWrapper.css({opacity: '0'});
		self.$itemsContainer.empty();
		self.currentposition = 0;
		self.renderNextPage();
		self.renderTotal(self.total);
	};

	// render next x elements
	this.renderNextPage = function () {
		var cars = self.carsList.cars;
		var max = Math.min(self.currentposition + self.pageSize, cars.length);
//		var scrollid = new Date().getTime() + "_car";
//		self.$itemsContainer.append("<div id='" + scrollid + "'></div>");
		for (var i = self.currentposition; i < max; i++) {
			var car = cars[i];
			// layouth fix
			car.layouth = ((self.currentposition % 2) === 1) ? "right" : "";
			car.$elem = $(carRender(car,i));
			self.$itemsContainer.append(car.$elem);
			car.$elem.show();
			car.$elem.css("opacity", 1);
			self.currentposition++;

		}
		self.animateContainer();
//		$.smoothScroll({scrollTarget: '#'+scrollid});
		if (max === 0)
			self.renderEmpty();
	};

	// render the "showing x of total" section
	this.renderTotal = function (total) {
		self.total = total;
		self.showing = self.currentposition;
		self.$showing.text(self.showing);
		self.$total.text(self.total);
		if (self.showing >= self.total)
			self.$showmore.hide();
		else
			self.$showmore.show();
		document.cookie = "search_auc_showing" + "=" + self.showing;
		window.search_auc_showing = self.showing;
	};
	
	
	this.getShowingNumber  = function (){
		
		var last = getCookie("search_auc_showing");
		if(last)
			window.serach_auc_total = parseInt(last);
		else
			window.serach_auc_total = this.pageSize;	
		return window.serach_auc_total;
	}
	
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		var object = null;
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ')
				c = c.substring(1);
			if (c.indexOf(name) == 0)
				object = c.substring(name.length, c.length);
		}
		if (object)
			object = JSON.parse(object);
		return object;
	}

	// rendes a no result message
	this.renderEmpty = function () {
		self.$itemsContainer.empty();
		self.$itemsContainer.html(
				'<h3 class="results heading2 center-text no-models">'
				+ 'No models found.</h3>');
	};

	this.renderLoading = function () {
		self.$itemsContainer.empty();
		self.$loading.show();
	};

	this.animateContainer = function () {
		// add little delay to get the $itemsContainer heigth properly.. otherwise it returns 0
		window.setTimeout(function () {
			var h = self.$itemsContainer.outerHeight() + 15;
			//self.$itemsWrapper.stop().animate({height: h}, 1000, "linear");
			self.$itemsWrapper.height(h);
			self.$itemsWrapper.stop().animate({opacity: '1'}, 500);
		}, 400);
	};

	this.setNewDirection = function (e) {
		var $icon = self.$sortdir.find("i");
		self.sortAscending = $icon.hasClass("icon-chevron-up");
		if (!self.sortAscending) {
			$icon.removeClass("icon-chevron-down");
			$icon.addClass("icon-chevron-up");
			if ($(".lt-ie8").length > 0)
				$icon.html("︽");
		} else {
			$icon.removeClass("icon-chevron-up");
			$icon.addClass("icon-chevron-down");
			if ($(".lt-ie8").length > 0)
				$icon.html("︾");
		}
		// set the new direction after change interface;
		self.sortAscending = !self.sortAscending;
		return self.sortAscending;
	};

	this.scrollTop = function () {
		$.smoothScroll({scrollTarget: ".search_result"});
	};

	// ################# Event Input Output #################

	// binds dom events to internal actions
	this.bindEvents = function () {
		//return top smooth
		$('.return_top').on('click', function (e) {
			e.preventDefault();
			$.smoothScroll({
				scrollTarget: '#car_filter'
			});
		});



		//resize continer on winresize
		$(window).resize(self.animateContainer);

		$(".show-list , .show-grid").click(function () {
			setTimeout(function () {
				self.animateContainer();
			}, 100);
		});
		
//		$("body").on("click",".result a",function(e){
//			e.preventDefault();
//			var $iframe = $("<iframe id='fulliframe'  src='"+$(this).attr("href")+"' />");
//			$("body").append($iframe);
//			$("body").height($iframe.find("body").height());
//			$("body").css("overflow","hidden");
//		});
		
	};

	// binds dom events to internal actions that generates a custom event captured by the model
	this.bindOutputEvents = function () {

		//click SortBy
		self.$sort.on("change", function () {
			self.sortField = self.$sort.val();
			self.renderLoading();
			self.$sort.trigger("SortBy");
		});

		//click ChangeOrder
		self.$sortdir.on("click", function (e) {
			e.preventDefault();
			self.setNewDirection();
			if (!self.$sort.val())
				return;
			self.renderLoading();
			self.$sortdir.trigger("ChangeOrder");
		});

		// click ShowMore = next page
		self.$showmore.click(function (e) {
			e.preventDefault();
			self.$showmore.trigger("ShowMore");
		});
	};




	window.setInterval(function () {
		var h = self.$itemsContainer.outerHeight() + 15;
		self.$itemsWrapper.height(h);
	}, 1000);
};