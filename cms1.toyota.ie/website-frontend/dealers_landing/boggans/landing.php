<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<header class="header-container padding">
	<div class="bg-image-hero bg-image-hero-med">
		<div class="coverBG" style="background-image: url(./images/dealers/boggans.jpg);"></div>
		<div class="hero-landing container_12 container_12_absolute center-text">
			<h1 class="heading1">Boggans</h1>
		</div>
	</div>
</header>
<div class="container_12 content-block">
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/gorey_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Gorey Garage</strong></h3>
		<p class="padding-small">Hugh Boggan Garage, Dublin Road, Gorey,<br/>Co. Wexford</p>
		<p class="padding"><a href="tel:0539420215"><strong>Telephone:</strong> (053) 9420215</a></p>
		<a class="uiBtn redBtn  hasIconRight" href="http://gorey.hughbogganmotors.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/carriglawn_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Carriglawn Garage</strong></h3>
		<p class="padding-small">Boggans of Wexford, Newtown Road,<br/>Co. Wexford</p>
		<p class="padding"><a href="tel:0539143788"><strong>Telephone:</strong> (053) 9143788</a></p>
		<a class="uiBtn redBtn  hasIconRight" href="http://carriglawn.hughbogganmotors.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
</div>


<!-- ################## PAGE END ########################-->
