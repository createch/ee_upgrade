<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<header class="header-container padding">
	<div class="bg-image-hero bg-image-hero-med">
		<div class="coverBG" style="background-image: url(./images/dealers/denismahony.jpg);"></div>
		<div class="hero-landing container_12 container_12_absolute center-text">
			<h1 class="heading1">Denis Mahony</h1>
		</div>
	</div>
</header>
<div class="container_12 content-block">
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/m50_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>M50 Garage</strong></h3>
		<p class="padding-small">Denis Mahony Garage, Exit 5, M50, North Road,<br/>Dublin 11</p>
		<p class="padding">
			<a href="tel:018647500"><strong>Telephone:</strong> (01) 864 7500</a><br/>
			<a href="mailto:toyotanr@denismahony.ie"><strong>Email:</strong> toyotanr@denismahony.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://toyotam50.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/kilbarrack_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Kilbarrack Garage</strong></h3>
		<p class="padding-small">Denis Mahony Garage, Kilbarrack Road, Kilbarrack,<br/>Dublin 5</p>
		<p class="padding">
			<a href="tel:018322701"><strong>Telephone:</strong> (01) 8322701</a><br/>
			<a href="mailto:toyotakb@denismahony.ie"><strong>Email:</strong> toyotakb@denismahony.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://www.toyotakilbarrack.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
</div>



<!-- ################## PAGE END ########################-->

