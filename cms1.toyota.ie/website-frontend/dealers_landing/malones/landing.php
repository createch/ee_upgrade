<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<header class="header-container padding">
	<div class="bg-image-hero bg-image-hero-med">
		<div class="coverBG" style="background-image: url(./images/dealers/malones.jpg);"></div>
		<div class="hero-landing container_12 container_12_absolute center-text">
			<h1 class="heading1">Malones</h1>
		</div>
	</div>
</header>
<div class="container_12 content-block">
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/navan_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Navan Garage</strong></h3>
		<p class="padding-small">Navan Garage, Navan,<br/>Co. Meath</p>
		<p class="padding">
			<a href="tel:0469071717"><strong>Telephone:</strong> (046) 9071717</a><br/>
			<a href="mailto:dave@malones.ie"><strong>Email:</strong> dave@malones.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://navan.malones.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/drogheda_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Drogheda  Garage</strong></h3>
		<p class="padding-small">Drogheda  Garage, North Road, Drogheda,<br/>Co. Louth</p>
		<p class="padding">
			<a href="tel:0419802420"><strong>Telephone:</strong> (041) 9802420</a><br/>
			<a href="mailto:johnd@malones.ie"><strong>Email:</strong> johnd@malones.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="hhttp://drogheda.malones.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
</div>



<!-- ################## PAGE END ########################-->

