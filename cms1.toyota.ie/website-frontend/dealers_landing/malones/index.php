<?php header('Access-Control-Allow-Origin: *'); ?>

<!DOCTYPE html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Toyota</title>
        <meta name="description" content="">
        <meta name="viewport" content="initial-scale=1.0">
        <!-- favicon -->
        <link rel="icon" type="image/x-icon" href="favicon.ico">
        <!-- CSS files -->
        <link rel="stylesheet" href="css/style.css">
		<style>
			.social-button.social-button-facebook{
				float: right;
				background-image: none;
				background: #3b5998;
				border-radius: 4px;
			}
			.social-button.social-button-twitter{
				float: right;
				background-image: none;
				background: #55acee;
				border-radius: 4px;
			}
			.social-button.social-button-youtube{
				float: right;
				background-image: none;
				background: #cc181e;
				border-radius: 4px;
			}
			.social-button .icon{
				line-height: 1.8em;
			}
		</style>
		<!--[if lt IE 9]>      <link rel="stylesheet" href="css/ie8.css"> <![endif]-->
		<!--[if lt IE 8]>      <link rel="stylesheet" href="css/ie7.css"> <![endif]-->

    </head>
    <body>
		<div id="fb-root"></div>
        <!--[if lt IE 7]>
            <p class="browserwarn">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> </p>
        <![endif]-->

        <!-- ### 1.1 Primary navigation position ### -->
		<!--
    Name: Primary Navigation
    ID: c_n1_001
    URL: http://toyota-stage.amaze.com/html/components/navigation/PrimaryNavigation.html
	
    1.0 Top bar, Search, Breadcrumb
		-->
		<div class="primary-nav" >
			<div class="primary-nav-outer">
				<div class="container_12 clearfix">
					<a href="index.html" class="logo-link">
						<img src="images/logo.png" alt="Toyota" width="94" height="152" />
						<span class="small-logo"></span>
					</a>

					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div id="main_nav"></div>
        <div class="outer-wrapper">

            <div class="master-wrapper">

                <!-- ### 1.2 Header component position ### fx-header class? -->

                <?php include "landing.php"?>

				<!-- ### 1.4 Footer component position ### -->
				<footer >
					<div class="footer-container isoForceHardwardCalc">
						<div class="footer">
							<div class="container_12 force">
								<div class="row">
									<div class="grid_9">
										<p class="copyright">Copyright &copy; Toyota Ireland</p>
									</div>
									<div class="grid_3">
										<ul class="align-right">
											<li class="link"><a class="hasIcon return_menu" href="#main_nav" ><span>Back to top</span><i class="icon icon-chevron-up"></i></a></li>
										</ul>
										<div class="clear"></div>
									</div>
									<div class="clear"></div>
								</div>
								<div class="row">
									<div class="grid_9 ">
										&nbsp;
									</div>
									<div class="grid_3 social-buttons">
										<a class="social-button social-button-facebook" href="https://www.facebook.com/toyotaireland" target="_blank">
											<i class="icon icon-facebook"></i>
										</a>
										<a class="social-button social-button-twitter" href="https://twitter.com/toyotaireland" target="_blank">
											<i class="icon icon-twitter"></i>
										</a>
										<a class="social-button social-button-youtube" href="https://www.youtube.com/user/ToyotaIreland" target="_blank">
											<i class="icon icon-youtube"></i>
										</a>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</div>
				</footer>

			</div>

        </div>

	</body>
</html>
