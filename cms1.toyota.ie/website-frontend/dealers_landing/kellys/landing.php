<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<header class="header-container padding">
	<div class="bg-image-hero bg-image-hero-med">
		<div class="coverBG" style="background-image: url(./images/dealers/kellys.jpg);"></div>
		<div class="hero-landing container_12 container_12_absolute center-text">
			<h1 class="heading1">Kellys</h1>
		</div>
	</div>
</header>
<div class="container_12 content-block">
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/letterkenny_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Letterkenny Garage</strong></h3>
		<p class="padding-small">Kellys Garage, Port Rd, Letterkenny,<br/>Co. Donegal</p>
		<p class="padding">
			<a href="tel:0749121385"><strong>Telephone:</strong> (074) 9121385</a><br/>
			<a href="mailto:sales@kellystoyota.com"><strong>Email:</strong> sales@kellystoyota.com</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://letterkenny.kellystoyota.com/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/mountcharles_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Mountcharles Garage</strong></h3>
		<p class="padding-small">Mountcharles Garage, Mountcharles,<br/>Co. Donegal</p>
		<p class="padding">
			<a href="tel:0749735011"><strong>Telephone:</strong> (074) 9735011</a><br/>
			<a href="mailto:salesmc@kellystoyota.com"><strong>Email:</strong> salesmc@kellystoyota.com</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://mountcharles.kellystoyota.com/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
</div>



<!-- ################## PAGE END ########################-->

