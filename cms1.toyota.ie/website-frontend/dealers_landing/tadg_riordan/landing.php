<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<header class="header-container padding">
	<div class="bg-image-hero bg-image-hero-med">
		<div class="coverBG" style="background-image: url(./images/dealers/tagriondiran.jpg);"></div>
		<div class="hero-landing container_12 container_12_absolute center-text">
			<h1 class="heading1">Tadg Riordan</h1>
		</div>
	</div>
</header>
<div class="container_12 content-block">
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/ashbourne_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Ashbourne Garage</strong></h3>
		<p class="padding-small">Frederick Street, Ashbourne,<br/>Co. Meath</p>
		<p class="padding">
			<a href="tel:018350084"><strong>Telephone:</strong> (01) 8350084</a><br/>
			<a href="mailto:sales@trmash.ie"><strong>Email:</strong> sales@trmash.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://www.trmash.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/tallaght_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Tallaght  Garage</strong></h3>
		<p class="padding-small">Tallaght Bypass, Tallaght,<br/>Dublin 24</p>
		<p class="padding">
			<a href="tel:014517447"><strong>Telephone:</strong> (01) 4517447</a><br/>
			<a href="mailto:sales@trmtallaght.ie"><strong>Email:</strong> sales@trmtallaght.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://www.trmtallaght.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
</div>

