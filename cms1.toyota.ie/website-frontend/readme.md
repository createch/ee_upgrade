## FrameWerk
Project Starter Template (facebook app & backbone). This project includes a series of `grunt tasks` that helps to improve the development workflow.
***

## Platform Setup
  - [AMMPS](http://www.ampps.com/) or [MAMP](http://www.mamp.info/en/index.html) ----> Apache + PHP
  - [Git](http://git-scm.com/book/en/Getting-Started-Installing-Git) ------------------------> Version Control
  - [Sourcetree](http://www.sourcetreeapp.com/) --------------> Git User Interface
  - [Node.js](http://nodejs.org/) -------------------> Javascript Enviroment
  - [Grunt](http://gruntjs.com/) --------------------> Javascript Task Runner "Workflow Automation"
  - [Bower](http://bower.io/) --------------------> Download Framewerk & Dependency Manager for framewerk.
  - [SASS](http://sass-lang.com/) --------------------> Download and install SASS compiler.

`Please read carefully the documentation of each componnet, epecially GRUNT and BOWER.`
***


## Installation Notes
1 - Install `AMMPS`, `Git`, `Sourcetree` and `Node.js` as any other software. `Node.js` Comes with the `npm` package manager. 
  
2 -  Install *Grunt* globally uisng the `npm` package manager. npm global installation "-g" requires admin rights & the grunt CLI tool.


```bash
    sudo npm install -g grunt
    sudo npm install -g grunt-cli
```

3 - Install *Bower* globally using `npm` package manager.


```bash
    sudo npm install -g bower
```

4 - Install SASS.


```bash
    sudo gem install sass
```

5 - Install fwsetup script:
- save the setup_framewerk.sh into www/utils/
- create alias on the bash_profile 
```bash    
    vi ~/.bash_profile
```
- add the following 
```bash 
alias fwsetup='sh /Applications/AMPPS/www/utils/setup_framewerk.sh'
alias ampps_home='cd /Applications/AMPPS/www'
```
- press ESC and :wq! to save


***
## Creating a new project based in framewerk using the fwsetup command
You can use the following command to reach the AMPPS home
```bash    
    ampps_home
```
go to the folder of the client you want to create an app for and digit
```bash    
    fwsetup
```
you'll be prompted to enter app name and config details (db name, db user etc) the script will suggest these values based on the app name and client name but you can alway owerwrite them.

once the script completed enter the folder of the project and run
```bash    
    grunt
```
there you'll asked to choose a task to run.

It's suggested to run:

    1 - Create MySql database
    
    2 - Create bitbucket and local git repo
    
    3 - Compile FrontEnd and Wait for changes


## Creating a new project based in framewerk using bower
You can use bower to download de project and give it a new name, or just use the traditional way of download the zip from bitbucket and rename it later. 
Since framewerk is a private repo Bower will ask you for your bitbuket and paswword credentials.

#### Install framewerk in the current directory with the Name: "new_project"
```bash
    bower install  new_project=git://bitbucket.org/createch/framewerk --config.directory=.
```
Be Sure to use `git://bitbucket...` NOT `https://bitbucket...`

#### Sintaxt of the command.
```bash
    bower install  projectName=git://bitbucket.org/user/repo  --config.directory=directory_to_install
```
Bower estores the downloaded projets in cache so to get the lates version is required to clean the cache.
```bash
    bower cache clean
```

`This method ONLY DOWNLOAD THE PROJECT IT DOES NOT CREATE A GIT REPO. If you want to work in an existing project instead create a new one just use git sourcetree.`
***




## Workflow Automatization (Using Grunt)
Grunt is used to automate most commont task in workflow development, to run any of the available tastks just run the command grunt and it will display a lsit of tasks
```bash
    grunt
```
#### Most usefult tasks
  - `project setup` => Configures the project "cleant, name , password , database , facebook_app, etc..."
  - `front end` => Compiles your sass files, concatenates and compress javascript libraries, and optimize images.
  - `front end Watch` => Creates a server that watch changes in your files to execute frontend automatically.
  - `Update google drive` => updates a spreadsheet in gedrive to store the porject's info.
  - `Create a new bitbuket repo`.
  - `Database creation` => creates a new Local Mysql database from the details stored on the config.php file
***





## Folder Structure & the FrontEnd Grunt Task
The FrontEnd task compiles some files of the `src/` folder into the `http_public/` directory, this is hardcoded in the Grunfile.js
To set up a diferrent folder structure is required to edit the Gruntfile.js

  - `src/images` => otimized to => `http_public/images`
  - `src/js/libs` => concatenated => `http_public/js/libs.js`
  - `src/js/*/*` => concatenated => `http_public/js/main.js`
  - `src/sass/style.scss` => compiled to => `http_public/css/style.css`
***


#### Download javascript & other libraries
  - Bower can be used to download any library from internet, to do that edit the `devDependencies` property in bower.json file, and add you own libraries i.e:
```
"devDependencies"  : {
    "jquery" : ">=2.1.0",
    "underscore": ">=1.5.2",
    "backbone" : ">=1.1.0",
    "mustache" : ">=0.8.1",
    "normalize-scss" : ">=2.1.3",
    "pure-scss" : "git://github.com/yui/pure/",
    "fontawesome" : "git://github.com/FortAwesome/Font-Awesome"
  }
```
 - Run `bower install` to download all libraries, they are installed in a folde named bower_components
***

## Concatenating javascript libraries
In the bower.json file edit the `concatJSdevDependencies` property and add an entry as follow `"path/to/folder" : ["file1.js","file2.js"]`. Wich correspomds to the path of the folder and an array with the js files to concatenate in each folder. 
```
"concatJSdevDependencies":{
    "bower_components/jquery/" : ["jquery.js", "jquery-migrate.js"],
    "bower_components/underscore/": ["underscore.js"],
    "bower_components/backbone/" : ["backbone.js"],
    "bower_components/mustache/" : ["mustache.js"]
  }
```
***