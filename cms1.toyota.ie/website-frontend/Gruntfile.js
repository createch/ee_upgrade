/* ##### CONFIG VARS ##### */

var srcPath = 'src/'; // always end the path with an slash '/'
var cmpilPath = 'http_public/'; // always end the path with an slash '/'
var bowerPath = "bower_components/"; // always end the path with an slash '/'
var Atoprefix = true;
require("colors");

/* ##### GRUNT ##### */
module.exports = function (grunt) {


    // require it at the top and pass in the grunt instance
    require('time-grunt')(grunt);

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        projectDefault: null, // initialized on demand
        googleApis: null, // initialized on demand

        // Console prompt tasks
        prompt: {}, // cofigured dinamically.

        // check old broswer incompatible error in js.
        jshint: {
            options: {
                es3: true
            },
            all: ['src/js/**/*.js']
        },
        //concatenates js files
        concat: {
            js_libs: {
                src: getJs_libs(),
                dest: cmpilPath + 'js/libs.js'
            },
            js_main: {
                src: [
                    srcPath + 'js/components/**/*.js'
                ],
                dest: cmpilPath + 'js/main.js'
            }
        },
        //minify previous concatenated javascript
        uglify: {
            options: {
                report: 'min'
            },
            js_libs: {
                src: [cmpilPath + 'js/libs.js'],
                dest: cmpilPath + 'js/libs.min.js'
            },
            js_main: {
                src: [cmpilPath + 'js/main.js'],
                dest: cmpilPath + 'js/main.min.js'
            }
        },
        // compile & minify sass (//TODO pending to configure ans join with less)
        sass: { // Task
            main: {
                options: { // Target options
                    style: 'expanded',
                    update:true
                },
                src: [
                    srcPath + 'sass/style.scss'
                ],
                dest: cmpilPath + 'css/style.css'
            }
        },
        cssnano: {
            options: {
                sourcemap: true
            },
            main_min: {
                src: [
                    cmpilPath + 'css/style.css'
                ],
                dest: cmpilPath + 'css/style.min.css'
            }
        },
        // copy js and css to external folder www/development.toyota.ie/inc
        copy: {
            js: {
                files: [
                    // js
                    {expand: true, flatten: true, src: ['./http_public/js/*'], dest: '../inc/js/', filter: 'isFile'},
                ]
            },
            css: {
                files: [
                    // css
                    {expand: true, flatten: true, src: ['./http_public/css/*'], dest: '../inc/css/', filter: 'isFile'}
                ]
            }
        },
        // watch for file modifications and run respective tasks
        watch: {
            sass: {
                files: [
                    srcPath + 'sass/components/*.scss',
                    srcPath + "sass/base/*.scss",
                    srcPath + "sass/*.scss"
                ],
                tasks: ["sass","cssnano","copy:css","build_number"],
                options: {
                    livereload: true,
                }
            },
            js_libs: {
                files: [srcPath + "js/utils/*.js"],
                tasks: ["concat:js_libs", "uglify:js_libs","copy:js","build_number"],
                options: {
                    livereload: true,
                }
            },
            js_main: {
                files: [
                    srcPath + "js/components/**/*.js"
                ],
                tasks: ["concat:js_main", "uglify:js_main","copy:js","build_number"],
                options: {
                    livereload: true,
                }
            }
        },
        imagemin: { // Task
            incImages: {
                files: [{
                    expand: true, // Enable dynamic expansion
                    cwd: 'src/', // Src matches are relative to this path
                    src: ['../src/images/**/*.{png,jpg,gif}'], // Actual patterns to match
                    dest: 'src/images_opt' // Destination path prefix
                }]
            }
        }
    });

    /* ################## LOAD NPM ################## */

    grunt.loadNpmTasks('grunt-prompt');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-cssnano');

    /* ################## TASKS ################## */

    grunt.registerTask('frontend', "Compile FrontEnd & Wait for changes.", function () {
        grunt.task.run("build_number");
        grunt.task.run("sass");
        grunt.task.run("cssnano");
        grunt.task.run("concat");
        grunt.task.run("uglify");
        grunt.task.run("copy");
        grunt.task.run("watch");
    });


    grunt.registerTask('build_number', 'Generates a build number file', function () {
        var  fs = require('fs');
        fs.writeFileSync('../inc/build-number.txt', Date.now(),'utf8');
    });

    grunt.registerTask('js', ['concat', 'uglify']);

    /* Load tasks in the tasks/ folder */
    grunt.loadTasks('tasks/');


    /* Launch a menu to select the tasks */
    grunt.registerTask('default', "#hide#", function () {
        var done = this.async();
        var choices = [];
        for (var index in grunt.task._tasks) {
            var task = grunt.task._tasks[index];
            if (task.meta.filepath.indexOf("node_modules/") < 0 && task.info.indexOf("#hide#") < 0) {
                choices.push(task.info);
            }
        }
        choices.push();
        var inquirer = require("inquirer");
        inquirer.prompt([{
            type: "list",
            name: "option",
            choices: choices,
            message: "Select What Task do you want to run".blue
        }], function (answer) {
            answer = answer.option;

            for (var index in grunt.task._tasks) {
                var task = grunt.task._tasks[index];
                if (task.info === answer) {
                    grunt.task.run(task.name);
                    done(true);
                }
            }
        });

    });

    /* ################## UTIL FUNCTIONS ################## */


    /* Reads the concatJSdevDependencies in bower.json and 
     * Generates an object to use in the concat task as config.
     * example of the returned object:
     js_libs: {
     src: [
     'bower_components/jquery/jquery.js',
     'bower_components/jquery/jquery-migrate.js',
     'bower_components/mustache/mustache.js',
     'bower_components/underscore/underscore.js',
     'bower_components/backbone/backbone.js',
     ],
     dest: ....,
     },
     */
    function getJs_libs() {
        var bower = grunt.file.readJSON('bower.json');
        var dependenciesList = bower.concatJSdevDependencies;
        var src = [];
        for (var dependency in dependenciesList) {
            var filesArray = dependenciesList[dependency];
            for (var file in filesArray)
                src.push(dependency + filesArray[file]);
        }
        return src;
    }

    /**
     * Returns a safe randomly genetated password.
     */
    function generatePassword() {
        var password = '';
        var availableSymbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0987654321";
        for (var i = 0; i < 12; i++) {
            var symbol = availableSymbols[(Math.floor(Math.random() * availableSymbols.length))];
            password += symbol;
        }
        return password;
    }

    /**
     * Reads config.php file and returns the config stored on it as JSON.
     */
    function getProjectJSON() {
        var phpcode = grunt.file.read(phpConfigPath);
        // reads the php file and split it into the automatic generated 
        // and the non automatic generated sections
        var firstcolom = phpcode.indexOf(';');
        var generatedcode = phpcode.substr(0, firstcolom);
        var normalcode = phpcode.substr(firstcolom);
        // extract the JSON from the automatic generated section
        var split = generatedcode.split("'");
        return JSON.parse(split[1]);
    }

    /**
     * Reads config.php and returns a new config.php
     * as string with the new project config instead the old one.
     * IMPORTANT: the new config.php is only a string and the original file
     * is not modified, to make the changes permanent it must be overwrited.
     */
    function getPHPconfig(project) {
        var phpcode = grunt.file.read(phpConfigPath);
        // reads the php file and extracts the json config section
        var firstcolom = phpcode.indexOf(';');
        var generatedcode = phpcode.substr(0, firstcolom);
        var normalcode = phpcode.substr(firstcolom);
        // generates the new config.php
        var split = generatedcode.split("'");
        return split[0] + "'" + JSON.stringify(project, null, "\t") + "'" + normalcode;
    }

    /**
     * This function recursively parses the projectConf, in orther to fill
     * the 'Questions Array' required by grunt-promp.
     * Grunt-prompt is configurated to store the answers in the grunt.config.project
     * More info at https://github.com/dylang/grunt-prompt
     */
    function createQuestions(projectConf, exclude, questions, path) {
        for (var key in projectConf) {
            // generates the varname = path from the root 
            var varname = (path === "" || typeof path === 'undefined') ? key : path + "." + key;
            // checks if this varname must be excluded from the questions.
            var skip = false;
            for (var i in exclude)
                if (varname == exclude[i])
                    skip = true;
            if (skip)
                continue;
            /* if this element is an Object 'including Arrays' then it make a recursion with the 
             * Object as root, if it is a simple property then its is added to the questions to ask.
             */
            switch (typeof projectConf[key]) {
                case "object":
                    createQuestions(projectConf[key], exclude, questions, varname);
                    break;
                default:
                    questions.push({
                        config: "project." + varname, //this is where prompt stores the answers, in this case grunt.project
                        message: varname,
                        default: getDefaultValue(varname, projectConf[key])
                    });
                    break;
            }
        }
    }
};