module.exports = function(grunt) {

    // Reads tasks/googleApis.json which contains the cliens secret and access token stored.
    var googleApis = grunt.file.readJSON('tasks/googleApis.json');
    var open = require("open");
    var colors = require('colors');
    var https = require('https');
    var SHEET_KEY = googleApis.SHEET_KEY;
    var WORKSHEET_KEY = googleApis.WORKSHEET_KEY;

    /* Gests a valid access token from OAuth 2.0 to acces the google spreadsheet api */
    grunt.registerTask('gdrive',"Create new entry in google-drive.", function() {
        // make the task async.
        var done = this.async();
        oAuth2(function(err, googleApis) {
            if (err) {
                done(err);
                return;
            }else{
                checkIfExists(done);
            }
        });

    });


    /* Checks if in the google drive spreadsheet exist another project with the same 
     * app.name and app.client as this one in config.php.
     * If there
     */
    function checkIfExists(done) {
        // Prepare a https request to spreadsheet API to check if there is a project with the same name and client
        var project = grunt.config.get("project");
        var options = {
            hostname: "spreadsheets.google.com",
            port: 443,
            path: "/feeds/list/"+SHEET_KEY+"/"+WORKSHEET_KEY+"/private/full?alt=json",
            method: "GET",
            headers: {
                Authorization: googleApis.tokens.token_type + " " + googleApis.tokens.access_token,
                'GData-Version': "3.0"
            }
        };
        options.path = options.path + encodeURI("&sq=appclient=" + project.app.client + " and appname=" + project.app.name + "");
        var req = https.request(options, function(res) {
            console.log("Check if project (row) already exist in google drive.");
            console.log("API Call: %s %s ", options.method.cyan, (options.hostname + options.path).cyan);
            console.log("Response statusCode:", res.statusCode);
            // Proccess chunck data received
            var data = "";
            res.on('data', function(chunk) {
                data += chunk;
            });

            /* After finish the request parses the response and 
             * checks if the project already exists in the spreadsheet */
            res.on('end', function() {
                var response = JSON.parse(data);
                //console.log(JSON.stringify(response,null," ").red);
                if (response.feed.entry) {
                    
                    if (response.feed.entry.length > 1) {
                        done(grunt.util.error('Seems like there are more than one row with the same project name and client. Please Check the Spreadshet Manually.'));
                        open("https://docs.google.com/spreadsheet/ccc?key="+SHEET_KEY+"&usp=drive_web#gid=0");
                        return;
                    }
                    console.log("!! This project Already Exist in google Drive !!".red);
                    open("https://docs.google.com/spreadsheet/ccc?key="+SHEET_KEY+"&usp=drive_web#gid=0");
                    done(true);
                } else {
                    putProject(done);
                }
            });
        });
        req.end();

        req.on('error', function(e) {
            done(e);
        });
    }

    function updateProject(entry, done) {
        var project = grunt.config.get("project");
        var xml = [
            '<entry xmlns="http://www.w3.org/2005/Atom" xmlns:gsx="http://schemas.google.com/spreadsheets/2006/extended" xmlns:gd="http://schemas.google.com/g/2005" >',
            '<id>' + entry['id']['$t'] + '</id>',
            '<updated>' + entry['updated']['$t'] + '</updated>',
            '<category scheme="' + entry['category']['scheme'] + '" term="' + entry['category']['term'] + '"/>',
            '<title type="text">' + project.app.client + '</title>',
            '<content type="text">' + entry['content']['$t'] + '</content>',
            '<link rel="self" type="application/atom+xml" href="' + entry['link'][0]['href'] + '"/>',
            '<link rel="edit" type="application/atom+xml" href="' + entry['link'][1]['href'] + '"/>',
            '<gsx:appclient >' + project.app.client + '</gsx:appclient>',
            '<gsx:appname >' + project.app.name + '</gsx:appname>',
            '<gsx:databasename >' + project.database.db + '</gsx:databasename>',
            '<gsx:databaseuser >' + project.database.user + '</gsx:databaseuser>',
            '<gsx:databasepass >' + project.database.pass + '</gsx:databasepass>',
            '<gsx:appurl >' + project.facebook.appURL + "admin.php" + '</gsx:appurl>',
            '<gsx:adminname >' + project.admin.username + '</gsx:adminname>',
            '<gsx:adminpass >' + project.admin.password + '</gsx:adminpass>',
            '</entry>'
        ].join(" ");

        var options = {
            hostname: "spreadsheets.google.com",
            port: 443,
            path: entry['link'][1]['href'].replace("https://spreadsheets.google.com", ""),
            method: "PUT",
            headers: {
                'Authorization': googleApis.tokens.token_type + " " + googleApis.tokens.access_token,
                'GData-Version': "3.0",
                "Content-Type": "application/atom+xml",
                "charset": "utf-8",
                "If-Match": entry['gd$etag'],
                'Transfer-Encoding': 'chunked'
            }
        };

        var req = https.request(options, function(res) {
            console.log("UPDATE project (row) in google drive.");
            console.log("API Call: %s %s ", options.method.cyan, (options.hostname + options.path).cyan);
            console.log("Response statusCode:", res.statusCode);
            res.setEncoding('utf8');
            var data = "";
            res.on('data', function(d) {
                data += d;
            });
            res.on('end', function() {
                //console.log(data.green);
                done(true);
            });
        });
        req.write(xml);
        req.end();

        req.on('error', function(e) {
            console.error(e);
        });

    }

    function putProject(done) {
        var project = grunt.config.get("project");
        var xml = [
            '<entry xmlns="http://www.w3.org/2005/Atom" xmlns:gsx="http://schemas.google.com/spreadsheets/2006/extended" >',
            '<gsx:appclient >' + project.app.client + '</gsx:appclient>',
            '<gsx:appname >' + project.app['name'] + '</gsx:appname>',
            '<gsx:databasename >' + project.database.db + '</gsx:databasename>',
            '<gsx:databaseuser >' + project.database.user + '</gsx:databaseuser>',
            '<gsx:databasepass >' + project.database.pass + '</gsx:databasepass>',
            '<gsx:appurl >' + project.facebook.appURL + "admin.php" + '</gsx:appurl>',
            '<gsx:adminname >' + project.admin.username + '</gsx:adminname>',
            '<gsx:adminpass >' + project.admin.password + '</gsx:adminpass>',
            '</entry>'
        ].join("");
        var options = {
            hostname: "spreadsheets.google.com",
            port: 443,
            path: "/feeds/list/"+SHEET_KEY+"/"+WORKSHEET_KEY+"/private/full",
            method: "POST",
            headers: {
                'Authorization': googleApis.tokens.token_type + " " + googleApis.tokens.access_token,
                'GData-Version': "3.0",
                "Content-Type": "application/atom+xml",
                "charset": "utf-8",
                'Transfer-Encoding': 'chunked'
            }
        };
        var reqput = https.request(options, function(res) {
            console.log("PUT new project (row) in google drive.");
            console.log("API Call: %s %s ", options.method.cyan, (options.hostname + options.path).cyan);
            console.log("Response statusCode:", res.statusCode);
            res.setEncoding('utf8');
            var data = "";
            res.on('data', function(d) {
                data += d;
            });
            res.on('end', function() {
                open("https://docs.google.com/spreadsheet/ccc?key="+SHEET_KEY+"&usp=drive_web#gid=0");
                done(true);
            });
        });
        reqput.write(xml);
        reqput.end();

        reqput.on('error', function(e) {
            console.error(e);
        });
    }

    /** 
     * Obtains a valid access token to acces google spreadsheets API.*This
     * function Uses the tasks/googleApis.json file to obtain the required * ClientID,
     * Client_Secret, and grant_type required to authenticate * and authorizate using OAuth2.
     * Once obtained the access token this is written to the tasks/googleApis.json file.*/
    function oAuth2(callback) {
        var googleapis = require('googleapis');
        var auth = new googleapis.OAuth2Client(googleApis.clienID, googleApis.clientSecret, googleApis.redirectUrl);


        /* Saves a new acces token to the tasks/googleApis.json file.
         * Assigns the variable oAuth to grunt config and
         * terminates this grunt task 'done(true)'.
         */
        var saveTokens = function(tokens, authcode) {
            console.log('OAth2 access_token: ', tokens.access_token.cyan);
            console.log('OAth2 refresh_token: ', tokens.refresh_token.cyan);
            auth.setCredentials(tokens);
            if (authcode) googleApis.auth_code = authcode;
            googleApis.tokens = tokens;
            grunt.file.write("tasks/googleApis.json", JSON.stringify(googleApis, null, "\t"));
            grunt.config.set("googleApis", googleApis);
            callback(false, googleApis);
        };
        /* 
         * Refresh a previous stored token in tasks/googleApis.json
         * If it fails refreshing the token then it calls getAuthCode() in order
         * to get a new Auth code from the browser */
        var refreshToken = function() {
            auth.setCredentials(googleApis.tokens);
            auth.refreshAccessToken(function(err, tokens) {
                if (err) {
                    console.log('Refresh token failed, proceding to geth a new Auth code.');
                    getAuthCode();
                } else
                    saveTokens(tokens);
            });
        };
        /*
         * Opens the browser to get and auth code, after the user inserts the code
         * in console this code is used to get a new acces token and store it in tasks/googleApis.json
         * If the procces fails to get a new token then task is avorted with error exit. */
        var getAuthCode = function() {
            var url = auth.generateAuthUrl({
                access_type: 'offline',
                scope: googleApis.grant_type
            });
            open(url);
            var inquirer = require("inquirer");
            inquirer.prompt([{
                type: "input",
                name: "code",
                message: "Enter Authorization Code:"
            }], function(answers) {
                auth.getToken(answers.code, function(err, tokens) {
                    if (err)
                        callback(grunt.util.error('Auth code not valid: ERROR CODE "' + err + '"'), null);
                    else
                        saveTokens(tokens, answers.code);
                });
            });
        };

        /* Checks if there is a previous access token stored in googleApis.jon
         * and refresh the token, If there is no token then it request an
         * Auth code and request for a new acces token */
        if (googleApis.tokens && typeof(googleApis.tokens) == 'object')
            refreshToken();
        else
            getAuthCode();
    }
};