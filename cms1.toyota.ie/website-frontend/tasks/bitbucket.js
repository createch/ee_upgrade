module.exports = function(grunt) {

    var bitbucket_user = "createch";
    var bitbucket_apikey = "e4XVpmWfkND21vOlcd01RU7gCzP3P0U3";
    var auth = 'Basic ' + new Buffer(bitbucket_user + ':' + bitbucket_apikey).toString('base64');
    var colors = require("colors");
    var https = require('https');



    /* Gests a valid access token from OAuth 2.0 to acces the google spreadsheet api */
    grunt.registerTask('bitbucket', "Create bitbucket & local git repo.", function() {
        //check if local repo already exists.
        var fs = require('fs');
        var files = fs.readdirSync("./");
        for (var fileindex in files) {
            if (files[fileindex].toLowerCase() === ".git") {
                console.log(("A local Git repository already exists.\nIt is not possible to create a new one.").red);
                return;
            }

        }
        // make the task async.
        var done = this.async();
        var project = grunt.config.get('project');
        var reponame = toSlug(project.app.client + "_" + project.app.name);
        var inquirer = require("inquirer");

        inquirer.prompt([{
            type: "input",
            name: "ans",
            default: reponame,
            message: "Name of the New repository".blue
        }], function(answer) {
            reponame = answer.ans;
            /* Check if a repository exist in bitbucket, if it not exists createNewrepo is called */
            var check = checkIfExist(reponame, function(error, exists) {
                if (error)
                    done(error);
                else {
                    if (!exists) {
                        createNewrepo();
                    } else {
                        console.log((reponame + " already exists in Bitbucket.").red);
                        done(true);
                    }
                }
            });
        });


        /* Setup a new local repo with bitbucket as remote. */
        var initGitLocal = function() {
            git(function(error) {
                if (error)
                    done(error);
                else {
                    console.log("Repository configured locally.");
                    done(true);
                }
            });
        };

        /* Create a new repository in bitbucket and calls initGitLocal  */
        var createNewrepo = function() {
            postNewRepo(reponame, function(error) {
                if (error)
                    done(error);
                else {
                    console.log("New repo '" + reponame.green + "' created on Bitbucket.");
                    initGitLocal();
                }
            });
        };

    });



    /* Uses the bitbucket API to check if a repository already exist in bitbucket.
     * callback(error, {true or false} ) */
    function checkIfExist(reponame, callback) {
        var options = {
            hostname: 'bitbucket.org',
            port: 443,
            path: '//api/2.0/repositories/' + bitbucket_user,
            method: 'GET',
            headers: {
                Authorization: auth,
            }
        };
        var reqGET = https.request(options, function(res) {
            console.log("Check if repo '" + reponame.cyan + "' exists in Bitbuket.");
            console.log("API Call: %s %s ", options.method.cyan, (options.hostname + options.path).cyan);
            console.log("Response statusCode:", res.statusCode);
            res.setEncoding('utf8');
            var data = "";
            res.on('data', function(d) {
                data += d;
            });
            res.on('end', function() {
                var repolist = JSON.parse(data).values;
                for (var repo in repolist) {
                    if (reponame === repolist[repo]["name"]) {
                        // calback confirm repository exists
                        callback(null, true);
                        return;
                    }
                }
                // calback confirm repository NOT exists
                callback(null, false);
            });

        });

        reqGET.end();

        reqGET.on('error', function(e) {
            callback(e, null);
        });
    }
    /* Uses the bitbucket API 2.0 to create a new repository an calls updatePackageJson.  */
    function postNewRepo(reponame, callback) {
        var post_data = '{"scm": "git", "is_private": true , "no_public_forks": true}';
        var options = {
            hostname: 'bitbucket.org',
            port: 443,
            path: '//api/2.0/repositories/' + bitbucket_user + "/" + reponame,
            method: 'POST',
            headers: {
                Authorization: auth,
                'Content-Type': 'application/json',
                'Content-Length': post_data.length
            }
        };
        var reqPOST = https.request(options, function(res) {
            console.log("Create new repo '" + reponame.cyan + "' to Bitbuket.");
            console.log("API Call: %s %s ", options.method.cyan, (options.hostname + options.path).cyan);
            console.log("Response statusCode:", res.statusCode);

            res.setEncoding('utf8');
            var data = "";
            res.on('data', function(d) {
                data += d;
            });
            res.on('end', function() {
                console.log(data);
                updatePackageJson(reponame);
                callback(null);
            });

        });
        reqPOST.write(post_data);
        reqPOST.end();

        reqPOST.on('error', function(e) {
            callback(e);
        });
    }

    /* Modifies the pakage.json file in order to reflect the new remote repo on bitbucket. */
    function updatePackageJson(reponame) {
        grunt.config.set("pkg.name", reponame);
        grunt.config.set("pkg.repository.url", "https://bitbucket.org/" + bitbucket_user + "/" + reponame + ".git");
        grunt.config.set("pkg.homepage", "https://bitbucket.org/" + bitbucket_user + "/" + reponame);
        grunt.file.write("package.json", JSON.stringify(grunt.config.get("pkg"), null, "\t"));
    }

    /* Returns and slug string, replacing spaces and removing non valid characters */
    function toSlug(Text) {
        return Text.toLowerCase().replace(/ /g, '_').replace(/[^\w-]+/g, '');
    }

    /* Creates and SetUp local git repo to use the bitbucket as remote. */
    function git(callback) {
        var sys = require('sys');
        var child = require('child_process');
        var exec = child.exec;

        var pkg = grunt.config.get("pkg");
        console.log("Configuring repo '" + pkg.name.green + "' locally.");

        var commands = function(exclist) {
            exec(exclist[0], function(error, stdout, stderr) {
                if (error)
                    callback(error);
                else {
                    console.log(exclist[0].cyan);
                    sys.puts(stdout);
                    if (exclist.length === 1)
                        callback(null);
                    else
                        commands(exclist.slice(1, exclist.lenght));
                }
            });
        };

        var remote = 'git remote add origin https://bitbucket.org/' + bitbucket_user + '/' + pkg.name + '.git';
        commands(['git init', 'git add -A', 'git commit -m "first commit"', remote]);
    }

};