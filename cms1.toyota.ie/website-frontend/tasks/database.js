module.exports = function(grunt) {

    var colors = require("colors");
    var https = require('https');
    var MysqlRootPass = 'mysql';



    /* Create a Database */
    grunt.registerTask('create-db', "Create MySql database.", function() {
        // make task async *required for mysql to work
        var done = this.async();
        // mysql
        var mysql = require('mysql');
        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: MysqlRootPass,
        });
        // reading grunt.conf.project to get database values.
        var project = grunt.config.get('project');
        var user = project.database.user;
        var dbname = project.database.db;
        var pass = project.database.pass;
        // the nexts SQL queries are the same that phpMyAdmin 4.0.9. uses when cratres a new user and its database
        var create_user = "CREATE USER '" + user + "'@'localhost' IDENTIFIED BY '" + pass + "';";
        var grant_usage = "GRANT USAGE ON *.* TO '" + user + "'@'localhost' IDENTIFIED BY '" + pass + "' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;";
        var create_db = "CREATE DATABASE `" + dbname + "` CHARACTER SET utf8 COLLATE utf8_general_ci;";
        var grant_priv = "GRANT ALL PRIVILEGES ON `" + dbname + "`.* TO '" + user + "'@'localhost';";
        //conetion and queries
        connection.connect();
        connection.query(create_user, function(err) {
            if (err)
                done(err);
            else
                console.log('SQL CREATE USER ' + user + '...' + ' OK'.green);
        });
        connection.query(grant_usage, function(err) {
            if (err)
                done(err);
            else
                console.log('SQL GRANT USAGE ON *.*...' + ' OK'.green);
        });
        connection.query(create_db, function(err) {
            if (err)
                done(err);
            else
                console.log('SQL CREATE DATABASE ' + dbname + '...' + ' OK'.green);
        });
        connection.query(grant_priv, function(err) {
            if (err)
                done(err);
            else
                console.log('SQL GRANT ALL PRIVILEGES ON...' + ' OK'.green);
        });
        connection.end(function(err) {
            if (err)
                done(err);
            else
                done(true);
        });
    });

};