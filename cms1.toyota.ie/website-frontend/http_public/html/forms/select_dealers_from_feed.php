<?php

global $dealer;
$dealer = (isset($_GET["dealer"])) ? filter_input(INPUT_GET, 'dealer', FILTER_SANITIZE_SPECIAL_CHARS) : false;
$json_dealers = file_get_contents("http://development.toyota.ie/api/dealer-list");
$dealer_list = json_decode($json_dealers);
?>
<select id="dealer" name="00Nb0000002AKok" title="Dealer Lookup" odata-validation="select">
	<option value="no_selection">...</option>
	<?php
	foreach ($dealer_list as $dealer) {
		//TODO this fields are not real 'not real name title and code, chack in json feed to fin out them.
		$name = $dealer->title;
		$code = $dealer->code;
		echo "<option value='$code'>$name</option>";
	}
	?>
</select>
