<?php
global $car_model;
$car_model = (isset($_GET["model"])) ? filter_input(INPUT_GET, 'model', FILTER_SANITIZE_SPECIAL_CHARS) : false;
$json_cars = file_get_contents("http://development.toyota.ie/api/new-cars");
$cars_list = json_decode($json_cars)->modelrange->items;
?>
<select name="model" id="model" data-validation="select">
	<option value="no_selection">...</option>
	<?php
	foreach ($cars_list as $car) {
		$car_model = $car->title;
		echo "<option value='$car_model'>$car_model</option>";
	}
	?>
</select>

