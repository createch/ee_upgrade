<div class="form_container">
	<form action="./formsubmit.php?action=bookatestdrive" method="POST" id="booktestdriveform" class="contactform">

		<input type=hidden name="oid"				id="oid"					value="00Db0000000HWqf">
		<input type=hidden name="00Nb0000002N6C8"	id="00Nb0000002N6C8"		value="1">
		<input type=hidden name="retURL"			id="retURL"					value="http://.... TODO">
		<input type=hidden name="recordType"		id="recordType"				value="012b0000000JPwH">
		<input type=hidden name="00Nb0000002AKok"	id="dealer"					value="4 digits code TODO">
		<input type=hidden name="lead_source"		id="lead_source"			value="Toyota.ie">
		<input type=hidden name="Campaign_ID"		id="Campaign_ID"			value="701b0000000Q6zR">
		<input type=hidden name="member_status"		id="member_status"			value="Test Drive Requested">
		<input type=hidden name="rating"			id="rating"					value="Warm">


		<div class="container_12">
			<div class="grid_12 header">
				<h2>What would you like to drive?</h2>
				<span class="intro"><?php if (!isset($_GET["dealer"])) { ?>Select a local dealer &AMP; <?php } ?>Choose a model that you would like to test drive.</span>
			</div>
		</div>

		<div class="container_12 extraspace">
			<?php if (isset($_GET["dealer"])) { ?>
				<input type=hidden name="00Nb0000002AKok" id="dealer" value="<?= filter_input(INPUT_GET, 'dealer', FILTER_SANITIZE_SPECIAL_CHARS) ?>">
			<?php } else { ?>
				<div class="grid_6">
					<h4 class="title1">Select Your Local Dealer  &nbsp;&nbsp;</h4>
					<div class="select dealers">
						<div class="value"></div>
						<i class="icon icon-chevron-down"></i>
						{embed="components/dealers"}
					</div>
				</div>
			<?php } ?>	
			<div class="grid_6">
				<h4 class="title1">Model  &nbsp;&nbsp; <i class="icon icon-car2"></i></h4>
				<div class="select">
					<div class="value"></div>
					<i class="icon icon-chevron-down"></i>
					<?php include './selects/models_from_feed.php'; ?>
				</div>
			</div>
		</div>


		<?php include './subsection/contactdetails.php'; ?>
		<?php include './subsection/dataprotection.php'; ?>
		<?php include './subsection/submit.php'; ?>

	</form>
	<div class="container_12 form_thanks">
		<div class="grid_12 header">
			<h2>Thank you for Arrange your Test Drive.</h2>
			<span class="intro">You'll be contacted as soon as possible to confirm the test date.</span>
		</div>
	</div>
</div>

