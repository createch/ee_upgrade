<div class="form_container">
	<form action="./formsubmit.php?action=contactus" method="POST" id="contactusform" class="contactform">
		<input type=hidden name="oid"				id="oid"					value="00Db0000000HWqf">
		<?php if (isset($_GET["dealer"])) { ?>
			<input type=hidden name="00Nb0000002AKok"	id="dealer"					value="<?= filter_input(INPUT_GET, 'dealer', FILTER_SANITIZE_SPECIAL_CHARS) ?>">
		<?php } ?>
		<div class="container_12">
			<div class="grid_12 header">
				<h2>Contact Us</h2>
				<span class="intro">We always aim to offer you on our website comprehensive and up to date information. If you still have unanswered questions, wish to offer suggestions or criticism, feel free to contact us.</span>
			</div>
		</div>

		<div class="container_12 nop">
			<div class="grid_6 nop">
				<div class="container_12">
					<div class="grid_6">
						<h4 class="title1">Salutation &nbsp;&nbsp; <i class="icon icon-user"></i></h4>
						<div class="select">
							<div class="value"></div>
							<i class="icon icon-chevron-down"></i>
							<select name="salutation" id="salutation" data-validation="select">
								<option value="no_selection">...</option>
								<option value="Dr">Dr</option>
								<option value="Mr">Mr</option>
								<option value="Ms">Ms</option>
							</select>
						</div>
					</div>
				</div>
				<div class="container_12">
					<div class="grid_6">
						<h4 class="title1">First Name &nbsp;&nbsp; <i class="icon icon-user"></i></h4>
						<input  id="first_name" maxlength="40" name="first_name"  type="text" placeholder="Name" data-validation="required"/>
					</div>
					<div class="grid_6">
						<h4 class="title1">Last Name &nbsp;&nbsp; <i class="icon icon-user"></i></h4>
						<input  id="last_name" maxlength="80" name="last_name"  type="text" placeholder="Surname" data-validation="required"/>
					</div>
				</div>
				<div class="container_12 ">
					<div class="grid_6">
						<h4 class="title1">Phone &nbsp;&nbsp; <i class="icon icon-phone"></i></h4>
						<input  id="phone" maxlength="14" name="phone"  type="text" placeholder="01 ..." data-validation="number"/>
					</div>
					<div class="grid_6">
						<h4 class="title1">Mobile &nbsp;&nbsp; <i class="icon icon-phone"></i></h4>
						<input  id="mobile" maxlength="14" name="mobile"  type="text" placeholder="08 ..." data-validation="number"/>
					</div>
				</div>
				<div class="container_12 extraspace">
					<div class="grid_12">
						<h4 class="title1">Email &nbsp;&nbsp; <i class="icon icon-envelope-alt"></i></h4>
						<input  id="email" maxlength="80" name="email"  type="text" placeholder="your@email.com" data-validation="email"/>
					</div>
				</div>
			</div>
			<div class="grid_6 nop">
				<div class="container_12">
					<div class="grid_6">
						<h4 class="title1">City &nbsp;&nbsp; <i class="icon icon-map-marker"></i></h4>
						<input  id="city" maxlength="40" name="city" type="text" placeholder="" data-validation="required"/>
					</div>
					<div class="grid_6">
						<h4 class="title1">County &nbsp;&nbsp; <i class="icon icon-map-marker"></i></h4>
						<div class="select">
							<div class="value">Please choose:</div>
							<i class="icon icon-chevron-down"></i>
							<?php include './selects/state.php'; ?>
						</div>
					</div>
				</div>
				<div class="container_12 extraspace">
					<div class="grid_12">
						<h4 class="title1">Address&nbsp;&nbsp; <i class="icon icon-map-marker"></i></h4>
						<input  id="address_line_1"  name="address_line_1" type="text" data-validation="required"/>
					</div>
					<div class="grid_12">
						<input  id="address_line_2"  name="address_line_2" type="text" />
					</div>
				</div>

			</div>
		</div>

		<div class="container_12 extraspace">
			<div class="grid_12">
				<h4 class="title1">Message </h4>
				<textarea id="query"  name="query"  placeholder="leave your message here."></textarea>
			</div>
		</div>

		<?php include './subsection/dataprotection.php'; ?>

		<?php include './subsection/submit.php'; ?>
	</form>
	<div class="container_12 form_thanks">
		<div class="grid_12 header">
			<h2>Thank you for contacting Us.</h2>
			<span class="intro">You'll be contacted back as soon as possible.</span>
		</div>
	</div>
</div>

