<div class="form_container">
	<form action="./formsubmit.php?action=requestabrochure" method="POST" id="requestbrochure" class="contactform">

		<input type=hidden name="oid"				id="oid"					value="00Db0000000HWqf">
		<input type=hidden name="00Nb0000002AKoo"	id="00Nb0000002AKoo"		value="1">
		<input type=hidden name="retURL"			id="retURL"					value="http://.... TODO">
		<input type=hidden name="recordType"		id="recordType"				value="012b0000000JPwH">
		<input type=hidden name="lead_source"		id="lead_source"			value="Toyota.ie">
		<input type=hidden name="Campaign_ID"		id="Campaign_ID"			value="701b0000000Q6zM">
		<input type=hidden name="member_status"		id="member_status"			value="sent">
		<input type=hidden name="rating"			id="rating"					value="Warm">
		<input type="hidden" name="XID" value="{XID_HASH}" />


		<div class="container_12">
			<div class="grid_12 header">
				<h2>Order or download model brochures</h2>
				<span class="intro">Choose a model that you would like a brochure from, then fill out the form below...</span>
			</div>
		</div>
		<div class="container_12">
			<?php if (isset($_GET["dealer"])) { ?>
				<input type=hidden name="00Nb0000002AKok" id="dealer" value="<?= filter_input(INPUT_GET, 'dealer', FILTER_SANITIZE_SPECIAL_CHARS) ?>">
			<?php } else { ?>
				<div class="grid_6">
					<h4 class="title1">Select Your Local Dealer  &nbsp;&nbsp;</h4>
					<div class="select dealers">
						<div class="value"></div>
						<i class="icon icon-chevron-down"></i>
						<?php include './selects/dealers.php'; ?>
					</div>
				</div>
			<?php } ?>	
			<div class="grid_6">
				<h4 class="title1">Model  &nbsp;&nbsp; <i class="icon icon-car2"></i></h4>
				<div class="select">
					<div class="value"></div>
					<i class="icon icon-chevron-down"></i>
					<?php include './selects/brochures.php'; ?>
				</div>
			</div>
		</div>

		<div class="container_12">
			<div class="grid_6">
				<h4 class="title1">Receive brochure via &nbsp;&nbsp; <span id="selecttype_icon"><i class="icon icon-file-pdf"></i></span></h4>
				<div class="select">
					<div class="value"></div>
					<i class="icon icon-chevron-down"></i>
					<select name="sendType" id="selecttype" data-validation="select">
						<option value="pdfDownload">Download as pdf</option>
						<option value="Ebrochure">Email brochure</option>
						<option value="Post">Post mail</option>
					</select>
				</div>
			</div>
		</div>

		<div id="postbrochureAdress">
			<div class="container_12" >
				<div class="grid_6">
					<h4 class="title1">City &nbsp;&nbsp; <i class="icon icon-map-marker"></i></h4>
					<input  id="city" maxlength="40" name="city" type="text" placeholder="" data-validation="required"/>
				</div>
				<div class="grid_6">
					<h4 class="title1">County &nbsp;&nbsp; <i class="icon icon-map-marker"></i></h4>
					<div class="select">
						<div class="value">Please choose:</div>
						<i class="icon icon-chevron-down"></i>
						<?php include './selects/state.php'; ?>
					</div>
				</div>
			</div>
			<div class="container_12">
				<div class="grid_12">
					<h4 class="title1">street&nbsp;&nbsp; <i class="icon icon-map-marker"></i></h4>
					<input  id="street"  name="street" type="text" data-validation="required"/>
				</div>
			</div>
		</div>

		<br/>

		<?php include './subsection/contactdetails.php'; ?>
		<?php include './subsection/dataprotection.php'; ?>
		<?php include './subsection/submit.php'; ?>


	</form>
	<div class="container_12 form_thanks">
		<div class="grid_12 header">
			<h2>Thank you for requesting a brochure.</h2>
			<span class="intro">If you order your brochure by post-mail it will be sent to you as soon as possible.</span>
		</div>
	</div>
</div>
