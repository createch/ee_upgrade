<?php
header('Access-Control-Allow-Origin: *');

$currentTab = (isset($_GET["tab"])) ? $_GET["tab"] : "pane-contact";
$form = (isset($_GET["form"])) ? $_GET["form"] : false;

global $dealer, $car_model;
$car_model = $_GET["model"];
$dealer = $_GET["dealer"];
?>
<!DOCTYPE html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Toyota</title>
        <meta name="description" content="">
        <meta name="viewport" content="initial-scale=1.0">
        <!-- favicon -->
        <link rel="icon" type="image/x-icon" href="favicon.ico">
        <!-- CSS files -->
        <link rel="stylesheet" href="../../css/style.css">
		<!--[if lt IE 9]>      <link rel="stylesheet" href="../../css/ie8.css"> <![endif]-->
		<!--[if lt IE 8]>      <link rel="stylesheet" href="../../css/ie7.css"> <![endif]-->
		<script src="../../js/modernizr.js"></script>
		<style>
			html , body, iframe{		
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
			}
			iframe{
				border:none;				
			}
		</style>
    </head>
    <body>
		<?php if (!$form) { ?>
			<nav class="toyota-tabs-select container form_nav">
				<div class="container_12 clearfix">
					<div class="grid_12">
						<span class="form_nav_item tabtrigger"  data-tab="pane-contact-tab">Contact us</span>
						<span class="form_nav_item tabtrigger"  data-tab="pane-brochure-tab">Request a brochure</span>
						<span class="form_nav_item tabtrigger"  data-tab="pane-test-drive-tab">Arrange a test drive</span>
						<span class="form_nav_item tabtrigger"  data-tab="pane-service-tab">Request a service.</span>
						<?php if (isset($_GET["dealer"])) { ?>
							<span class="form_nav_item tabtrigger"  data-tab="pane-service-tab">Get Directions.</span>			
						<?php } ?>
					</div>
				</div>
			</nav>
			<div class="" id="form_tabs">
				<div class="tabContent" id="pane-test-drive-tab">
					<?php include_once"./bookatestdrive.php" ?>"
				</div>
				<div class="tabContent" id="pane-contact-tab">
					<?php include_once"./contactus.php" ?>
				</div>
				<div class="tabContent" id="pane-service-tab">
					<?php include_once"./bookaservice.php" ?>
				</div>
				<div class="tabContent" id="pane-brochure-tab">
					<?php include_once"./brochure.php" ?>
				</div>
				<?php if (isset($_GET["dealer"])) { ?>
					<div class="tabContent" id="pane-directions-tab">

					</div>				
				<?php } ?>
			</div>
			<?php
		} else {
			include_once("./" . $form . ".php");
		}
		?>
		<script src="../../js/libs.js"></script>
        <script src="../../js/main.js"></script>
		<!--[if lt IE 9]> <script src="../../js/ie8.js"></script> <![endif]-->
		<?php if (!$form) { ?>
			<script type="text/javascript">
				if ($(".tabtrigger").length > 0) {
					var $currentTab = $("#<?= $currentTab ?>-tab");
					var $currentControl = $(".tabtrigger[data-tab='<?= $currentTab ?>-tab']");
					$currentControl.addClass("active");
					$currentTab.show();

					$('.tabtrigger').click(function () {
						$currentControl.removeClass("active");
						$(this).addClass("active");
						var id = $(this).data("tab");
						var $nextTab = $("#" + id);
						$currentTab.hide();
						$nextTab.show();
						$currentTab = $nextTab;
						$currentControl = $(this);
					});
				}
			</script>
		<?php } ?> 
	</body>
</html>










