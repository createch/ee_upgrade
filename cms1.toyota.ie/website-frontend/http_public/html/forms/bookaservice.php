<div class="form_container">
	<form action="./formsubmit.php?action=bokaservice" method="POST" id="bookserviceform" class="contactform">
		<input type=hidden name="oid"				id="oid"					value="00Db0000000HWqf">


		<div class="container_12">
			<div class="grid_12 header">
				<h2>Request a Service.</h2>
			</div>
		</div>


		<?php if (isset($_GET["dealer"])) { ?>
			<input type=hidden name="00Nb0000002AKok" id="dealer" value="<?= filter_input(INPUT_GET, 'dealer', FILTER_SANITIZE_SPECIAL_CHARS) ?>">
		<?php } else { ?>
			<div class="container_12">
				<div class="grid_6">
					<h4 class="title1">Select Your Local Dealer  &nbsp;&nbsp;</h4>
					<div class="select dealers">
						<div class="value"></div>
						<i class="icon icon-chevron-down"></i>
						<?php include './selects/dealers.php'; ?>
					</div>
				</div>
			</div>
		<?php } ?>	
		<div class="container_12">
			<div class="grid_6">
				<h4 class="title1">Make </h4>
				<div class="select">
					<div class="value"></div>
					<i class="icon icon-chevron-down"></i>
					<?php include './selects/make.php'; ?>
				</div>
			</div>
			<div class="grid_6">
				<h4 class="title1">Model  &nbsp;&nbsp; <i class="icon icon-car2"></i></h4>
				<div class="select">
					<div class="value"></div>
					<i class="icon icon-chevron-down"></i>
					<?php include './selects/models.php'; ?>
				</div>
			</div>
		</div>
		<div class="container_12 ">
			<div class="grid_6">
				<h4 class="title1">Year  </h4>
				<input  id="year"  name="year"  type="text" placeholder="" data-validation="number"/>
			</div>
			<div class="grid_6">
				<h4 class="title1">Mileage </h4>
				<input  id="mileage"  name="mileage"  type="text" placeholder="KM" data-validation="number"/>
			</div>
		</div>
		<div class="container_12 ">
			<div class="grid_6">
				<h4 class="title1">Preferred Date 1 </h4>
				<input  id="preferred_date1"  name="preferred_date1" class="datepicker" type="text" placeholder="Month/Day/Year" data-validation="date"  data-validation-format="mm/dd/yyyy"/>
			</div>
			<div class="grid_6">
				<h4 class="title1">Preferred Date 2 </h4>
				<input  id="preferred_date2"  name="preferred_date2"  class="datepicker" type="text" placeholder="Month/Day/Year" data-validation="date" data-validation-format="mm/dd/yyyy"/>
			</div>
		</div>
		<div class="container_12 ">
			<div class="grid_6">
				<h4 class="title1">Subject </h4>
				<input  id="subject"  name="subject" type="text" placeholder=""/>
			</div>
		</div>

		<div class="container_12 extraspace">
			<div class="grid_12">
				<h4 class="title1">Comment </h4>
				<textarea id="comments"  name="comments"  placeholder="leave any comment here."></textarea>
			</div>
		</div>




		<?php include './subsection/contactdetails.php'; ?>
		<?php include './subsection/dataprotection.php'; ?>
		<?php include './subsection/submit.php'; ?>

	</form>
	<div class="container_12 form_thanks">
		<div class="grid_12 header">
			<h2>Thank you for request your service.</h2>
			<span class="intro">You'll be contacted back as soon as possible to confirm your booking.</span>
		</div>
	</div>
</div>
