/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

;
$(document).ready(function() {
	$('.carousel').each(function() {
		var $content = $(this).find('.block');
		var $link = $(this).find('.carousel-link');
		var $bg = $(this).find('.carousel-bg');
		var imgs = $bg.data("carousel-assets").split("|");
		var index = -1;
		var last=-1;
		var left = "";
		var right = "";
		if ($('html').hasClass('lt-ie8')){
			var left = "<";
			var right = ">";
		}
		var controls =
				'<div class="carousel-nav-container">' +
				'<a class="carousel-arrow right icon icon-angle-right" href="#" data-icon="">'+right+'</a>' +
				'<a class="carousel-arrow left icon icon-angle-left" href="#" data-icon="">'+left+'</a>' +
				'<ul class="carousel-nav">' +
				'<li class=""><a href="#"></a></li>' +
				'<li class=""><a href="#"></a></li>' +
				'<li class="active"><a href="#"></a></li>' +
				'</ul></div>';
		var img = "<img id='iMgCaRouSel'   href='' class='coverBG carousel-bg-item active'/>";
		$bg.html(controls + img);
		var nav = $bg.find(".carousel-nav li");

		var thread = 0;
		var current = 0;

		var animate = function() {
			index++;
			if (index >= $content.length) {
				index = 0;
			}
			
			
			$("#iMgCaRouSel").attr("src", imgs[index]);
			
			nav.each(function(){
				$(this).hide();
			});
			$(nav.get(index)).show();


			$content.each(function(){
				$(this).hide();
			});
			window.setTimeout(function(){
				$($content.get(index)).show();
			},300);
			

			var $l = $(nav.get(index)).data("carousel-link");
			$link.attr("href", $l);
			last = index;
		};
		
		
		var back = function(){
			index-=2;
			if(index < 0){
				index =  $content.length + index;
			}
		};

		var move = function(forceanimation) {
			animate();
//			if (current === 0) {
//				
//				setTimeout(function() {
//					move();
//				}, 4000);
//			} else {
//				if (forceanimation) {
//					animate();
//					setTimeout(function() {
//						current--;
//						if(current===0){
//							move();
//						}
//					}, 4000);
//				}
//			}
		};


		$bg.find(".right").click(function(e) {
			e.preventDefault();
			current++;
			move(true);
			setTimer();
		});

		$bg.find(".left").click(function(e) {
			e.preventDefault();
			back();
			current++;
			move(true);
			setTimer();
		});

		move();
		
		var interval = null;
		
		var setTimer = function(){
			if(interval)
				window.clearInterval(interval);
			interval = window.setInterval(function(){
				current++;
				move();
			},4000);
		};
		
		setTimer();

	});
});
