

<!-- #################### COMPONENTS ######################### -->

<h1 class="container_12" >full-width-quote-text</h1>
<?php include('html/article/full-width-quote-text.html'); ?>

<h1 class="container_12" >full-width-intro-text</h1>
<?php include('html/article/full-width-intro-text.html'); ?>

<h1 class="container_12" >2text-2media</h1>
<?php include('html/article/2text-2media.html'); ?>

<h1 class="container_12" >2text-2related</h1>
<?php include('html/article/2text-2related.html'); ?>

<h1 class="container_12" >2text-2spotlight</h1>
<?php include('html/article/2text-2spotlight.html'); ?>

<h1 class="container_12" >3bullet</h1>
<?php include('html/article/3bullet.html'); ?>

<h1 class="container_12" >3text-1image</h1>
<?php include('html/article/3text-1image.html'); ?>

<h1 class="container_12" >3text-1quote</h1>
<?php include('html/article/3text-1quote.html'); ?>

<h1 class="container_12" id="gallery-article">gallery-article</h1>
<?php include('html/gallery/article.html'); ?>
