<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<header class="header-container padding">
	<div class="bg-image-hero bg-image-hero-med">
		<div class="coverBG" style="background-image: url(./images/dealers/boggans.jpg);"></div>
		<div class="hero-landing container_12 container_12_absolute center-text">
			<h1 class="heading1">Boggans</h1>
		</div>
	</div>
</header>
<div class="container_12 content-block">
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/gorey_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Gorey Garage</strong></h3>
		<p class="padding-small">Hugh Boggan Garage, Dublin Road, Gorey,<br/>Co. Wexford</p>
		<p class="padding"><a href="tel:0539420215"><strong>Telephone:</strong> (053) 9420215</a></p>
		<a class="uiBtn redBtn  hasIconRight" href="http://gorey.hughbogganmotors.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/carriglawn_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Carriglawn Garage</strong></h3>
		<p class="padding-small">Boggans of Wexford, Newtown Road,<br/>Co. Wexford</p>
		<p class="padding"><a href="tel:0539143788"><strong>Telephone:</strong> (053) 9143788</a></p>
		<a class="uiBtn redBtn  hasIconRight" href="http://carriglawn.hughbogganmotors.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
</div>


<!-- ################## PAGE END ########################-->



<header class="header-container padding">
	<div class="bg-image-hero bg-image-hero-med">
		<div class="coverBG" style="background-image: url(./images/dealers/denismahony_blur.jpg);"></div>
		<div class="hero-landing container_12 container_12_absolute center-text">
			<h1 class="heading1">Denis Mahony</h1>
		</div>
	</div>
</header>
<div class="container_12 content-block">
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/m50_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>M50 Garage</strong></h3>
		<p class="padding-small">Denis Mahony Garage, Exit 5, M50, North Road,<br/>Dublin 11</p>
		<p class="padding">
			<a href="tel:018647500"><strong>Telephone:</strong> (01) 864 7500</a><br/>
			<a href="mailto:toyotanr@denismahony.ie"><strong>Email:</strong> toyotanr@denismahony.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://toyotam50.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/kilbarrack_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Kilbarrack Garage</strong></h3>
		<p class="padding-small">Denis Mahony Garage, Kilbarrack Road, Kilbarrack,<br/>Dublin 5</p>
		<p class="padding">
			<a href="tel:018322701"><strong>Telephone:</strong> (01) 8322701</a><br/>
			<a href="mailto:toyotakb@denismahony.ie"><strong>Email:</strong> toyotakb@denismahony.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://www.toyotakilbarrack.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
</div>



<!-- ################## PAGE END ########################-->




<header class="header-container padding">
	<div class="bg-image-hero bg-image-hero-med">
		<div class="coverBG" style="background-image: url(./images/dealers/kellys.jpg);"></div>
		<div class="hero-landing container_12 container_12_absolute center-text">
			<h1 class="heading1">Kellys</h1>
		</div>
	</div>
</header>
<div class="container_12 content-block">
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/letterkenny_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Letterkenny Garage</strong></h3>
		<p class="padding-small">Kellys Garage, Port Rd, Letterkenny,<br/>Co. Donegal</p>
		<p class="padding">
			<a href="tel:0749121385"><strong>Telephone:</strong> (074) 9121385</a><br/>
			<a href="mailto:sales@kellystoyota.com"><strong>Email:</strong> sales@kellystoyota.com</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://letterkenny.kellystoyota.com/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/mountcharles_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Mountcharles Garage</strong></h3>
		<p class="padding-small">Mountcharles Garage, Mountcharles,<br/>Co. Donegal</p>
		<p class="padding">
			<a href="tel:0749735011"><strong>Telephone:</strong> (074) 9735011</a><br/>
			<a href="mailto:salesmc@kellystoyota.com"><strong>Email:</strong> salesmc@kellystoyota.com</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://mountcharles.kellystoyota.com/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
</div>



<!-- ################## PAGE END ########################-->



<header class="header-container padding">
	<div class="bg-image-hero bg-image-hero-med">
		<div class="coverBG" style="background-image: url(./images/dealers/malones.jpg);"></div>
		<div class="hero-landing container_12 container_12_absolute center-text">
			<h1 class="heading1">Malones</h1>
		</div>
	</div>
</header>
<div class="container_12 content-block">
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/navan_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Navan Garage</strong></h3>
		<p class="padding-small">Navan Garage, Navan,<br/>Co. Meath</p>
		<p class="padding">
			<a href="tel:0469071717"><strong>Telephone:</strong> (046) 9071717</a><br/>
			<a href="mailto:dave@malones.ie"><strong>Email:</strong> dave@malones.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://navan.malones.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/drogheda_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Drogheda  Garage</strong></h3>
		<p class="padding-small">Drogheda  Garage, North Road, Drogheda,<br/>Co. Louth</p>
		<p class="padding">
			<a href="tel:0419802420"><strong>Telephone:</strong> (041) 9802420</a><br/>
			<a href="mailto:johnd@malones.ie"><strong>Email:</strong> johnd@malones.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="hhttp://drogheda.malones.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
</div>



<!-- ################## PAGE END ########################-->



<header class="header-container">
	<div class="bg-image-hero bg-image-hero-med">
		<div class="coverBG" style="background-image: url(./images/dealers/tagriondiran.jpg);"></div>
		<div class="hero-landing container_12 container_12_absolute center-text">
			<h1 class="heading1">Tadg Riordan</h1>
		</div>
	</div>
</header>
<div class="container_12 content-block">
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/ashbourne_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Ashbourne Garage</strong></h3>
		<p class="padding-small">Frederick Street, Ashbourne,<br/>Co. Meath</p>
		<p class="padding">
			<a href="tel:018350084"><strong>Telephone:</strong> (01) 8350084</a><br/>
			<a href="mailto:sales@trmash.ie"><strong>Email:</strong> sales@trmash.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://www.trmash.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
	<div class="grid_6 padding-large">
		<img src="./images/dealers/pages/tallaght_garage.jpg" class="padding-small fullwidth">
		<h3 class="heading3 padding-small"><strong>Tallaght  Garage</strong></h3>
		<p class="padding-small">Tallaght Bypass, Tallaght,<br/>Dublin 24</p>
		<p class="padding">
			<a href="tel:014517447"><strong>Telephone:</strong> (01) 4517447</a><br/>
			<a href="mailto:sales@trmtallaght.ie"><strong>Email:</strong> sales@trmtallaght.ie</a>
		</p>
		<a class="uiBtn redBtn  hasIconRight" href="http://www.trmtallaght.ie/">Enter Site &nbsp;&nbsp;&nbsp;<i class="icon icon-chevron-right"></i></a>
	</div>
</div>

