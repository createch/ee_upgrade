<div id="filter" class="container_12 clearfix content-block">
	<div class="grid_12">
		<div class="section_title" id="filterable_section">
			<h2 class="heading1">{Here comes the Dealer Name} <span>News</span></h2>
		</div>
	</div>
	<div class="grid_12" >
		<div class=" filter-bar no-js-hide">
			<div class="fb-inner">
				<ul class="fb-group fb-list">
					<li><a href="#filter" class="filter-control uiBtn greyBtn active" id="all-data" data-section-filter=".fc-group" data-default>All</a></li>
					<li><a href="#filter" class="filter-control uiBtn greyBtn" data-section-filter=".toyota-group">Toyota News</a></li>
					<li><a href="#filter" class="filter-control uiBtn greyBtn" data-section-filter=".local-group">Dealer News</a></li>
				</ul>
			</div>
		</div>
	</div>	
	<?php include_once 'json-news.html'; ?>
	<div class="grid_12 padding showMoreGroup">
		<div class=" filter-bar no-js-hide">
			<div class="fb-inner">
				<ul class="fb-group fb-list">
					<li ><a href="#filter" class=" uiBtn greyBtn showMore"  data-section-filter=".fc-group"><span class="text">Show More </span>&nbsp; <i class="icon icon-chevron-down"></i></a></li>
					<li ><a href="#filter" class=" uiBtn greyBtn showMore " data-section-filter=".toyota-group"><span class="text">Show More Toyota news</span>&nbsp; <i class="icon icon-chevron-down"></i></a></li>
					<li ><a href="#filter" class=" uiBtn greyBtn showMore " data-section-filter=".local-group"><span class="text">Show More Dealer News</span>&nbsp; <i class="icon icon-chevron-down"></i></a></li>
					<li class="return_top"><a href="#filter" class=" uiBtn greyBtn" ><span class="text">Back to top</span>&nbsp; <i class="icon icon-chevron-up"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
