
		<div class="grid_3 fc-group local-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/local-article/marlons-news">
						<span class="imgContainer">
							<img class="img100" src="http://development.toyota.ie/images/uploads/headers/_preview/Gareth_Keenan.jpg" alt="
								 Marlon&#039;s News">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								
								Dealer
								&nbsp;news 9
							</span>
							Marlon&#039;s News						</h3>
						<p class="summary">
							Marlon Jerez is from Valencia in Spain. 						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group local-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/local-article/hughs-news">
						<span class="imgContainer">
							<img class="img100" src="http://development.toyota.ie/images/uploads/headers/_preview/96ynmc.jpg" alt="
								 Hugh&#039;s News">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								
								Dealer
								&nbsp;news 10
							</span>
							Hugh&#039;s News						</h3>
						<p class="summary">
							Hugh Kirwan was born in Ranelagh, Dublin. 						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group local-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/local-article/this-is-a-nice-lovely-news-article">
						<span class="imgContainer">
							<img class="img100" src="http://development.toyota.ie/images/uploads/headers/_preview/Gareth_Keenan.jpg" alt="
								 This is a nice lovely news article">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								
								Dealer
								&nbsp;news
							</span>
							This is a nice lovely news article						</h3>
						<p class="summary">
							This is where your summary text will go, a short intro about the story so people can see what they&#039;re about to read. ...						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group local-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/local-article/this-is-another-news-article">
						<span class="imgContainer">
							<img class="img100" src="http://development.toyota.ie/images/uploads/headers/_preview/image1.jpg" alt="
								 This is another News article">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn event">
								
								Dealer
								&nbsp;event 11
							</span>
							This is another News article						</h3>
						<p class="summary">
							This is another news article						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group local-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/local-article/art-test-image-size">
						<span class="imgContainer">
							<img class="img100" src="http://development.toyota.ie/images/uploads/headers/_preview/test-image-size.jpg" alt="
								 Test Image size">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								
								Dealer
								&nbsp;news 12
							</span>
							Test Image size						</h3>
						<p class="summary">
							test-image size						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group local-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/local-article/art-my-news-article">
						<span class="imgContainer">
							<img class="img100" src="http://development.toyota.ie/images/uploads/headers/_preview/image1-sara.jpg" alt="
								 my news article">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								
								Dealer
								&nbsp;news 13
							</span>
							my news article						</h3>
						<p class="summary">
							Test						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group toyota-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/toyota-article/world-of-toyota/articles-news-events/2014/hybrid-experiment.json">
						<span class="imgContainer">
							<img class="img100" src="https://s3-eu-west-1.amazonaws.com/tme-toyotaone-prev/toyotaone/glen/toyota-news-2014-hybrid-experiment-spotlight_tcm-10-140169.jpg" alt="
								 The Hybrid Experiment">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								Toyota
								
								&nbsp;news 14
							</span>
							The Hybrid Experiment						</h3>
						<p class="summary">
							Putting Toyota Hybrids to the test						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group toyota-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/toyota-article/world-of-toyota/articles-news-events/2014/mewe-design-award.json">
						<span class="imgContainer">
							<img class="img100" src="https://s3-eu-west-1.amazonaws.com/tme-toyotaone-prev/toyotaone/glen/toyota-news-2014-mewe-design-award-spotlight_tcm-10-140154.jpg" alt="
								 Toyota ME.WE nominated for 2014 Designs of the Year Awards">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								Toyota
								
								&nbsp;news 15
							</span>
							Toyota ME.WE nominated for 2014 Designs of the Year Awards						</h3>
						<p class="summary">
							We are delighted to announce that our visionary TOYOTA ME.WE concept car has been nominated for the “Designs of the Year ...						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group toyota-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/toyota-article/world-of-toyota/articles-news-events/2014/toyota-touch-2-how-to-videos.json">
						<span class="imgContainer">
							<img class="img100" src="https://s3-eu-west-1.amazonaws.com/tme-toyotaone-prev/toyotaone/glen/toyota-news-2014-toyota-touch-2-how-to-spotlight_tcm-10-219645.jpg" alt="
								 Toyota Touch 2® - How-to videos">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								Toyota
								
								&nbsp;news 16
							</span>
							Toyota Touch 2® - How-to videos						</h3>
						<p class="summary">
							Whether you own a Toyota equipped with Toyota Touch®2 or would like to see what our multimedia system has to offer, we want ...						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group toyota-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/toyota-article/world-of-toyota/articles-news-events/2014/aygo-2014-discovery.json">
						<span class="imgContainer">
							<img class="img100" src="https://s3-eu-west-1.amazonaws.com/tme-toyotaone-prev/toyotaone/glen/toyota-news-2014-aygo-discovery-spotlight_tcm-10-139998.jpg" alt="
								 An interactive tour of new AYGO">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								Toyota
								
								&nbsp;news 17
							</span>
							An interactive tour of new AYGO						</h3>
						<p class="summary">
							 ...						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group toyota-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/toyota-article/world-of-toyota/articles-news-events/2014/aygo-2014-reveal.json">
						<span class="imgContainer">
							<img class="img100" src="https://s3-eu-west-1.amazonaws.com/tme-toyotaone-prev/toyotaone/glen/toyota-news-2014-aygo-reveal-spotlight_tcm-10-219683.jpg" alt="
								 All-new AYGO. Launch from a different perspective?">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								Toyota
								
								&nbsp;news 18
							</span>
							All-new AYGO. Launch from a different perspective?						</h3>
						<p class="summary">
							Each year the world’s car manufacturers show off their latest creations at the premier motor shows. Launched with great ...						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group toyota-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/toyota-article/world-of-toyota/articles-news-events/2014/rav4-anniversary.json">
						<span class="imgContainer">
							<img class="img100" src="https://s3-eu-west-1.amazonaws.com/tme-toyotaone-prev/toyotaone/glen/toyota-news-2014-rav4-anniversary-spotlight_tcm-10-219675.jpg" alt="
								 RAV4 20th Anniversary">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								Toyota
								
								&nbsp;news 19
							</span>
							RAV4 20th Anniversary						</h3>
						<p class="summary">
							In 1989, at the Tokyo Motor show, Toyota displayed a brand new concept car to the world and from that day the Recreational ...						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group local-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/toyota-article/world-of-toyota/articles-news-events/2014/highlander-2013-movie.json">
						<span class="imgContainer">
							<img class="img100" src="https://s3-eu-west-1.amazonaws.com/tme-toyotaone-prev/toyotaone/glen/toyota-news-2014-highlander-spotlight_tcm-10-220200.jpg" alt="
								 Toyota Highlander">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								Dealer
								
								&nbsp;news 20
							</span>
							Toyota Highlander						</h3>
						<p class="summary">
							The older you get, the less time you seem to have. Those carefree days of youth, where days went on forever and boredom was ...						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group local-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/toyota-article/world-of-toyota/articles-news-events/2014/gt86-gran-turismo.json">
						<span class="imgContainer">
							<img class="img100" src="https://s3-eu-west-1.amazonaws.com/tme-toyotaone-prev/toyotaone/glen/toyota-news-2013-gt86-gt-spotlight_tcm-10-220197.jpg" alt="
								 Toyota GT86 adds real-life fun to the launch of Gran Turismo® 6">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								Dealer
								
								&nbsp;news 21
							</span>
							Toyota GT86 adds real-life fun to the launch of Gran Turismo® 6						</h3>
						<p class="summary">
							 Toyota participated in the international launch of the Gran Turismo® 6 driving simulator for the Sony PlayStation® 3 console. ...						</p>
					</a>
				</article>
			</div>
		</div>

		<div class="grid_3 fc-group local-group">
			<div class="fc-content clearfix">
				<article class="item offer-spotlight news">
					<a href="/pages/news/toyota-article/world-of-toyota/articles-news-events/2014/40-million-corolla.json">
						<span class="imgContainer">
							<img class="img100" src="https://s3-eu-west-1.amazonaws.com/tme-toyotaone-prev/toyotaone/glen/toyota-news-2013-40-millions-corolla-spotlight_tcm-10-124044.jpg" alt="
								 Toyota Corolla: World’s Most Popular Car">
						</span>
						<h3>
							<span class="uiBtn redBtn smBtn news">
								Dealer
								
								&nbsp;news 22
							</span>
							Toyota Corolla: World’s Most Popular Car						</h3>
						<p class="summary">
							World Sales Leader with More Than 40 Million Sold.						</p>
					</a>
				</article>
			</div>
		</div>
