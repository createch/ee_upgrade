<?php

class Ajax {

	public static function route($method,$action,$func){
		$methodType = $_SERVER['REQUEST_METHOD'];
		if($method==$methodType||$method=='ALL'){
			$uri = explode("/", substr(@$_SERVER['PATH_INFO'], 1));
			$route = explode("/",$action);

			if(Ajax::checkRoute($uri,$route)){
				$vars=Ajax::getRouteVars($uri,$route);
				$func($vars);
			}
		}
	}

	public static function checkRoute($uri,$route){
		$pass=true;
		for($q=0;$q<sizeof($route);$q++){
			if($uri[$q]){
				if($uri[$q]!=$route[$q]&&substr($route[$q],0,1)!=':'||$route[$q]==':'){
					$pass=false;
				}
			}
		}
		return $pass;
	}

	public static function getRouteVars($uri,$route){
		$vars=$_REQUEST;

		for($q=0;$q<sizeof($route);$q++){
			if($uri[$q]){
				if(substr($route[$q],0,1)==':'&&$route[$q]!=':'){
					$vars[str_replace(':','',$route[$q])]=$uri[$q];
				}
			}
		}
		return $vars;
	}

}