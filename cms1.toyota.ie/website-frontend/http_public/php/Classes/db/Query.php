<?php
class Query{
	public static function sql($table,$vars=NULL,$orderby=NULL,$limit=NULL){
		if($fields=Query::getColumns($table)){
		
			$queryResults=array();
			if($vars){
				$keys=array_keys($vars);
			}
			$sql='SELECT * FROM '.$table;
			for($q=0;$q<sizeof($vars);$q++){
				if($q==0){ $sql.=' WHERE'; }
				else { $sql.=" AND"; }
				$sql.=" ".mysql_real_escape_string($keys[$q])."='".mysql_real_escape_string($vars[$keys[$q]])."'";
			}
			if($orderby){ $sql.=' ORDER BY '.$orderby.' DESC'; }
			if($limit){ $sql.=' LIMIT '.$limit; }
			$results=mysql_query($sql);
			while($row=mysql_fetch_assoc($results)){
				$obj=new $table();
				foreach($fields as $f){
					$obj->set(array($f=>$row[$f]));	
				}
				$queryResults[]=$obj;
			}
			return $queryResults;
		} else {
			return false;
		}
	}
	
	public static function sqlCount($table,$vars=NULL){
		$fields=Query::getColumns($table);
		
		$queryResults=array();
		if($vars){
			$keys=array_keys($vars);
		}
		$sql='SELECT * FROM '.$table;
		for($q=0;$q<sizeof($vars);$q++){
			if($q==0){ $sql.=' WHERE'; }
			else { $sql.=" AND"; }
			$sql.=" ".mysql_real_escape_string($keys[$q])."='".mysql_real_escape_string($vars[$keys[$q]])."'";
		}
		$results=mysql_query($sql);
		$sqlCount=mysql_num_rows($results);
		return $sqlCount;
	}
	
	public static function getColumns($table){
		$sql='SHOW COLUMNS FROM '.$table;
		$fields=array();
		$results=mysql_query($sql);
		if($results!==FALSE){
			while($row=mysql_fetch_assoc($results)){
				$fields[]=$row['Field'];
			}
			return $fields;
		} else {
			return false;
		}
	}
}