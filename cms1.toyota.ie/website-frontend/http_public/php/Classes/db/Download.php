<?php
	class Download {
		
		public function xls($db_data,$fields,$appName){
			$line = '';
			foreach($fields as $field){
				if((!isset($field))||($field=="")){
					$field="\t"; 
				} else {
					$field=str_replace('"','""',$field)."\t";
				}
				$line.=$field;
			}
			$data.=trim($line)."\n";
			
			//
			foreach($db_data as $row){
				$line = '';
				foreach($fields as $field){
					$value=$row->get($field);
					if((!isset($value))||($value=="")){
						$value="\t"; 
					} else {
						$value=str_replace('"','""',$value);
						$value='"'.$value.'"'."\t"; 
					}	
					$line.=$value;
				}				
				$data.=trim($line)."\n";	
			}
			//
			$data = str_replace("\r","",$data);
			$datestamp = date("Ymd");
			$appName = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $appName);
			$appName = strtolower(trim($appName, '_'));
			$appName = preg_replace("/[\/_|+ -]+/", '_', $appName);

			header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=".$appName."_entries_".$datestamp.".xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			print $data;
			exit;	
		}
		
	}
