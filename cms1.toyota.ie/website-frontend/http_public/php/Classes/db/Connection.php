<?php
	class Connection {
		
		private $server,$user,$pass,$db;
		private $status=false;
		
		public function __construct($new_server,$new_user,$new_pass){
			$this->server=$new_server;
			$this->user=$new_user;
			$this->pass=$new_pass;
			$this->connect();
		}
		
		public function connect(){
			if(mysql_connect($this->server,$this->user,$this->pass)){
				$this->status=true;
				return true;
			} else {
				return false;	
			}
		}
		
		public function connected(){
			if($this->status){ return true;}
			else { return false; }
		}
		
		public function getStatus(){
			if($this->status){ return "Connected"; }
			else { return "Not Connected"; }	
		}
		
	}
?>