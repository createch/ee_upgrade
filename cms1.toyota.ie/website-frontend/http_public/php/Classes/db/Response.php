<?php
class Response {

	public function __construct(){
		$this->response=$this->success('No action called',array());
	}

	public function success($message,$data){
		$this->response=array(
				'status'=>'success',
				'message'=>$message,
				'data'=>$data
			);
	}

	public function error($message,$data){
		$this->response=array(
				'status'=>'error',
				'message'=>$message,
				'data'=>$data
			);
	}

	public function output(){
		echo json_encode($this->response);
	}
}