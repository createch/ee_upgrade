<?php
	class FB extends Facebook {
		
		protected $appURL,$localAppURL;
		private $postTitle,$postText,$shareText,$fbPage=NULL;
		private $appData;
		public $liked=false;
		public $signed_request;
		
		public function __construct($config) {
			$config['cookie']=true;
			parent::__construct($config);
			$this->appURL=$config['appURL'];
			$this->signed_request=$this->parse_signed_request($_REQUEST['signed_request'],$config['secret']);
			$this->liked();
			$this->share($config);
			$this->appData=$this->signed_request['app_data'];
		}
		
		private function parse_signed_request($signed_request, $secret){
			list($encoded_sig, $payload) = explode('.', $signed_request, 2);
			// decode the data
			$sig = $this->base64_url_decode($encoded_sig);
			$data = json_decode($this->base64_url_decode($payload), true);
			if(strtoupper($data['algorithm']) !== 'HMAC-SHA256'){
				error_log('Unknown algorithm. Expected HMAC-SHA256');
				return null;
			}
			// check sig
			$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
			if ($sig !== $expected_sig) {
				error_log('Bad Signed JSON signature!');
				return null;
			}
			return $data;
		}
		
		private function base64_url_decode($input) {
			return base64_decode(strtr($input, '-_', '+/'));
		}
		
		public function setLocal($url){
			$this->localAppURL=$url;
		}
		
		public function liked(){
			if($this->signed_request["page"]["id"]&&$this->signed_request["page"]["liked"]){
				$this->liked=true;	
			} else {
				$this->liked=false;	
			}
		}
		
		public function getData(){
			return $this->appData;
		}
		
		public function share($share){
			$this->postTitle=$share['post_title'];
			$this->postText=$share['post_text'];
			$this->shareText=$share['share_text'];
			$this->fbPage=$share['fb_page'];
		}
		
		public function getProtocol(){
			return '//';
		}
		
		public function root($prot=true){
			if(strstr(file_exists($_SERVER['SERVER_NAME']),"localhost")){
				if($prot){ return $this->getProtocol().$this->localAppURL; }
				else { return $this->localAppURL; }
			} else {
				if($prot){ return $this->getProtocol().$this->appURL; }
				else { return $this->appURL; }
			}
		}
	}
?>