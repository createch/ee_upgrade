<?php

/*
  ######## AUTOMATIC GENERATED SECTION ########
  - Never modify the first sentence in the code $json ...
  - This sentence is chopped and the json is extracted by other sotware.
  - No other sentence must go before this one.
 */

$json = '{
	"app": {
		"name": "framewerk",
		"client": "www"
	},
	"database": {
		"server": "localhost",
		"db": "apps_www_framewerk",
		"user": "www_framewerk",
		"pass": "7IOijbLxe4mm"
	},
	"facebook": {
		"appId": "0000000000",
		"secret": "00000000000000",
		"appURL": "rdappshost.com/www/framewerk",
		"post_title": "SHARE_TITLE",
		"post_text": "SHARE_TEXT",
		"share_text": "SHARE_TEXT",
		"fb_page": "FB_PAGE"
	},
	"admin": {
		"username": "admin",
		"password": "ymKQ942jk4t8"
	},
	"api": {
		"model": {
			"write": [],
			"public": [],
			"admin": [
				"id",
				"timestamp",
				"first_name"
			]
		}
	}
}';
/*
  ######## END OF AUTOMATIC GENERATED SECTION ########
 */



$config = json_decode($json, true);

require 'php/Classes/mustache-2.6.1/src/Mustache/Autoloader.php';
Mustache_Autoloader::register();

function framewerkAutoload($classname) {
    $path = "php/Classes/";
    $results = scandir($path);
    foreach ($results as $result) {
        if (is_dir($path . $result)) {
            if (file_exists($path . $result . "/" . $classname . ".php")) {
                $path = $path . $result . "/";
            }
        }
    }
    include($path . $classname . ".php");
}


spl_autoload_register('framewerkAutoload');
