<?php header('Access-Control-Allow-Origin: *'); ?>

<!DOCTYPE html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Toyota</title>
        <meta name="description" content="">
        <meta name="viewport" content="initial-scale=1.0">
        <!-- favicon -->
        <link rel="icon" type="image/x-icon" href="favicon.ico">
        <!-- CSS files -->
        <link rel="stylesheet" href="css/style.css">
		<style>
			h1.container_12{
				color:#f83;
				margin-top: 2em !important;
				margin-bottom: 1em !important;
				text-align: center;
				text-transform: uppercase;
			}
			.pages{
				margin: 3em 0;
			}
			.pages li{
				display: block;
				height: 55px;

			}
			.show{
				color:#f83;
				margin-top: 2em !important;
				margin-bottom: 1em !important;
			}
		</style>
		<!--[if lt IE 9]>      <link rel="stylesheet" href="css/ie8.css"> <![endif]-->
		<!--[if lt IE 8]>      <link rel="stylesheet" href="css/ie7.css"> <![endif]-->
		<script src="js/modernizr.js"></script>
    </head>
    <body>
		<div id="fb-root"></div>
        <!--[if lt IE 7]>
            <p class="browserwarn">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> </p>
        <![endif]-->

        <!-- ### 1.1 Primary navigation position ### -->
		<?php include('html/nav/primary.html'); ?>
		<?php include('html/nav/secondary.html'); ?>
        <div class="outer-wrapper">

            <div class="master-wrapper">

                <!-- ### 1.2 Header component position ### fx-header class? -->
                <header class="header-container"></header>

                <div class="inner-wrapper isoForceHardwardCalc">

                    <section>
                        <!-- ### 1.3 Teir 1 component(s) - Organised into multiple section elements on loger pages as required ### -->

						<!-- #################### COMPONENTS ######################### -->

						<?php
						if ($_GET['page']) {
							include './pages/' . $_GET['page'] . ".php";
						} else {
							?>
							<h1 class="container_12">Components Showcase.</h1>
							<div class="container_12 center-text">
								<div class="grid-12">
									<a href="./?page=headers" class="uiBtn">Headers</a>
									<a href="./?page=top" class="uiBtn">Top Features</a>
									<a href="./?page=car" class="uiBtn">Car & Gallery</a>
									<a href="./?page=articles" class="uiBtn">Articles</a>
									<a href="./?page=spot" class="uiBtn">Spotlight</a>
									<a href="./?page=elems" class="uiBtn">Elements</a>
									<a href="./?page=footers" class="uiBtn">Footer</a>
									<a href="./?page=forms" class="uiBtn">Forms</a>
									<a href="./?page=search" class="uiBtn">Search</a>
									<a href="./?page=news" class="uiBtn">News</a>
									<a href="./?page=dealerslanding" class="uiBtn">landing</a>
									<a href="./?page=articlecarousel" class="uiBtn">Article Carousel</a>
								</div>
							</div>
							<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
							<?php
						}
						?>





					</section>

				</div>
			
			<!-- ### 1.4 Footer component position ### -->
			<footer >
				<?php include('html/footer/large.html'); ?>
			</footer>

</div>

        </div>
		<script src="js/libs.js"></script>
        <script src="js/main.js"></script>
		<!--[if lt IE 9]> <script src="js/ie8.js"></script> <![endif]-->
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "beeac92e-6cdb-46cc-9d56-876fd7dab5d1", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
		<script>(function (d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id))
					return;
				js = d.createElement(s);
				js.id = id;
				js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
	</body>
</html>
