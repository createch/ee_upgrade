

<!DOCTYPE html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Toyota</title>
        <meta name="description" content="">
        <meta name="viewport" content="initial-scale=1.0">
        <!-- favicon -->
        <link rel="icon" type="image/x-icon" href="favicon.ico">
        <!-- CSS files -->
        <link rel="stylesheet" href="css/style.css">
		<style>
			h1.container_12{
				color:#f83;
				margin-top: 2em !important;
				margin-bottom: 1em !important;
				text-align: center;
				text-transform: uppercase;
			}
			.pages{
				margin: 3em 0;
			}
			.pages li{
				display: block;
				height: 55px;

			}
			.show{
				color:#f83;
				margin-top: 2em !important;
				margin-bottom: 1em !important;
			}
		</style>
		<!--[if lt IE 9]>      <link rel="stylesheet" href="css/ie8.css"> <![endif]-->
		<!--[if lt IE 8]>      <link rel="stylesheet" href="css/ie7.css"> <![endif]-->
		<script src="js/modernizr.js"></script>
    </head>
    <body>
        <iframe src="http://t1-production-ie.herokuapp.com/carconfig/prius"/>
    </body>
</html>