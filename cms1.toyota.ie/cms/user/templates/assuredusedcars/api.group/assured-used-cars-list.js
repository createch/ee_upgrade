<?php
	$url="http://toyotasurvey.force.com/auc/AUC_Used_Car_List_Page";

	function readPage($url){

		$page=json_decode(trim(@file_get_contents($url)));


		$items=$page[0]->Cars;

		$aux=array();

		foreach ($items as $item) {
			
			$url='http://toyotasurvey.force.com/auc/AUC_Car_Details?carid='.$item->carID;

			$model=json_decode(trim(@file_get_contents($url)));

			$model[0]->LastModifiedDate=date('Y-m-d h:m:s',strtotime($model[0]->LastModifiedDate));
			$model[0]->LastModifiedDate=str_replace(' ', 'T', $model[0]->LastModifiedDate).'Z';

			$aux[]=$model[0];

		}

		$cars=json_encode($aux);

		$data_string = json_encode($cars);

		$verbose = fopen('php://temp', 'rw+');                                                                               
		 
		$ch = curl_init('http://84.51.229.199:8080/solr/used-cars/update/json');                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_STDERR, $verbose);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
		 
		$result = curl_exec($ch);

		rewind($verbose);
		$verboseLog = stream_get_contents($verbose);

		print_r($verboseLog);
		print_r($result);

		if($page[0]->NextPageURL!='LastPage') {

			readPage($page[0]->NextPageURL);
		}

	}

	readPage($url);
?>
