{
	"content":[{
		"meta":"",
		"main":{exp:json:entries channel="home_page"},
		"generic_settings":{exp:json:entries channel="generic_settings"},
		"model_range":{exp:json_feed:print_feed
			src="{external_feed_base}/page/models/index.json?cache_problem=1234e12213"
			src_custom="{insecure_site_url}api/custom_cars_feed"
			section="modelrange"
		},
		"hybrid":[{exp:json_feed:print_feed src='{external_feed_base}/page/index.json' key='id' val="hybrid-spotlights"}],
		"news":[{"news":{
			"title":"Latest News",
			"items":{exp:json_feed:print_feed src='{insecure_site_url}api/news'}}
		}]
	}]
}