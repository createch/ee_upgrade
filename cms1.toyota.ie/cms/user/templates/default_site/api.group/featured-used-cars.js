<?php
$segment_3 = "{segment_3}";
$delaer_id = "{exp:json_feed:globlaVariable name='dealer_id'}";


$url = ($segment_3)
    ?"http://toyotasurvey.force.com/auc/AUC_Used_Car_List_Page?dealerid=$segment_3"
    :"http://toyotasurvey.force.com/auc/AUC_Used_Car_List_Page?dealerid=$delaer_id";


if($delaer_id == "9999"){
    $url = "http://developmenttest.toyota.ie/inc/test_data/dealer_cars.json";
}

$json=json_decode(file_get_contents($url));

function isfeatured(&$item)
{
	return $item->featured=='1';
}

$featured=array_filter($json[0]->Cars,'isfeatured');


$only_4 = array_slice($featured,0,4);

echo json_encode($only_4);

?>