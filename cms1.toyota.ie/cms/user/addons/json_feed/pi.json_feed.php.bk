<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$plugin_info = array(
						'pi_name'			=> 'JSON Feed',
						'pi_version'		=> '1.0',
						'pi_author'			=> 'Salvo Vaccarino',
						'pi_author_url'		=> 'http://radical.ie/',
						'pi_description'	=> 'Json Feed plugin.',
						'pi_usage'			=> Json_feed::usage()
					);

define("JSON_FEEDS_FOLDER", BASEPATH."/feeds/");

/**
 * Json_feed Class
 *
 * @package			ExpressionEngine
 * @category		Plugin
 * @author			Salvo Vaccarino
 * @copyright		Copyright (c), Radical
 * @link			http://radical.ie
 */

require 'Handlebars/Autoloader.php';
Handlebars\Autoloader::register();
use Handlebars\Handlebars;


class Json_feed {

	public $return_data="";
	public $feed_url;
	public $feed_content="";
	public $handlebars=NULL;
	public $temp;
	public $level=0;
	public $current_node=array();
	public $page_uri;

	/**
	 * Constructor
	 *
	 */
	public function __construct()
	{

		
		$this->EE =& get_instance();

		$this->EE->load->library('logger');

		$this->page_uri= $this->EE->uri->uri_string();

		if($this->temp=$this->EE->TMPL->fetch_param('template')){

			$options =  array('extension' => '.html');

			list($tgroup,$tname)=explode("/",$this->temp);

			$this->handlebars = new Handlebars(array(
	    		'loader'=> new \Handlebars\Loader\FilesystemLoader(dirname(__FILE__)."/../../templates/default_site/".$tgroup.'.group/',$options),
	    		'partials_loader'=> new \Handlebars\Loader\FilesystemLoader(dirname(__FILE__)."/../../templates/default_site/components.group/",$options)
	    	));
		}

		if($this->EE->TMPL->fetch_param('src')!=""){
			$this->feed_url=str_replace(' ','',$this->EE->TMPL->fetch_param('src'));

			if($this->EE->TMPL->fetch_param('clean_url')=='yes'){
				if($this->parse_url($this->feed_url)!=false){
					$this->feed_url=$this->parse_url($this->feed_url);
					$this->return_data=$this->get_feed();
				}else{
					$this->return_data='';
				}
			}else{
				$this->return_data=$this->get_feed();
			}
			//Getting the Feed
			
		}
		elseif($this->EE->TMPL->tagdata=="") $this->return_data="{ 'status':'error', 'function':'__construct', 'message':'data not specified'}";
	}

	/**
	* Get Feed
	*
	* Plugin Json_feed
	*
	* @access	public
	*/
	function get_feed()
	{

		$this->EE =& get_instance();

		$url_components=parse_url($this->feed_url);
		$filepath=$url_components['path'];

		$cached=$this->EE->cache->get($filepath);

		$no_cache=$this->EE->TMPL->fetch_param('no_cache');

		//if($no_cache==true || !$cached){

		try{

			$this->feed_content = trim(@file_get_contents($this->feed_url));

		}catch(Exception $e){

			//$this->EE->logger->developer('Function: get_feed() - Page:'.$this->page_uri.' - Error: Failed trying to get content from: '.$this->feed_url);
			
			return "Error: Not Found!";
		}

		//}

		if(!$this->feed_content)
		{
			//Logs the error in the curl execution in the developer log

			$this->feed_content = trim($this->EE->cache->get($filepath));
			if(!empty($this->feed_content)){
				return $this->feed_content;
			}else{
				
				//$this->EE->logger->developer('Function: get_feed() - Page:'.$this->page_uri.' - Error: feed '.$this->feed_url.' not found either remotely or in cache . File path:'.$filepath);
				
				return "Error: Not Found!";
			}
		}else{

			//check if is a valid json
			$p=json_decode($this->feed_content);

            if(!is_dir($filepath) && $p!=NULL){
            	//check if the file has extension json
            	$filepath=(strpos($filepath,".json")=== false)?$filepath.".json":$filepath;

				if(!$this->EE->cache->save( $filepath, $this->feed_content, 3600)) {
					
					//$this->EE->logger->developer('Function: get_feed() - Page:'.$this->page_uri.' - Error: failed writing: '.$filepath.' on cache');
					
					return false;
				
				}else{
					
					return $this->feed_content;
				}
			}else{

				return "Error: Not Found!";
			}		
		}

	}

	/**
	* parse url
	*
	* Plugin Json Feed
	*
	* @access	public
	* @return	string
	*/
	public function parse_url($url) {

		if( strpos($url, "offers/toyota-article")!==false || strpos($url, "news/toyota-article")!==false || strpos($url, "hybrid/hybrid-innovation")!==false || strpos($url, "pages/hybrid")!==false || strpos($url, "pages/new-car-details")!==false ){
        
	        $url=str_replace("offers/toyota-article","article",$url);

	        $url=str_replace("news/toyota-article","article",$url);

	        $url=str_replace("hybrid/hybrid-innovation","hybrid-subpages/hybrid-innovation",$url);
	        
	        $url=str_replace("/pages/","/api/",$url);

	        $url=str_replace("index.php","",$url);

	        return $url;

    	}else{

    		return false;
    	}

    }


	/**
	* Store html components: not sure yet
	*
	* Plugin Store Html Components
	*
	* @access	public
	* @return	bool
	*/
	function store_html_components($component){

	}

	/**
	* Parse php : parses any php passed trough the template
	*
	* Plugin Json_feed
	*
	* @access	private
	* @return	string
	*/

	function parse_php(){
		if($this->EE->TMPL->tagdata!=""){	
			eval($this->EE->TMPL->tagdata);
		}
	}


	/**
	* Render html : render a the feed provided using a specific template
	*
	* Plugin Parse Feed
	*
	* @access	public
	* @return	string
	*/

	function render_html(){

		$tagdata=trim($this->EE->TMPL->tagdata);

		if(!empty($this->feed_content) || !empty($tagdata)){
			
			if(!empty($this->feed_content)) $parsed_content=json_decode($this->feed_content,3);
			elseif(!empty($tagdata)) $parsed_content=json_decode($tagdata,3);


			$caller=$this->EE->TMPL->fetch_param('caller');

			if($this->json_errors()) {
				$this->EE->logger->developer('Function render_html() - Page:'.$this->page_uri.' - caller:'.$caller.' content: '.$this->dump_var($this->feed_content)); 
				return false;
			}else{
				$section=$this->EE->TMPL->fetch_param('section');
				$key=$this->EE->TMPL->fetch_param('key');
				$val=$this->EE->TMPL->fetch_param('val');
				try{

					if($this->temp==""){
						//$this->EE->logger->developer('Function: render_html() - Page:'.$this->page_uri.' - Error:Template not defined');
						return false;
					}else{

						list($tgroup,$tname)=explode("/",$this->temp);
						$content=$parsed_content;
						if($section!=''){
							if(($found=$this->search_section($section,$content))!==false){
								$content=array($section=>$found);	
							}else{
								//$this->EE->logger->developer('Function: render_html() - Page:'.$this->page_uri.' - Error:Section '.$section.' not found in '.$this->feed_url);
								return false;
							}
						}

						if($key!='' && $val!=''){
							if(($found=$this->search_by_id($key,$val,$content))!==false){
								$content=array($val=>$found);	
							}else{
								//$this->EE->logger->developer('Function: render_html() - Page:'.$this->page_uri.' - Error: Section containing '.$key.'='.$val.' not found');
								return false;
							}
						}

						$result=$this->handlebars->render($tname,$content);
					}
					return $result;

				}catch(Exception $e){
					echo "Error: ".$e->getMessage();
				}
			}
		}else{
			$noredirect=$this->EE->TMPL->fetch_param('no_redirect');
			if($noredirect!='yes') return '<script type="text/javascript">window.location.href="/404"</script>';
		}
	}


	/**
	* Render Section : render a specific section (array) using the template specified (tgroup/tname)
	*
	* Plugin Parse Feed
	*
	* @access	public
	* @return	string
	*/
	function render_section(){
		return $this->EE->TMPL->fetch_param('template');
	}

	/**
	* Print feed 
	*
	* Plugin Parse Feed
	*
	* @access	public
	* @return	string
	*/

	function print_feed(){

		if(!empty($this->feed_content)){

			$content=json_decode($this->feed_content,3);

			$caller=$this->EE->TMPL->fetch_param('caller');

			if($this->json_errors()) {
				//$this->EE->logger->developer('Function print_feed() - Page:'.$this->page_uri.' caller:'.$caller.'-  content: '.empty($this->feed_content)); 
				return false;
			}else{
				$section=$this->EE->TMPL->fetch_param('section');
				if($section!=''){
					if(($found=$this->search_section($section,$content))!==false){
						$content=array($section=>$found);	
					}else{
						//$this->EE->logger->developer('Function: print_feed() - Page:'.$this->page_uri.' -  Error: Section '.$section.' not found');
						return false;
					}
				}

				$key=$this->EE->TMPL->fetch_param('key');
				$val=$this->EE->TMPL->fetch_param('val');

				if($key!='' && $val!=''){

					if(($found=$this->search_by_id($key,$val,$content))!==false){

						$content=array($val=>$found);

					}else{

						//$this->EE->logger->developer('Function: print_feed() - Page:'.$this->page_uri.' -  Error: Section containing '.$key.'='.$val.' not found');
						return false;

					}

				}

				return str_replace("'", "&#039;", json_encode($content));
			}
		}
	}


	/**
	* Search section: search the array provided in $arrat and returns the section specified in $section  or false if not found.
	*
	* Plugin Parse Feed
	*
	* @access	public
	* @return	string
	*/

	function search_section($section,$array){

		if(count($array)){
			foreach($array as $nodename => $nodevalue){
				if($nodename==$section && $nodename!='0'){
					$this->current_node[$this->level]=$nodevalue;
					return $this->current_node[$this->level];
				}
				if(is_array($nodevalue)){
					$this->level+=1;
					if(($this->current_node[$this->level]=$this->search_section($section,$nodevalue)) !== false){
						return $this->current_node[$this->level];
					}
				}
			}
		}
		return false;
	}

	/**
	* Search by parameter: search the array provided in $array and returns the section containing the specified Key=>value couple.
	*
	* Plugin Parse Feed
	*
	* @access	public
	* @return	string
	*/

	function search_by_id($key,$val,$array){
		if(count($array)){
			foreach($array as $nodename => $nodevalue){
				if(is_array($nodevalue) && array_key_exists($key,$nodevalue) && $nodevalue[$key]==$val){
					$this->current_node[$this->level]=$nodevalue;
					return $this->current_node[$this->level];
				}
				if(is_array($nodevalue)){
					$this->level+=1;
					if(($this->current_node[$this->level]=$this->search_by_id($key,$val,$nodevalue)) !== false){
						return $this->current_node[$this->level];
					}
				}
			}
		}
		return false;
	}

	

	/**
	* Dump Var
	*
	* Plugin Parse Feed
	*
	* @access	public
	* @return	string
	*/


	function dump_var($var){

		ob_start();

		var_dump($var);

		$buffer = ob_get_clean();

		return $buffer;

	}

	/**
	*
	* Json Errors
	*
	* @access	public
	* @return	void
	*/


	function json_errors(){

		$error=false;
		
		switch (json_last_error()) {
	        case JSON_ERROR_DEPTH:
	        	$error=true;
	            //$this->EE->logger->developer('Function: json_errors() - Page:'.$this->page_uri.' - Error: Maximum stack depth exceeded');
	        break;
	        case JSON_ERROR_STATE_MISMATCH:
	       		$error=true;
	            //$this->EE->logger->developer('Function: json_errors() - Page:'.$this->page_uri.' - Error: Underflow or the modes mismatch');
	        break;
	        case JSON_ERROR_CTRL_CHAR:
	            $error=true;
	            //$this->EE->logger->developer('Function: json_errors() - Page:'.$this->page_uri.' - Error: Unexpected control character found');
	        break;
	        case JSON_ERROR_SYNTAX:
	            $error=true;
	            //$this->EE->logger->developer('Function: json_errors() - Page:'.$this->page_uri.' - Error: Syntax error, malformed JSON');
	        break;
	        case JSON_ERROR_UTF8:
	            $error=true;
	            //$this->EE->logger->developer('Function: json_errors() - Page:'.$this->page_uri.' - Error: Malformed UTF-8 characters, possibly incorrectly encoded');
	        break;
    	}

    	return $error;

	}

	// --------------------------------------------------------------------

	/**
	 * Usage
	 *
	 * Plugin Usage
	 *
	 * @access	public
	 * @return	string
	 */
	function usage()
	{
		ob_start();
		?>
			This plugin is used to get remote Json feed, store it locally, parse it and returning results.  It is used in templates that have to display remote content.

			To use this plugin in the template that needs to contain a feed write:

			{exp:json_feed src='http://thejsonfeed'}

			{/exp:json_feed}

			Version 1.0

		<?php
		$buffer = ob_get_contents();

		ob_end_clean();

		return $buffer;
	}

	// --------------------------------------------------------------------

}
// END CLASS

/* End of file pi.json_feed.php */
/* Location: /system/expressionengine/third_party/json_feed/pi.json_feed.php */