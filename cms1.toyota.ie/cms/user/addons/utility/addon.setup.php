<?php 

return array(
	'author'      => 'Lea Hayes',
    'author_url'  => 'http://leahayes.co.uk',
    'name'        => 'Utility Functions',
    'description' => 'A selection of utility functions for use in templates.',
    'version'     => '1.0.1',
    'namespace'   => 'util\utility'

/*
    'pi_name'        => 'Utility Functions',
    'pi_version'     => '1.0.0',
    'pi_author'      => 'Lea Hayes',
    'pi_author_url'  => 'http://leahayes.co.uk',
    'pi_description' => 'A selection of utility functions for use in templates.',
    'pi_usage'       => utility::usage() */
);