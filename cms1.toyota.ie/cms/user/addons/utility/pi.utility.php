<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class utility {

    public function htmlspecialchars() {
        $this->EE = &get_instance();
        return htmlspecialchars($this->EE->TMPL->tagdata);
    }

    public function removequotes() {
        $this->EE = &get_instance();
        return str_replace("'","\'",$this->EE->TMPL->tagdata);
    }

    public function removedoublequotes() {
        $this->EE = &get_instance();
        return str_replace('"','\"',$this->EE->TMPL->tagdata);
    }

    public static function usage() {
        return '{exp:util:htmlspecialchars}Place unescaped content here...{/exp:util:htmlspecialchars}';
    }

}
/* End of file pi.utility.php */
/* Location: ./system/user/addons/utility/pi.utility.php */
