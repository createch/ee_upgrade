<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Update description
 *
 * @package             Gmaps for EE3
 * @author              Rein de Vries (info@reinos.nl)
 * @copyright           Copyright (c) 2015 Rein de Vries
 * @license  			http://reinos.nl/add-ons/commercial-license
 * @link                http://reinos.nl/add-ons/gmaps
 */
 
include(PATH_THIRD.'gmaps_fieldtype/config.php');
 
class Gmaps_ft_upd_343
{
	private $EE;
	private $version = '3.4.3';
	
	// ----------------------------------------------------------------

	/**
	 * Construct method
	 *
	 * @return      boolean         TRUE
	 */
	public function __construct()
	{		
		//load the classes
		ee()->load->dbforge();

		//require the settings
		require PATH_THIRD.'gmaps/settings.php';
	}

	// ----------------------------------------------------------------
	
	/**
	 * Run the update
	 *
	 * @return      boolean         TRUE
	 */
	public function run_update()
	{
        $sql = array();

        //change the lat and lng type in the DB from float 10,6 to varchar 50
        $sql[] = "ALTER TABLE `exp_gmaps_fieldtype_migration` ADD `entry_id` INT(7) NOT NULL AFTER `migration_id`;";
        $sql[] = "ALTER TABLE `exp_gmaps_fieldtype_migration` CHANGE `entry_id` `entry_id` INT(7) UNSIGNED NOT NULL DEFAULT '0';";

        foreach ($sql as $query)
        {
            ee()->db->query($query);
        }

	}
}