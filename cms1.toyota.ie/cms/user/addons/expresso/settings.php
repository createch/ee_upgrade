<?php
// define settings
$this->ckeditor_version = '4.5.10';
$this->ckeditor_skin = 'flat';
$this->extra_link_modules = array('Navee', 'Structure', 'Pages');
$this->default_settings = array(
    'license_number' => '',
    'autoGrow_maxHeight' => '400',
    'toolbar_icons' => array('List Block', 'Justify Block', 'File', 'Maximize'),
    'headers' => array(),
    'fullWidth' => '',
    'startupOutlineBlocks' => '',
    'entities_additional' => '#39,#123,#125',
    'channel_uris' => array(),
    'contentsCss' => '',
    'custom_toolbar' => '',
    'styles' => '',
);
$this->field_settings = array('autoGrow_maxHeight', 'contentsCss', 'fullWidth', 'startupOutlineBlocks', 'entities_additional');
$this->all_toolbar_icons = array('Subscript', 'Superscript', 'List Block', 'Indent', 'Blockquote', 'Justify Block', 'PasteFromWord', 'RemoveFormat', 'Anchor', 'MediaEmbed', 'Flash', 'Table', 'Iframe', 'Maximize', 'ShowBlocks', 'Source');
$this->toolbar_options = array('full' => 'Full', 'simple' => 'Basic', 'light' => 'Light', 'custom' => 'Custom');
// END
